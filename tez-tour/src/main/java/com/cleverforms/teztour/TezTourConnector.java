/*
 * Tez Tour core library.
 * Copyright(c) 2017-2024. 
 * October 18, 2017.
 * tez-tour 
 * 
 */
package com.cleverforms.teztour;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.cleverforms.comms.server.RESTConnectorImpl;
import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.shared.util.Casting;
import com.cleverforms.ics.db.model.ExtSystemKey;
import com.cleverforms.ics.db.model.dictionary.City;
import com.cleverforms.ics.db.model.dictionary.Country;
import com.cleverforms.ics.db.model.dictionary.Region;
import com.cleverforms.ics.db.model.pub.ExtSystem;
import com.cleverforms.ics.shared.dictionary.proxy.CityProxy;
import com.cleverforms.ics.shared.dictionary.proxy.ConsumerProxy;
import com.cleverforms.iss.server.integration.marker.HasBooking;
import com.cleverforms.iss.server.model.hotel.Hotel;
import com.cleverforms.iss.server.model.hotel.HotelClass;
import com.cleverforms.iss.server.model.hotel.HotelFood;
import com.cleverforms.iss.server.provider.ISSPersistentProvider;
import com.cleverforms.iss.server.travel.TravelConnector;
import com.cleverforms.iss.server.util.ISSQueryBuilder;
import com.cleverforms.iss.shared.hotel.HotelDictionaryType;
import com.cleverforms.iss.shared.hotel.model.RoomTypeModel;
import com.cleverforms.iss.shared.hotel.proxy.HotelProxy;
import com.cleverforms.iss.shared.travel.model.OneWayTicketAvailability;
import com.cleverforms.iss.shared.travel.model.TravelBookModel;
import com.cleverforms.iss.shared.travel.model.TravelItineraryModel;
import com.cleverforms.iss.shared.travel.model.TravelOrderModel;
import com.cleverforms.iss.shared.travel.model.TwoWayTicketAvailability;
import com.cleverforms.iss.shared.travel.proxy.TravelBookInfo;
import com.cleverforms.iss.shared.travel.proxy.TravelItineraryProxy;
import com.cleverforms.iss.shared.travel.proxy.TravelOrderProxy;
import com.cleverforms.iss.shared.travel.proxy.TravelSearchRequestProxy;

@SuppressWarnings("deprecation")
public class TezTourConnector extends RESTConnectorImpl
		implements TravelConnector, InitializingBean, HasBooking<TravelOrderProxy, TravelBookModel> {

	@Autowired
	protected ISSPersistentProvider provider;
	@Value("${tezTour.systemId:26}")
	private long systemId;
	protected final SimpleDateFormat bookingFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm");
	protected final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
	protected Map<String, Long> currencies;
	protected Map<AbstractMap.SimpleEntry<Short, Short>, Long> accommodations;
	private String currencyIds;
	private String accommodationIds;
	
	/**
	 * Construct a new instance of the {@link TezTourConnector}, with default parameters.
	 */
	public TezTourConnector() {
		this(new RestTemplate());
	}
	
	/**
	 * Construct a new instance of the {@link TezTourConnector}, with the given {@link RestTemplate}.
	 * @param restTemplate the {@link RestTemplate} to use.
	 */
	public TezTourConnector(RestTemplate restTemplate) {
		super(restTemplate);
		currencies = new HashMap<>();
		accommodations = new HashMap<>();
	}

	/**
	 * Construct a new instance of the {@link TezTourConnector}, with the given {@link ClientHttpRequestFactory}.
	 * @see RestTemplate#RestTemplate(ClientHttpRequestFactory)
	 */
	public TezTourConnector(ClientHttpRequestFactory requestFactory) {
		super(requestFactory);
		currencies = new HashMap<>();
		accommodations = new HashMap<>();
	}

	@Override
	public TezTourConnector clone() {
		return (TezTourConnector) super.clone();
	}

	public long getSystemId() {
		return systemId;
	}
	
	public void setSystemId(long systemId) {
		this.systemId = systemId;
	}
	
	public void setCurrencyIds(String ids) {
		this.currencyIds = ids;
	}

	public void setAccommodationIds(String accommodationIds) {
		this.accommodationIds = accommodationIds;
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<TravelItineraryProxy> getTravelItineraries(TravelSearchRequestProxy searchModel) {
		List<TravelItineraryProxy> travelItineraries = new ArrayList<>();
		
		Long currency = currencies.get(searchModel.getCurrency().getCode());
		if (currency == null) {
			warn("Can't find currency for code: " + searchModel.getCurrency().getCode());
			return travelItineraries;
		}

		final HotelClass hotelClass = Util.proxyHasId(searchModel.getHotelClass()) ? provider.find(HotelClass.class, searchModel.getHotelClass().getId()) : null;
		if (hotelClass == null) {
			warn("Can't find hotel class with id: " + searchModel.getHotelClass().getId());
			return travelItineraries;
		}
		final String hca = hotelClass.getAlias(systemId);
		final String hotelClassAlias = hca != null ? hca.split(";", 2)[0] : null;
		if (Util.isBlank(hotelClassAlias)) {
			warn("Can't find main alias for: " + hotelClass);
			return travelItineraries;
		}
		
		Long accommodation = accommodations.get(new AbstractMap.SimpleEntry<Short, Short>(searchModel.getAdultCount(), searchModel.getChildCount()));
		if (accommodation == null) {
			warn("Can't find accommodations for adults = " + searchModel.getAdultCount() + ", childs = " + searchModel.getChildCount());
			return travelItineraries;
		}

		final HotelFood hotelFood = provider.find(HotelFood.class, searchModel.getHotelFood().getId());
		if (hotelFood == null) {
			warn("Can't find hotel food with id: " + searchModel.getHotelFood().getId());
			return travelItineraries;
		}
		final String hfa = hotelFood.getAlias(systemId);
		final String hotelFoodAlias = hfa != null ? hfa.split(";", 2)[0] : null;
		if (Util.isBlank(hotelFoodAlias)) {
			warn("Can't find alias for: " + hotelFood);
			return travelItineraries;
		}
		
		final City city = Util.proxyHasId(searchModel.getCity()) ? provider.find(City.class, searchModel.getCity().getId()) : null;
		if (city == null) {
			warn("Can't find city with id: " + searchModel.getCity().getId());
			return travelItineraries;
		}
		final String cityAlias = city.getAlias(systemId);
		if (Util.isBlank(cityAlias)) {
			warn("Can't find alias for: " + city);
			return travelItineraries;
		}
		final String[] cityIds = cityAlias.strip().split(";");
		if (cityIds.length < 2) {
			warn("City alias (" + cityAlias + ") must have 2 ids separated \";\".");
			return travelItineraries;
		}
		final String cityId = cityIds[1];
	
		final Country country = provider.find(Country.class, searchModel.getCountry().getId());
		if (country == null) {
			warn("Can't find country with id: " + searchModel.getCountry().getId());
			return travelItineraries;
		}
		final String countryId = country.getAlias(systemId);
		if (Util.isBlank(countryId)) {
			warn("Can't find alias for: " + country);
			return travelItineraries;
		}

		final Set<Long> destinationCityIds = new HashSet<>();
		final Set<String> regionIds = new HashSet<>();
		final Set<City> destinationCities = new HashSet<>();
		if (searchModel.getDestinationCities().isEmpty()) {
			final List<City> countryCities = provider.getResultList("from City where country = ?0", 0, -1, country);
			if (countryCities != null) destinationCities.addAll(countryCities);
		} else {
			for (CityProxy proxy : searchModel.getDestinationCities()) {
				final City destinationCity = Util.proxyHasId(proxy) ? provider.find(City.class, proxy.getId()) : null;
				if (destinationCity != null) destinationCities.add(destinationCity);
			}
		}
		for (City destinationCity : destinationCities) {
			final Region region = destinationCity.getRegion();
			final String regionId = region != null ? region.getAlias(systemId) : null; 
			if (Util.isNotBlank(regionId)) {
				regionIds.add(regionId);
				destinationCityIds.add(destinationCity.getId());
			}
		}
		if (regionIds.isEmpty()) {
			warn("Can't find regions for destination cities: " + searchModel.getDestinationCities());
			return travelItineraries;
		}

		final Set<String> hotelIds = new HashSet<>();
		for (HotelProxy proxy : searchModel.getHotels()) {
			final Hotel hotel = Util.proxyHasId(proxy) ? provider.find(Hotel.class, proxy.getId()) : null;
			final String hotelId = hotel != null ? hotel.getAlias(systemId) : null;
			if (Util.isNotBlank(hotelId)) hotelIds.add(hotelId);
		}
		if (hotelIds.isEmpty()) {
			if (!searchModel.getHotels().isEmpty()) {
				warn("Can't find hotels: " + searchModel.getHotels());
				return travelItineraries;
			}
			for (Long destinationCityId : destinationCityIds) {
				final List<Hotel> hotels = provider.getResultList("from Hotel where city.id = ?0", 0, -1, destinationCityId);
				if (hotels != null && !hotels.isEmpty()) {
					for (Hotel hotel : hotels) {
						final String hotelId = hotel.getAlias(systemId);
						if (Util.isNotBlank(hotelId)) hotelIds.add(hotelId);
					}
				}
			}
		}

		String childBirthday1 = null, childBirthday2 = null;
		if (searchModel.getChildBirthdays() != null) {
			String[] childBirthdays = searchModel.getChildBirthdays().strip().split(",");
			if (childBirthdays.length == 1) {
				childBirthday1 = childBirthdays[0];
			} else if (childBirthdays.length == 2) {
				childBirthday1 = childBirthdays[0];
				childBirthday2 = childBirthdays[1];
			} else {
				warn("Child birthdays are not filled");
				return travelItineraries;
			}
		}

		try {
			String url = String.format(
					"https://www.tez-tour.com/"
							+ "tariffsearch/getResult?priceMin=%d&priceMax=%d&currency=%d&nightsMin=%d&nightsMax=%d"
							+ "&hotelClassId=%s&accommodationId=%d&rAndBId=%s&tourType=%s&locale=ru&cityId=%s&countryId=%s"
							+ "&after=%s&before=%s&hotelClassBetter=true&rAndBBetter=true&xml=true",
					(int) searchModel.getMinPrice(), (int) searchModel.getMaxPrice(), currency,
					searchModel.getMinNights(), searchModel.getMaxNights(), hotelClassAlias, accommodation, hotelFoodAlias,
					searchModel.getTourType(), cityId, countryId, sdf.format(searchModel.getAfter()),
					sdf.format(searchModel.getBefore()));
			for (String region : regionIds) {
				url += "&tourId=" + region;
			}
			for (String hotel : hotelIds) {
				url += "&hotelId=" + hotel;
			}
			if (childBirthday1 != null) {
				url += "&childBirthday1=" + childBirthday1;
			}
			if (childBirthday2 != null) {
				url += "&childBirthday2=" + childBirthday2;
			}
			ResponseEntity<String> response = restTemplate().getForEntity(url, String.class);
			DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = documentBuilder.parse(new InputSource(new StringReader(response.getBody())));
			XPath xPath = XPathFactory.newInstance().newXPath();
			
			NodeList items = (NodeList) xPath.compile("/searchResult/data/item").evaluate(document, XPathConstants.NODESET);
			final ExtSystemKey hotelKey = new ExtSystemKey(provider.find(ExtSystem.class, systemId), HotelDictionaryType.HOTEL);
			final ExtSystemKey foodKey = new ExtSystemKey(provider.find(ExtSystem.class, systemId), HotelDictionaryType.HOTEL_FOOD);
			for (int i = 0; i < items.getLength(); i++) {
				//TODO Fix incorrect hotel alias
				try {
					TravelItineraryProxy travelItinerary = new TravelItineraryModel((long) i, "TEZ TOUR");
					Element item = (Element) items.item(i);
					String checkInString = item.getElementsByTagName("checkIn").item(0).getTextContent();
					Date checkIn = sdf.parse(checkInString);
					travelItinerary.setCheckIn(checkIn);
					Date checkOut = sdf.parse(item.getElementsByTagName("checkOut").item(0).getTextContent() + checkInString.substring(5));
					if (!checkOut.after(checkIn)) {
						checkOut.setYear(checkOut.getYear() + 1);
					}
					travelItinerary.setCheckOut(checkOut);
					
					// Check if the hotel of a result item is from a city of a request
					String hotelId = ((Element) item.getElementsByTagName("hotel").item(0)).getElementsByTagName("id").item(0).getTextContent();
					final Hotel hotel = (Hotel) provider.getSingleResult("from Hotel h join h.aliases a where a = ?0 and ?1 in indices(a)", hotelId, hotelKey);
					if (hotel == null) {
						warn("Can't find hotel with id: " + hotelId);
						continue;
					}
					if (!destinationCityIds.isEmpty() && (hotel.getCity() == null || !destinationCityIds.contains(hotel.getCity().getId()))) {
						warn("The hotel has no indication of the city or the city is not included in the list of required cities.");
						continue;
					}
					travelItinerary.setHotel((HotelProxy) hotel.getProxy());
	
					String foodId = ((Element)item.getElementsByTagName("pansion").item(0)).getElementsByTagName("id").item(0).getTextContent();
					final HotelFood food = (HotelFood) provider.getSingleResult("from HotelFood h join h.aliases a where a like ?0 and ?1 in indices(a)", "%" + foodId + "%", foodKey);
					if (food == null) {
						warn("Can't find hotel food with id: " + foodId);
						travelItinerary.setFood(hotelFood.getProxy());
					} else travelItinerary.setFood(food.getProxy());
					
					final String roomTypeName = ((Element)item.getElementsByTagName("hotelRoomType").item(0)).getElementsByTagName("name").item(0).getTextContent();
					travelItinerary.setRoomType(new RoomTypeModel(0l, roomTypeName));
					
					// NightCount
					travelItinerary.setNightCount(Casting.asShort(item.getElementsByTagName("nightCount").item(0).getTextContent()));
					final Element price = (Element) item.getElementsByTagName("price").item(0);
					
					final String currencyId = price.getElementsByTagName("currencyId").item(0).getTextContent();
					for (Entry<String, Long> entry : currencies.entrySet()) {
						if (Util.equalsWithNull(String.valueOf(entry.getValue()), currencyId)) {
							travelItinerary.setCurrencyCode(entry.getKey());
							break;
						}
					}
					
					// Price
					travelItinerary.setPrice(Casting.asDouble(price.getElementsByTagName("total").item(0).getTextContent()));
					
					final Element ageGroup = (Element) item.getElementsByTagName("ageGroupType").item(0);
					// AdultCount
					final NodeList adults = ageGroup.getElementsByTagName("adult");
					if (adults.getLength() == 0) {
						travelItinerary.setAdultCount((short) 0);
					} else {
						travelItinerary.setAdultCount(Casting.asShort(((Element)adults.item(0)).getElementsByTagName("count").item(0).getTextContent()));
					}
					// ChildCount
					final NodeList bigChildren = ageGroup.getElementsByTagName("bigChild");
					short bigChildCount = bigChildren.getLength() > 0 ? Casting.asShort(((Element)bigChildren.item(0)).getElementsByTagName("count").item(0).getTextContent()) : 0;
					final NodeList smallChildren = ageGroup.getElementsByTagName("smallChild");
					final short smallChildCount = smallChildren.getLength() > 0 ? Casting.asShort((((Element)smallChildren.item(0)).getElementsByTagName("count").item(0).getTextContent())) : 0;
					travelItinerary.setChildCount((short) (bigChildCount + smallChildCount));
	
					final String bookingUrl = xPath.compile("bookingUrl/bookingUrl/url").evaluate(item);
					travelItinerary.setBookingParams(bookingUrl);
					
					Element seatSetPair = (Element) ((Element) item.getElementsByTagName("seatSets").item(0)).getElementsByTagName("seatSetPair").item(0);
					Element to = (Element) seatSetPair.getElementsByTagName("to").item(0);
					Element from = (Element) seatSetPair.getElementsByTagName("from").item(0);
	
					travelItinerary.setTicketAvailability(new TwoWayTicketAvailability(
							new OneWayTicketAvailability(((Element)to.getElementsByTagName("first").item(0)).getElementsByTagName("seatSet").item(0).getTextContent(),
									((Element)to.getElementsByTagName("business").item(0)).getElementsByTagName("seatSet").item(0).getTextContent(),
									((Element)to.getElementsByTagName("econom").item(0)).getElementsByTagName("seatSet").item(0).getTextContent(),
									((Element)to.getElementsByTagName("premiumEconom").item(0)).getElementsByTagName("seatSet").item(0).getTextContent()),
							new OneWayTicketAvailability(((Element)from.getElementsByTagName("first").item(0)).getElementsByTagName("seatSet").item(0).getTextContent(),
									((Element)from.getElementsByTagName("business").item(0)).getElementsByTagName("seatSet").item(0).getTextContent(),
									((Element)from.getElementsByTagName("econom").item(0)).getElementsByTagName("seatSet").item(0).getTextContent(),
									((Element)from.getElementsByTagName("premiumEconom").item(0)).getElementsByTagName("seatSet").item(0).getTextContent())));
					
					travelItineraries.add(travelItinerary);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return travelItineraries;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		for (String c : currencyIds.split(",")) {
			String[] s = c.split("=");
			currencies.put(s[0], Long.parseLong(s[1]));
		}

		for (String a : accommodationIds.split(",")) {
			String[] s1 = a.split("=");
			String[] s2 = s1[0].split(":");
			accommodations.put(
					new AbstractMap.SimpleEntry<Short, Short>(Short.parseShort(s2[0]), Short.parseShort(s2[1])),
					Long.parseLong(s1[1]));
		}
	}

	@Override
	@Transactional
	public List<TravelOrderProxy> booking(TravelBookModel bookModel) {
		List<TravelOrderProxy> travelOrders = new ArrayList<>();
		for (TravelBookInfo tbi : bookModel.getBookingItems()) {
			String bookingUrl = tbi.getBookingUrl();

			int index = bookingUrl.indexOf("rar=");
			if (index == -1) {
				return travelOrders;
			}
			index += 4;
			String rar;
			int tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				rar = bookingUrl.substring(index);
			} else {
				rar = bookingUrl.substring(index, tempIndex);
			}
			index = bookingUrl.indexOf("rdr=");
			if (index == -1) {
				return travelOrders;
			}
			index += 4;
			String rdr;
			tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				rdr = bookingUrl.substring(index);
			} else {
				rdr = bookingUrl.substring(index, tempIndex);
			}
			index = bookingUrl.indexOf("ltt=");
			if (index == -1) {
				return travelOrders;
			}
			index += 4;
			String ltt;
			tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				ltt = bookingUrl.substring(index);
			} else {
				ltt = bookingUrl.substring(index, tempIndex);
			}
			index = bookingUrl.indexOf("ftt=");
			if (index == -1) {
				return travelOrders;
			}
			index += 4;
			String ftt;
			tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				ftt = bookingUrl.substring(index);
			} else {
				ftt = bookingUrl.substring(index, tempIndex);
			}
			index = bookingUrl.indexOf("cResId=");
			if (index == -1) {
				return travelOrders;
			}
			index += 7;
			String cResId;
			tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				cResId = bookingUrl.substring(index);
			} else {
				cResId = bookingUrl.substring(index, tempIndex);
			}
			index = bookingUrl.indexOf("cFlyIds=");
			if (index == -1) {
				return travelOrders;
			}
			index += 8;
			String cFlyIds;
			tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				cFlyIds = bookingUrl.substring(index);
			} else {
				cFlyIds = bookingUrl.substring(index, tempIndex);
			}
			index = bookingUrl.indexOf("priceOfferId=");
			if (index == -1) {
				return travelOrders;
			}
			index += 13;
			String priceOfferId;
			tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				priceOfferId = bookingUrl.substring(index);
			} else {
				priceOfferId = bookingUrl.substring(index, tempIndex);
			}
			index = bookingUrl.indexOf("depCity=");
			if (index == -1) {
				return travelOrders;
			}
			index += 8;
			String depCity;
			tempIndex = bookingUrl.indexOf("&", index);
			if (tempIndex == -1) {
				depCity = bookingUrl.substring(index);
			} else {
				depCity = bookingUrl.substring(index, tempIndex);
			}

			String login = "ButovAV";
			String password = "ZLNRc45";
			String authenticateUrl = "http://xml.tez-tour.com/xmlgate/auth_data.jsp?j_login_request=1&j_login=" + login
					+ "&j_passwd=" + password;
			ResponseEntity<String> response3 = restTemplate().getForEntity(authenticateUrl, String.class);
			try {
				DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Document document = documentBuilder.parse(new InputSource(new StringReader(response3.getBody())));
				XPath xPath = XPathFactory.newInstance().newXPath();

				String sessionId = xPath.compile("/auth_data/sessionId").evaluate(document);

				StringBuilder url = new StringBuilder(
						"http://xml.tez-tour.com/xmlgate/order/orderFromOfferId.jsp?tariffDepCityId=");
				url.append(depCity);
				url.append("&firstTransferType=");
				url.append(ftt);
				url.append("&lastTransferType=");
				url.append(ltt);
				url.append("&resortArrivalRegionId=");
				url.append(rar);
				url.append("&resortDepartureRegionId=");
				url.append(rdr);
				url.append("&resTariffs=");
				url.append(cResId);
				url.append("&priceOfferId=");
				url.append(priceOfferId);
				url.append("&flyTariffs=");
				url.append(cFlyIds);
				url.append("&aid=");
				url.append(sessionId);

				ResponseEntity<String> response = restTemplate().getForEntity(url.toString(), String.class);

				document = documentBuilder.parse(new InputSource(new StringReader(response.getBody())));

				NodeList tourists = (NodeList) xPath.compile("/order/Tourist").evaluate(document,
						XPathConstants.NODESET);
				Iterator<ConsumerProxy> consumers = bookModel.getConsumers().iterator();
				for (int i = 0; i < tourists.getLength(); i++) {
					Element tourist = (Element) tourists.item(i);

					ConsumerProxy consumer =  consumers.next();
					
					tourist.getElementsByTagName("surname").item(0).setTextContent(consumer.getLastName());
					tourist.getElementsByTagName("name").item(0).setTextContent(consumer.getName());
					tourist.getElementsByTagName("birthday").item(0).setTextContent(sdf.format(consumer.getBirthday()));
					String citizenship = consumer.getDocument().substring(0, consumer.getDocument().indexOf(";"));
					Country c = (Country) provider.getSingleResult("from Country c left join c.properties p where index(p) = 'IATA_CODE' and p = ?0", citizenship);
					if(c == null) {
						return travelOrders;
					} else {
						for (ExtSystemKey key : c.getAliases().keySet()) {
							if (key.getSystem() != null && key.getSystem().getId() == systemId) {
								tourist.getElementsByTagName("nationality").item(0).setTextContent(c.getAliases().get(key));
								break;
							}
						}
					}
				}

				Node order = (Node) xPath.compile("/order").evaluate(document, XPathConstants.NODE);
				Element timeLimit = document.createElement("TimeLimit");
				Element type = document.createElement("type");
				type.setTextContent("128770");
				Element value = document.createElement("value");
				Calendar c = Calendar.getInstance();
				c.add(Calendar.DATE, 1);
				value.setTextContent(bookingFormat.format(c.getTime()));
				timeLimit.appendChild(type);
				timeLimit.appendChild(value);
				order.appendChild(timeLimit);
				TransformerFactory tf = TransformerFactory.newInstance();
				Transformer transformer = tf.newTransformer();

				DOMSource source = new DOMSource(document);
				StringWriter writer = new StringWriter();
				StreamResult result = new StreamResult(writer);

				transformer.transform(source, result);

				String bookXml = writer.toString();
				
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_XML);

				HttpEntity<String> entity = new HttpEntity<String>(bookXml, headers);
				ResponseEntity<String> response2 = restTemplate()
						.postForEntity("http://xml.tez-tour.com/xmlgate/order/book", entity, String.class);

				document = documentBuilder.parse(new InputSource(new StringReader(response2.getBody())));

				TravelOrderProxy top = new TravelOrderModel();
				top.setNumber(xPath.compile("/booking-result/orderId").evaluate(document));
				travelOrders.add(top);
			} catch (XPathExpressionException | DOMException | ParserConfigurationException | SAXException | IOException
					| TransformerException e) {
				return travelOrders;
			}
		}
		return travelOrders;
	}
	
	public ISSQueryBuilder queryBuilder() {
		return (ISSQueryBuilder) (provider != null ? provider.providerUtil().queryBuilder() : null);
	}
	
}

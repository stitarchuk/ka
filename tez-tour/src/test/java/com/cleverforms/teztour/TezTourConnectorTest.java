/*
 * Tez Tour core library.
 * Copyright(c) 2017-2024. 
 * October 18, 2017.
 * tez-tour 
 * 
 */
package com.cleverforms.teztour;

import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import com.cleverforms.comms.server.WithLoggerImpl;
import com.cleverforms.comms.shared.model.CurrencyModel;
import com.cleverforms.ics.db.model.ExtSystemKey;
import com.cleverforms.ics.db.model.pub.ExtSystem;
import com.cleverforms.ics.shared.dictionary.model.CityModel;
import com.cleverforms.ics.shared.dictionary.model.ConsumerModel;
import com.cleverforms.ics.shared.dictionary.model.CountryModel;
import com.cleverforms.ics.shared.dictionary.proxy.CityProxy;
import com.cleverforms.ics.shared.dictionary.proxy.ConsumerProxy;
import com.cleverforms.iss.server.model.hotel.Hotel;
import com.cleverforms.iss.server.model.hotel.HotelFood;
import com.cleverforms.iss.server.provider.ISSPersistentProvider;
import com.cleverforms.iss.shared.hotel.HotelDictionaryType;
import com.cleverforms.iss.shared.hotel.model.HotelClassModel;
import com.cleverforms.iss.shared.hotel.model.HotelFoodModel;
import com.cleverforms.iss.shared.hotel.proxy.HotelProxy;
import com.cleverforms.iss.shared.travel.model.TravelBookInfoModel;
import com.cleverforms.iss.shared.travel.model.TravelBookModel;
import com.cleverforms.iss.shared.travel.model.TravelSearchRequestModel;
import com.cleverforms.iss.shared.travel.proxy.TravelBookInfo;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContext.xml" })
public class TezTourConnectorTest extends WithLoggerImpl {

	@Autowired
	protected ISSPersistentProvider provider;
	@Autowired
	protected TezTourConnector connector;

	@Test
	public void getTravelItineraries() {
		TravelSearchRequestModel searchModel = new TravelSearchRequestModel();
		
		/*searchModel.setHotelFood(new HotelFoodModel(2l)); // Breakfast
		searchModel.setHotelClass(new HotelClassModel(2l)); // 5* 
		searchModel.setCity(new CityModel(1123l)); // Kyiv
		//searchModel.setCountry(new CountryModel(113l)); // Mauritius
		searchModel.setCountry(new CountryModel(203l)); // Turkey
		Set<CityProxy> destinationCities = new HashSet<>();
		//destinationCities.add(new CityModel(504l)); // Mauritius
		destinationCities.add(new CityModel(1974l)); // Antalya
		searchModel.setDestinationCities(destinationCities);
		Set<HotelProxy> hotels = new HashSet<>();
		//hotels.add(new HotelModel(4640l));
		hotels.add(new HotelModel(4679l));
		searchModel.setHotels(hotels);
		//searchModel.setChildBirthdays("01.01.2011,02.02.2012");
		
		searchModel.setMinPrice(0);
		searchModel.setMaxPrice(15000000);
		searchModel.setCurrency(new CurrencyModel("UAH"));
		searchModel.setAdultCount((short) 2);
		searchModel.setChildCount((short) 0);
		searchModel.setTourType((short) 1);
		searchModel.setAfter(new GregorianCalendar(2018, 3, 29).getTime()); // 29.03.2018
		searchModel.setBefore(new GregorianCalendar(2018, 4, 3).getTime()); // 03.05.2018
		searchModel.setMinNights((short) 4);
		searchModel.setMaxNights((short) 12);*/
		
		searchModel.setHotelFood(new HotelFoodModel(5l)); // Breakfast
		searchModel.setHotelClass(new HotelClassModel(1l)); // 3* 
		searchModel.setCity(new CityModel(1123l)); // Kyiv
		searchModel.setCountry(new CountryModel(203l)); // Turkey
		Set<CityProxy> destinationCities = new HashSet<>();
		//destinationCities.add(new CityModel(1974l)); // Antalya
		searchModel.setDestinationCities(destinationCities);
		Set<HotelProxy> hotels = new HashSet<>();
		//hotels.add(new HotelModel(4678l)); // PRIMA
		searchModel.setHotels(hotels);
		//searchModel.setChildBirthdays("01.01.2011,02.02.2012");
		
		searchModel.setMinPrice(0);
		searchModel.setMaxPrice(15000000);
		searchModel.setCurrency(new CurrencyModel("UAH"));
		searchModel.setAdultCount((short) 2);
		searchModel.setChildCount((short) 0);
		searchModel.setTourType((short) 1);
		searchModel.setAfter(new GregorianCalendar(2018, 6, 18).getTime()); // 17.07.2018
		searchModel.setBefore(new GregorianCalendar(2018, 6, 18).getTime()); // 17.07.2018
		searchModel.setMinNights((short) 8);
		searchModel.setMaxNights((short) 10);
		
		debug(connector.getTravelItineraries(searchModel));
	}

	@Test
	public void testBooking() {
		final TravelBookModel bookModel = new TravelBookModel();
		bookModel.setClientId(1l);
		bookModel.setPostId(1l);
		ConsumerProxy consumer = new ConsumerModel();
		consumer.setName("Ivan");
		consumer.setLastName("Petrov");
		consumer.setBirthday(new GregorianCalendar(1980, 0, 1).getTime());
		consumer.setDocument("TR;;;;;;;");
		bookModel.getConsumers().add(consumer);
		consumer = new ConsumerModel();
		consumer.setName("Petro");
		consumer.setLastName("Ivanov");
		consumer.setDocument("TR;;;;;;;");
		consumer.setBirthday(new GregorianCalendar(1981, 1, 2).getTime());
		bookModel.getConsumers().add(consumer);
		final TravelBookInfo bookInfo = new TravelBookInfoModel();
		bookInfo.setBookingUrl("https://online.tez-tour.com/armmanager/workplace/section/new-order?depCity=3667&amp;arrivalCity=3667&amp;hotStType=2&amp;locale=ru&amp;ftt=3635&amp;ltt=3635&amp;ftv=&amp;ltv=&amp;sk=1&amp;rar=21301&amp;rdr=21301&amp;cResId=47439199012&amp;priceOfferId=12636963&amp;cFlyIds=76518520");

		bookModel.getBookingItems().add(bookInfo);
		
		debug(connector.booking(bookModel));
	}

	@Test
	@Transactional
	public void testExtAliases() {
		final long systemId = 26l;
		
		final ExtSystemKey hotelKey = new ExtSystemKey(provider.find(ExtSystem.class, systemId), HotelDictionaryType.HOTEL);
		final String hotelId = "10636";
		final Hotel hotel = (Hotel) provider.getSingleResult("from Hotel h join h.aliases a where a = ?0 and ?1 in indices(a)", hotelId, hotelKey);
		debug(hotel);

		final ExtSystemKey foodKey = new ExtSystemKey(provider.find(ExtSystem.class, systemId), HotelDictionaryType.HOTEL_FOOD);
		final String foodId = "306513";
		final HotelFood food = (HotelFood) provider.getSingleResult("from HotelFood h join h.aliases a where a like ?0 and ?1 in indices(a)", "%" + foodId + "%", foodKey);
		debug(food);
	}
	
	
}

/*
 * Tez Tour core library.
 * Copyright(c) 2017-2024. 
 * July 17, 2018.
 * tez-tour 
 * 
 */
package com.cleverforms.teztour.api;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cleverforms.comms.shared.enumerator.MessageDigestAlgorithm;
import com.cleverforms.comms.shared.loader.DictionaryFilterBean;
import com.cleverforms.comms.shared.loader.SortDir;
import com.cleverforms.comms.shared.loader.SortInfoBean;
import com.cleverforms.comms.shared.util.date.DateTime;
import com.cleverforms.ics.core.ICSRESTAPITestHelper;
import com.cleverforms.ics.core.api.APITest;
import com.cleverforms.ics.core.api.TransactionType;
import com.cleverforms.ics.core.api.model.LoadPayload;
import com.cleverforms.ics.core.api.model.NewPayload;
import com.cleverforms.ics.shared.DictionaryType;
import com.cleverforms.iss.server.converters.ISSXStreamHelper;
import com.cleverforms.iss.server.provider.ISSPersistentProvider;
import com.cleverforms.iss.shared.airline.AirlineDictionaryType;
import com.cleverforms.iss.shared.model.ISSDictionaryConfig;
import com.cleverforms.iss.shared.railway.RailwayDictionaryType;
import com.cleverforms.iss.shared.travel.TravelDictionaryType;

/** 
 * Tests API operations.
 * @author Sergey Titarchuk
 */
@Rollback(true)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContext.xml"})
public class ISSAPITest extends APITest {

	@Autowired
	protected ISSPersistentProvider provider;

	@Override
	protected ICSRESTAPITestHelper createHelper(Map<String, Object> configuration) {
		LogManager.getLogger(getClass()).debug("hashAlgorithm" + provider.hashAlgorithm());
		configuration.put("hashAlgorithm", provider.hashAlgorithm());
//		configuration.put("baseUrl", "http://195.250.43.69:8080/tss");
//		configuration.put("baseUrl", "http://192.168.93.240:8080/tss");
//		configuration.put("hostname", "kiyavia.com");
		configuration.put("baseUrl", "http://dev.clever-forms.com/tss");
		configuration.put("hostname", "ICS");
		configuration.put("hashAlgorithm", MessageDigestAlgorithm.SHA256);
		System.out.println(configuration);
		return new ICSRESTAPITestHelper(ISSAPITest.class, configuration) {
			@Override
			public ISSXStreamHelper xHelper(MediaType mediaType) {
				return new ISSXStreamHelper(mediaType, provider.mapper());
			}
		};
	}

	@Test
	@Override
	public void testParameters() throws Exception {
		helper.debug(helper.publicKey());
		helper.debug(helper.privateKey());
		String uri = helper.url("/api");
		helper.debug("url: " + helper.getURL(uri, helper.baseParams("sale.dictionary")));
	}

	@Test
	@Override
	public void testRequestData() throws Exception {
    	ISSDictionaryConfig config = new ISSDictionaryConfig(RailwayDictionaryType.RAILWAY_ITINERARY, "uk", "UAH");
    	config.addSortInfo(new SortInfoBean("id", SortDir.DESC));
    	config.addFilter(new DictionaryFilterBean("adults", "number", "eq", 1));
		// Kyiv
    	config.addFilter(new DictionaryFilterBean("from#0", "string", "eq", "2200001"));
		// Vinnitsa
    	config.addFilter(new DictionaryFilterBean("to#0", "string", "eq", "2200200"));
    	config.addFilter(new DictionaryFilterBean("when#0", "date", "eq", new DateTime().addDays(10).clearTime().getTime()));
		helper.debug("data: " + helper.requestAsString(TransactionType.DICTIONARY_LIST, config));
	}

	@Test
	@Override
	public void testGetCertificate() {
		getCertificate("1412858472296");
	}

	@Test
	@Override
	public void testGetKeyStore() {
		getKeyStore("1412858472296", "12345");
	}

	@Test
	@Override
	public void testInitKeys() {
		initKeys("1412858472296", "12345");
	}

	@Test
	@Override
	public void testAuthenticate() {
		authenticate("sergey", "w6N2XSgG7Bf", "s.titarchuk@a-express.com.ua", "gfhjkm");
	}

	@Test
	@Override
	public void testDictionaryList() throws Exception {
//		ISSDictionaryConfig config = new ISSDictionaryConfig(TravelDictionaryType.TRAVEL_ITINERARY, helper.locale(), "UAH");
//		config.setLimit(2);
//		config.addFilter(new DictionaryFilterBean("minPrice", "numeric", "eq", 0));
//		config.addFilter(new DictionaryFilterBean("maxPrice", "numeric", "eq", 15000000));
//		config.addFilter(new DictionaryFilterBean("adultCount", "numeric", "eq", 2));
//		config.addFilter(new DictionaryFilterBean("childCount", "numeric", "eq", 0));
//		config.addFilter(new DictionaryFilterBean("tourType", "numeric", "eq", 1));
//		config.addFilter(new DictionaryFilterBean("after", "date", "eq", "2018-07-18T00:00:00.000+0000"));
//		config.addFilter(new DictionaryFilterBean("before", "date", "eq", "2018-07-18T00:00:00.000+0000"));
//		config.addFilter(new DictionaryFilterBean("minNights", "numeric", "eq", 8));
//		config.addFilter(new DictionaryFilterBean("maxNights", "numeric", "eq", 10));
//		config.addFilter(new DictionaryFilterBean("food", "numeric", "eq", 2));
//		config.addFilter(new DictionaryFilterBean("hotelClass", "numeric", "eq", 1));
//		config.addFilter(new DictionaryFilterBean("city", "numeric", "eq", 1123));
//		config.addFilter(new DictionaryFilterBean("country", "numeric", "eq", 203));
//		config.addFilter(new DictionaryFilterBean("destinationCities", "string", "eq", "1974"));
//		//config.addFilter(new DictionaryFilterBean("hotels", "string", "eq", "4681"));
//		//config.addFilter(new DictionaryFilterBean("childBirthdays", "string", "eq", "10.10.2010,20.20.2011"));
//		
//		helper.setMediaType(MediaType.APPLICATION_XML);
//    	testRequest("/api", "sale.dictionary", TransactionType.DICTIONARY_LIST, config);
    	
//    	ISSDictionaryConfig config = new ISSDictionaryConfig(HotelDictionaryType.HOTEL_CLASS, "uk");
//    	config.addSortInfo(new SortInfoBean("name", SortDir.ASC));
//    	config.setLimit(20);

    	ISSDictionaryConfig config = new ISSDictionaryConfig(AirlineDictionaryType.AIRPORT, "uk");
    	config.addFilter(new DictionaryFilterBean("name", "string", "contains", "lon"));
//    	config.addSortInfo(new SortInfoBean("name", SortDir.ASC));
    	config.setLimit(20);

		// for JSON
    	helper.setMediaType(MediaType.APPLICATION_JSON);
    	testRequest("/api", "sale.dictionary", TransactionType.DICTIONARY_LIST, config);
    	
//    	// for XML
//    	helper.setMediaType(MediaType.APPLICATION_XML);
//    	testRequest("/api", "sale.dictionary", TransactionType.DICTIONARY_LIST, config);
	}

	@Override
	public void testFind() throws Exception {
    	ISSDictionaryConfig config = new ISSDictionaryConfig(DictionaryType.CURRENCY, "en", "UAH");
		config.addFilter(new DictionaryFilterBean("id", "numeric", "eq", 2));
    	// for JSON
    	helper.setMediaType(MediaType.APPLICATION_JSON);
    	testRequest("/api", "sale.dictionary", TransactionType.FIND, config);
    	// for XML
    	config.setDictionaryType(AirlineDictionaryType.AIRPORT);
    	helper.setMediaType(MediaType.APPLICATION_XML);
    	testRequest("/api", "sale.dictionary", TransactionType.FIND, config);
	}

	@Override
	public void testLoad() throws Exception {
		helper.setRequestMethod(RequestMethod.GET);
    	// for JSON
    	helper.setMediaType(MediaType.APPLICATION_JSON);
    	testRequest("/api", "sale.load", TransactionType.LOAD, new LoadPayload(DictionaryType.CURRENCY, 1l, "ru"));
    	// for XML
    	helper.setMediaType(MediaType.APPLICATION_XML);
    	testRequest("/api", "sale.load", TransactionType.LOAD, new LoadPayload(DictionaryType.CURRENCY, 1l, "uk"));
	}

	@Test
	@Override
	public void testLoadFormTemplates() throws Exception {
    	// for JSON
    	helper.setMediaType(MediaType.APPLICATION_JSON);
    	testRequest("/api", "sale.load.form_templates", TransactionType.LOAD, new LoadPayload(null, 1l, "ru"));
    	// for XML
    	helper.setMediaType(MediaType.APPLICATION_XML);
    	testRequest("/api", "sale.load.form_templates", TransactionType.LOAD, new LoadPayload(null, 1l, "ru"));
	}

	@Override
	public void testCreate() throws Exception {
    	// for JSON
    	helper.setMediaType(MediaType.APPLICATION_JSON);
    	testRequest("/api", "sale.new", TransactionType.NEW, new NewPayload(DictionaryType.ORDER, "ru"));
    	// for XML
    	helper.setMediaType(MediaType.APPLICATION_XML);
    	testRequest("/api", "sale.new", TransactionType.NEW, new NewPayload(DictionaryType.ORDER, "ru"));
	}

	@Test
	public void testGetTravelItineraries() throws Exception {
		ISSDictionaryConfig config = new ISSDictionaryConfig(TravelDictionaryType.TRAVEL_ITINERARY, helper.locale(), "UAH");
		config.setLimit(101);
		config.addFilter(new DictionaryFilterBean("minPrice", "numeric", "eq", 0));
		config.addFilter(new DictionaryFilterBean("maxPrice", "numeric", "eq", 15000000));
		config.addFilter(new DictionaryFilterBean("adultCount", "numeric", "eq", 2));
		config.addFilter(new DictionaryFilterBean("childCount", "numeric", "eq", 0));
		config.addFilter(new DictionaryFilterBean("tourType", "numeric", "eq", 1));
		config.addFilter(new DictionaryFilterBean("after", "date", "eq", "2018-07-18T00:00:00.000+0000"));
		config.addFilter(new DictionaryFilterBean("before", "date", "eq", "2018-07-18T00:00:00.000+0000"));
		config.addFilter(new DictionaryFilterBean("minNights", "numeric", "eq", 8));
		config.addFilter(new DictionaryFilterBean("maxNights", "numeric", "eq", 10));
		config.addFilter(new DictionaryFilterBean("food", "numeric", "eq", 2));
		config.addFilter(new DictionaryFilterBean("hotelClass", "numeric", "eq", 1));
		config.addFilter(new DictionaryFilterBean("city", "numeric", "eq", 1123));
		config.addFilter(new DictionaryFilterBean("country", "numeric", "eq", 203));
		config.addFilter(new DictionaryFilterBean("destinationCities", "string", "eq", "1974"));
		//config.addFilter(new DictionaryFilterBean("hotels", "string", "eq", "4681"));
		//config.addFilter(new DictionaryFilterBean("childBirthdays", "string", "eq", "10.10.2010,20.20.2011"));
		
		helper.setMediaType(MediaType.APPLICATION_XML);
    	testRequest("/api", "sale.dictionary", TransactionType.DICTIONARY_LIST, config);
		//helper.debug("data: " + helper.requestAsString(TransactionType.DICTIONARY_LIST, config));
	}

}

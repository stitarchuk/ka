/*
 * Tez Tour core library.
 * Copyright(c) 2017-2024. 
 * October 19, 2017.
 * tez-tour 
 * 
 */
package com.cleverforms.teztour.api;

import java.util.GregorianCalendar;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cleverforms.comms.shared.loader.DictionaryFilterBean;
import com.cleverforms.ics.core.ICSRESTAPITestHelper;
import com.cleverforms.ics.core.ICSAppControllerTest;
import com.cleverforms.ics.core.api.TransactionType;
import com.cleverforms.ics.core.api.model.LoadPayload;
import com.cleverforms.ics.core.converters.ICSXStreamHelper;
import com.cleverforms.ics.db.model.ExtSystemKey;
import com.cleverforms.ics.db.model.pub.ExtSystem;
import com.cleverforms.ics.shared.dictionary.model.ConsumerModel;
import com.cleverforms.ics.shared.dictionary.proxy.ConsumerProxy;
import com.cleverforms.ics.shared.enumerator.ServiceType;
import com.cleverforms.iss.server.converters.ISSXStreamHelper;
import com.cleverforms.iss.server.model.hotel.Hotel;
import com.cleverforms.iss.server.provider.ISSPersistentProvider;
import com.cleverforms.iss.shared.hotel.HotelDictionaryType;
import com.cleverforms.iss.shared.model.ISSDictionaryConfig;
import com.cleverforms.iss.shared.travel.TravelDictionaryType;
import com.cleverforms.iss.shared.travel.model.TravelBookInfoModel;
import com.cleverforms.iss.shared.travel.model.TravelBookModel;
import com.cleverforms.iss.shared.travel.proxy.TravelBookInfo;

@Rollback(true)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContext.xml"})
public class RESTControllerTest extends ICSAppControllerTest {

	@Override
	protected ISSPersistentProvider provider() {
		return (ISSPersistentProvider) super.provider();
	}

	@Override
	protected ICSRESTAPITestHelper createHelper(Map<String, Object> configuration) {
		ISSXStreamHelper.COMPACT_MODE = false;
		return new ICSRESTAPITestHelper(getClass(), configuration) {
			@Override
			public ISSXStreamHelper xHelper(MediaType mediaType) {
				return new ISSXStreamHelper(mediaType, provider().mapper());
			}
		};
	}
	
	@Test
	public void testGetTravelItineraries() throws Exception {
		Map<String, String> params = requestParams("sale.dictionary");
		
		ISSDictionaryConfig config = new ISSDictionaryConfig(TravelDictionaryType.TRAVEL_ITINERARY, helper().locale(), "UAH");
		config.setLimit(101);
		config.addFilter(new DictionaryFilterBean("minPrice", "numeric", "eq", 0));
		config.addFilter(new DictionaryFilterBean("maxPrice", "numeric", "eq", 15000000));
		config.addFilter(new DictionaryFilterBean("adultCount", "numeric", "eq", 2));
		config.addFilter(new DictionaryFilterBean("childCount", "numeric", "eq", 0));
		config.addFilter(new DictionaryFilterBean("tourType", "numeric", "eq", 1));
		config.addFilter(new DictionaryFilterBean("after", "date", "eq", "2018-07-18T00:00:00.000+0000"));
		config.addFilter(new DictionaryFilterBean("before", "date", "eq", "2018-07-18T00:00:00.000+0000"));
		config.addFilter(new DictionaryFilterBean("minNights", "numeric", "eq", 8));
		config.addFilter(new DictionaryFilterBean("maxNights", "numeric", "eq", 10));
		config.addFilter(new DictionaryFilterBean("food", "numeric", "eq", 2));
		config.addFilter(new DictionaryFilterBean("hotelClass", "numeric", "eq", 1));
		config.addFilter(new DictionaryFilterBean("city", "numeric", "eq", 1123));
		config.addFilter(new DictionaryFilterBean("country", "numeric", "eq", 203));
		config.addFilter(new DictionaryFilterBean("destinationCities", "string", "eq", "1974"));
		//config.addFilter(new DictionaryFilterBean("hotels", "string", "eq", "4681"));
		//config.addFilter(new DictionaryFilterBean("childBirthdays", "string", "eq", "10.10.2010,20.20.2011"));
		
		helper().setMediaType(MediaType.APPLICATION_XML);
		debug(requestPlain(params, requestAsString(TransactionType.DICTIONARY_LIST, config)));
	}
	
	@Test
	public void testTravelBooking() throws Exception {
		Map<String, String> params = requestParams("sale.booking");
		params.put("type", ServiceType.TRAVEL.name());
		
		TravelBookModel tbm = new TravelBookModel();
		ConsumerProxy consumer = new ConsumerModel();
		consumer.setName("Ivan");
		consumer.setLastName("Petrov");
		consumer.setBirthday(new GregorianCalendar(1980, 0, 1).getTime());
		consumer.setDocument("TR;;;;;;;");
		tbm.getConsumers().add(consumer);
		consumer = new ConsumerModel();
		consumer.setName("Petro");
		consumer.setLastName("Ivanov");
		consumer.setBirthday(new GregorianCalendar(1981, 1, 2).getTime());
		consumer.setDocument("TR;;;;;;;");
		tbm.getConsumers().add(consumer);
		TravelBookInfo tbi = new TravelBookInfoModel();
		tbi.setGds("TEZ TOUR");
		tbi.setBookingUrl("https://online.tez-tour.com/armmanager/workplace/section/new-order?depCity=3667&amp;arrivalCity=3667&amp;hotStType=2&amp;locale=ru&amp;ftt=3635&amp;ltt=3635&amp;ftv=&amp;ltv=&amp;sk=1&amp;rar=21301&amp;rdr=21301&amp;cResId=47439199012&amp;priceOfferId=12636963&amp;cFlyIds=76518520");
		tbm.getBookingItems().add(tbi);
		
		helper().setMediaType(MediaType.APPLICATION_XML);
		debug(requestPlain(params, requestAsString(TransactionType.RESERVE, tbm)));
	}
	
	@Test
	public void testHotels() throws Exception {
		// Test 1
//		Map<String, String> params = baseParams(person, "sale.dictionary");
//		
//		TSSDictionaryConfig config = new TSSDictionaryConfig(DictionaryType.HOTEL, helper().locale());
//		config.setLimit(100);
//		config.setOffset(4580);
//		
//		config.addFilter(new DictionaryFilterBean("id", "numeric", "eq", 4640));
//		
//		helper().setMediaType(MediaType.APPLICATION_XML);
//		debug(requestPlain(params, requestAsString(TransactionType.DICTIONARY_LIST, config)));
		
		// Test 2
		Map<String, String> params = requestParams("sale.load");
		
		ICSXStreamHelper.COMPACT_MODE = false;
		helper().setRequestMethod(RequestMethod.GET);
    	helper().setMediaType(MediaType.APPLICATION_XML);
    	debug(requestPlain(params, requestAsString(TransactionType.LOAD, new LoadPayload(HotelDictionaryType.HOTEL, 4640l, "en"))));
	}
	
	@Test
	@Transactional
	public void testAliases() throws Exception {
		String hotelId = "10636";
		final ExtSystemKey hotelKey = new ExtSystemKey(provider().find(ExtSystem.class, 25l), HotelDictionaryType.HOTEL);
		final Hotel hotel = (Hotel) provider().getSingleResult("from Hotel h join h.aliases a where a = ?0 and ('hotel', ?1) in indices(a)", hotelId, hotelKey);
		debug(hotel);
	}

}


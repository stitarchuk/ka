/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 21, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core;

import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import com.cleverforms.comms.server.util.Util;
import com.cleverforms.gpt.core.model.hotel.HotelRestaurantLocationCode;
import com.cleverforms.gpt.core.model.hotel.HotelRestaurantTypeCode;
import com.cleverforms.gpt.core.model.hotelinfo.save.HotelDescriptionInfoRequest;
import com.cleverforms.gpt.core.model.hotelinfo.save.HotelRestaurant;
import com.cleverforms.gpt.core.payload.HotelInfo;
import com.cleverforms.ics.core.ICSAppControllerTest;
import com.cleverforms.ics.core.api.TransactionType;
import com.cleverforms.ics.core.converters.ICSXStreamHelper;
import com.cleverforms.iss.server.model.hotel.CoffeeRoom;
import com.cleverforms.iss.server.model.hotel.Hotel;
import com.cleverforms.iss.server.provider.ISSPersistentProvider;
import com.cleverforms.iss.shared.hotel.HotelDictionaryType;

//@Transactional
@Rollback(true)
@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {
	"file:src/test/resources/application-context.xml",
	"file:src/main/resources/gp-travel-context.xml"
})
//@TransactionConfiguration(defaultRollback = true)
public class GPTConnectorTest extends ICSAppControllerTest {

	@Autowired
	protected ISSPersistentProvider provider;
	@Autowired
	protected GPTConnector connector;

	protected ICSXStreamHelper xHelper() {
		return new ICSXStreamHelper(helper().mediaType(), provider().mapper());
	}
	
	@Override
	public void initialize() {
		super.initialize();
		ICSXStreamHelper.COMPACT_MODE = false;
	}

	@Test
	public void testAuthenticate() {
		debug("connector = " + connector.authenticate());
	}

	@Test
	public void testLoadHotel() {
		debug("authenticate = " + connector.authenticate());
//		debug("hotel = " + connector.get(HotelInfo.URI_BY_CODE, HotelInfo.class, "TEST01"));
		debug("hotel = " + connector.get(HotelInfo.URI_BY_ID, Util.LOCALE_RUSSIAN, HotelInfo.class, 9764141));
	}

	@Test
	@Transactional
	@Rollback(false)
	public void testCreateHotel() {
		final Hotel hotel = provider.find(Hotel.class, 2l);
//		hotel.setAlias(provider, 4, null);
//		final HotelDescriptionInfoRequest proxy = HotelDescriptionInfoRequest.populate(connector, hotel);
//		debug(proxy);
//		debug("authenticate = " + connector.authenticate());
//		debug("hotel = " + connector.create(HotelDescriptionInfoRequest.URI_METHOD_CREATE, Util.LOCALE_RUSSIAN, proxy, HotelDescriptionInfoResponse.class));
//		debug(connector.syncHotel(hotel, Util.LOCALE_RUSSIAN));
		provider.save(hotel.getProxy());
	}

	protected HotelRestaurant createRestaurant(Long id) {
		final HotelRestaurant restaurant = new HotelRestaurant();
		restaurant.setId(id);
		restaurant.getName().put("en", "Buffet breakfast");
		restaurant.getName().put("ru", "Шведский стол");
		restaurant.getName().put("uk", "Шведський стіл");
		restaurant.setLocation(HotelRestaurantLocationCode.N);
		restaurant.setType(HotelRestaurantTypeCode.C);
//		restaurant.getShortDescription().put("uk", "Шведський стіл");
//		restaurant.getDescription().put("uk", "Шведський стіл без кухні");
		restaurant.setActive(true);
		return restaurant;
	}

	@Test
	@Transactional
	@Rollback(true)
	public void testRemove() throws Exception {
    	Map<String, String> params = requestParams("remove");
		params.put("type", HotelDictionaryType.COFFEE_ROOM);
    	
		CoffeeRoom coffeeRoom = (CoffeeRoom) provider.getSingleResult("from CoffeeRoom");

		// for JSON
    	helper().setMediaType(MediaType.APPLICATION_JSON);
    	debug(requestPlain(params, requestAsString(TransactionType.REMOVE, coffeeRoom.getProxy("ru"))));
    	
	}

	@Test
	@Transactional
	@Rollback(true)
	public void test() throws Exception {
		final Hotel hotel = provider.find(Hotel.class, 2l);
		debug(hotel);
		HotelDescriptionInfoRequest request = new HotelDescriptionInfoRequest().serialize(connector, hotel);
		debug(request);
	}

}

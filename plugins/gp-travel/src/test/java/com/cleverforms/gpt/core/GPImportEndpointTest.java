/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * March 31, 2020.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core;

import java.io.IOException;
import java.nio.file.Path;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.Source;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.ws.soap.SoapBody;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.soap.SoapMessageFactory;

import com.cleverforms.comms.server.WithLoggerImpl;
import com.cleverforms.comms.server.util.io.SFTPUtil;
import com.cleverforms.gpt.export.ExchangeResponse;
import com.cleverforms.gpt.export.Order;
import com.cleverforms.gpt.mos.Document;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {
	"file:src/test/resources/application-context.xml",
	"file:src/main/resources/gp-travel-context.xml"
})
public class GPImportEndpointTest extends WithLoggerImpl {

	@Autowired
	GPImportEndpoint endPoint;
	@Autowired
	SoapMessageFactory factory;
	
	@SuppressWarnings("unchecked")
	public <E> E unmarshal(Source source) throws JAXBException {
		final JAXBContext jaxbContext = JAXBContext.newInstance(Document.class, Order.class);
		final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		return (E) unmarshaller.unmarshal(source);
	}
	
	@Test
	public void testDate() throws IOException, JAXBException, DatatypeConfigurationException {
		XMLGregorianCalendar cal = DatatypeFactory.newInstance().newXMLGregorianCalendar("2020-04-01T11:58:06+03:00");
		debug("calendar: " + cal);
		debug("xml date: " + endPoint.createXMLDate(cal));
		debug("xml time: " + endPoint.createXMLTime(cal));
	}
	@Test
	public void testImport() throws IOException, JAXBException {
		final SoapMessage message = factory.createWebServiceMessage(getClass().getResourceAsStream("gp-example.txt"));
		final SoapBody soapBody = message.getSoapBody();
		final Order order = (Order) unmarshal(soapBody.getPayloadSource());
		final ExchangeResponse response = endPoint.importOrder(order).getReturn();
		debug("save: result = " + response.getResult()
			+ ", errorCode = " + response.getErrorCode()
			+ ", errorMessage = " + response.getErrorMessage());
	}

	@Test
	public void whenUploadFileSFTP() throws IOException {
		final Path xmlPath = endPoint.getTempFolder().resolve("HIS-1585670598916.xml");
//		final String host = "195.250.43.91";
//		final String folder = "/home/sftp/his/data/";
		final String host = "192.168.91.5";
		final String folder = "data/";
		final String username = "his";
		final String password = "rpi2r3SSMBX2";
//		SFTPUtil.get().delete(host, username, password, folder + xmlPath.getFileName().toString());
		SFTPUtil.get().upload(host, username, password, xmlPath, folder + xmlPath.getFileName().toString());
	}
}




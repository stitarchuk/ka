{
  "services": [
    {
      "id": 1777178,
      "code": "hm.safe",
      "name": "Сейф",
      "active": false
    },
    {
      "id": 1777179,
      "code": "hm.24h",
      "name": "Обслуживание номеров 24 ч.",
      "active": false
    },
    {
      "id": 1777180,
      "code": "hm.beauty",
      "name": "Салон красоты/парикмахерская",
      "active": false
    },
    {
      "id": 1777181,
      "code": "hm.carparking",
      "name": "Парковка автомобиля",
      "active": false
    },
    {
      "id": 1777182,
      "code": "hm.atm",
      "name": "Банкомат",
      "active": false
    },
    {
      "id": 1777183,
      "code": "hm.exchange",
      "name": "Обмен валют",
      "active": false
    },
    {
      "id": 1777184,
      "code": "hm.carrental",
      "name": "Аренда авто",
      "active": false
    },
    {
      "id": 1777185,
      "code": "hm.laundry",
      "name": "Прачечная",
      "active": false
    },
    {
      "id": 1777186,
      "code": "hm.giftshop",
      "name": "Сувенирная лавка",
      "active": false
    },
    {
      "id": 1777187,
      "code": "hm.pets",
      "name": "Размещение с животными",
      "active": false
    },
    {
      "id": 1777188,
      "code": "hm.doc",
      "name": "Врач",
      "active": false
    },
    {
      "id": 1777189,
      "code": "hm.dry",
      "name": "Химчистка",
      "active": false
    },
    {
      "id": 1777190,
      "code": "hm.freeairtrnasf",
      "name": "Бесплатный трансфер",
      "active": false
    },
    {
      "id": 1777191,
      "code": "hm.multiling",
      "name": "Персонал, владеющий иностранными языками",
      "active": false
    },
    {
      "id": 1777192,
      "code": "hm.beach",
      "name": "Собственный пляж",
      "active": false
    },
    {
      "id": 1777193,
      "code": "hm.card.ve",
      "name": "Visa Electron",
      "active": false
    },
    {
      "id": 1777194,
      "code": "hm.card.v",
      "name": "Visa",
      "active": false
    },
    {
      "id": 1777195,
      "code": "hm.card.mc",
      "name": "Master Card",
      "active": false
    },
    {
      "id": 1777196,
      "code": "hm.card.dc",
      "name": "Dinners Club",
      "active": false
    },
    {
      "id": 1777197,
      "code": "hm.card.ae",
      "name": "American Express",
      "active": false
    },
    {
      "id": 1777198,
      "code": "hm.sport.golf",
      "name": "Гольф клуб",
      "active": false
    },
    {
      "id": 1777199,
      "code": "hm.sport.tennis",
      "name": "Теннисный корт",
      "active": false
    },
    {
      "id": 1777200,
      "code": "hm.sport.tabletennis",
      "name": "Настольный теннис",
      "active": false
    },
    {
      "id": 1777201,
      "code": "hm.sport.fitness",
      "name": "Фитнес центр",
      "active": false
    },
    {
      "id": 1777202,
      "code": "hm.sport.darts",
      "name": "Дартс",
      "active": false
    },
    {
      "id": 1777203,
      "code": "hm.kids.babysit",
      "name": "Няня",
      "active": false
    },
    {
      "id": 1777204,
      "code": "hm.kids.babycot",
      "name": "Колыбель",
      "active": false
    },
    {
      "id": 1777205,
      "code": "hm.kids.ground",
      "name": "Игровая площадка",
      "active": false
    },
    {
      "id": 1777206,
      "code": "hm.kids.padding",
      "name": "Бассейн для детей",
      "active": false
    },
    {
      "id": 1777207,
      "code": "hm.kids.club",
      "name": "Детский клуб",
      "active": false
    },
    {
      "id": 1777208,
      "code": "hm.beach",
      "name": "Пляж",
      "active": false
    },
    {
      "id": 1777209,
      "code": "hm.swimming-pool",
      "name": "Бассейн",
      "active": false
    },
    {
      "id": 1777210,
      "code": "hm.swimming-pool-ex",
      "name": "Бассейн с горками",
      "active": false
    },
    {
      "id": 1777211,
      "code": "hm.aqua-park",
      "name": "Аквапарк",
      "active": false
    },
    {
      "id": 1777212,
      "code": "hm.fitness",
      "name": "Спортивный зал",
      "active": false
    },
    {
      "id": 1777213,
      "code": "hm.gym",
      "name": "Тренажеры",
      "active": false
    },
    {
      "id": 1777214,
      "code": "hm.volleyball",
      "name": "Волейбол",
      "active": false
    },
    {
      "id": 1777215,
      "code": "hm.basketball",
      "name": "Баскетбол",
      "active": false
    },
    {
      "id": 1777216,
      "code": "hm.football",
      "name": "Футбол",
      "active": false
    },
    {
      "id": 1777217,
      "code": "hm.badminton",
      "name": "Бадминтон",
      "active": false
    },
    {
      "id": 1777218,
      "code": "hm.billiards",
      "name": "Бильярд",
      "active": false
    },
    {
      "id": 1777219,
      "code": "hm.bowling",
      "name": "Боулинг",
      "active": false
    },
    {
      "id": 1777220,
      "code": "hm.table.games",
      "name": "Настольные игры",
      "active": false
    },
    {
      "id": 1777221,
      "code": "hm.sauna",
      "name": "Сауна",
      "active": false
    },
    {
      "id": 1777222,
      "code": "hm.russian.baths",
      "name": "Русская баня",
      "active": false
    },
    {
      "id": 1777223,
      "code": "hm.karaoke",
      "name": "Караоке",
      "active": false
    },
    {
      "id": 1777224,
      "code": "hm.dancing.floor",
      "name": "Танцплощадка/Дискотека",
      "active": false
    },
    {
      "id": 1777225,
      "code": "hm.night.club",
      "name": "Ночной клуб",
      "active": false
    },
    {
      "id": 1777226,
      "code": "hm.concert.hall",
      "name": "Киноконцертный зал",
      "active": false
    },
    {
      "id": 1777227,
      "code": "hm.animation",
      "name": "Анимация",
      "active": false
    },
    {
      "id": 1777228,
      "code": "hm.child.menu",
      "name": "Детское меню",
      "active": false
    },
    {
      "id": 1777229,
      "code": "hm.child.room",
      "name": "Детская комната",
      "active": false
    },
    {
      "id": 1777230,
      "code": "hm.child.animation",
      "name": "Детская анимация",
      "active": false
    },
    {
      "id": 1777231,
      "code": "hm.mini.zoo",
      "name": "Мини-зоопарк",
      "active": false
    },
    {
      "id": 1777232,
      "code": "hm.trampoline",
      "name": "Батут",
      "active": false
    },
    {
      "id": 1777233,
      "code": "hm.shop",
      "name": "Магазин",
      "active": false
    },
    {
      "id": 1777234,
      "code": "hm.library",
      "name": "Библиотека",
      "active": false
    },
    {
      "id": 1777235,
      "code": "hm.rent.sports.equipment",
      "name": "Прокат инвентаря",
      "active": false
    },
    {
      "id": 1777236,
      "code": "hm.flight.and.rail.reservations",
      "name": "Заказ авиа и ж/д билетов",
      "active": false
    },
    {
      "id": 1777237,
      "code": "hm.bussiness.center",
      "name": "Бизнес-центр",
      "active": false
    },
    {
      "id": 1777238,
      "code": "hm.internet",
      "name": "Интернет",
      "active": false
    },
    {
      "id": 1777239,
      "code": "hm.free.wifi",
      "name": "Бесплатный Wi-Fi",
      "active": false
    },
    {
      "id": 1777240,
      "code": "hm.conference.hall",
      "name": "Конференц-зал",
      "active": false
    },
    {
      "id": 1777241,
      "code": "hm.conference.equipment",
      "name": "Оборудование для конференций",
      "active": false
    },
    {
      "id": 1777242,
      "code": "hm.secured.premises",
      "name": "Охраняемая территория",
      "active": false
    },
    {
      "id": 1777243,
      "code": "hm.video.surveillance",
      "name": "Видео-наблюдение",
      "active": false
    },
    {
      "id": 1777244,
      "code": "hm.electronic.keys",
      "name": "Электронные ключи",
      "active": false
    },
    {
      "id": 1777245,
      "code": "hm.cardiovascular.system",
      "name": "Сердечно-сосудистая система",
      "active": false
    },
    {
      "id": 1777246,
      "code": "hm.nervous.system",
      "name": "Нервная система",
      "active": false
    },
    {
      "id": 1777247,
      "code": "hm.musculoskeletal.system",
      "name": "Опорно-двигательный аппарат",
      "active": false
    },
    {
      "id": 1777248,
      "code": "hm.endocrine.system",
      "name": "Эндокринная система",
      "active": false
    },
    {
      "id": 1777249,
      "code": "hm.upper.air.passages",
      "name": "Верхние дыхательные пути",
      "active": false
    },
    {
      "id": 1777250,
      "code": "hm.gastrointestinal.tract",
      "name": "Желудочно-кишечный тракт",
      "active": false
    },
    {
      "id": 1777251,
      "code": "hm.skin.diseases",
      "name": "Кожные заболевания",
      "active": false
    },
    {
      "id": 1777252,
      "code": "hm.genital.sphere.diseases",
      "name": "Заболевания женской и мужской половых сфер",
      "active": false
    },
    {
      "id": 1777253,
      "code": "hm.macistin.procedures",
      "name": "Мацестинские процедуры",
      "active": false
    },
    {
      "id": 1777254,
      "code": "hm.spa.procedures",
      "name": "SPA-процедуры",
      "active": false
    },
    {
      "id": 1777255,
      "code": "hm.dinning.room",
      "name": "Столовая/обеденный зал",
      "active": false
    },
    {
      "id": 1777256,
      "code": "hm.full.board",
      "name": "Комплексное питание",
      "active": false
    },
    {
      "id": 1777257,
      "code": "hm.a.la.carte",
      "name": "Заказное меню",
      "active": false
    },
    {
      "id": 1777258,
      "code": "hm.open.buffet",
      "name": "Шведский стол",
      "active": false
    },
    {
      "id": 1777259,
      "code": "hm.menu.for.children",
      "name": "Детское меню",
      "active": false
    },
    {
      "id": 1777260,
      "code": "hm.diet.menu",
      "name": "Диетическое меню",
      "active": false
    },
    {
      "id": 1777261,
      "code": "hm.home.cuisine",
      "name": "Домашняя кухня",
      "active": false
    },
    {
      "id": 1777262,
      "code": "hm.self.catering",
      "name": "Кухня самообслуживания",
      "active": false
    },
    {
      "id": 1777263,
      "code": "hm.caffe",
      "name": "Кафе",
      "active": false
    },
    {
      "id": 1777264,
      "code": "hm.bar",
      "name": "Бар",
      "active": false
    },
    {
      "id": 1777265,
      "code": "hm.restaurant",
      "name": "Ресторан",
      "active": false
    },
    {
      "id": 1777266,
      "code": "hm.rooms.alarm.clock",
      "name": "Будильник",
      "active": false
    },
    {
      "id": 1777267,
      "code": "hm.rooms.safe",
      "name": "Сейф",
      "active": false
    },
    {
      "id": 1777268,
      "code": "hm.rooms.phone",
      "name": "Телефон",
      "active": false
    },
    {
      "id": 1777269,
      "code": "hm.rooms.fax",
      "name": "Факс",
      "active": false
    },
    {
      "id": 1777270,
      "code": "hm.rooms.desk",
      "name": "Рабочий стол",
      "active": false
    },
    {
      "id": 1777271,
      "code": "hm.rooms.seating.area.with.sofa",
      "name": "Гостиный уголок с диваном/креслом",
      "active": false
    },
    {
      "id": 1777272,
      "code": "hm.rooms.fireplace",
      "name": "Камин",
      "active": false
    },
    {
      "id": 1777273,
      "code": "hm.rooms.internet",
      "name": "Интернет",
      "active": false
    },
    {
      "id": 1777274,
      "code": "hm.rooms.wireless.internet",
      "name": "Беспроводной интернет",
      "active": false
    },
    {
      "id": 1777275,
      "code": "hm.rooms.high.speed.internet",
      "name": "Высокоскоростной интернет",
      "active": false
    },
    {
      "id": 1777276,
      "code": "hm.rooms.television",
      "name": "Телевизор",
      "active": false
    },
    {
      "id": 1777277,
      "code": "hm.rooms.cable",
      "name": "Кабельное/Спутниковое телевидение",
      "active": false
    },
    {
      "id": 1777278,
      "code": "hm.rooms.private.bathroom",
      "name": "Санузел в номере",
      "active": false
    },
    {
      "id": 1777279,
      "code": "hm.rooms.shared.bathroom",
      "name": "Общий санузел",
      "active": false
    },
    {
      "id": 1777280,
      "code": "hm.rooms.bathtub",
      "name": "Ванна",
      "active": false
    },
    {
      "id": 1777281,
      "code": "hm.rooms.shower",
      "name": "Душ",
      "active": false
    },
    {
      "id": 1777282,
      "code": "hm.rooms.jacuzzi",
      "name": "Джакузи",
      "active": false
    },
    {
      "id": 1777283,
      "code": "hm.rooms.private.pool",
      "name": "Приватный бассейн",
      "active": false
    },
    {
      "id": 1777284,
      "code": "hm.rooms.bathrobe",
      "name": "Халат",
      "active": false
    },
    {
      "id": 1777285,
      "code": "hm.rooms.bathroom.amenities",
      "name": "Принадлежности для умывания",
      "active": false
    },
    {
      "id": 1777286,
      "code": "hm.rooms.hairdryer",
      "name": "Фен",
      "active": false
    },
    {
      "id": 1777287,
      "code": "hm.rooms.ironing.board",
      "name": "Гладильная доска",
      "active": false
    },
    {
      "id": 1777288,
      "code": "hm.rooms.iron",
      "name": "Утюг",
      "active": false
    },
    {
      "id": 1777289,
      "code": "hm.rooms.washer",
      "name": "Стиральная машина",
      "active": false
    },
    {
      "id": 1777290,
      "code": "hm.rooms.air.conditioning",
      "name": "Кондиционер",
      "active": false
    },
    {
      "id": 1777291,
      "code": "hm.rooms.ceiling.fan",
      "name": "Потолочный вентилятор",
      "active": false
    },
    {
      "id": 1777292,
      "code": "hm.rooms.air.filtration",
      "name": "Система фильтрации воздуха",
      "active": false
    },
    {
      "id": 1777293,
      "code": "hm.rooms.heating.system",
      "name": "Система отопления",
      "active": false
    },
    {
      "id": 1777294,
      "code": "hm.rooms.minibar",
      "name": "Мини-бар",
      "active": false
    },
    {
      "id": 1777295,
      "code": "hm.rooms.coffee.or.tea.maker",
      "name": "Чайник/Кофеварка",
      "active": false
    },
    {
      "id": 1777296,
      "code": "hm.rooms.mini.refrigerator",
      "name": "Мини-холодильник",
      "active": false
    },
    {
      "id": 1777297,
      "code": "hm.rooms.refrigerator",
      "name": "Холодильник",
      "active": false
    },
    {
      "id": 1777298,
      "code": "hm.rooms.microwave",
      "name": "Микроволновая печь",
      "active": false
    },
    {
      "id": 1777299,
      "code": "hm.rooms.stove",
      "name": "Плита",
      "active": false
    },
    {
      "id": 1777300,
      "code": "hm.rooms.dishwasher",
      "name": "Посудомоечная машина",
      "active": false
    },
    {
      "id": 1777301,
      "code": "hm.rooms.kitchen",
      "name": "Кухня",
      "active": false
    },
    {
      "id": 1777302,
      "code": "hm.rooms.adjoining.room",
      "name": "Смежный номер",
      "active": false
    },
    {
      "id": 1777303,
      "code": "hm.rooms.terrace",
      "name": "Балкон/Веранда/Терраса",
      "active": false
    },
    {
      "id": 1777304,
      "code": "hm.rooms.smoking",
      "name": "Номер для курящих",
      "active": false
    },
    {
      "id": 1777305,
      "code": "hm.rooms.non.smoking",
      "name": "Номер для некурящих",
      "active": false
    },
    {
      "id": 1777306,
      "code": "hm.rooms.pets.allowed",
      "name": "Допускается размещение с домашними животными",
      "active": false
    },
    {
      "id": 1777307,
      "code": "hm.rooms.housekeeping.service",
      "name": "Уборка номеров",
      "active": false
    },
    {
      "id": 1777308,
      "code": "hm.rooms.mountain.view",
      "name": "С видом на горы",
      "active": false
    },
    {
      "id": 1777309,
      "code": "hm.rooms.gulf.view",
      "name": "С видом на залив",
      "active": false
    },
    {
      "id": 1777310,
      "code": "hm.rooms.ocean.view",
      "name": "С видом на океан",
      "active": false
    },
    {
      "id": 1777311,
      "code": "hm.air",
      "name": "Кондиционер/климат контроль",
      "active": false
    },
    {
      "id": 1777433,
      "code": "hm.elevators",
      "name": "Лифт",
      "active": false
    },
    {
      "id": 1777434,
      "code": "hm.sport.pool",
      "name": "Бассейн",
      "active": false
    },
    {
      "id": 1777435,
      "code": "hm.adults_only",
      "name": "Только взрослые",
      "active": false
    }
  ],
  "hotelClassifications": [
    {
      "id": 22,
      "code": "Modern",
      "name": "Современный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 26,
      "code": "Asian",
      "name": "Азиатский",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 30,
      "code": "Southwest",
      "name": "Юго-западной архитектуры",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 40,
      "code": "Western",
      "name": "Западной архитектуры",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 44,
      "code": "Ancient",
      "name": "Древний",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 48,
      "code": "Themed",
      "name": "Тематический",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 80,
      "code": "Original",
      "name": "Самобытный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 82,
      "code": "Design",
      "name": "Дизайнерский",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 15,
      "code": "Historic",
      "name": "Историческое здание",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 19,
      "code": "Mediterranean",
      "name": "Средиземноморской архитектуры",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 33,
      "code": "Traditional",
      "name": "Традиционный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 37,
      "code": "Victorian",
      "name": "Викторианский",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 81,
      "code": "Stylish",
      "name": "Стильный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 1,
      "code": "Art deco",
      "name": "Арт-деко",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 5,
      "code": "Brazilian",
      "name": "Бразильский",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 11,
      "code": "High rise",
      "name": "Высотный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 16,
      "code": "East",
      "name": "Восток",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 20,
      "code": "Expressway",
      "name": "У автострады",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 34,
      "code": "Resort",
      "name": "Курортный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 38,
      "code": "Rural",
      "name": "Сельский",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 52,
      "code": "Beachfront",
      "name": "С видом на пляж",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 60,
      "code": "Entertainment district",
      "name": "Развлекательный район",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 62,
      "code": "Financial district",
      "name": "Финансовый район",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 64,
      "code": "Theatre district",
      "name": "Театральный район",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 66,
      "code": "Countryside",
      "name": "Сельская местность",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 68,
      "code": "Bay",
      "name": "У залива / бухты",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 70,
      "code": "Park",
      "name": "Рядом с парком",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 72,
      "code": "Tourist site",
      "name": "Туристический район",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 74,
      "code": "South suburb",
      "name": "Южный пригород",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 76,
      "code": "West suburb",
      "name": "Западный пригород",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 78,
      "code": "Ski resort",
      "name": "Лыжный курорт",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 2,
      "code": "Airport",
      "name": "В аэропорту",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 6,
      "code": "Beach",
      "name": "Пляжный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 12,
      "code": "Downtown",
      "name": "Центр города",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 23,
      "code": "Lake",
      "name": "Около озера",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 27,
      "code": "Mountain",
      "name": "Горы",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 31,
      "code": "North",
      "name": "Север",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 41,
      "code": "South",
      "name": "Юг",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 45,
      "code": "Suburban",
      "name": "Пригородный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 49,
      "code": "West",
      "name": "Запад",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 55,
      "code": "Oceanfront",
      "name": "С видом на океан",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 59,
      "code": "Business district",
      "name": "Бизнес район",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 63,
      "code": "Shopping district",
      "name": "Торговый район",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 69,
      "code": "Sea",
      "name": "У моря",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 71,
      "code": "River",
      "name": "У реки",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 73,
      "code": "North suburb",
      "name": "Северный пригород",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 75,
      "code": "East suburb",
      "name": "Восточный пригород",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 77,
      "code": "Waterfront",
      "name": "На берегу/ в порту",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 9,
      "code": "City",
      "name": "Городской",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 24,
      "code": "First class",
      "name": "Первый класс",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 28,
      "code": "Luxury",
      "name": "Роскошный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 42,
      "code": "Resort",
      "name": "Курортный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 46,
      "code": "Tourist",
      "name": "Туристический",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 50,
      "code": "Upscale",
      "name": "Высококлассный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 56,
      "code": "Standard",
      "name": "Стандартный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 58,
      "code": "Midscale",
      "name": "Средний уровень",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 10,
      "code": "Corporate business transient",
      "name": "Корпоративный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 17,
      "code": "Economy",
      "name": "Экономный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 21,
      "code": "Extended stay",
      "name": "Длительное пребывание",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 35,
      "code": "Moderate",
      "name": "Умеренный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 53,
      "code": "Efficiency",
      "name": "Рентабельный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 61,
      "code": "Quality",
      "name": "Качественный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 65,
      "code": "Midscale without F&B",
      "name": "Средний без общественного питания",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 67,
      "code": "Upper upscale",
      "name": "Высокий",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 3,
      "code": "All suite",
      "name": "Все включено",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 7,
      "code": "Budget",
      "name": "Бюджетный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 13,
      "code": "Deluxe",
      "name": "Делюкс",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 14,
      "code": "Group or meeting",
      "name": "Для групп и встреч",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 18,
      "code": "Conference",
      "name": "Для конференций",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 32,
      "code": "Golf",
      "name": "Гольф",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 36,
      "code": "Only Adults",
      "name": "Только для взрослых",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 54,
      "code": "Winter",
      "name": "Зимний отдых",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 4,
      "code": "Business",
      "name": "Деловой",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 8,
      "code": "Personal/leisure",
      "name": "Отдых",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 25,
      "code": "Adventure",
      "name": "Приключенческий",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 29,
      "code": "Family",
      "name": "Семейный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 39,
      "code": "Pets Friendly",
      "name": "Разрешается проживание с животными",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 43,
      "code": "SPA",
      "name": "СПА",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 47,
      "code": "Wedding",
      "name": "Для свадеб",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 51,
      "code": "Wellnes & Active",
      "name": "Оздоровительный и для активного времяпровождения",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 57,
      "code": "Sport",
      "name": "Спортивный",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 79,
      "code": "Gay Friendly",
      "name": "Для лиц нетрадиционной сексуальной ориентации",
      "supplierId": 1,
      "active": false
    },
    {
      "id": 83,
      "code": "Eco hotel",
      "name": "Эко-отель",
      "supplierId": 1,
      "active": false
    }
  ]
}
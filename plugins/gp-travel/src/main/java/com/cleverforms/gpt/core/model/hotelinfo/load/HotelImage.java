/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import static com.cleverforms.comms.shared.marker.HasTitle.TITLE;

import com.cleverforms.comms.shared.marker.HasId;
import com.cleverforms.comms.shared.proxy.ValueProxy;
import com.cleverforms.comms.shared.util.Format;
import com.cleverforms.gpt.core.model.XmlizableLocalizableString;

/**
 * A base {@code ValueProxy} for hotel {@code Hotel}'s image data.
 * @author Sergey Titarchuk
 */
public class HotelImage implements ValueProxy, HasId<Long> {

	/** The serail version UID */
	private static final long serialVersionUID = 6272251250548106671L;

	/** Key to object ID property */
	public final static String OBJECT_ID = "objectId";

	/** Key to object name property */
	public final static String OBJECT_NAME = "objectName";

	/** Key to object type property */
	public final static String OBJECT_TYPE = "objectType";

	/** Key to omain photo property */
	public final static String MAIN_PHOTO = "mainPhoto";

	/** Key to type ID property */
	public final static String TYPE_ID = "typeId";

	/** Key to url property */
	public final static String URL = "url";

	private long id;
	private long objectId;
	private String objectType;
	private String objectName;
	private String url;
	private XmlizableLocalizableString title;
	private long typeId;
	private Boolean mainPhoto;

	public HotelImage() {}

	@Override
	public Long getId() {
		return Long.valueOf(id);
	}

	@Override
	public void setId(Long id) {
		this.id = id != null ? id.longValue() : 0;
	}

	@Override
	public String key() {
		return String.valueOf(id);
	}

	public long getObjectId() {
		return objectId;
	}

	public void setObjectId(long objectId) {
		this.objectId = objectId;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public XmlizableLocalizableString getTitle() {
		return title;
	}

	public void setTitle(XmlizableLocalizableString title) {
		this.title = title;
	}

	public long getTypeId() {
		return typeId;
	}

	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}

	public Boolean getMainPhoto() {
		return mainPhoto;
	}

	public void setMainPhoto(Boolean mainPhoto) {
		this.mainPhoto = mainPhoto;
	}

	@Override
	public String toString() {
		return Format.toStringHelper(this)
			.add(ID, id)
			.add(OBJECT_ID, objectId)
			.add(OBJECT_TYPE, objectType)
			.add(OBJECT_NAME, objectName)
			.add(URL, url)
			.add(TITLE, title)
			.add(TYPE_ID, typeId)
			.add(MAIN_PHOTO, mainPhoto)
			.toString();
	}

}

/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * March 27, 2020.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core;

import java.math.BigInteger;
import java.nio.file.Path;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.vfs2.FileSystemException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.cleverforms.comms.server.WithLoggerImpl;
import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.server.util.io.FileUtil;
import com.cleverforms.comms.server.util.io.SFTPUtil;
import com.cleverforms.comms.shared.exception.AppException;
import com.cleverforms.comms.shared.exception.ErrorType;
import com.cleverforms.gpt.export.ExchangeResponse;
import com.cleverforms.gpt.export.Order;
import com.cleverforms.gpt.export.OrderResponse;
import com.cleverforms.gpt.mos.DateTime;
import com.cleverforms.gpt.mos.Document;
import com.cleverforms.gpt.mos.DocumentType;
import com.cleverforms.gpt.mos.NamedEntity;
import com.cleverforms.gpt.mos.ObjectFactory;
import com.cleverforms.gpt.mos.OtherSale;
import com.cleverforms.gpt.mos.YesNo;

@Endpoint
public class GPImportEndpoint extends WithLoggerImpl implements InitializingBean {

    private static final String NAMESPACE_URI = "http://www.software.travel/gptours/export";

	@Value("${gpt.tempFolder:}")
    private Path tempFolder;
	@Value("${gpt.sftp.host:}")
	private String sftpHost;
	@Value("${gpt.sftp.path:}")
	private String sftpPath;
	@Value("${gpt.sftp.username:}")
	private String username = "his";
	@Value("${gpt.sftp.password:}")
	private String password = "rpi2r3SSMBX2";

    private final ObjectFactory jaxbFactory;

    public GPImportEndpoint() {
		jaxbFactory = new ObjectFactory();
	}

	public Path getTempFolder() {
		return tempFolder;
	}

	public void setTempFolder(Path tempFolder) {
		this.tempFolder = tempFolder;
	}

	public String getSftpHost() {
		return sftpHost;
	}

	public void setSftpHost(String sftpHost) {
		this.sftpHost = sftpHost;
	}

	public String getSftpPath() {
		return sftpPath;
	}

	public void setSftpPath(String sftpPath) {
		this.sftpPath = sftpPath;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (tempFolder == null || Util.isBlank(tempFolder.toString())) {
			tempFolder = FileUtil.getTempPath().resolve("gpt.import");
		}
		tempFolder = FileUtil.createFolder(tempFolder);
		trace("GP Travel import endpoint started.");
	}

	protected OrderResponse createResponse(ExchangeResponse exchangeResponse) {
		final OrderResponse response = new OrderResponse();
		response.setReturn(exchangeResponse);
		return response;
	}

	protected OrderResponse createOkResponse() {
		final ExchangeResponse response = new ExchangeResponse();
		response.setResult("OK");
		return createResponse(response);
	}

	protected OrderResponse createErrorResponse(AppException exception) {
		final ExchangeResponse response = new ExchangeResponse();
		response.setErrorCode(String.valueOf(exception.getErrorCode()));
		response.setErrorMessage(exception.getMessage());
		return createResponse(response);
	}

	protected NamedEntity createNamedEntity(long id, String name) {
		final NamedEntity serviceName = jaxbFactory.createNamedEntity();
		serviceName.setID(BigInteger.valueOf(id));
		serviceName.setName(name);
		return serviceName;
	}

	protected XMLGregorianCalendar createXMLDateTime(Date date) {
		if (date != null) {
			try {
				GregorianCalendar calendar = new GregorianCalendar();
				calendar.setTime(date);
				return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
			} catch (DatatypeConfigurationException e) {
				error("Can't create XML date: " + date, e);
			}
		}
		return null;
	}

	protected XMLGregorianCalendar createXMLDate(XMLGregorianCalendar calendar) {
		if (calendar != null) {
			try {
				GregorianCalendar cal = calendar.toGregorianCalendar();
				return DatatypeFactory.newInstance().newXMLGregorianCalendarDate(
						cal.get(Calendar.YEAR),
				        // Calendar.MONTH is zero based, XSD Date datatype's month field starts
				        // with JANUARY as 1.
						cal.get(Calendar.MONTH) + 1,
						cal.get(Calendar.DAY_OF_MONTH),
				        // Calendar ZONE_OFFSET and DST_OFFSET fields are in milliseconds.
						(cal.get(Calendar.ZONE_OFFSET) + cal.get(Calendar.DST_OFFSET)) / (60 * 1000)
				);
			} catch (DatatypeConfigurationException e) {
				error("Can't create XML date: " + calendar, e);
			}
		}
		return null;
	}

	protected XMLGregorianCalendar createXMLTime(XMLGregorianCalendar calendar) {
		if (calendar != null) {
			try {
				GregorianCalendar cal = calendar.toGregorianCalendar();
				return DatatypeFactory.newInstance().newXMLGregorianCalendarTime(
						cal.get(Calendar.HOUR_OF_DAY),
						cal.get(Calendar.MINUTE),
						cal.get(Calendar.SECOND),
						cal.get(Calendar.MILLISECOND),
				        // Calendar ZONE_OFFSET and DST_OFFSET fields are in milliseconds.
						(cal.get(Calendar.ZONE_OFFSET) + cal.get(Calendar.DST_OFFSET)) / (60 * 1000)
				);
			} catch (DatatypeConfigurationException e) {
				error("Can't create XML date: " + calendar, e);
			}
		}
		return null;
	}

	protected DateTime createDateTime(XMLGregorianCalendar calendar) {
		final DateTime dateTime = new DateTime();
		dateTime.setDate(createXMLDate(calendar));
		dateTime.setTime(createXMLTime(calendar));
		return dateTime;
	}

	protected NamedEntity createServiceName() {
		return createNamedEntity(16, "Услуги по Украине");
	}

	protected NamedEntity createSystem() {
		return createNamedEntity(38, "HIS");
	}

	protected OtherSale.OperationDetails createOperationDetails(Order order) {
		final OtherSale.OperationDetails operationDetails = jaxbFactory.createOtherSaleOperationDetails();
		operationDetails.setDate(createXMLDate(order.getModified()));
		operationDetails.setTime(createXMLTime(order.getModified()));
		operationDetails.setAny(order);
		return operationDetails;
	}

	protected OtherSale createOtherSale(Order order) {
		final OtherSale otherSale = jaxbFactory.createOtherSale();
		otherSale.setCreateDetails(createDateTime(order.getCreated()));
		otherSale.setOperationDetails(createOperationDetails(order));
		return otherSale;
	}

	protected Document createDocument(Order order) {
		final Document document = jaxbFactory.createDocument();
		document.setServiceName(createServiceName());
		document.setSystem(createSystem());
		document.setOnline(YesNo.YES);
		document.setDocumentType(DocumentType.OTHER_SALE);
		document.setOtherSale(createOtherSale(order));
		return document;
	}

	protected void saveXml(JAXBElement<Document> data, Path xmlPath) throws JAXBException {
		final JAXBContext context = JAXBContext.newInstance(Document.class, Order.class);
		final Marshaller marshaller = context.createMarshaller();
		marshaller.marshal(data, new StreamResult(xmlPath.toFile()));
	}

	public void saveXmlAndUpload(JAXBElement<Document> data, Path xmlPath) throws FileSystemException, JAXBException {
		saveXml(data, xmlPath);
		SFTPUtil.get().upload(sftpHost, username, password, xmlPath, sftpPath + xmlPath.getFileName().toString());
	}

	@ResponsePayload
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "order")
	public OrderResponse importOrder(@RequestPayload Order order) {
		final Path xmlPath = tempFolder.resolve("HIS-" + System.currentTimeMillis() + ".xml");
		try {
			saveXmlAndUpload(jaxbFactory.createDocument(createDocument(order)), xmlPath);
		} catch (Exception e) {
			error("Can't create xml: " + xmlPath.toString(), e);
			return createErrorResponse(new AppException(ErrorType.ERROR_WRITE_DATA, e.getMessage()));
		}
		trace(xmlPath.toString() + " was created.");
		return createOkResponse();
	}

}

/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.shared.proxy.ValueProxy;
import com.cleverforms.comms.shared.util.Format;
import com.cleverforms.gpt.core.model.XmlizableLocalizableString;

/**
 * A base {@code ValueProxy} for {@code Hotel} address data
 * @author Sergey Titarchuk
 */
public class HotelAddress implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = 9182366375540248104L;

	/** Key to address line property */
	public final static String ADDRESS_LINE = "addressLine";

	/** Key to country name property */
	public final static String COUNTRY_NAME = "countryName";

	/** Key to city name property */
	public final static String CITY_NAME = "cityName";

	/** Key to zip property */
	public final static String ZIP = "zip";

	/** Key to zone name property */
	public final static String ZONE_NAME = "zoneName";

	private String zip;
	private XmlizableLocalizableString countryName;
	private XmlizableLocalizableString cityName;
	private XmlizableLocalizableString addressLine;
	private XmlizableLocalizableString zoneName;

	public HotelAddress() {}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public XmlizableLocalizableString getCountryName() {
		return countryName;
	}

	public void setCountryName(XmlizableLocalizableString countryName) {
		this.countryName = countryName;
	}

	public XmlizableLocalizableString getCityName() {
		return cityName;
	}

	public void setCityName(XmlizableLocalizableString cityName) {
		this.cityName = cityName;
	}

	public XmlizableLocalizableString getAddressLine() {
		return addressLine;
	}

	public void setAddressLine(XmlizableLocalizableString addressLine) {
		this.addressLine = addressLine;
	}

	public XmlizableLocalizableString getZoneName() {
		return zoneName;
	}

	public void setZoneName(XmlizableLocalizableString zoneName) {
		this.zoneName = zoneName;
	}

	@Override
	public String toString() {
		return Format.toStringHelper(this)
			.add(ZIP, zip)
			.add(COUNTRY_NAME, countryName)
			.add(CITY_NAME, cityName)
			.add(ZONE_NAME, zoneName)
			.toString();
	}

}

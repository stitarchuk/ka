/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotel;

import java.io.Serializable;
/**
 * A small enumerator for category group code of {@code Hotel}.
 * @author Sergey Titarchuk
 */
public enum HotelCategoryGroupCode implements Serializable {

	ARCHITECTURE, BUDGET, CATEGORY, LOCATION, OTHER, TARGET, TYPE;

}

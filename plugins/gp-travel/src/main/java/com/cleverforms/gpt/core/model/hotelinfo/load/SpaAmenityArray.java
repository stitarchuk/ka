/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.shared.proxy.ValueProxy;

/**
 * A wrapper class for wrap {@code SpaAmenity} as Array.
 * @author Sergey Titarchuk
 */
public class SpaAmenityArray implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = -7234749731173130291L;

	private TypedAmenity[] spaAmenity;

	public SpaAmenityArray() {}

	public TypedAmenity[] getSpaAmenity() {
		return spaAmenity;
	}

	public void setSpaAmenity(TypedAmenity[] spaAmenity) {
		this.spaAmenity = spaAmenity;
	}

	@Override
	public String toString() {
		return Casting.arrayToString(spaAmenity);
	}

}

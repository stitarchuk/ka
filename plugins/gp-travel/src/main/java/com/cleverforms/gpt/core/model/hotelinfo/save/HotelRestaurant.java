/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.save;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.shared.marker.HasType;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.gpt.core.GPTConnector;
import com.cleverforms.gpt.core.GPTUtil;
import com.cleverforms.gpt.core.model.NamedDescriptionModel;
import com.cleverforms.gpt.core.model.hotel.HotelRestaurantLocationCode;
import com.cleverforms.gpt.core.model.hotel.HotelRestaurantTypeCode;
import com.cleverforms.iss.server.model.hotel.CoffeeRoom;
import com.cleverforms.iss.shared.hotel.CoffeeRoomProperty;

/**
 * A base {@link NamedDescriptionModel} for hotel {@code Restaurant} info data.
 * @author Sergey Titarchuk
 */
public class HotelRestaurant extends NamedDescriptionModel implements HasType<HotelRestaurantTypeCode> {

	/** The serial version UID */
	private static final long serialVersionUID = -488973807376287260L;

	/** Key to cuisine property */
	public final static String CUISINE = "cuisine";

	/** Key to location property */
	public final static String LOCATION = "location";

	private HotelRestaurantLocationCode location;
	private HotelRestaurantTypeCode type;
	private Map<String, String> cuisine; 
	
	public HotelRestaurant() {}

	public HotelRestaurantLocationCode getLocation() {
		return location;
	}

	public void setLocation(HotelRestaurantLocationCode location) {
		this.location = location;
	}

	@Override
	public HotelRestaurantTypeCode getType() {
		return type;
	}

	@Override
	public void setType(HotelRestaurantTypeCode type) {
		this.type = type;
	}

	public Map<String, String> getCuisine() {
		if (cuisine == null) cuisine = new HashMap<String, String>();
		return cuisine;
	}

	public void setCuisine(Map<String, String> cuisine) {
		this.cuisine = cuisine;
	}

	public String cuisine(String locale) {
		return cuisine != null && locale != null ? cuisine.get(locale.toLowerCase()) : null;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ cuisine, location, type });
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof HotelRestaurant) {
			final HotelRestaurant model = (HotelRestaurant) obj;
			return super.equals(obj) 
				&& Util.equalsWithNull(cuisine, model.cuisine)
				&& Util.equalsWithNull(location, model.location)
				&& Util.equalsWithNull(type, model.type);
		} else if (obj instanceof CoffeeRoom) {
			final CoffeeRoom entity = (CoffeeRoom) obj;
			boolean equals = Util.equalsWithNull(location, HotelRestaurantLocationCode.parse(entity.getLocation()))
					&& Util.equalsWithNull(type, HotelRestaurantTypeCode.parse(entity.getType()))
					&& Util.equalsWithNull(entity.getProperty(CoffeeRoomProperty.KITCHEN_TYPE), cuisine(Util.LOCALE_UKRAINE.getLanguage()));
			for (Locale l : Util.SUPPORTED_LOCALES) {
				final String lang = l.getLanguage();
				equals = equals && Util.equalsWithNull(entity.getProperty(entity.parseProperty("NAME", lang)), name(lang))
						&& Util.equalsWithNull(entity.getProperty(entity.parseProperty("DESCRIPTION", lang)), shortDescription(lang))
						&& Util.equalsWithNull(entity.getProperty(entity.parseProperty("FULL_DESCRIPTION", lang)), description(lang));
			}
			return equals;
		}
		return false;
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper()
			.add(LOCATION, location)
			.add(TYPE, type)
			.add(CUISINE, cuisine);
	}

	public HotelRestaurant serialize(GPTConnector connector, CoffeeRoom coffeeRoom) {
		final long systemId = connector.getSystemId();
		final GPTUtil util = GPTUtil.get();
		setId(Casting.asLong(coffeeRoom.getAlias(systemId), null));
		// names
		setName(util.getLocalizedProperties(coffeeRoom, CoffeeRoomProperty.SEARCH_PROPERTIES));
		// descriptions
		setShortDescription(util.getLocalizedProperties(coffeeRoom, CoffeeRoomProperty.DESCRIPTION_PROPERTIES));
		setDescription(util.getLocalizedProperties(coffeeRoom, CoffeeRoomProperty.FULL_DESCRIPTION_PROPERTIES));
		// cuisine
		final String cuisine = coffeeRoom.getProperty(CoffeeRoomProperty.KITCHEN_TYPE);
		if (Util.isNotBlank(cuisine)) {
			getCuisine().put(Util.LOCALE_UKRAINE.getLanguage(), cuisine);
		}
		location = HotelRestaurantLocationCode.parse(coffeeRoom.getLocation());
		type = HotelRestaurantTypeCode.parse(coffeeRoom.getType());
		setActive(true);
		return this;
	}

}

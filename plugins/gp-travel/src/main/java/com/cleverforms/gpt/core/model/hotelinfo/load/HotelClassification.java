/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.shared.util.Format;
import com.cleverforms.gpt.core.model.XmlizableLocalizableString;
import com.cleverforms.gpt.core.model.hotel.HotelCategoryGroupCode;
import com.cleverforms.ics.shared.marker.HasGroup;

/**
 * A base {@code ValueProxy} for hotel {@code Classification} info data.
 * @author Sergey Titarchuk
 */
public class HotelClassification extends OTAInfo implements HasGroup<HotelCategoryGroupCode> {

	/** The serial version UID */
	private static final long serialVersionUID = -6680939704671002857L;

	/** Key to show on UI property */
	public static final String SHOW_ON_UI = "showOnUI";

	/** Key to supplier classification name property */
	public static final String SUPPLIER_CLASSIFICATION_NAME = "supplierClassificationName";

	private HotelCategoryGroupCode group;
	private XmlizableLocalizableString supplierClassificationName;
	private Boolean showOnUI;

	public HotelClassification() {}

	@Override
	public HotelCategoryGroupCode getGroup() {
		return group;
	}

	@Override
	public void setGroup(HotelCategoryGroupCode group) {
		this.group = group;
	}

	public XmlizableLocalizableString getSupplierClassificationName() {
		return supplierClassificationName;
	}

	public void setSupplierClassificationName(XmlizableLocalizableString supplierClassificationName) {
		this.supplierClassificationName = supplierClassificationName;
	}

	public Boolean getShowOnUI() {
		return showOnUI;
	}

	public void setShowOnUI(Boolean showOnUI) {
		this.showOnUI = showOnUI;
	}

	@Override
	public String toString() {
		return Format.toStringHelper(this)
			.add(OTA_CODE, getOtaCode())
			.add(OTA_NAME, getOtaName())
			.add(GROUP, group)
			.add(SUPPLIER_CLASSIFICATION_NAME, supplierClassificationName)
			.add(SHOW_ON_UI, showOnUI)
			.toString();
	}

}

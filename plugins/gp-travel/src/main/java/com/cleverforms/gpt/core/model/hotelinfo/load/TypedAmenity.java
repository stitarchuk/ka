/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.shared.marker.HasType;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;

/**
 * A base {@code ValueProxy} for {@code Amenity} with type data.
 * @author Sergey Titarchuk
 */
public class TypedAmenity extends Amenity implements HasType<String> {

	/** The serial version UID */
	private static final long serialVersionUID = -3365145805391874214L;

	private String type;

	public TypedAmenity() {}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper().add(TYPE, type);
	}

}

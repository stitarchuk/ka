/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.shared.marker.HasName;
import com.cleverforms.comms.shared.marker.HasType;
import com.cleverforms.comms.shared.proxy.ValueProxy;
import com.cleverforms.comms.shared.util.Format;

/**
 * A base {@code ValueProxy} for hotel {@code RelativePosition} data.
 * @author Sergey Titarchuk
 */
public class RelativePosition implements ValueProxy, HasName, HasType<String> {

	/** The serial version UID */
	private static final long serialVersionUID = -1603932453555134218L;

	/** Key to measure property */
	public static final String MEASURE = "measure";

	/** Key to distance property */
	public static final String DISTANCE = "distance";

	private String name;
	private String type;
	private String measure;
	private String distance;

	public RelativePosition() {}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	public String getMeasure() {
		return measure;
	}

	public void setMeasure(String measure) {
		this.measure = measure;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	@Override
	public String toString() {
		return Format.toStringHelper(this)
			.add(NAME, name)
			.add(TYPE, type)
			.add(MEASURE, measure)
			.add(DISTANCE, distance)
			.toString();
	}

}

/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model;

import java.util.Arrays;

import com.cleverforms.comms.shared.proxy.ValueProxy;
import com.cleverforms.comms.shared.util.Format;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;

/**
 * A base {@code ValueProxy} for {@code Object} with active field.
 * @author Sergey Titarchuk
 */
public class ActiveModel implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = 6869083891398925610L;

	/** Key to active property */
	public final static String ACTIVE = "active";

	private boolean active;

	public ActiveModel() {}

	public ActiveModel(boolean active) {
		this.active = active;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active != null ? active.booleanValue() : false;
	}

	protected Object[] hashFields() {
		return new Object[]{ Boolean.valueOf(active) };
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(hashFields());
	}
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ActiveModel) {
			return active == ((ActiveModel) obj).active;
		}
		return false;
	}

	protected ObjectToStringHelper stringHelper() {
		return Format.toStringHelper(this);
	}

	@Override
	public String toString() {
		return stringHelper().toString();
	}

}

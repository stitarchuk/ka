/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.save;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.gpt.core.model.NamedDescriptionModel;
import com.cleverforms.gpt.core.model.hotel.HotelSportType;

/**
 * A base {@link NamedDescriptionModel} for hotel {@code Sport} info data.
 * @author Sergey Titarchuk
 */
public class HotelSport extends NamedDescriptionModel {

	/** The serial version UID */
	private static final long serialVersionUID = 3762382318153769787L;

	/** Key to sport type property */
	public final static String SPORT_TYPE = "sportType";

	private HotelSportType sportType;
	
	public HotelSport() {}

	public HotelSport(Long id, boolean active) {
		super(id, active);
	}

	public HotelSportType getSportType() {
		return sportType;
	}

	public void setSportType(HotelSportType sportType) {
		this.sportType = sportType;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ getId(), sportType });
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof HotelSport) {
			final HotelSport model = (HotelSport) obj;
			return super.equals(obj) 
					&& Util.equalsWithNull(getId(), model.getId())
					&& Util.equalsWithNull(sportType, model.sportType);
		}
		return false;
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper().add(SPORT_TYPE, sportType);
	}
	
}

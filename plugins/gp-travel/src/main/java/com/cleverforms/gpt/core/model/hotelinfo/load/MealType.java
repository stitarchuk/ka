/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.shared.marker.HasCode;
import com.cleverforms.comms.shared.marker.HasType;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.gpt.core.model.ActiveModel;
import com.cleverforms.gpt.core.model.XmlizableLocalizableString;

/**
 * A base {@link ActiveModel} for {@code Meal} type info data.
 * @author Sergey Titarchuk
 */
public class MealType extends ActiveModel implements HasCode<String>, HasType<String> {

	/** The serial version UID */
	private static final long serialVersionUID = -5716203549187752942L;

	/** Key to description property */
	public final static String DESCRIPTION = "description";

	/** Key to name property */
	public static final String NAME = "name";

	/** Key to short description property */
	public final static String SHORT_DESCRIPTION = "shortDescription";

	/** Key to use as default property */
	public static final String USE_AS_DEFAULT = "useAsDefault";

	private String code;
	private XmlizableLocalizableString name;
	private String type;
	private XmlizableLocalizableString description;
	private XmlizableLocalizableString shortDescription;
	private Boolean useAsDefault;

	public MealType() {}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	public XmlizableLocalizableString getName() {
		return name;
	}

	public void setName(XmlizableLocalizableString name) {
		this.name = name;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	public XmlizableLocalizableString getDescription() {
		return description;
	}

	public void setDescription(XmlizableLocalizableString description) {
		this.description = description;
	}

	public XmlizableLocalizableString getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(XmlizableLocalizableString shortDescription) {
		this.shortDescription = shortDescription;
	}

	public Boolean getUseAsDefault() {
		return useAsDefault;
	}

	public void setUseAsDefault(Boolean useAsDefault) {
		this.useAsDefault = useAsDefault;
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper()
			.add(CODE, code)
			.add(NAME, name)
			.add(TYPE, type)
			.add(DESCRIPTION, description)
			.add(SHORT_DESCRIPTION, shortDescription)
			.add(USE_AS_DEFAULT, useAsDefault)
			.add(ACTIVE, getActive());
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ code, name, type, description, shortDescription, useAsDefault });
	}
	
}

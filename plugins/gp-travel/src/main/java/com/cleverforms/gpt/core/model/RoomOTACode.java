/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * February 20, 2020.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model;

import java.io.Serializable;

/**
 * A small enumerator for room OTA code of {@code Hotel}.
 * @author Sergey Titarchuk
 */
public enum RoomOTACode implements Serializable {

	SINGLE(1),
	DOUBLE(2),
	TRIPLE(6),
	QUADRUPLE(7),
	FAMILY(8);

	private final int code;
	
	private RoomOTACode(int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}

	@Override
	public String toString() {
		return String.valueOf(code);
	} 

	public static synchronized RoomOTACode parse(Integer typeId) {
		try {
			return RoomOTACode.values()[typeId];
		} catch (Exception e) {}
		return null;
	}

}

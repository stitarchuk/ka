/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.payload;

import com.cleverforms.comms.shared.proxy.ValueProxy;
import com.cleverforms.gpt.core.model.hotelinfo.load.HotelDescriptionInfo;
/**
 * A base payload for load {@code Hotel} info data
 * @author Sergey Titarchuk
 */
public class HotelInfo implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = -1076307650140048040L;

	/** The URI template of method */
	public static final String URI_METHOD = "/hotelInfo";

	/** The URI template of method */
	public static final String URI_BY_ID = URI_METHOD + "?hotelId={0}";

	/** The URI template of method */
	public static final String URI_BY_CODE = URI_METHOD + "?hotelCode={0}";

	private HotelDescriptionInfo hotelDescriptionInfo;

	public HotelInfo() {}

	public HotelDescriptionInfo getHotelDescriptionInfo() {
		return hotelDescriptionInfo;
	}

	public void setHotelDescriptionInfo(HotelDescriptionInfo hotelDescriptionInfo) {
		this.hotelDescriptionInfo = hotelDescriptionInfo;
	}

	@Override
	public String toString() {
		return String.valueOf(hotelDescriptionInfo);
	}

}

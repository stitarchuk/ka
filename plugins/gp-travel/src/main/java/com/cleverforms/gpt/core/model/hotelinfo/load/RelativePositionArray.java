/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.shared.proxy.ValueProxy;

/**
 * A wrapper class for wrap {@link RelativePosition} as Array.
 * @author Sergey Titarchuk
 */
public class RelativePositionArray implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = 8762810095267905444L;

	private RelativePosition[] relativePosition;

	public RelativePositionArray() {}

	public RelativePosition[] getRelativePosition() {
		return relativePosition;
	}

	public void setRelativePosition(RelativePosition[] relativePosition) {
		this.relativePosition = relativePosition;
	}

	@Override
	public String toString() {
		return Casting.arrayToString(relativePosition);
	}

}

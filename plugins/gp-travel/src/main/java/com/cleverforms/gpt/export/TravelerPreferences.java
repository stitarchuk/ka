//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.07 at 06:24:49 PM EEST 
//


package com.cleverforms.gpt.export;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TravelerPreferences complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TravelerPreferences"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="mealTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mealTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="seatAllocationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="seatAllocationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TravelerPreferences", propOrder = {
    "mealTypeCode",
    "mealTypeName",
    "seatAllocationCode",
    "seatAllocationName"
})
public class TravelerPreferences {

    protected String mealTypeCode;
    protected String mealTypeName;
    protected String seatAllocationCode;
    protected String seatAllocationName;

    /**
     * Gets the value of the mealTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMealTypeCode() {
        return mealTypeCode;
    }

    /**
     * Sets the value of the mealTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMealTypeCode(String value) {
        this.mealTypeCode = value;
    }

    /**
     * Gets the value of the mealTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMealTypeName() {
        return mealTypeName;
    }

    /**
     * Sets the value of the mealTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMealTypeName(String value) {
        this.mealTypeName = value;
    }

    /**
     * Gets the value of the seatAllocationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatAllocationCode() {
        return seatAllocationCode;
    }

    /**
     * Sets the value of the seatAllocationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatAllocationCode(String value) {
        this.seatAllocationCode = value;
    }

    /**
     * Gets the value of the seatAllocationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeatAllocationName() {
        return seatAllocationName;
    }

    /**
     * Sets the value of the seatAllocationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeatAllocationName(String value) {
        this.seatAllocationName = value;
    }

}

/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 21, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.payload;

import java.util.TimeZone;

import com.cleverforms.comms.shared.proxy.ValueProxy;
import com.cleverforms.comms.shared.util.Format;
/**
 * A payload for {@code /authorization} method
 * @author Sergey Titarchuk
 */
public class Authenticate implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = -52983614122758689L;

	/** The URI template of method */
	public static final String URI_METHOD = "/authorization";

	/** The URI template of method */
	public static final String URI = URI_METHOD + "?apiKey={0}&companyCodeOrAlias={1}&login={2}&password={3}";

	/** Key to avatar property */
	public static final String AVATAR = "avatar";

	/** Key to available products property */
	public static final String AVAILABLE_PRODUCTS = "availableProducts";

	/** Key to company name property */
	public static final String COMPANY_NAME = "companyName";

	/** Key to company type property */
	public static final String COMPANY_TYPE = "companyType";

	/** Key to contract currency property */
	public static final String CONTRACT_CURRENCY = "contractCurrency";

	/** Key to server TimeZone property */
	public static final String SERVER_TIMEZONE = "serverTimeZone";

	/** Key to system currency property */
	public static final String SYSTEM_CURRENCY = "systemCurrency";

	/** Key to token property */
	public static final String TOKEN = "token";

	/** Key to user ID property */
	public static final String USER_ID = "userId";

	/** Key to user name property */
	public static final String USER_NAME = "userName";

	/** Key to user TimeZone property */
	public static final String USER_TIMEZONE = "userTimeZone";

	private long userId;
	private String userName;
	private String companyName;
	private String companyType;
	private String token;
	private String contractCurrency;
	private String systemCurrency;
	private TimeZone serverTimeZone;
	private TimeZone userTimeZone;
	private String avatar;
	private String[] availableProducts;
	
	public Authenticate() {}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getContractCurrency() {
		return contractCurrency;
	}

	public void setContractCurrency(String contractCurrency) {
		this.contractCurrency = contractCurrency;
	}

	public String getSystemCurrency() {
		return systemCurrency;
	}

	public void setSystemCurrency(String systemCurrency) {
		this.systemCurrency = systemCurrency;
	}

	public TimeZone getServerTimeZone() {
		return serverTimeZone;
	}

	public void setServerTimeZone(TimeZone serverTimeZone) {
		this.serverTimeZone = serverTimeZone;
	}

	public TimeZone getUserTimeZone() {
		return userTimeZone;
	}

	public void setUserTimeZone(TimeZone userTimeZone) {
		this.userTimeZone = userTimeZone;
	}
	
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String[] getAvailableProducts() {
		return availableProducts;
	}

	public void setAvailableProducts(String[] availableProducts) {
		this.availableProducts = availableProducts;
	}

	@Override
	public String toString() {
		return Format.toStringHelper(this)
			.add(TOKEN, token)
			.add(USER_ID, userId)
			.add(USER_NAME, userName)
			.add(COMPANY_NAME, companyName)
			.add(COMPANY_TYPE, companyType)
			.add(CONTRACT_CURRENCY, contractCurrency)
			.add(SYSTEM_CURRENCY, systemCurrency)
			.add(SERVER_TIMEZONE, serverTimeZone)
			.add(USER_TIMEZONE, userTimeZone)
			.add(AVATAR, avatar)
			.add(AVAILABLE_PRODUCTS, availableProducts)
			.toString();
	}

}

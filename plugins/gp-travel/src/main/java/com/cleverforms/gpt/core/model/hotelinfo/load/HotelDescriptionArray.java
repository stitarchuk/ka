/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.shared.proxy.ValueProxy;

/**
 * A wrapper class for wrap {@link HotelDescription} as Array.
 * @author Sergey Titarchuk
 */
public class HotelDescriptionArray implements ValueProxy {

	/** The serial version UID*/
	private static final long serialVersionUID = -4265297189220493125L;

	private HotelDescription[] description;
	
	public HotelDescriptionArray() {}

	public HotelDescription[] getDescription() {
		return description;
	}

	public void setDescription(HotelDescription[] description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return Casting.arrayToString(description);
	}

}

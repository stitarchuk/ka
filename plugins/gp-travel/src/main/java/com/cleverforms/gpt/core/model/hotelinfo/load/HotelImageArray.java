/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.shared.proxy.ValueProxy;

/**
 * A wrapper class for wrap {@link HotelImage} as Array.
 * @author Sergey Titarchuk
 */
public class HotelImageArray implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = -2643152998475773410L;

	private HotelImage[] image;

	public HotelImageArray() {}

	public HotelImage[] getImage() {
		return image;
	}

	public void setImage(HotelImage[] image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return Casting.arrayToString(image);
	}

}

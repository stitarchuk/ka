/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.gpt.core.model.ActiveEntityModel;
import com.cleverforms.gpt.core.model.XmlizableLocalizableString;

/**
 * A base {@link ActiveEntityModel} for {@code Amenity} data.
 * @author Sergey Titarchuk
 */
public class Amenity extends ActiveEntityModel {

	/** The serial version UID */
	private static final long serialVersionUID = -1592373769908690975L;

	/** Key to description property */
	public final static String DESCRIPTION = "description";

	/** Key to images property */
	public final static String IMAGES = "images";

	/** Key to name property */
	public final static String NAME = "name";

	/** Key to short description property */
	public final static String SHORT_DESCRIPTION = "shortDescription";

	private XmlizableLocalizableString name;
	private XmlizableLocalizableString description;
	private XmlizableLocalizableString shortDescription;
	private HotelImage[] images;

	public Amenity() {}

	public XmlizableLocalizableString getName() {
		return name;
	}

	public void setName(XmlizableLocalizableString name) {
		this.name = name;
	}

	public XmlizableLocalizableString getDescription() {
		return description;
	}

	public void setDescription(XmlizableLocalizableString description) {
		this.description = description;
	}

	public XmlizableLocalizableString getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(XmlizableLocalizableString shortDescription) {
		this.shortDescription = shortDescription;
	}

	public HotelImage[] getImages() {
		return images;
	}

	public void setImages(HotelImage[] images) {
		this.images = images;
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper()
			.add(NAME, name)
			.add(DESCRIPTION, description)
			.add(SHORT_DESCRIPTION, shortDescription)
			.add(IMAGES, images);
	}

}

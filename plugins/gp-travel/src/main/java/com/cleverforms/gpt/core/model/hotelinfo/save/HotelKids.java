/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.save;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.server.util.Util;
import com.cleverforms.gpt.core.model.NamedDescriptionModel;

/**
 * A base {@link NamedDescriptionModel} for hotel {@code Kids} info data.
 * @author Sergey Titarchuk
 */
public class HotelKids extends NamedDescriptionModel {

	/** The serail version UID */
	private static final long serialVersionUID = 7637334752720494179L;

	public HotelKids() {}

	public HotelKids(Long id, boolean active) {
		super(id, active);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof HotelKids) {
			return super.equals(obj) && Util.equalsWithNull(getId(), ((HotelKids) obj).getId());
		}
		return false;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ getId() });
	}

}

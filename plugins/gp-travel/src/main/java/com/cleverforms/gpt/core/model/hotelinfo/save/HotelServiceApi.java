/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.save;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.server.util.Util;
import com.cleverforms.gpt.core.model.ActiveEntityModel;
import com.cleverforms.gpt.core.model.hotelinfo.load.HotelService;

/**
 * A base {@link ActiveEntityModel} for {@link HotelService} API info data
 * @author Sergey Titarchuk
 */
public class HotelServiceApi extends ActiveEntityModel {

	/** The serial version UID */
	private static final long serialVersionUID = -8707106784257141807L;

	public HotelServiceApi() {}

	public HotelServiceApi(Long id, boolean active) {
		super(id, active);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof HotelServiceApi) {
			return super.equals(obj) && Util.equalsWithNull(getId(), ((HotelServiceApi) obj).getId());
		}
		return false;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ getId() });
	}

}

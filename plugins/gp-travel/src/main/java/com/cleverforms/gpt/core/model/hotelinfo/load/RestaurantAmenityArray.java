/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.shared.proxy.ValueProxy;

/**
 * A wrapper class for wrap {@link RestaurantAmenity} as Array.
 * @author Sergey Titarchuk
 */
public class RestaurantAmenityArray implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = -4212795281615614337L;

	private RestaurantAmenity[] restaurantAmenity;

	public RestaurantAmenityArray() {}

	public RestaurantAmenity[] getRestaurantAmenity() {
		return restaurantAmenity;
	}

	public void setRestaurantAmenity(RestaurantAmenity[] restaurantAmenity) {
		this.restaurantAmenity = restaurantAmenity;
	}

	@Override
	public String toString() {
		return Casting.arrayToString(restaurantAmenity);
	}

}

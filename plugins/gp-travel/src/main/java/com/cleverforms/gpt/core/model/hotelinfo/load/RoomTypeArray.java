/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.shared.proxy.ValueProxy;

/**
 * A wrapper class for wrap {@link RoomType} as Array.
 * @author Sergey Titarchuk
 */
public class RoomTypeArray implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = -5929405614982681048L;

	private RoomType[] roomType;

	public RoomTypeArray() {}

	public RoomType[] getRoomType() {
		return roomType;
	}

	public void setRoomType(RoomType[] roomType) {
		this.roomType = roomType;
	}

	@Override
	public String toString() {
		return Casting.arrayToString(roomType);
	}

}

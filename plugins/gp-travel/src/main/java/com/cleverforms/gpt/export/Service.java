//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.0 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.04.07 at 06:24:49 PM EEST 
//


package com.cleverforms.gpt.export;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for service complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="service"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="serviceId" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="serviceType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="created" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="modified" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="serviceStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="paymentMethodId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="supplierId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="supplierCompanyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="supplierCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="parentTO1Id" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="refNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="startDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="endDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="serviceGroupId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="serviceContainerId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="customFields" type="{http://www.software.travel/gptours/export}CustomFieldInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="travelers" type="{http://www.software.travel/gptours/export}traveler" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="salesTerms" type="{http://www.software.travel/gptours/export}SalesTermsType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="priceBeforeCancellation" type="{http://www.software.travel/gptours/export}price" minOccurs="0"/&gt;
 *         &lt;element name="loyaltyPoints" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="conversionRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="modificationAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="cancellationAllowed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="violation" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="violationPolicyId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="violationPolicyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="violationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="violationDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="bookingAdditionalQuestions" type="{http://www.software.travel/gptours/export}BookingAdditionalQuestion" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="serviceDetails" type="{http://www.software.travel/gptours/export}AbstractServiceDetails" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="extraServices" type="{http://www.software.travel/gptours/export}extraService" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="channelManagerBookingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="taxesAndFeesIncluded" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "service", propOrder = {
    "serviceId",
    "serviceType",
    "created",
    "modified",
    "status",
    "serviceStatus",
    "paymentMethodId",
    "supplierId",
    "supplierCompanyName",
    "supplierCode",
    "parentTO1Id",
    "refNum",
    "startDateTime",
    "endDateTime",
    "serviceGroupId",
    "serviceContainerId",
    "customFields",
    "travelers",
    "salesTerms",
    "priceBeforeCancellation",
    "loyaltyPoints",
    "conversionRate",
    "modificationAllowed",
    "cancellationAllowed",
    "violation",
    "violationPolicyId",
    "violationPolicyName",
    "violationCode",
    "violationDescription",
    "comments",
    "bookingAdditionalQuestions",
    "serviceDetails",
    "extraServices",
    "channelManagerBookingNumber",
    "taxesAndFeesIncluded"
})
public class Service {

    protected long serviceId;
    @XmlElement(required = true)
    protected String serviceType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar created;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar modified;
    protected String status;
    protected String serviceStatus;
    protected Long paymentMethodId;
    protected Long supplierId;
    protected String supplierCompanyName;
    protected String supplierCode;
    protected long parentTO1Id;
    protected String refNum;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDateTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endDateTime;
    protected Long serviceGroupId;
    protected Long serviceContainerId;
    protected List<CustomFieldInfo> customFields;
    @XmlElement(nillable = true)
    protected List<Traveler> travelers;
    protected List<SalesTermsType> salesTerms;
    protected Price priceBeforeCancellation;
    protected BigDecimal loyaltyPoints;
    protected String conversionRate;
    protected Boolean modificationAllowed;
    protected Boolean cancellationAllowed;
    protected Boolean violation;
    protected Long violationPolicyId;
    protected String violationPolicyName;
    protected String violationCode;
    protected String violationDescription;
    protected String comments;
    protected List<BookingAdditionalQuestion> bookingAdditionalQuestions;
    @XmlElement(nillable = true)
    protected List<AbstractServiceDetails> serviceDetails;
    @XmlElement(nillable = true)
    protected List<ExtraService> extraServices;
    protected String channelManagerBookingNumber;
    protected Boolean taxesAndFeesIncluded;

    /**
     * Gets the value of the serviceId property.
     * 
     */
    public long getServiceId() {
        return serviceId;
    }

    /**
     * Sets the value of the serviceId property.
     * 
     */
    public void setServiceId(long value) {
        this.serviceId = value;
    }

    /**
     * Gets the value of the serviceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * Sets the value of the serviceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceType(String value) {
        this.serviceType = value;
    }

    /**
     * Gets the value of the created property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreated() {
        return created;
    }

    /**
     * Sets the value of the created property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreated(XMLGregorianCalendar value) {
        this.created = value;
    }

    /**
     * Gets the value of the modified property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getModified() {
        return modified;
    }

    /**
     * Sets the value of the modified property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setModified(XMLGregorianCalendar value) {
        this.modified = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the serviceStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceStatus() {
        return serviceStatus;
    }

    /**
     * Sets the value of the serviceStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceStatus(String value) {
        this.serviceStatus = value;
    }

    /**
     * Gets the value of the paymentMethodId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    /**
     * Sets the value of the paymentMethodId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPaymentMethodId(Long value) {
        this.paymentMethodId = value;
    }

    /**
     * Gets the value of the supplierId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSupplierId() {
        return supplierId;
    }

    /**
     * Sets the value of the supplierId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSupplierId(Long value) {
        this.supplierId = value;
    }

    /**
     * Gets the value of the supplierCompanyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierCompanyName() {
        return supplierCompanyName;
    }

    /**
     * Sets the value of the supplierCompanyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierCompanyName(String value) {
        this.supplierCompanyName = value;
    }

    /**
     * Gets the value of the supplierCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSupplierCode() {
        return supplierCode;
    }

    /**
     * Sets the value of the supplierCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSupplierCode(String value) {
        this.supplierCode = value;
    }

    /**
     * Gets the value of the parentTO1Id property.
     * 
     */
    public long getParentTO1Id() {
        return parentTO1Id;
    }

    /**
     * Sets the value of the parentTO1Id property.
     * 
     */
    public void setParentTO1Id(long value) {
        this.parentTO1Id = value;
    }

    /**
     * Gets the value of the refNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefNum() {
        return refNum;
    }

    /**
     * Sets the value of the refNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefNum(String value) {
        this.refNum = value;
    }

    /**
     * Gets the value of the startDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDateTime() {
        return startDateTime;
    }

    /**
     * Sets the value of the startDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDateTime(XMLGregorianCalendar value) {
        this.startDateTime = value;
    }

    /**
     * Gets the value of the endDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDateTime() {
        return endDateTime;
    }

    /**
     * Sets the value of the endDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDateTime(XMLGregorianCalendar value) {
        this.endDateTime = value;
    }

    /**
     * Gets the value of the serviceGroupId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getServiceGroupId() {
        return serviceGroupId;
    }

    /**
     * Sets the value of the serviceGroupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setServiceGroupId(Long value) {
        this.serviceGroupId = value;
    }

    /**
     * Gets the value of the serviceContainerId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getServiceContainerId() {
        return serviceContainerId;
    }

    /**
     * Sets the value of the serviceContainerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setServiceContainerId(Long value) {
        this.serviceContainerId = value;
    }

    /**
     * Gets the value of the customFields property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customFields property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomFields().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomFieldInfo }
     * 
     * 
     */
    public List<CustomFieldInfo> getCustomFields() {
        if (customFields == null) {
            customFields = new ArrayList<CustomFieldInfo>();
        }
        return this.customFields;
    }

    /**
     * Gets the value of the travelers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the travelers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTravelers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Traveler }
     * 
     * 
     */
    public List<Traveler> getTravelers() {
        if (travelers == null) {
            travelers = new ArrayList<Traveler>();
        }
        return this.travelers;
    }

    /**
     * Gets the value of the salesTerms property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the salesTerms property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSalesTerms().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SalesTermsType }
     * 
     * 
     */
    public List<SalesTermsType> getSalesTerms() {
        if (salesTerms == null) {
            salesTerms = new ArrayList<SalesTermsType>();
        }
        return this.salesTerms;
    }

    /**
     * Gets the value of the priceBeforeCancellation property.
     * 
     * @return
     *     possible object is
     *     {@link Price }
     *     
     */
    public Price getPriceBeforeCancellation() {
        return priceBeforeCancellation;
    }

    /**
     * Sets the value of the priceBeforeCancellation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Price }
     *     
     */
    public void setPriceBeforeCancellation(Price value) {
        this.priceBeforeCancellation = value;
    }

    /**
     * Gets the value of the loyaltyPoints property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLoyaltyPoints() {
        return loyaltyPoints;
    }

    /**
     * Sets the value of the loyaltyPoints property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLoyaltyPoints(BigDecimal value) {
        this.loyaltyPoints = value;
    }

    /**
     * Gets the value of the conversionRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConversionRate() {
        return conversionRate;
    }

    /**
     * Sets the value of the conversionRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConversionRate(String value) {
        this.conversionRate = value;
    }

    /**
     * Gets the value of the modificationAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isModificationAllowed() {
        return modificationAllowed;
    }

    /**
     * Sets the value of the modificationAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setModificationAllowed(Boolean value) {
        this.modificationAllowed = value;
    }

    /**
     * Gets the value of the cancellationAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCancellationAllowed() {
        return cancellationAllowed;
    }

    /**
     * Sets the value of the cancellationAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCancellationAllowed(Boolean value) {
        this.cancellationAllowed = value;
    }

    /**
     * Gets the value of the violation property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isViolation() {
        return violation;
    }

    /**
     * Sets the value of the violation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setViolation(Boolean value) {
        this.violation = value;
    }

    /**
     * Gets the value of the violationPolicyId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getViolationPolicyId() {
        return violationPolicyId;
    }

    /**
     * Sets the value of the violationPolicyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setViolationPolicyId(Long value) {
        this.violationPolicyId = value;
    }

    /**
     * Gets the value of the violationPolicyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getViolationPolicyName() {
        return violationPolicyName;
    }

    /**
     * Sets the value of the violationPolicyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setViolationPolicyName(String value) {
        this.violationPolicyName = value;
    }

    /**
     * Gets the value of the violationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getViolationCode() {
        return violationCode;
    }

    /**
     * Sets the value of the violationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setViolationCode(String value) {
        this.violationCode = value;
    }

    /**
     * Gets the value of the violationDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getViolationDescription() {
        return violationDescription;
    }

    /**
     * Sets the value of the violationDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setViolationDescription(String value) {
        this.violationDescription = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the bookingAdditionalQuestions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bookingAdditionalQuestions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookingAdditionalQuestions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BookingAdditionalQuestion }
     * 
     * 
     */
    public List<BookingAdditionalQuestion> getBookingAdditionalQuestions() {
        if (bookingAdditionalQuestions == null) {
            bookingAdditionalQuestions = new ArrayList<BookingAdditionalQuestion>();
        }
        return this.bookingAdditionalQuestions;
    }

    /**
     * Gets the value of the serviceDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AbstractServiceDetails }
     * 
     * 
     */
    public List<AbstractServiceDetails> getServiceDetails() {
        if (serviceDetails == null) {
            serviceDetails = new ArrayList<AbstractServiceDetails>();
        }
        return this.serviceDetails;
    }

    /**
     * Gets the value of the extraServices property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extraServices property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtraServices().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExtraService }
     * 
     * 
     */
    public List<ExtraService> getExtraServices() {
        if (extraServices == null) {
            extraServices = new ArrayList<ExtraService>();
        }
        return this.extraServices;
    }

    /**
     * Gets the value of the channelManagerBookingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelManagerBookingNumber() {
        return channelManagerBookingNumber;
    }

    /**
     * Sets the value of the channelManagerBookingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelManagerBookingNumber(String value) {
        this.channelManagerBookingNumber = value;
    }

    /**
     * Gets the value of the taxesAndFeesIncluded property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTaxesAndFeesIncluded() {
        return taxesAndFeesIncluded;
    }

    /**
     * Sets the value of the taxesAndFeesIncluded property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTaxesAndFeesIncluded(Boolean value) {
        this.taxesAndFeesIncluded = value;
    }

}

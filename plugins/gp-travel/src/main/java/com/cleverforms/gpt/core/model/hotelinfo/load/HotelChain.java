/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.shared.proxy.ValueProxy;
import com.cleverforms.comms.shared.util.Format;

/**
 * A base {@code ValueProxy} for {@code Hotel} chain data.
 * @author Sergey Titarchuk
 */
public class HotelChain implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = 5786862863060388402L;

	/** Key to hotel chain property */
	public static final String HOTEL_CHAIN = "hotelChain";

	/** Key to hotel chain code property */
	public static final String HOTEL_CHAIN_CODE = "hotelChainCode";

	/** Key to supplier hotel chain property */
	public static final String SUPPLIER_HOTEL_CHAIN = "supplierHotelChain";

	private String hotelChain;
	private String hotelChainCode;
	private String supplierHotelChain;

	public HotelChain() {}

	public String getHotelChain() {
		return hotelChain;
	}

	public void setHotelChain(String hotelChain) {
		this.hotelChain = hotelChain;
	}

	public String getHotelChainCode() {
		return hotelChainCode;
	}

	public void setHotelChainCode(String hotelChainCode) {
		this.hotelChainCode = hotelChainCode;
	}

	public String getSupplierHotelChain() {
		return supplierHotelChain;
	}

	public void setSupplierHotelChain(String supplierHotelChain) {
		this.supplierHotelChain = supplierHotelChain;
	}

	@Override
	public String toString() {
		return Format.toStringHelper(this)
			.add(HOTEL_CHAIN, hotelChain)
			.add(HOTEL_CHAIN_CODE, hotelChainCode)
			.add(SUPPLIER_HOTEL_CHAIN, supplierHotelChain)
			.toString();
	}

}

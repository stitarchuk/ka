/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.shared.proxy.ValueProxy;
import com.cleverforms.comms.shared.util.Format;

/**
 * A base {@code ValueProxy} for {@code VillaDescription} info data.
 * @author Sergey Titarchuk
 */
public class VillaDescriptionInfo implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = 3699769149861388499L;

	/** Key to build year property */
	public final static String BUILD_YEAR = "buildYear";

	/** Key to building material property */
	public final static String BUILDING_MATERIAL = "buildingMaterial";

	/** Key to house area property */
	public final static String HOUSE_AREA = "houseArea";

	/** Key to kids free property */
	public final static String KIDS_FREE = "kidsFree";

	/** Key to modernized year property */
	public final static String MODERNIZED_YEAR = "modernizedYear";

	/** Key to number of people property */
	public final static String NUMBER_OF_PEOPLE = "numberOfPeople";

	/** Key to pet count property */
	public final static String PET_COUNT = "petCount";

	/** Key to pet size limit property */
	public final static String PET_SIZE_LIMIT = "petSizeLimit";

	/** Key to quantity bars property */
	public final static String QUANTITY_BARS = "quantityBars";

	/** Key to quantity bath rooms property */
	public final static String QUANTITY_BATH_ROOMS = "quantityBathRooms";

	/** Key to quantity double rooms property */
	public final static String QUANTITY_DOUBLE_ROOMS = "quantityDoubleRooms";

	/** Key to quantity floors property */
	public final static String QUANTITY_FLOORS = "quantityFloors";

	/** Key to quantity indoor pools property */
	public final static String QUANTITY_INDOOR_POOLS = "quantityIndoorPools";

	/** Key to quantity outdoor pools property */
	public final static String QUANTITY_OUTDOOR_POOLS = "quantityOutdoorPools";

	/** Key to quantity restaurants property */
	public final static String QUANTITY_RESTAURANTS = "quantityRestaurants";

	/** Key to quantity rooms property */
	public final static String QUANTITY_ROOMS = "quantityRooms";

	/** Key to quantity single rooms property */
	public final static String QUANTITY_SINGLE_ROOMS = "quantitySingleRooms";

	/** Key to quantity triple or more rooms property */
	public final static String QUANTITY_TRIPLE_OR_MORE_ROOMS = "quantityTripleOrMoreRooms";

	/** Key to territory area property */
	public final static String TERRITORY_AREA = "territoryArea";

	private int buildYear;
	private int modernizedYear;
	private int territoryArea;
	private int houseArea;
	private int numberOfPeople;
	private int kidsFree;
	private int petCount;
	private int quantityRooms;
	private int quantitySingleRooms;
	private int quantityDoubleRooms;
	private int quantityTripleOrMoreRooms;
	private int quantityBathRooms;
	private int quantityFloors;
	private int quantityBars;
	private int quantityRestaurants;
	private int quantityIndoorPools;
	private int quantityOutdoorPools;
	private int petSizeLimit;
	private String buildingMaterial;

	public VillaDescriptionInfo() {}

	public int getBuildYear() {
		return buildYear;
	}

	public void setBuildYear(int buildYear) {
		this.buildYear = buildYear;
	}

	public int getModernizedYear() {
		return modernizedYear;
	}

	public void setModernizedYear(int modernizedYear) {
		this.modernizedYear = modernizedYear;
	}

	public int getTerritoryArea() {
		return territoryArea;
	}

	public void setTerritoryArea(int territoryArea) {
		this.territoryArea = territoryArea;
	}

	public int getHouseArea() {
		return houseArea;
	}

	public void setHouseArea(int houseArea) {
		this.houseArea = houseArea;
	}

	public int getNumberOfPeople() {
		return numberOfPeople;
	}

	public void setNumberOfPeople(int numberOfPeople) {
		this.numberOfPeople = numberOfPeople;
	}

	public int getKidsFree() {
		return kidsFree;
	}

	public void setKidsFree(int kidsFree) {
		this.kidsFree = kidsFree;
	}

	public int getPetCount() {
		return petCount;
	}

	public void setPetCount(int petCount) {
		this.petCount = petCount;
	}

	public int getQuantityRooms() {
		return quantityRooms;
	}

	public void setQuantityRooms(int quantityRooms) {
		this.quantityRooms = quantityRooms;
	}

	public int getQuantitySingleRooms() {
		return quantitySingleRooms;
	}

	public void setQuantitySingleRooms(int quantitySingleRooms) {
		this.quantitySingleRooms = quantitySingleRooms;
	}

	public int getQuantityDoubleRooms() {
		return quantityDoubleRooms;
	}

	public void setQuantityDoubleRooms(int quantityDoubleRooms) {
		this.quantityDoubleRooms = quantityDoubleRooms;
	}

	public int getQuantityTripleOrMoreRooms() {
		return quantityTripleOrMoreRooms;
	}

	public void setQuantityTripleOrMoreRooms(int quantityTripleOrMoreRooms) {
		this.quantityTripleOrMoreRooms = quantityTripleOrMoreRooms;
	}

	public int getQuantityBathRooms() {
		return quantityBathRooms;
	}

	public void setQuantityBathRooms(int quantityBathRooms) {
		this.quantityBathRooms = quantityBathRooms;
	}

	public int getQuantityFloors() {
		return quantityFloors;
	}

	public void setQuantityFloors(int quantityFloors) {
		this.quantityFloors = quantityFloors;
	}

	public int getQuantityBars() {
		return quantityBars;
	}

	public void setQuantityBars(int quantityBars) {
		this.quantityBars = quantityBars;
	}

	public int getQuantityRestaurants() {
		return quantityRestaurants;
	}

	public void setQuantityRestaurants(int quantityRestaurants) {
		this.quantityRestaurants = quantityRestaurants;
	}

	public int getQuantityIndoorPools() {
		return quantityIndoorPools;
	}

	public void setQuantityIndoorPools(int quantityIndoorPools) {
		this.quantityIndoorPools = quantityIndoorPools;
	}

	public int getQuantityOutdoorPools() {
		return quantityOutdoorPools;
	}

	public void setQuantityOutdoorPools(int quantityOutdoorPools) {
		this.quantityOutdoorPools = quantityOutdoorPools;
	}

	public int getPetSizeLimit() {
		return petSizeLimit;
	}

	public void setPetSizeLimit(int petSizeLimit) {
		this.petSizeLimit = petSizeLimit;
	}

	public String getBuildingMaterial() {
		return buildingMaterial;
	}

	public void setBuildingMaterial(String buildingMaterial) {
		this.buildingMaterial = buildingMaterial;
	}

	@Override
	public String toString() {
		return Format.toStringHelper(this)
			.add(BUILD_YEAR, buildYear)
			.add(MODERNIZED_YEAR, modernizedYear)
			.add(TERRITORY_AREA, territoryArea)
			.add(NUMBER_OF_PEOPLE, numberOfPeople)
			.add(KIDS_FREE, kidsFree)
			.add(PET_COUNT, petCount)
			.add(QUANTITY_ROOMS, quantityRooms)
			.add(QUANTITY_SINGLE_ROOMS, quantitySingleRooms)
			.add(QUANTITY_DOUBLE_ROOMS, quantityDoubleRooms)
			.add(QUANTITY_TRIPLE_OR_MORE_ROOMS, quantityTripleOrMoreRooms)
			.add(QUANTITY_BATH_ROOMS, quantityBathRooms)
			.add(QUANTITY_FLOORS, quantityFloors)
			.add(QUANTITY_BARS, quantityBars)
			.add(QUANTITY_RESTAURANTS, quantityRestaurants)
			.add(QUANTITY_INDOOR_POOLS, quantityIndoorPools)
			.add(QUANTITY_OUTDOOR_POOLS, quantityOutdoorPools)
			.add(PET_SIZE_LIMIT, petSizeLimit)
			.add(BUILDING_MATERIAL, buildingMaterial)
			.toString();
	}

}

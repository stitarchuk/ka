/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.shared.marker.HasId;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;

/**
 * A base {@code ValueProxy} for {@code RoomService} info data
 * @author Sergey Titarchuk
 */
public class RoomService extends Service implements HasId<Long> {

	/** The serial version UID */
	private static final long serialVersionUID = -1172716284909349704L;

	/** Key to room ID property */
	public final static String ROOM_ID = "roomId";

	private long id;
	private long roomId;

	public RoomService() {}

	@Override
	public Long getId() {
		return Long.valueOf(id);
	}

	@Override
	public void setId(Long id) {
		this.id = id != null ? id.longValue() : 0;
	}

	@Override
	public String key() {
		return String.valueOf(id);
	}

	public long getRoomId() {
		return roomId;
	}

	public void setRoomId(long roomId) {
		this.roomId = roomId;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ Long.valueOf(roomId) });
	}
	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper()
			.add(ID, id)
			.add(ROOM_ID, roomId);
	}

}

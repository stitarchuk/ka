/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;

/**
 * A base {@code ValueProxy} for {@code HotelService} info data
 * @author Sergey Titarchuk
 */
public class HotelService extends Service {

	/** The serial version UID */
	private static final long serialVersionUID = 2699239367227888038L;

	/** Key to end property */
	public final static String END = "end";

	/** Key to hotel service group ID property */
	public final static String HOTEL_SERVICE_GROUP_ID = "hotelServiceGroupId";

	/** Key to hotel service group name property */
	public final static String HOTEL_SERVICE_GROUP_NAME = "hotelServiceGroupName";

	/** Key to max age property */
	public final static String MAX_AGE = "maxAge";

	/** Key to min age property */
	public final static String MIN_AGE = "minAge";

	/** Key to start property */
	public final static String START = "start";

	private String end;
	private String start;
	private int maxAge;
	private int minAge;
	private String hotelServiceGroupId;
	private String hotelServiceGroupName;

	public HotelService() {}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public String getHotelServiceGroupId() {
		return hotelServiceGroupId;
	}

	public void setHotelServiceGroupId(String hotelServiceGroupId) {
		this.hotelServiceGroupId = hotelServiceGroupId;
	}

	public String getHotelServiceGroupName() {
		return hotelServiceGroupName;
	}

	public void setHotelServiceGroupName(String hotelServiceGroupName) {
		this.hotelServiceGroupName = hotelServiceGroupName;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ start, end,
			maxAge, minAge, hotelServiceGroupId, hotelServiceGroupName });
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper()
			.add(END, end)
			.add(START, start)
			.add(MAX_AGE, maxAge)
			.add(MIN_AGE, minAge)
			.add(HOTEL_SERVICE_GROUP_ID, hotelServiceGroupId)
			.add(HOTEL_SERVICE_GROUP_NAME, hotelServiceGroupName);
	}

}

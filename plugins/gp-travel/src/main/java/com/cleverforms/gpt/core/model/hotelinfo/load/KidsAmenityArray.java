/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.shared.proxy.ValueProxy;

/**
 * A wrapper class for wrap {@code KidsAmenity} as Array.
 * @author Sergey Titarchuk
 */
public class KidsAmenityArray implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = -1820110082461182327L;

	private Amenity[] kidsAmenity;

	public KidsAmenityArray() {}

	public Amenity[] getKidsAmenity() {
		return kidsAmenity;
	}

	public void setKidsAmenity(Amenity[] kidsAmenity) {
		this.kidsAmenity = kidsAmenity;
	}

	@Override
	public String toString() {
		return Casting.arrayToString(kidsAmenity);
	}

}

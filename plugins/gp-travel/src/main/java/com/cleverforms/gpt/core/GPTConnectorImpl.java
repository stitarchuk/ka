/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 21, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.lang.NonNull;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.cleverforms.comms.server.RESTConnector;
import com.cleverforms.comms.server.RESTConnectorImpl;
import com.cleverforms.comms.server.event.EntityEvent;
import com.cleverforms.comms.server.model.Entity;
import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.server.util.io.IOUtil;
import com.cleverforms.gpt.core.model.hotelinfo.save.HotelDescriptionInfoRequest;
import com.cleverforms.gpt.core.model.hotelinfo.save.HotelDescriptionInfoResponse;
import com.cleverforms.gpt.core.model.hotelinfo.save.HotelRestaurant;
import com.cleverforms.gpt.core.model.hotelinfo.save.HotelRoom;
import com.cleverforms.gpt.core.model.hotelinfo.save.Image;
import com.cleverforms.gpt.core.payload.Authenticate;
import com.cleverforms.iss.server.model.hotel.CoffeeRoom;
import com.cleverforms.iss.server.model.hotel.Hotel;
import com.cleverforms.iss.server.model.hotel.RoomMap;
import com.cleverforms.iss.server.provider.ISSPersistentProvider;

/**
 * A base {@link RESTConnector} for GP Travel Enterprise API.
 * @author Sergey Titarchuk
 */
public class GPTConnectorImpl extends RESTConnectorImpl implements GPTConnector, InitializingBean {
	
	@Autowired
	private ISSPersistentProvider provider;
	private long systemId;
	private String apiKey;
	private String code;
	private String username;
	private String password;
	private String version;
	private Authenticate authData;
	@Value("${gpt.contextPath:/tss/api}")
	private String contextPath;

	/**
	 * Construct a new instance of the {@link GPTConnectorImpl}, with default parameters.
	 */
	public GPTConnectorImpl() {
		this(new RestTemplate());
	}
	
	/**
	 * Construct a new instance of the {@link GPTConnectorImpl}, with the given {@link RestTemplate}.
	 * @param restTemplate the {@link RestTemplate} to use.
	 */
	public GPTConnectorImpl(RestTemplate restTemplate) {
		super(restTemplate);
	}

	/**
	 * Construct a new instance of the {@link GPTConnectorImpl}, with the given {@link ClientHttpRequestFactory}.
	 * @see RestTemplate#RestTemplate(ClientHttpRequestFactory)
	 */
	public GPTConnectorImpl(ClientHttpRequestFactory requestFactory) {
		super(requestFactory);
	}

	protected HttpHeaders createHeaders(List<Locale> acceptLocales) {
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(IOUtil.APPLICATION_JSON_UTF8);
		if (checkToken()) headers.set("Token", authData.getToken());
		if (acceptLocales.size() > 0) headers.setAcceptLanguageAsLocales(acceptLocales);
		return headers;
	}

	protected <T> HttpEntity<T> requestEntity(T payload, Locale... locales) {
		return new HttpEntity<T>(payload, createHeaders(locales != null ? Arrays.asList(locales) : new ArrayList<>()));
	}

	public <T> T exchange(String uri, HttpMethod method, Locale locale, Serializable requestEntity, Class<T> responseType, Object... uriVariables) {
		try {
			final ResponseEntity<T> response = restTemplate().exchange(apiUrl() + uri, method, requestEntity(requestEntity, locale), responseType, uriVariables);
			trace("exchange response: " + response.getBody());
			return response.getBody();
		} catch (HttpStatusCodeException e) {
			error("exchange body: " + e.getResponseBodyAsString());
		}
		return null;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (!GPTUtil.isInit()) provider.registerBean("gptUtil", GPTUtil.class);
		trace("GP Travel connector started");
	}

	@Override
	public ISSPersistentProvider provider() {
		return provider;
	}

	public long getSystemId() {
		return systemId;
	}

	public void setSystemId(long systemId) {
		this.systemId = systemId;
	}

	@Override
	public String getApiKey() {
		return apiKey;
	}

	@Override
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getVersion() {
		return version;
	}
	
	@Override
	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String contextPath() {
		return contextPath;
	}

	@Override
	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	@Override
	public boolean authenticate() {
		authData = super.authenticate() ? get(Authenticate.URI, Util.LOCALE_UKRAINE, Authenticate.class, apiKey, code, username, password) : null;
		return checkToken();
	}

	public boolean checkToken() {
		return authData != null && Util.isNotBlank(authData.getToken());
	}

	@Override
	public boolean checkConnector() {
		if (systemId <= 0)
			throw new RuntimeException("System ID must be more than 0");
		if (Util.isBlank(apiKey)) 
			throw new RuntimeException("API Key cannot be empty");
		if (Util.isBlank(code)) 
			throw new RuntimeException("Company code or alias cannot be empty");
		if (Util.isBlank(username)) 
			throw new RuntimeException("User login cannot be empty");
		if (Util.isBlank(password)) 
			throw new RuntimeException("User password cannot be empty");
		return super.checkConnector();
	}

	@Override
	public GPTConnector clone() {
		final GPTConnector connector = (GPTConnector) super.clone();
		connector.setApiKey(apiKey);
		connector.setCode(code);
		connector.setUsername(username);
		connector.setPassword(password);
		connector.setVersion(version);
		return connector;
	}

	protected HotelDescriptionInfoResponse updateHotel(HotelDescriptionInfoRequest request, String hotelId, Locale locale) {
		return Util.isBlank(hotelId)
			? create(HotelDescriptionInfoRequest.URI_METHOD_CREATE, locale, request, HotelDescriptionInfoResponse.class)
			: update(HotelDescriptionInfoRequest.URI_METHOD_UPDATE, locale, request, HotelDescriptionInfoResponse.class, hotelId);
	}

	@SuppressWarnings("unchecked")
	public HotelDescriptionInfoRequest prepareRequest(Hotel hotel, Locale locale) {
		if (hotel == null || !authenticate()) return null;
		final HotelDescriptionInfoRequest request = new HotelDescriptionInfoRequest().serialize(this, hotel);
		// rooms
		final List<RoomMap> roomMaps = provider.getResultList("from RoomMap where hotel = ?0", 0, -1, hotel);
		if (roomMaps != null && !roomMaps.isEmpty()) {
			final Set<HotelRoom> roomsSet = new HashSet<HotelRoom>();
			roomMaps.forEach(roomMap -> {
				roomsSet.add(new HotelRoom().serialize(this, roomMap));
			});
			request.setRooms(roomsSet.stream().toArray(HotelRoom[]::new));
		}
		// restaurants
		final List<CoffeeRoom> coffeeRooms = provider.getResultList("from CoffeeRoom where hotel = ?0", 0, -1, hotel);
		if (coffeeRooms != null && !coffeeRooms.isEmpty()) {
			final Set<HotelRestaurant> restaurantsSet = new HashSet<HotelRestaurant>();
			coffeeRooms.forEach(coffeeRoom -> {
				restaurantsSet.add(new HotelRestaurant().serialize(this, coffeeRoom));
			});
			request.setRestaurants(restaurantsSet.stream().toArray(HotelRestaurant[]::new));
		}
		return request;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Hotel syncHotel(Hotel hotel, Locale locale) {
		if (hotel == null || !authenticate()) return null;
		final HotelDescriptionInfoRequest request = new HotelDescriptionInfoRequest().serialize(this, hotel);
		final String hotelId = hotel.getAlias(systemId);
		final boolean update = Util.isNotBlank(hotelId);
		final GPTUtil util = GPTUtil.get();
		final Map<Integer, RoomMap> rmMap = new HashMap<Integer, RoomMap>();
		final Map<Integer, CoffeeRoom> crMap = new HashMap<Integer, CoffeeRoom>();
		// is update - populate child's objects
		if (update) {
			final List<Image> images = new ArrayList<Image>(Arrays.asList(request.getImages()));
			// rooms
			final List<RoomMap> roomMaps = provider.getResultList("from RoomMap where hotel = ?0", 0, -1, hotel);
			if (roomMaps != null && !roomMaps.isEmpty()) {
				final Set<HotelRoom> roomsSet = new HashSet<HotelRoom>();
				roomMaps.forEach(roomMap -> {
					final HotelRoom room = new HotelRoom().serialize(this, roomMap);
					if (room.getId() == null) rmMap.put(room.hashCode(), roomMap);
					else {
						// images
						final Path mainImage = roomMap.mainImage();
						Optional.ofNullable(util.serializeImages(contextPath, roomMap.getId(), RoomMap.FOLDER,
								Casting.asLong(roomMap.getAlias(systemId), null), mainImage))
							.ifPresent(array -> images.addAll(Arrays.asList(array)));
					}
					roomsSet.add(room);
				});
				request.setRooms(roomsSet.stream().toArray(HotelRoom[]::new));
			}
			// restaurants
			final List<CoffeeRoom> coffeeRooms = provider.getResultList("from CoffeeRoom where hotel = ?0", 0, -1, hotel);
			if (coffeeRooms != null && !coffeeRooms.isEmpty()) {
				final Set<HotelRestaurant> restaurantsSet = new HashSet<HotelRestaurant>();
				coffeeRooms.forEach(coffeeRoom -> {
					final HotelRestaurant restaurant = new HotelRestaurant().serialize(this, coffeeRoom);
					if (restaurant.getId() == null) crMap.put(restaurant.hashCode(), coffeeRoom);
					else {
						// images
						final Path mainImage = coffeeRoom.mainImage();
						Optional.ofNullable(util.serializeImages(contextPath, coffeeRoom.getId(), CoffeeRoom.FOLDER,
								Casting.asLong(coffeeRoom.getAlias(systemId), null), mainImage))
							.ifPresent(array -> images.addAll(Arrays.asList(array)));
					}
					restaurantsSet.add(restaurant);
				});
				request.setRestaurants(restaurantsSet.stream().toArray(HotelRestaurant[]::new));
			}
			request.setImages(images.isEmpty() ? null : images.stream().toArray(Image[]::new));
		}
		final HotelDescriptionInfoResponse response = updateHotel(request, hotelId, locale);
		if (response == null || response.getHotelId() == 0) return null;
		if (update) {
			if (!rmMap.isEmpty()) {
				for (HotelRoom room : response.getRooms()) {
					final RoomMap roomMap = rmMap.get(room.hashCode());
					if (roomMap != null) {
						roomMap.setAlias(provider, systemId, String.valueOf(room.getId()));
						provider.save(roomMap);
					}
				}
			}
			if (!crMap.isEmpty()) {
				for (HotelRestaurant restaurant : response.getRestaurants()) {
					final CoffeeRoom coffeeRoom = crMap.get(restaurant.hashCode());
					if (coffeeRoom != null) {
						coffeeRoom.setAlias(provider, systemId, String.valueOf(restaurant.getId()));
						provider.save(coffeeRoom);
					}
				}
			}
			util.deserializeImages(response.getImages());
			return hotel;
		}
		trace("syncHotel, alias: " + String.valueOf(response.getHotelId()));
		hotel.setAlias(provider, systemId, String.valueOf(response.getHotelId()));
		trace("syncHotel, hotel: " + hotel);
		return provider.save(hotel);
	}

	public void removeCoffeeRoom(CoffeeRoom coffeeRoom, Locale locale) {
		final Hotel hotel = coffeeRoom.getHotel();
		final String hotelId = hotel.getAlias(systemId);
		if (Util.isBlank(hotelId)) return;
		final Long coffeeRoomId = Casting.asLong(coffeeRoom.getAlias(systemId), null);
		trace(coffeeRoomId);
		if (coffeeRoomId != null && coffeeRoomId.longValue() > 0) {
			final HotelDescriptionInfoRequest request = prepareRequest(hotel, locale);
			if (request == null) return;
			for (HotelRestaurant restaurant : request.getRestaurants()) {
				if (Util.equalsWithNull(restaurant.getId(), coffeeRoomId)) {
					restaurant.setActive(false);
					break;
				}
			}
			update(HotelDescriptionInfoRequest.URI_METHOD_UPDATE, locale, request, HotelDescriptionInfoResponse.class, hotelId);
		}
	}

	public void removeHotel(Hotel hotel, Locale locale) {
		final String hotelId = hotel.getAlias(systemId);
		if (Util.isBlank(hotelId)) return;
		final HotelDescriptionInfoRequest request = prepareRequest(hotel, locale);
		if (request == null) return;
		request.setActive(false);
		update(HotelDescriptionInfoRequest.URI_METHOD_UPDATE, locale, request, HotelDescriptionInfoResponse.class, hotelId);
	}

	public void removeRoomMap(RoomMap roomMap, Locale locale) {
		final Hotel hotel = roomMap.getHotel();
		final String hotelId = hotel.getAlias(systemId);
		if (Util.isBlank(hotelId)) return;
		final Long roomMapId = Casting.asLong(roomMap.getAlias(systemId), null);
		if (roomMapId != null && roomMapId.longValue() > 0) {
			final HotelDescriptionInfoRequest request = prepareRequest(hotel, locale);
			if (request == null) return;
			for (HotelRoom room : request.getRooms()) {
				if (Util.equalsWithNull(room.getId(), roomMapId)) {
					room.setActive(false);
					break;
				}
			}
			update(HotelDescriptionInfoRequest.URI_METHOD_UPDATE, locale, request, HotelDescriptionInfoResponse.class, hotelId);
		}
	}

	@Override
	@EventListener
	@Transactional
	public void handleEntityAction(@NonNull EntityEvent event) {
		try {
			final Entity entity = event.getEntity();
			final Locale locale = Util.extractLocale(event.getParameter(Util.PARAMETER_LOCALE));
			if (entity instanceof Hotel) {
				switch (event.getAction()) {
					case CREATE:
					case UPDATE:
						syncHotel(provider.find(Hotel.class, entity.getId()), locale);
						break;
					case DELETE:
						removeHotel(provider.find(Hotel.class, entity.getId()), locale);
						break;
					default: break;
				}

			} else if (entity instanceof CoffeeRoom) {
				switch (event.getAction()) {
					case CREATE:
					case UPDATE:
						syncHotel(provider.find(Hotel.class, ((CoffeeRoom) entity).getHotel().getId()), locale);
						break;
					case DELETE:
						removeCoffeeRoom(provider.find(CoffeeRoom.class, entity.getId()), locale);
						break;
					default: break;
				}
			} else if (entity instanceof RoomMap) {
				switch (event.getAction()) {
					case CREATE:
					case UPDATE:
						syncHotel(provider.find(Hotel.class, ((RoomMap) entity).getHotel().getId()), locale);
						break;
					case DELETE:
						removeRoomMap(provider.find(RoomMap.class, entity.getId()), locale);
						break;
					default: break;
				}
			}
		} catch (Exception e) {
			error("Can't sync entity: ", e);
		}
    }

}

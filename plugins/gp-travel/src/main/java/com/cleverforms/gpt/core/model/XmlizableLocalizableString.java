/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.shared.marker.HasValue;
import com.cleverforms.comms.shared.proxy.ValueProxy;
import com.cleverforms.comms.shared.util.Format;

/**
 * A base {@code ValueProxy} for XML localizable strings.
 * @author Sergey Titarchuk
 */
public class XmlizableLocalizableString implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = -4275230524511349058L;

	/** Key to blank in all locales property */
	public static final String BLANK_IN_ALL_LOCALES = "blankInAllLocales";

	/** Key to localized strings property */
	public static final String LOCALIZED_STRINGS = "localizedStrings";

	/** Key to strings property */
	public static final String STRINGS = "strings";

	/** Key to supported locales property */
	public static final String SUPPORTED_LOCALES = "supportedLocales";

	/**
	 * A base wrapper for localized String.
	 * @author Sergey Titarchuk
	 */
	public static class ILocalizedString implements ValueProxy, HasValue<String> {
		
		/** The serial version UID */
		private static final long serialVersionUID = -6694684365349423133L;

		private String value;

		public ILocalizedString() {}
		
		public ILocalizedString(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	
		@Override
		public String toString() {
			return value;
		}

		@Override
		public int hashCode() {
			return value == null ? 0 : 31 + value.hashCode();
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof ILocalizedString) {
				return Util.equalsWithNull(value, ((ILocalizedString) obj).value);
			}
			return false;
		}
	
	}

	private ILocalizedString[] localizedStrings;
	private Map<String, ILocalizedString> strings;
	private boolean blankInAllLocales;
	private Collection<Locale> supportedLocales;

	public XmlizableLocalizableString() {}

	public XmlizableLocalizableString(String value) {
		localizedStrings = new ILocalizedString[] { new ILocalizedString(value) };
	}

	public ILocalizedString[] getLocalizedStrings() {
		return localizedStrings;
	}

	public void setLocalizedStrings(ILocalizedString[] localizedStrings) {
		this.localizedStrings = localizedStrings;
	}

	public Map<String, ILocalizedString> getStrings() {
		return strings;
	}

	public void setStrings(Map<String, ILocalizedString> strings) {
		this.strings = strings;
	}

	public boolean getBlankInAllLocales() {
		return blankInAllLocales;
	}

	public void setBlankInAllLocales(Boolean blankInAllLocales) {
		this.blankInAllLocales = blankInAllLocales != null ? blankInAllLocales.booleanValue() : false;
	}

	public Collection<Locale> getSupportedLocales() {
		return supportedLocales;
	}

	public void setSupportedLocales(Collection<Locale> supportedLocales) {
		this.supportedLocales = supportedLocales;
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(new Object[] { localizedStrings, strings, blankInAllLocales, supportedLocales });
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof XmlizableLocalizableString) {
			final XmlizableLocalizableString model = (XmlizableLocalizableString) obj;
			return super.equals(obj) && blankInAllLocales == model.blankInAllLocales
				&& Util.equalsWithNull(localizedStrings, model.localizedStrings)
				&& Util.equalsWithNull(strings, model.strings)
				&& Util.equalsWithNull(supportedLocales, model.supportedLocales);
		}
		return false;
	}

	@Override
	public String toString() {
		return Format.toStringHelper(this)
			.add(LOCALIZED_STRINGS,localizedStrings)
			.add(STRINGS, strings)
			.add(BLANK_IN_ALL_LOCALES, blankInAllLocales)
			.add(SUPPORTED_LOCALES, supportedLocales)
			.toString();
	}

}

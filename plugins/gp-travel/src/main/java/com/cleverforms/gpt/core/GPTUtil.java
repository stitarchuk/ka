/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 21, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;

import com.cleverforms.comms.server.UriUtils;
import com.cleverforms.comms.server.WithLoggerImpl;
import com.cleverforms.comms.server.model.EnumComplexPersistentEntity;
import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.server.util.io.FileUtil;
import com.cleverforms.comms.shared.util.Casting;
import com.cleverforms.gpt.core.model.hotelinfo.save.HotelClassificationApi;
import com.cleverforms.gpt.core.model.hotelinfo.save.HotelServiceApi;
import com.cleverforms.gpt.core.model.hotelinfo.save.Image;
import com.cleverforms.iss.server.model.hotel.CoffeeRoom;
import com.cleverforms.iss.server.model.hotel.Hotel;
import com.cleverforms.iss.server.model.hotel.RoomMap;
/**
 * Some utilize methods for {@code GPTConnector}
 * @author Sergey Titarchuk
 */
public class GPTUtil extends WithLoggerImpl implements InitializingBean {

	public static final String IMAGE_ALIASES_FILE = "gp_images.aliases";

	static class DictionaryRage {

		private long min;
		private long max;

		public DictionaryRage(long min, long max) {
			this.min = min;
			this.max = max;
			this.correct();
		}
		
		public DictionaryRage(String range) {
			this(range, "\\,");
		}

		public DictionaryRage(String range, String delimiter) {
			if (range != null) {
				final String[] values = range.split(delimiter);
				if (values.length > 0) min = Casting.asLong(values[0]);
				if (values.length > 1) max = Casting.asLong(values[1]);
				this.correct();
			}
		}

		protected void correct() {
			if (min > max) {
				if (max == 0) max = min;
				else min = max;
			}
		}

		public boolean contains(long value) {
	        return value >= min && value <= max;
	    }

	}

	public static class HotelAddData {

		private final Set<Long> classification;
		private final Set<Long> service;

		public HotelAddData() {
			classification = new HashSet<Long>();
			service = new HashSet<Long>();
		}
	
		public Set<Long> classifications() {
			return classification;
		}

		public HotelClassificationApi[] hotelClassifications() {
			if (classification == null) return null;
			final HotelClassificationApi[] result = new HotelClassificationApi[classification.size()];
			int idx = 0;
			for (long id : classification) {
				result[idx++] = new HotelClassificationApi(id, true);
			}
			return result;
		}
		
		public Set<Long> service() {
			return service;
		}

		public HotelServiceApi[] hotelServices() {
			if (service == null) return null;
			final HotelServiceApi[] result = new HotelServiceApi[service.size()];
			int idx = 0;
			for (long id : service) {
				result[idx++] = new HotelServiceApi(id, true);
			}
			return result;
		}

		public void add(HotelAddData data) {
			if (data != null) {
				classification.addAll(data.classifications());
				service.addAll(data.service());
			}
		}
	}

	public static class ImagesMap {

		private final Map<String, Map<Long, Properties>> data;

		public ImagesMap() {
			data = new HashMap<String, Map<Long,Properties>>();
		}

		public void loadAliases(GPTUtil util, String imageType, long entityId) {
			if (!data.containsKey(imageType)) {
				data.put(imageType, new HashMap<Long, Properties>());
			}
			data.get(imageType).put(entityId, util.loadImageAliases(imageType, entityId));
		}

	} 

	private static GPTUtil instance;
	
	public final static GPTUtil get() {
		return instance;
	}

	public final static boolean isInit() {
		return instance != null;
	}

	@Value("${gpt.hotelServices:classpath:hotel_services.properties}")
	private Resource hotelServicesResource;
	private DictionaryRage hotelClassifications;
	private DictionaryRage hotelServices;
	private final Map<String, long[]> hotelServicesSections;

	public GPTUtil() {
		hotelServicesSections = new HashMap<String, long[]>();
		instance = this;
	}

	public void setHotelServicesResource(Resource hotelServicesResource) {
		this.hotelServicesResource = hotelServicesResource;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (hotelServicesResource != null && hotelServicesResource.exists()) {
			final Properties properties = FileUtil.loadProperties(hotelServicesResource.getInputStream());
			hotelClassifications = new DictionaryRage((String) properties.remove("range.classifications"));
			hotelServices = new DictionaryRage((String) properties.remove("range.services"));
			properties.keySet().forEach(k -> {
				final String key = String.valueOf(k);
				final String[] items = properties.getProperty(key, "").split("\\,");
				final long[] values = new long[items.length];
				for (int i = 0; i < items.length; i++) {
					values[i] = Casting.asLong(items[i]);
				}
				hotelServicesSections.put(key, values);
			});
		}
	}

	public String buildImageUrl(String contextUrl, Path path) throws MalformedURLException, UnsupportedEncodingException {
		if (path != null) {
			final int count = path.getNameCount(); 
	    	final Map<String, String> params = new HashMap<String, String>();
	    	params.put(Util.PARAMETER_METHOD, "resource.download");
//	    	params.put(Util.PARAMETER_INLINE, "true");
	    	params.put(Util.PARAMETER_PATH, URLEncoder.encode(path.getFileName().toString(), StandardCharsets.UTF_8.toString()));
	    	params.put(Util.PARAMETER_TYPE, path.subpath(count - 4, count - 1).toString());
	    	return UriUtils.getInstance().buildURL(contextUrl, params).toString();
		}
		return null;
	}

	public HotelAddData addHotelAddData(HotelAddData data, String section, Long value) {
		if (data != null && value != null && value.longValue() > 0) {
			data.add(parseHotelAddData(section, value.longValue()));
		}
		return data;
	}

	public Long extractEntityId(UriUtils uriUtil, String url) throws UnsupportedEncodingException, URISyntaxException {
		final String type = uriUtil.extractParam(url, Util.PARAMETER_TYPE);
		if (type != null) {
			final String[] parts = type.split("/");
			if (parts.length == 3) return Casting.asLong(parts[1], null);
		}
		return null;
	}

	public Long extractEntityId(String url) throws UnsupportedEncodingException, URISyntaxException {
		return extractEntityId( UriUtils.getInstance(), url);
	}

	public HotelAddData parseHotelAddData(String section, long value) {
		final HotelAddData info = new HotelAddData();
		if (value > 0 && section != null && hotelServicesSections.containsKey(section)) {
			final long[] values = hotelServicesSections.get(section);
			for (int i = 0; i < values.length; i++) {
				if (Util.containsBit(value, i + 1)) {
					if (hotelClassifications.contains(values[i])) info.classifications().add(values[i]);
					else if (hotelServices.contains(values[i])) info.service().add(values[i]);
				}
			}
		}
		return info;
	}

	public HotelAddData parseHotelAddData(Hotel hotel) {
		final HotelAddData hotelAddData = new HotelAddData();
		if (hotel != null) {
			addHotelAddData(hotelAddData, "budget", hotel.getBudget());
			addHotelAddData(hotelAddData, "conditions", hotel.getConditions());
			addHotelAddData(hotelAddData, "foods", hotel.getFoods());
			addHotelAddData(hotelAddData, "forChildren", hotel.getForChildren());
			addHotelAddData(hotelAddData, "medicalProfile", hotel.getMedicalProfile());
			addHotelAddData(hotelAddData, "payment", hotel.getPayment());
//			addHotelAddData(hotelAddData, "position", hotel.getPosition());
			addHotelAddData(hotelAddData, "security", hotel.getSecurity());
			addHotelAddData(hotelAddData, "sportsComfort", hotel.getSportsComfort());
			addHotelAddData(hotelAddData, "visitObjectives", hotel.getVisitObjectives());
		}
		return hotelAddData;
	}

	public String parseImageTypeId(Integer imageTypeId) {
		if (imageTypeId != null) {
			switch (imageTypeId) {
				case 19: return CoffeeRoom.FOLDER;
				case 20: return RoomMap.FOLDER;
				default: break;
			}
		}
		return Hotel.FOLDER;
	}

	public Integer getImageTypeId(String imageType) {
		if (imageType != null) {
			switch (imageType) {
				case CoffeeRoom.FOLDER: return 19;
				case RoomMap.FOLDER: return 20;
				default: break;
			}
		}
		return 22;
	}

	public <E extends Enum<E>> Map<String, String> getLocalizedProperties(EnumComplexPersistentEntity<E> entity, List<E> properties, Consumer<String> action) {
        Objects.requireNonNull(action);
		final Map<String, String> values = new HashMap<String, String>();
		properties.forEach(property -> {
			final String value = entity.getProperty(property);
			if (Util.isNotBlank(value)) {
				final String key = property.name().toLowerCase();
				values.put(key.substring(key.length() - 2), value);
				if (action != null) action.accept(value);
			}
		});
		return !values.isEmpty() ? values : null;
	}

	public <E extends Enum<E>> Map<String, String> getLocalizedProperties(EnumComplexPersistentEntity<E> entity, List<E> properties) {
		return getLocalizedProperties(entity, properties, action -> {});
	}

	public Image[] serializeImages(String contextPath, long entityId, String imageType, Long objectId, Path mainImage) {
		if (objectId == null || entityId <= 0) return null;
		final Set<Image> imagesSet = new HashSet<Image>();
		final Properties aliases = loadImageAliases(imageType, entityId);
		final Integer imageTypeId = getImageTypeId(imageType);
		final String baseUrl = Util.get().baseUrl();
		final String contextUrl = (baseUrl != null ? baseUrl : "") + (contextPath != null ? contextPath : "");
		try {
			FileUtil.walkImages(imageType, entityId).forEach(path -> {
				try {
					final Image image = new Image(objectId, buildImageUrl(contextUrl, path));
					image.setId(Casting.asLong(aliases.getProperty(path.getFileName().toString()), null));
					image.setImageTypeId(imageTypeId);
					if (path.equals(mainImage)) image.setMainPhoto(true);
					imagesSet.add(image);
				} catch (Exception e) {
					error("Can't add image: ", e);
				}
			});
		} catch (IOException e) {}
		return imagesSet.isEmpty() ? null : imagesSet.stream().toArray(Image[]::new);
	}

	public void deserializeImages(Image[] images) {
		if (images != null && images.length > 0) {
			final UriUtils uriUtil = UriUtils.getInstance();
			final Map<String, Map<Long, Properties>> aliasesMap = new HashMap<String, Map<Long, Properties>>();
			for (Image image : images) {
				if (image.getId() == null || Util.isBlank(image.getUrl())) continue;
				try {
					final Long entityId = extractEntityId(uriUtil, image.getUrl());
					if (entityId == null || entityId.longValue() <= 0) continue;
					final String imageType = parseImageTypeId(image.getImageTypeId());
					if (!aliasesMap.containsKey(imageType)) {
						aliasesMap.put(imageType, new HashMap<Long, Properties>());
					}
					final Map<Long, Properties> map = aliasesMap.get(imageType);
					if (!map.containsKey(entityId)) {
						map.put(entityId, loadImageAliases(imageType, entityId));
					}
					map.get(entityId).setProperty(uriUtil.extractParam(image.getUrl(), Util.PARAMETER_PATH), String.valueOf(image.getId()));
				} catch (UnsupportedEncodingException | URISyntaxException e) {}
			}
			aliasesMap.forEach((type, aliases) -> {
				aliases.forEach((entityId, properties) -> saveImageAliases(type, entityId, properties));
			});
		}
	}
	
	public Path getImageAliasesPath(String imageType, long objectId) {
		return FileUtil.getImagesPath(imageType, objectId).resolve(IMAGE_ALIASES_FILE);
	}

	public Properties loadImageAliases(String imageType, long objectId) {
		try {
			return FileUtil.loadFromXml(getImageAliasesPath(imageType, objectId));
		} catch (Exception e) {
			error("Can't read GP Travel aliases file", e);
		}
		return new Properties();
	}

	public void saveImageAliases(String imageType, long objectId, Properties properties) {
		try {
			FileUtil.saveToXml(getImageAliasesPath(imageType, objectId), properties, "GP Travel images aliases");
		} catch (Exception e) {
			error("Can't save GP Travel aliases file", e);
		}
	}

	@SuppressWarnings("unchecked")
	public <E> E[] concatenate(E[] dest, E[] src) {
		return dest = (E[]) Stream.of(dest, src).flatMap(Stream::of).toArray();
    }

}

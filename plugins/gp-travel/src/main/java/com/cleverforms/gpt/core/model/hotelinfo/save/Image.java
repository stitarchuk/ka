/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.save;

import static com.cleverforms.comms.shared.marker.HasTitle.TITLE;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.shared.marker.HasId;
import com.cleverforms.comms.shared.proxy.ValueProxy;
import com.cleverforms.comms.shared.util.Format;
import com.cleverforms.gpt.core.model.ImageType;

/**
 * A base {@code ValueProxy} for hotel {@code Hotel}'s image data.
 * @author Sergey Titarchuk
 */
public class Image implements ValueProxy, HasId<Long> {

	/** The serial version UID */
	private static final long serialVersionUID = 1670714352237258568L;

	/** Key to image type ID property */
	public final static String IMAGE_TYPE_ID = "imageTypeId";

	/** Key to main image property */
	public final static String MAIN_IMAGE = "mainImage";

	/** Key to main photo property */
	public final static String MAIN_PHOTO = "mainPhoto";

	/** Key to object ID property */
	public final static String OBJECT_ID = "objectId";

	/** Key to url property */
	public final static String URL = "url";

	private Long id;
	private Long objectId;
	private String url;
	private Map<String, String> title;
	private Integer imageTypeId;
	private Boolean mainImage;
	// only for ROOMS
	private Boolean mainPhoto;

	public Image() {}

	public Image(Long objectId, String url) {
		this(objectId, null, url);
	}

	public Image(String title, String url) {
		this(null, title, url);
	}

	public Image(Long objectId, String title, String url) {
		this.objectId = objectId;
		this.url = url;
		if (Util.isNotBlank(title)) {
			this.title = new HashMap<String, String>();
			Util.SUPPORTED_LOCALES.forEach(l -> this.title.put(l.getLanguage(), title));
		}
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String key() {
		return String.valueOf(id);
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Map<String, String> getTitle() {
		if (title == null) title = new HashMap<String, String>();
		return title;
	}

	public void setTitle(Map<String, String> title) {
		this.title = title;
	}

	public Integer getImageTypeId() {
		return imageTypeId;
	}

	public void setImageTypeId(Integer imageTypeId) {
		this.imageTypeId = imageTypeId;
	}

	public void setImageType(ImageType imageType) {
		this.imageTypeId = imageType != null ? imageType.getId() : null;
	}

	public Boolean getMainImage() {
		return mainImage;
	}

	public void setMainImage(Boolean mainImage) {
		this.mainImage = mainImage;
	}

	public Boolean getMainPhoto() {
		return mainPhoto;
	}

	public void setMainPhoto(Boolean mainPhoto) {
		this.mainPhoto = mainPhoto;
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(new Object[] { objectId, url, title, imageTypeId, mainImage, mainPhoto });
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Image) {
			final Image model = (Image) obj;
			return Util.equalsWithNull(objectId, model.objectId)
				&& Util.equalsWithNull(url, model.url)
				&& Util.equalsWithNull(title, model.title)
				&& Util.equalsWithNull(imageTypeId, model.imageTypeId)
				&& Util.equalsWithNull(mainImage, model.mainImage)
				&& Util.equalsWithNull(mainPhoto, model.mainPhoto);
		}
		return false;
	}

	@Override
	public String toString() {
		return Format.toStringHelper(this)
			.add(ID, id)
			.add(IMAGE_TYPE_ID, imageTypeId)
			.add(OBJECT_ID, objectId)
			.add(URL, url)
			.add(TITLE, title)
			.add(MAIN_IMAGE, mainImage)
			.add(MAIN_PHOTO, mainPhoto)
			.toString();
	}

}

/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.shared.marker.HasType;
import com.cleverforms.comms.shared.proxy.ValueProxy;
import com.cleverforms.comms.shared.util.Format;
import com.cleverforms.gpt.core.model.XmlizableLocalizableString;

/**
 * A base {@code ValueProxy} for hotel {@code Description} data.
 * @author Sergey Titarchuk
 */
public class HotelDescription implements ValueProxy, HasType<String> {

	/** The serial version UID */
	private static final long serialVersionUID = 5387514134179776352L;

	/** Key to description property */
	public final static String DESCRIPTION = "description";

	private XmlizableLocalizableString description;
	private String type;

	public HotelDescription() {}

	public XmlizableLocalizableString getDescription() {
		return description;
	}

	public void setDescription(XmlizableLocalizableString description) {
		this.description = description;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return Format.toStringHelper(this)
			.add(DESCRIPTION, description)
			.add(TYPE, type)
			.toString();
	}

}

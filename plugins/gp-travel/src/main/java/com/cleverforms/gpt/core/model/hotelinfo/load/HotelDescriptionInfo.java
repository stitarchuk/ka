/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.shared.marker.HasEmail;
import com.cleverforms.comms.shared.marker.HasPhone;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.gpt.core.model.ActiveModel;
import com.cleverforms.gpt.core.model.XmlizableLocalizableString;
import com.cleverforms.gpt.core.model.hotel.HotelCategory;

/**
 * A base {@link ActiveModel} for {@code Hotel} info data
 * @author Sergey Titarchuk
 */
public class HotelDescriptionInfo extends ActiveModel implements HasEmail, HasPhone {

	/** The serial version UID */
	private static final long serialVersionUID = -2386406077319637704L;

	/** Key to additional category property */
	public final static String ADDITIONAL_CATEGORY = "additionalCategory";

	/** Key to additional category rating symbol property */
	public final static String ADDITIONAL_CATEGORY_RATING_SYMBOL = "additionalCategoryRatingSymbol";

	/** Key to address property */
	public final static String ADDRESS = "address";

	/** Key to check-in property */
	public final static String CHECK_IN = "checkIn";

	/** Key to check-out property */
	public final static String CHECK_OUT = "checkOut";

	/** Key to classifications property */
	public final static String CLASSIFICATIONS = "classifications";

	/** Key to content provider ID property */
	public final static String CONTENT_PROVIDER_ID = "contentProviderId";

	/** Key to descriptions property */
	public final static String DESCRIPTIONS = "descriptions";

	/** Key to destination types property */
	public final static String DESTINATION_TYPES = "destinationTypes";

	/** Key to distance from center property */
	public final static String DISTANCE_FROM_CENTER = "distanceFromCenter";

	/** Key to fax property */
	public final static String FAX = "fax";

	/** Key to full info available property */
	public final static String FULL_INFO_AVAILABLE = "fullInfoAvailable";

	/** Key to hotel ID property */
	public final static String HOTEL_ID = "hotelId";

	/** Key to hotel code property */
	public final static String HOTEL_CODE = "hotelCode";

	/** Key to hotel chain property */
	public final static String HOTEL_CHAIN = "hotelChain";

	/** Key to hotel info extended type property */
	public final static String HOTEL_INFO_EXTENDED_TYPE = "hotelInfoExtendedType";

	/** Key to hotel info type property */
	public final static String HOTEL_INFO_TYPE = "hotelInfoType";

	/** Key to hotel name property */
	public final static String HOTEL_NAME = "hotelName";

	/** Key to images property */
	public final static String IMAGES = "images";

	/** Key to kids amenities property */
	public final static String KIDS_AMENITIES = "kidsAmenities";

	/** Key to latitude property */
	public final static String LATITUDE = "latitude";

	/** Key to link to TripAdvisor property */
	public final static String LINK_TO_TRIP_ADVISOR = "linkToTripAdvisor";

	/** Key to local mapped hotel ID property */
	public final static String LOCAL_MAPPED_HOTEL_ID = "localMappedHotelId";

	/** Key to logo image property */
	public final static String LOGO_IMAGE = "logoImage";

	/** Key to longitude property */
	public final static String LONGITUDE = "longitude";

	/** Key to main image property */
	public final static String MAIN_IMAGE = "mainImage";

	/** Key to map property */
	public final static String MAP = "map";

	/** Key to meal types property */
	public final static String MEAL_TYPES = "mealTypes";

	/** Key to next day checkout property */
	public final static String NEXT_DAY_CHECKOUT = "nextDayCheckout";

	/** Key to rating property */
	public final static String RATING = "rating";

	/** Key to relative positions property */
	public final static String RELATIVE_POSITIONS = "relativePositions";

	/** Key to restaurant amenities property */
	public final static String RESTAURANT_AMENITIES = "restaurantAmenities";

	/** Key to room types property */
	public final static String ROOM_TYPES = "roomTypes";

	/** Key to services property */
	public final static String SERVICES = "services";

	/** Key to SPA amenities property */
	public final static String SPA_AMENITIES = "spaAmenities";

	/** Key to sport amenities property */
	public final static String SPORT_AMENITIES = "sportAmenities";

	/** Key to standard category property */
	public final static String STD_CATEGORY = "stdCategory";

	/** Key to supplier ID property */
	public final static String SUPPLIER_ID = "supplierId";

	/** Key to url property */
	public final static String URL = "url";

	/** Key to villa property */
	public final static String VILLA = "villa";

	private long hotelId;
	private String hotelCode;
	private XmlizableLocalizableString hotelName;
	private HotelServiceArray services;
	private HotelCategory stdCategory;
	private XmlizableLocalizableString additionalCategory;
	private HotelChain hotelChain;
	private HotelAddress address;
	private String email;
	private String phone;
	private String fax;
	private String url;
	private HotelDescriptionArray descriptions;
	private RelativePositionArray relativePositions;
	private HotelClassificationArray classifications;
	private XmlizableLocalizableString hotelInfoType;
	private Long localMappedHotelId;
	private Long contentProviderId;
	private Long supplierId;
	private String checkOut;
	private String checkIn;
	private double rating;
	private String linkToTripAdvisor;
	private RestaurantAmenityArray restaurantAmenities;
	private SportAmenityArray sportAmenities;
	private SpaAmenityArray spaAmenities;
	private KidsAmenityArray kidsAmenities;
	private RoomTypeArray roomTypes;
	private MealTypeArray mealTypes;
	private String[] destinationTypes;
	private Double distanceFromCenter;
	private VillaDescriptionInfo villa;
	private XmlizableLocalizableString additionalCategoryRatingSymbol;
	private XmlizableLocalizableString hotelInfoExtendedType;
	private HotelImage mainImage;
	private HotelImage logoImage;
	private Double latitude;
	private Double longitude;
	private HotelImage map;
	private HotelImageArray images;
	private Boolean fullInfoAvailable;
	private Boolean nextDayCheckout;
	
	public HotelDescriptionInfo() {}

	public long getHotelId() {
		return hotelId;
	}

	public void setHotelId(long hotelId) {
		this.hotelId = hotelId;
	}

	public String getHotelCode() {
		return hotelCode;
	}

	public void setHotelCode(String hotelCode) {
		this.hotelCode = hotelCode;
	}

	public XmlizableLocalizableString getHotelName() {
		return hotelName;
	}

	public void setHotelName(XmlizableLocalizableString hotelName) {
		this.hotelName = hotelName;
	}

	public HotelServiceArray getServices() {
		return services;
	}

	public void setServices(HotelServiceArray services) {
		this.services = services;
	}

	public HotelCategory getStdCategory() {
		return stdCategory;
	}

	public void setStdCategory(HotelCategory stdCategory) {
		this.stdCategory = stdCategory;
	}

	public XmlizableLocalizableString getAdditionalCategory() {
		return additionalCategory;
	}

	public void setAdditionalCategory(XmlizableLocalizableString additionalCategory) {
		this.additionalCategory = additionalCategory;
	}

	public HotelChain getHotelChain() {
		return hotelChain;
	}

	public void setHotelChain(HotelChain hotelChain) {
		this.hotelChain = hotelChain;
	}

	public HotelAddress getAddress() {
		return address;
	}

	public void setAddress(HotelAddress address) {
		this.address = address;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String getPhone() {
		return phone;
	}

	@Override
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public HotelDescriptionArray getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(HotelDescriptionArray descriptions) {
		this.descriptions = descriptions;
	}

	public RelativePositionArray getRelativePositions() {
		return relativePositions;
	}

	public void setRelativePositions(RelativePositionArray relativePositions) {
		this.relativePositions = relativePositions;
	}

	public HotelClassificationArray getClassifications() {
		return classifications;
	}

	public void setClassifications(HotelClassificationArray classifications) {
		this.classifications = classifications;
	}

	public XmlizableLocalizableString getHotelInfoType() {
		return hotelInfoType;
	}

	public void setHotelInfoType(XmlizableLocalizableString hotelInfoType) {
		this.hotelInfoType = hotelInfoType;
	}

	public Long getLocalMappedHotelId() {
		return localMappedHotelId;
	}

	public void setLocalMappedHotelId(Long localMappedHotelId) {
		this.localMappedHotelId = localMappedHotelId;
	}

	public Long getContentProviderId() {
		return contentProviderId;
	}

	public void setContentProviderId(Long contentProviderId) {
		this.contentProviderId = contentProviderId;
	}

	public Long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Long supplierId) {
		this.supplierId = supplierId;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}

	public String getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating != null ? rating.doubleValue() : 0;
	}

	public String getLinkToTripAdvisor() {
		return linkToTripAdvisor;
	}

	public void setLinkToTripAdvisor(String linkToTripAdvisor) {
		this.linkToTripAdvisor = linkToTripAdvisor;
	}

	public RestaurantAmenityArray getRestaurantAmenities() {
		return restaurantAmenities;
	}

	public void setRestaurantAmenities(RestaurantAmenityArray restaurantAmenities) {
		this.restaurantAmenities = restaurantAmenities;
	}

	public SportAmenityArray getSportAmenities() {
		return sportAmenities;
	}

	public void setSportAmenities(SportAmenityArray sportAmenities) {
		this.sportAmenities = sportAmenities;
	}

	public SpaAmenityArray getSpaAmenities() {
		return spaAmenities;
	}

	public void setSpaAmenities(SpaAmenityArray spaAmenities) {
		this.spaAmenities = spaAmenities;
	}

	public KidsAmenityArray getKidsAmenities() {
		return kidsAmenities;
	}

	public void setKidsAmenities(KidsAmenityArray kidsAmenities) {
		this.kidsAmenities = kidsAmenities;
	}

	public RoomTypeArray getRoomTypes() {
		return roomTypes;
	}

	public void setRoomTypes(RoomTypeArray roomTypes) {
		this.roomTypes = roomTypes;
	}

	public MealTypeArray getMealTypes() {
		return mealTypes;
	}

	public void setMealTypes(MealTypeArray mealTypes) {
		this.mealTypes = mealTypes;
	}

	public String[] getDestinationTypes() {
		return destinationTypes;
	}

	public void setDestinationTypes(String[] destinationTypes) {
		this.destinationTypes = destinationTypes;
	}

	public Double getDistanceFromCenter() {
		return distanceFromCenter;
	}

	public void setDistanceFromCenter(Double distanceFromCenter) {
		this.distanceFromCenter = distanceFromCenter;
	}

	public VillaDescriptionInfo getVilla() {
		return villa;
	}

	public void setVilla(VillaDescriptionInfo villa) {
		this.villa = villa;
	}

	public XmlizableLocalizableString getAdditionalCategoryRatingSymbol() {
		return additionalCategoryRatingSymbol;
	}

	public void setAdditionalCategoryRatingSymbol(XmlizableLocalizableString additionalCategoryRatingSymbol) {
		this.additionalCategoryRatingSymbol = additionalCategoryRatingSymbol;
	}

	public XmlizableLocalizableString getHotelInfoExtendedType() {
		return hotelInfoExtendedType;
	}

	public void setHotelInfoExtendedType(XmlizableLocalizableString hotelInfoExtendedType) {
		this.hotelInfoExtendedType = hotelInfoExtendedType;
	}

	public HotelImage getMainImage() {
		return mainImage;
	}

	public void setMainImage(HotelImage mainImage) {
		this.mainImage = mainImage;
	}

	public HotelImage getLogoImage() {
		return logoImage;
	}

	public void setLogoImage(HotelImage logoImage) {
		this.logoImage = logoImage;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public HotelImage getMap() {
		return map;
	}

	public void setMap(HotelImage map) {
		this.map = map;
	}

	public HotelImageArray getImages() {
		return images;
	}

	public void setImages(HotelImageArray images) {
		this.images = images;
	}

	public Boolean getFullInfoAvailable() {
		return fullInfoAvailable;
	}

	public void setFullInfoAvailable(Boolean fullInfoAvailable) {
		this.fullInfoAvailable = fullInfoAvailable;
	}

	public Boolean getNextDayCheckout() {
		return nextDayCheckout;
	}

	public void setNextDayCheckout(Boolean nextDayCheckout) {
		this.nextDayCheckout = nextDayCheckout;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ hotelCode, hotelName, services, stdCategory,
			additionalCategory, hotelChain, address, email, phone, fax, url, descriptions, relativePositions,
			classifications, hotelInfoType, checkOut, checkIn, Double.valueOf(rating), linkToTripAdvisor,
			destinationTypes, distanceFromCenter, villa, additionalCategoryRatingSymbol, hotelInfoExtendedType,
			latitude, longitude, Boolean.valueOf(fullInfoAvailable), Boolean.valueOf(nextDayCheckout) });
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper()
			.add(HOTEL_ID, hotelId)
			.add(HOTEL_CODE, hotelCode)
			.add(HOTEL_NAME, hotelName)
			.add(SERVICES, services)
			.add(STD_CATEGORY, stdCategory)
			.add(ADDITIONAL_CATEGORY, additionalCategory)
			.add(HOTEL_CHAIN, hotelChain)
			.add(ADDRESS, address)
			.add(E_MAIL, email)
			.add(PHONE, phone)
			.add(FAX, fax)
			.add(URL, url)
			.add(DESCRIPTIONS, descriptions)
			.add(RELATIVE_POSITIONS, relativePositions)
			.add(CLASSIFICATIONS, classifications)
			.add(HOTEL_INFO_TYPE, hotelInfoType)
			.add(LOCAL_MAPPED_HOTEL_ID, localMappedHotelId)
			.add(CONTENT_PROVIDER_ID, contentProviderId)
			.add(SUPPLIER_ID, supplierId)
			.add(CHECK_OUT, checkOut)
			.add(CHECK_IN, checkIn)
			.add(RATING, rating)
			.add(LINK_TO_TRIP_ADVISOR, linkToTripAdvisor)
			.add(RESTAURANT_AMENITIES, restaurantAmenities)
			.add(SPORT_AMENITIES, sportAmenities)
			.add(SPA_AMENITIES, spaAmenities)
			.add(KIDS_AMENITIES, kidsAmenities)
			.add(ROOM_TYPES, roomTypes)
			.add(MEAL_TYPES, mealTypes)
			.add(DESTINATION_TYPES, destinationTypes)
			.add(DISTANCE_FROM_CENTER, distanceFromCenter)
			.add(VILLA, villa)
			.add(ADDITIONAL_CATEGORY_RATING_SYMBOL, additionalCategoryRatingSymbol)
			.add(HOTEL_INFO_EXTENDED_TYPE, hotelInfoExtendedType)
			.add(MAIN_IMAGE, mainImage)
			.add(LOGO_IMAGE, logoImage)
			.add(LATITUDE, latitude)
			.add(LONGITUDE, longitude)
			.add(MAP, map)
			.add(IMAGES, images)
			.add(ACTIVE, getActive())
			.add(FULL_INFO_AVAILABLE, fullInfoAvailable)
			.add(NEXT_DAY_CHECKOUT, nextDayCheckout);
	}

}

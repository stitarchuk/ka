/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.shared.proxy.ValueProxy;

/**
 * A wrapper class for wrap {@code SportAmenity} as Array.
 * @author Sergey Titarchuk
 */
public class SportAmenityArray implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = -4368155640725315952L;

	private TypedAmenity[] sportAmenity;

	public SportAmenityArray() {}

	public TypedAmenity[] getSportAmenity() {
		return sportAmenity;
	}

	public void setSportAmenity(TypedAmenity[] sportAmenity) {
		this.sportAmenity = sportAmenity;
	}

	@Override
	public String toString() {
		return Casting.arrayToString(sportAmenity);
	}

}

/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotel;

import java.io.Serializable;

/**
 * A small enumerator for category of {@code Hotel}.
 * @author Sergey Titarchuk
 */
public enum HotelCategory implements Serializable {

	APARTMENT, FIVE, FOUR, ONE, OTHER, PANSION, THREE, TWO;

	public static HotelCategory getByRating(int number) {
		switch (number) {
			case 1: return ONE;
			case 2: return TWO;
			case 3: return THREE;
			case 4: return FOUR;
			case 5: return FIVE;
			default: return OTHER;
		}
	}
}

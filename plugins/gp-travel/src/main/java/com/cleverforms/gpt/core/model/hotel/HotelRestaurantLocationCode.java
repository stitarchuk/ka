/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotel;

import java.io.Serializable;

import com.cleverforms.iss.shared.hotel.CoffeeRoomLocation;

/**
 * A small enumerator for restaurant location code of {@code Hotel}.
 * @author Sergey Titarchuk
 */
public enum HotelRestaurantLocationCode implements Serializable {

	N("INSIDE"),
	O("NEARBY");

	private final String title;
	
	private HotelRestaurantLocationCode(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}

	public static synchronized HotelRestaurantLocationCode parse(CoffeeRoomLocation location) {
		if (location != null) {
			switch (location) {
				case NEAR_HOTEL:
				case ON_THE_BEACH:
				case NEAR_POOL: return O;
				case INSIDE_HOTEL:
				case RECEPTION:
				default: break;
			}
		}
		return N;
	}

}

/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.save;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
/**
 * A base response payload for create or update {@code Hotel} info data
 * @author Sergey Titarchuk
 */
public class HotelDescriptionInfoResponse extends HotelDescriptionInfoRequest {

	/** The serial version UID */
	private static final long serialVersionUID = -8328253298706149581L;

	/** Key to hotel ID property */
	public final static String HOTEL_ID = "hotelId";

	private long hotelId;

	HotelDescriptionInfoResponse() {}


	public long getHotelId() {
		return hotelId;
	}

	public void setHotelId(Long hotelId) {
		this.hotelId = hotelId != null ? hotelId.longValue() : 0;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ hotelId });
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof HotelDescriptionInfoResponse) {
			return super.equals(obj) && Util.equalsWithNull(hotelId, ((HotelDescriptionInfoResponse) obj).hotelId);
		}
		return false;
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper().add(HOTEL_ID, hotelId);
	}

}

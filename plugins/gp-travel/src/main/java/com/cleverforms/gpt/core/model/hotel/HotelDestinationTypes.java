/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * February 20, 2020.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotel;

import java.io.Serializable;

/**
 * A small enumerator for destination type of {@code Hotel}.
 * @author Sergey Titarchuk
 */
public enum HotelDestinationTypes implements Serializable {

	AIRPORT, BEACH, BIGCITY, BUSSTOP, CENTER, COUNTRYSIDE, FAIRSITE, FOREST,
	GOLF, LAKE, METRO, MOUNTAINSIDE, NEARCENTER, PARK, PLACEOFINTREST, PORT,
	PUBLICTRANSPORT, RESTAURANTS, RIVER,  SEA, SHOPPINGAREA, SKIAREA, STATION,
	SUBURBS, TOURISTCENTRE, UNKNOWN;
	
	@Override
	public String toString() {
		return name().toLowerCase();
	}

}

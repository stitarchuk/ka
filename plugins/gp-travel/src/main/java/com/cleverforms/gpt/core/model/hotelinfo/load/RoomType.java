/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.shared.marker.HasCode;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.gpt.core.model.XmlizableLocalizableString;

/**
 * A base {@code ValueProxy} for {@code RoomType} info data.
 * @author Sergey Titarchuk
 */
public class RoomType extends Amenity implements HasCode<String> {

	/** The serial version UID */
	private static final long serialVersionUID = 8099846585434819191L;

	/** Key to services property */
	public final static String SERVICES = "services";

	/** Key to extra beds property */
	public final static String EXTRA_BEDS = "extraBeds";

	/** Key to max occupancy property */
	public final static String MAX_OCCUPANCY = "maxOccupancy";

	/** Key to max sharing place property */
	public final static String MAX_SHARING_PLACE = "maxSharingPlace";

	/** Key to max total occupancy property */
	public final static String MAX_TOTAL_OCCUPANCY = "maxTotalOccupancy";

	/** Key to min occupancy property */
	public final static String MIN_OCCUPANCY = "minOccupancy";

	/** Key to min total occupancy property */
	public final static String MIN_TOTAL_OCCUPANCY = "minTotalOccupancy";

	/** Key to room category property */
	public final static String ROOM_CATEGORY = "roomCategory";

	private String code;
	private RoomService[] services;
	private Integer extraBeds;
	private int maxOccupancy;
	private int minOccupancy;
	private int maxTotalOccupancy;
	private int minTotalOccupancy;
	private XmlizableLocalizableString roomCategory;
	private Integer maxSharingPlace;

	public RoomType() {}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	public RoomService[] getServices() {
		return services;
	}

	public void setServices(RoomService[] services) {
		this.services = services;
	}

	public Integer getExtraBeds() {
		return extraBeds;
	}

	public void setExtraBeds(Integer extraBeds) {
		this.extraBeds = extraBeds;
	}

	public int getMaxOccupancy() {
		return maxOccupancy;
	}

	public void setMaxOccupancy(int maxOccupancy) {
		this.maxOccupancy = maxOccupancy;
	}

	public int getMinOccupancy() {
		return minOccupancy;
	}

	public void setMinOccupancy(int minOccupancy) {
		this.minOccupancy = minOccupancy;
	}

	public int getMaxTotalOccupancy() {
		return maxTotalOccupancy;
	}

	public void setMaxTotalOccupancy(int maxTotalOccupancy) {
		this.maxTotalOccupancy = maxTotalOccupancy;
	}

	public int getMinTotalOccupancy() {
		return minTotalOccupancy;
	}

	public void setMinTotalOccupancy(int minTotalOccupancy) {
		this.minTotalOccupancy = minTotalOccupancy;
	}

	public XmlizableLocalizableString getRoomCategory() {
		return roomCategory;
	}

	public void setRoomCategory(XmlizableLocalizableString roomCategory) {
		this.roomCategory = roomCategory;
	}

	public Integer getMaxSharingPlace() {
		return maxSharingPlace;
	}

	public void setMaxSharingPlace(Integer maxSharingPlace) {
		this.maxSharingPlace = maxSharingPlace;
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper()
			.add(CODE, code)
			.add(SERVICES, services)
			.add(EXTRA_BEDS, extraBeds)
			.add(MAX_OCCUPANCY, maxOccupancy)
			.add(MIN_OCCUPANCY, minOccupancy)
			.add(MAX_TOTAL_OCCUPANCY, maxTotalOccupancy)
			.add(MIN_TOTAL_OCCUPANCY, minTotalOccupancy)
			.add(ROOM_CATEGORY, roomCategory)
			.add(MAX_SHARING_PLACE, maxSharingPlace);
	}
	
}

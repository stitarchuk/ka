/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.shared.proxy.ValueProxy;
import com.cleverforms.comms.shared.util.Format;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.gpt.core.model.XmlizableLocalizableString;

/**
 * A base {@code ValueProxy} for {@code OTA} info data.
 * @author Sergey Titarchuk
 */
public class OTAInfo implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = -2466251558752090052L;

	/** Key to OTA code property */
	public static final String OTA_CODE = "otaCode";

	/** Key to OTA name property */
	public static final String OTA_NAME = "otaName";

	private String otaCode;
	private XmlizableLocalizableString otaName;

	public OTAInfo() {}

	public String getOtaCode() {
		return otaCode;
	}

	public void setOtaCode(String otaCode) {
		this.otaCode = otaCode;
	}

	public XmlizableLocalizableString getOtaName() {
		return otaName;
	}

	public void setOtaName(XmlizableLocalizableString otaName) {
		this.otaName = otaName;
	}

	protected ObjectToStringHelper stringHelper() {
		return Format.toStringHelper(this)
			.add(OTA_CODE, otaCode)
			.add(OTA_NAME, otaName);
	}

	@Override
	public String toString() {
		return stringHelper().toString();
	}

}

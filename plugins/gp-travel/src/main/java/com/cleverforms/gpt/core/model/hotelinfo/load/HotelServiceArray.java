/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.shared.proxy.ValueProxy;

/**
 * A wrapper class for wrap {@link HotelService} as Array.
 * @author Sergey Titarchuk
 */
public class HotelServiceArray implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = -1599040256173782643L;

	private HotelService[] service;

	public HotelServiceArray() {}

	public HotelService[] getService() {
		return service;
	}

	public void setService(HotelService[] service) {
		this.service = service;
	}

	@Override
	public String toString() {
		return Casting.arrayToString(service);
	}

}

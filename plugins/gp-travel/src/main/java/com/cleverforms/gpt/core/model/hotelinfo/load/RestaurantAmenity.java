/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 22, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.gpt.core.model.XmlizableLocalizableString;

/**
 * A base {@code ValueProxy} for {@code Restaurant} data.
 * @author Sergey Titarchuk
 */
public class RestaurantAmenity extends TypedAmenity  {

	/** The serial version UID */
	private static final long serialVersionUID = 6732201544228585576L;

	/** Key to cuisine property */
	public final static String CUISINE = "cuisine";

	/** Key to location property */
	public final static String LOCATION = "location";

	private String location;
	private XmlizableLocalizableString cuisine;

	public RestaurantAmenity() {}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public XmlizableLocalizableString getCuisine() {
		return cuisine;
	}

	public void setCuisine(XmlizableLocalizableString cuisine) {
		this.cuisine = cuisine;
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper()
			.add(LOCATION, location)
			.add(CUISINE, cuisine);
	}

}

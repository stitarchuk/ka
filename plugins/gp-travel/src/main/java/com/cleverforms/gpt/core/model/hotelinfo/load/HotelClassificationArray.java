/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.shared.proxy.ValueProxy;

/**
 * A wrapper class for wrap {@link HotelClassification} as Array.
 * @author Sergey Titarchuk
 */
public class HotelClassificationArray implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = -3548870378211818044L;

	private HotelClassification[] classification;
	
	public HotelClassificationArray() {}

	public HotelClassification[] getClassification() {
		return classification;
	}

	public void setClassification(HotelClassification[] classification) {
		this.classification = classification;
	}

	@Override
	public String toString() {
		return Casting.arrayToString(classification);
	}

}

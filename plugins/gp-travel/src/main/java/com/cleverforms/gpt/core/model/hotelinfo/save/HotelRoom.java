/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.save;

import java.util.Locale;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.gpt.core.GPTConnector;
import com.cleverforms.gpt.core.GPTUtil;
import com.cleverforms.gpt.core.model.NamedDescriptionModel;
import com.cleverforms.gpt.core.model.RoomOTACode;
import com.cleverforms.iss.server.model.hotel.RoomMap;
import com.cleverforms.iss.server.model.hotel.RoomType;
import com.cleverforms.iss.shared.hotel.RoomMapProperty;

/**
 * A base {@code ValueProxy} for {@code Room} API info data
 * @author Sergey Titarchuk
 */
public class HotelRoom extends NamedDescriptionModel {

	/** The serial version UID */
	private static final long serialVersionUID = -5614166203058231405L;

	/** Key to category ID property */
	public static final String CATEGORY_ID = "categoryId";

	/** Key to OTA code property */
	public static final String OTA_CODE = "otaCode";

	/** Key to max extraplace property */
	public final static String MAX_EXTRAPLACE = "maxExtraplace";

	/** Key to max occupancy property */
	public final static String MAX_OCCUPANCY = "maxOccupancy";

	/** Key to max total occupancy property */
	public final static String MAX_TOTAL_OCCUPANCY = "maxTotalOccupancy";

	/** Key to max without place property */
	public final static String MAX_WITHOUT_PLACE = "maxWithoutPlace";

	/** Key to min occupancy property */
	public final static String MIN_OCCUPANCY = "minOccupancy";

	/** Key to services property */
	public final static String SERVICES = "services";

	private HotelRoomService[] services;
	private Long categoryId;
	private RoomOTACode otaCode;
	private int maxOccupancy;
	private int minOccupancy;
	private int maxExtraplace;
	private int maxWithoutPlace;
	private int maxTotalOccup;

	public HotelRoom() {}

	public HotelRoomService[] getServices() {
		return services;
	}

	public void setServices(HotelRoomService[] services) {
		this.services = services;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public RoomOTACode getOtaCode() {
		return otaCode;
	}

	public void setOtaCode(RoomOTACode otaCode) {
		this.otaCode = otaCode;
	}

	public int getMaxOccupancy() {
		return maxOccupancy;
	}

	public void setMaxOccupancy(int maxOccupancy) {
		this.maxOccupancy = maxOccupancy;
	}

	public int getMinOccupancy() {
		return minOccupancy;
	}

	public void setMinOccupancy(int minOccupancy) {
		this.minOccupancy = minOccupancy;
	}

	public int getMaxExtraplace() {
		return maxExtraplace;
	}

	public void setMaxExtraplace(int maxExtraplace) {
		this.maxExtraplace = maxExtraplace;
	}

	public int getMaxWithoutPlace() {
		return maxWithoutPlace;
	}

	public void setMaxWithoutPlace(int maxWithoutPlace) {
		this.maxWithoutPlace = maxWithoutPlace;
	}

	public int getMaxTotalOccup() {
		return maxTotalOccup;
	}

	public void setMaxTotalOccup(int maxTotalOccup) {
		this.maxTotalOccup = maxTotalOccup;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ services, categoryId,
			otaCode, minOccupancy, maxOccupancy, maxExtraplace, maxWithoutPlace, maxTotalOccup });
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof HotelRoom) {
			final HotelRoom model = (HotelRoom) obj;
			return super.equals(obj) 
				&& Util.equalsWithNull(services, model.services)
				&& Util.equalsWithNull(categoryId, model.categoryId)
				&& Util.equalsWithNull(otaCode, model.otaCode)
				&& minOccupancy == model.minOccupancy
				&& maxOccupancy == model.maxOccupancy
				&& maxExtraplace == model.maxExtraplace
				&& maxWithoutPlace == model.maxWithoutPlace
				&& maxTotalOccup == model.maxTotalOccup;

		} else if (obj instanceof RoomMap) {
			final RoomMap roomMap = (RoomMap) obj;
			final RoomType type = roomMap.getType();
			boolean equals = Util.equalsWithNull(maxExtraplace, roomMap.getExtraBeds());
			for (Locale l : Util.SUPPORTED_LOCALES) {
				final String lang = l.getLanguage();
				equals = equals && Util.equalsWithNull(roomMap.getProperty(roomMap.parseProperty("NAME", lang)), name(lang))
						&& Util.equalsWithNull(roomMap.getProperty(roomMap.parseProperty("DESCRIPTION", lang)), shortDescription(lang))
						&& Util.equalsWithNull(roomMap.getProperty(roomMap.parseProperty("FULL_DESCRIPTION", lang)), description(lang));

			}
			if (type != null) {
				equals = equals && maxOccupancy == type.getBedNumber()
						&& Util.equalsWithNull(otaCode, RoomOTACode.parse(Long.valueOf(type.getId()).intValue()));
			} else {
				equals = equals && maxOccupancy == 0 && otaCode == null;
			}
			return equals;
		}
		return false;
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper()
			.add(SERVICES, services)
			.add(CATEGORY_ID, categoryId)
			.add(OTA_CODE, otaCode)
			.add(MIN_OCCUPANCY, minOccupancy)
			.add(MAX_OCCUPANCY, maxOccupancy)
			.add(MAX_EXTRAPLACE, maxExtraplace)
			.add(MAX_WITHOUT_PLACE, maxWithoutPlace)
			.add(MAX_TOTAL_OCCUPANCY, maxTotalOccup);
	}

	public HotelRoom serialize(GPTConnector connector, RoomMap roomMap) {
		final long systemId = connector.getSystemId();
		final GPTUtil util = GPTUtil.get();
		setId(Casting.asLong(roomMap.getAlias(systemId), null));
		// category
		categoryId = roomMap.getCategory() != null ? Casting.asLong(roomMap.getCategory().getAlias(systemId), null) : null;
		// names
		setName(util.getLocalizedProperties(roomMap, RoomMapProperty.SEARCH_PROPERTIES));
		// descriptions
		setShortDescription(util.getLocalizedProperties(roomMap, RoomMapProperty.DESCRIPTION_PROPERTIES));
		setDescription(util.getLocalizedProperties(roomMap, RoomMapProperty.FULL_DESCRIPTION_PROPERTIES));
		// places
		minOccupancy = 1;
		if (roomMap.getType() != null) {
			maxOccupancy = maxTotalOccup = roomMap.getType().getBedNumber();
			otaCode = RoomOTACode.parse(Casting.asInteger(roomMap.getType().getId() - 1));
		} else {
			maxOccupancy = maxTotalOccup = minOccupancy;
			otaCode = null;
		}
		maxExtraplace = roomMap.getExtraBeds() != null ? roomMap.getExtraBeds().intValue() : 0;
		maxTotalOccup += maxExtraplace;
		setActive(true);
		return this;
	}

}

/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.shared.proxy.ValueProxy;

/**
 * A wrapper class for wrap {@link MealType} as Array.
 * @author Sergey Titarchuk
 */
public class MealTypeArray implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = 4385241603027502889L;

	private MealType[] mealType;

	public MealTypeArray() {}

	public MealType[] getMealType() {
		return mealType;
	}

	public void setMealType(MealType[] mealType) {
		this.mealType = mealType;
	}

	@Override
	public String toString() {
		return Casting.arrayToString(mealType);
	}

}

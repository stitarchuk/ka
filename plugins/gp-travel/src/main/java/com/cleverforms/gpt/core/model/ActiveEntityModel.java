/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model;

import com.cleverforms.comms.shared.marker.HasId;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;

/**
 * A base {@link ActiveModel} for {@code Entity} info data.
 * @author Sergey Titarchuk
 */
public class ActiveEntityModel extends ActiveModel implements HasId<Long> {

	/** The serial version UID */
	private static final long serialVersionUID = -3116269228377009250L;

	private Long id;
	
	public ActiveEntityModel() {
		this(0l, false);
	}

	public ActiveEntityModel(Long id, boolean active) {
		super(active);
		this.id = id;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String key() {
		return id != null ? id.toString() : null;
	}

	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper().add(ID, id).add(ACTIVE, getActive());
	}

}

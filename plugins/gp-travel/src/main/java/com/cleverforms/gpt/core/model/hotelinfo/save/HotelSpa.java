/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.save;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.gpt.core.model.NamedDescriptionModel;
import com.cleverforms.gpt.core.model.hotel.HotelSpaType;

/**
 * A base {@link NamedDescriptionModel} for hotel {@code SPA} info data.
 * @author Sergey Titarchuk
 */
public class HotelSpa extends NamedDescriptionModel {

	/** The serial version UID */
	private static final long serialVersionUID = 2780634192459315109L;

	/** Key to SPA type property */
	public final static String SPA_TYPE = "spaType";

	private HotelSpaType spaType;

	public HotelSpa() {}

	public HotelSpa(Long id, boolean active) {
		super(id, active);
	}

	public HotelSpaType getSpaType() {
		return spaType;
	}

	public void setSpaType(HotelSpaType spaType) {
		this.spaType = spaType;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ getId(), spaType });
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof HotelSpa) {
			final HotelSpa model = (HotelSpa) obj;
			return super.equals(obj) 
					&& Util.equalsWithNull(getId(), model.getId())
					&& Util.equalsWithNull(spaType, model.spaType);
		}
		return false;
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper().add(SPA_TYPE, spaType);
	}

}

/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 28, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.payload;

import com.cleverforms.comms.shared.proxy.ValueProxy;
import com.cleverforms.comms.shared.util.Format;
import com.cleverforms.gpt.core.model.hotel.HotelDictionaryInfo;

/**
 * A base payload for read dictionaries of {@code Hotel} info data.
 * @author Sergey Titarchuk
 */
public class HotelDictionariesInfoResponse implements ValueProxy {

	/** The serial version UID */
	private static final long serialVersionUID = 7781915249439291084L;

	/** Key to categories property */
	public static final String CATEGORIES = "categories";

	/** Key to hotel classifications property */
	public static final String HOTEL_CLASSIFICATIONS = "hotelClassifications";

	/** Key to hotel types property */
	public static final String HOTEL_TYPES = "hotelTypes";

	/** Key to services property */
	public static final String SERVICES = "services";

	private HotelDictionaryInfo[] services;
	private HotelDictionaryInfo[] categories;
	private HotelDictionaryInfo[] hotelClassifications;
	private HotelDictionaryInfo[] hotelTypes;
	
	public HotelDictionariesInfoResponse() {}

	public HotelDictionaryInfo[] getServices() {
		return services;
	}

	public void setServices(HotelDictionaryInfo[] services) {
		this.services = services;
	}

	public HotelDictionaryInfo[] getCategories() {
		return categories;
	}

	public void setCategories(HotelDictionaryInfo[] categories) {
		this.categories = categories;
	}

	public HotelDictionaryInfo[] getHotelClassifications() {
		return hotelClassifications;
	}

	public void setHotelClassifications(HotelDictionaryInfo[] hotelClassifications) {
		this.hotelClassifications = hotelClassifications;
	}

	public HotelDictionaryInfo[] getHotelTypes() {
		return hotelTypes;
	}

	public void setHotelTypes(HotelDictionaryInfo[] hotelTypes) {
		this.hotelTypes = hotelTypes;
	}

	@Override
	public String toString() {
		return Format.toStringHelper(this)
			.add(SERVICES, services)
			.add(CATEGORIES, categories)
			.add(HOTEL_CLASSIFICATIONS, hotelClassifications)
			.add(HOTEL_TYPES, hotelTypes)
			.toString();
	}

}

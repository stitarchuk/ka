/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;

/**
 * A base {@link ActiveEntityModel} for entities with name and descriptions.
 * @author Sergey Titarchuk
 */
public class NamedDescriptionModel extends ActiveEntityModel {

	/** The serial version UID */
	private static final long serialVersionUID = 2995079026940918360L;

	/** Key to description property */
	public final static String DESCRIPTION = "description";

	/** Key to name property */
	public final static String NAME = "name";

	/** Key to short description property */
	public final static String SHORT_DESCRIPTION = "shortDescription";

	private Map<String, String> name;
	private Map<String, String> description;
	private Map<String, String> shortDescription;

	public NamedDescriptionModel() {}

	public NamedDescriptionModel(Long id, boolean active) {
		super(id, active);
	}

	public Map<String, String> getName() {
		if (name == null) name = new HashMap<String, String>();
		return name;
	}

	public void setName(Map<String, String> name) {
		this.name = name;
	}

	public Map<String, String> getDescription() {
		if (description == null) description = new HashMap<String, String>();
		return description;
	}

	public void setDescription(Map<String, String> description) {
		this.description = description;
	}

	public Map<String, String> getShortDescription() {
		if (shortDescription == null) shortDescription = new HashMap<String, String>();
		return shortDescription;
	}

	public void setShortDescription(Map<String, String> shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String name(String locale) {
		return name != null && locale != null ? name.get(locale.toLowerCase()) : null;
	}

	public String description(String locale) {
		return description != null && locale != null ? description.get(locale.toLowerCase()) : null;
	}

	public String shortDescription(String locale) {
		return shortDescription != null && locale != null ? shortDescription.get(locale.toLowerCase()) : null;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ name, description, shortDescription });
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof NamedDescriptionModel) {
			final NamedDescriptionModel model = (NamedDescriptionModel) obj;
			return super.equals(obj) 
				&& Util.equalsWithNull(name, model.name)
				&& Util.equalsWithNull(description, model.description)
				&& Util.equalsWithNull(shortDescription, model.shortDescription);
		}
		return false;
	}
	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper()
			.add(NAME, name);
//			.add(DESCRIPTION, description)
//			.add(SHORT_DESCRIPTION, shortDescription);
	}

}

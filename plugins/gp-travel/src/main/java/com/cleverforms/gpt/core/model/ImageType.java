/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * February 20, 2020.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model;

import java.io.Serializable;

/**
 * A small enumerator for types of image.
 * @author Sergey Titarchuk
 */
public enum ImageType implements Serializable {

	FOR_CHILD(25),
	RESTAURANT(19),
	ROOMS(20),
	SPA(21),
	SPORT(29);

	private final int id;
	
	private ImageType(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

}

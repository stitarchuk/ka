/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.save;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.gpt.core.model.NamedDescriptionModel;
import com.cleverforms.gpt.core.model.hotel.MealTypeCode;

/**
 * A base {@link NamedDescriptionModel} for hotel {@code MealType} info data.
 * @author Sergey Titarchuk
 */
public class HotelMealType extends NamedDescriptionModel {

	/** The serial version UID */
	private static final long serialVersionUID = 6594625225361346290L;

	/** Key to meal type code property */
	public final static String MEAL_TYPE_CODE = "mealTypeCode";

	private MealTypeCode mealTypeCode;

	public HotelMealType() {}

	public HotelMealType(Long id, boolean active) {
		super(id, active);
	}

	public MealTypeCode getMealTypeCode() {
		return mealTypeCode;
	}

	public void setMealTypeCode(MealTypeCode mealTypeCode) {
		this.mealTypeCode = mealTypeCode;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ getId(), mealTypeCode });
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof HotelMealType) {
			final HotelMealType model = (HotelMealType) obj;
			return super.equals(obj) 
					&& Util.equalsWithNull(getId(), model.getId())
					&& Util.equalsWithNull(mealTypeCode, model.mealTypeCode);
		}
		return false;
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper().add(MEAL_TYPE_CODE, mealTypeCode);
	}


}

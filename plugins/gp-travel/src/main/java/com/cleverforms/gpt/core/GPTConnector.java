/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 21, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core;

import java.io.Serializable;
import java.util.Locale;

import org.springframework.http.HttpMethod;
import org.springframework.lang.NonNull;

import com.cleverforms.comms.server.event.EntityEvent;
import com.cleverforms.comms.shared.marker.HasCode;
import com.cleverforms.ics.db.model.pub.ExtSystem;
import com.cleverforms.iss.server.hotel.HotelConnector;
import com.cleverforms.iss.server.model.hotel.Hotel;
import com.cleverforms.iss.server.provider.ISSPersistentProvider;

/**
 * A marker interface for GP Travel Enterprise API base connector.
 * @author Sergey Titarchuk
 */
public interface GPTConnector extends HotelConnector, HasCode<String> {

	/** Content provider ID property */
	final long CONTENT_PROVIDER_ID = 1l;

	ISSPersistentProvider provider();

	/**
	 * Gets the GP Travel system ID in {@link ExtSystem} table.
	 * @return the GP Travel system ID.
	 */
	long getSystemId();

	/**
	 * Sets the GP Travel system ID in {@link ExtSystem} table.
	 * @param systemId the GP Travel system ID to set.
	 */
	void setSystemId(long systemId);

	/**
	 * Get the unique API Key that identifies the organization
	 * and application that requests the API.
	 * @return the unique API Key.
	 */
	String getApiKey();

	/**
	 * Sets the unique API Key that identifies the organization
	 * and application that requests the API.
	 * @param apiKey unique API Key to set.
	 */
	void setApiKey(String apiKey);

	/**
	 * Gets the user login for authorization.
	 * @return the user login for authorization.
	 */
	String getUsername();

	/**
	 * Sets the user login for authorization.
	 * @param username the user login to set.
	 */
	void setUsername(String username);

	/**
	 * Gets the user password for authorization.
	 * @return the user password for authorization
	 */
	String getPassword();

	/**
	 * Sets the user password for authorization.
	 * @param password the user password to set.
	 */
	void setPassword(String password);

	/**
	 * Get the API version number as String.
	 * @return the API version number as String.
	 */
	String getVersion();

	/**
	 * Sets the API version number as String.
	 * @param version the API version number to set.
	 */
	void setVersion(String version);

	String contextPath();

	void setContextPath(String contextPath);

	<T> T exchange(String uri, HttpMethod method, Locale locale, Serializable requestEntity, Class<T> responseType, Object... uriVariables);
	
	default <T> T create(String uri, Locale locale, Serializable requestEntity, Class<T> responseType, Object... uriVariables) {
		return exchange(uri, HttpMethod.POST, locale, requestEntity, responseType, uriVariables);
	}

	default <T> T get(String uri, Locale locale, Class<T> responseType, Object... uriVariables) {
		return exchange(uri, HttpMethod.GET, locale, null, responseType, uriVariables);
	}

	default <T> T update(String uri, Locale locale, Serializable requestEntity, Class<T> responseType, Object... uriVariables) {
		return exchange(uri, HttpMethod.PUT, locale, requestEntity, responseType, uriVariables);
	}

	Hotel syncHotel(Hotel hotel, Locale locale);

	void handleEntityAction(@NonNull EntityEvent event);

	@Override
	GPTConnector clone();

}

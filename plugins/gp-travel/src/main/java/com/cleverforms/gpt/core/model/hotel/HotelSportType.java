/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotel;

import java.io.Serializable;

/**
 * A small enumerator for sport type of {@code Hotel}.
 * @author Sergey Titarchuk
 */
public enum HotelSportType implements Serializable {

	CLUB("Club"),
	GYM("Gym");

	private final String title;
	
	private HotelSportType(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public String toString() {
		return title;
	}

}

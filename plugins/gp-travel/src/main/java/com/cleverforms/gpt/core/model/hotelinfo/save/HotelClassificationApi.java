/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.save;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.server.util.Util;
import com.cleverforms.gpt.core.model.ActiveEntityModel;
import com.cleverforms.gpt.core.model.hotelinfo.load.HotelClassification;

/**
 * A base {@link ActiveEntityModel} for hotel {@link HotelClassification} API info data.
 * @author Sergey Titarchuk
 */
public class HotelClassificationApi extends ActiveEntityModel {

	/** The serial version UID */
	private static final long serialVersionUID = -5805754654648563904L;

	public HotelClassificationApi() {}

	public HotelClassificationApi(Long id, boolean active) {
		super(id, active);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof HotelClassificationApi) {
			return super.equals(obj) && Util.equalsWithNull(getId(), ((HotelClassificationApi) obj).getId());
		}
		return false;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ getId() });
	}

}

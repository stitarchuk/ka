/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 23, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.gpt.core.model.ActiveModel;
import com.cleverforms.gpt.core.model.XmlizableLocalizableString;

/**
 * A base {@link ActiveModel} for {@code Service} info data
 * @author Sergey Titarchuk
 */
public class Service extends ActiveModel {

	/** The serial version UID */
	private static final long serialVersionUID = -289830670993686979L;

	/** Key to fee property */
	public final static String FEE = "fee";

	/** Key to OTA code property */
	public static final String OTA_CODE = "oTACode";

	/** Key to OTA name property */
	public static final String OTA_NAME = "oTAName";

	/** Key to quantity property */
	public final static String QUANTITY = "quantity";

	/** Key to supplier service name property */
	public static final String SUPPLIER_SERVICE_NAME = "supplierServiceName";

	/** Key to supplier specific code property */
	public static final String SUPPLIER_SPECIFIC_CODE = "supplierSpecificCode";

	private XmlizableLocalizableString oTAName;
	private Integer oTACode;
	private XmlizableLocalizableString supplierServiceName;
	private String supplierSpecificCode;
	private Fee fee;
	private Integer quantity;

	public Service() {}

	public XmlizableLocalizableString getoTAName() {
		return oTAName;
	}

	public void setoTAName(XmlizableLocalizableString oTAName) {
		this.oTAName = oTAName;
	}

	public Integer getoTACode() {
		return oTACode;
	}

	public void setoTACode(Integer oTACode) {
		this.oTACode = oTACode;
	}

	public XmlizableLocalizableString getSupplierServiceName() {
		return supplierServiceName;
	}

	public void setSupplierServiceName(XmlizableLocalizableString supplierServiceName) {
		this.supplierServiceName = supplierServiceName;
	}

	public String getSupplierSpecificCode() {
		return supplierSpecificCode;
	}

	public void setSupplierSpecificCode(String supplierSpecificCode) {
		this.supplierSpecificCode = supplierSpecificCode;
	}

	public Fee getFee() {
		return fee;
	}

	public void setFee(Fee fee) {
		this.fee = fee;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ oTACode, oTAName,
			supplierSpecificCode, supplierServiceName, fee, quantity });
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper()
			.add(OTA_CODE, oTACode)
			.add(OTA_NAME, oTAName)
			.add(SUPPLIER_SERVICE_NAME, supplierServiceName)
			.add(SUPPLIER_SPECIFIC_CODE, supplierSpecificCode)
			.add(FEE, fee)
			.add(QUANTITY, quantity)
			.add(ACTIVE, getActive());
	}

}

/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.save;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.server.util.Util;
import com.cleverforms.gpt.core.model.ActiveEntityModel;

/**
 * A base {@link ActiveEntityModel} for {@link HotelRoom} service info data
 * @author Sergey Titarchuk
 */
public class HotelRoomService extends ActiveEntityModel {

	/** The serial version UID */
	private static final long serialVersionUID = 5918347330762547482L;

	public HotelRoomService() {}

	public HotelRoomService(Long id, boolean active) {
		super(id, active);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof HotelRoomService) {
			return super.equals(obj) && Util.equalsWithNull(getId(), ((HotelRoomService) obj).getId());
		}
		return false;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ getId() });
	}

}

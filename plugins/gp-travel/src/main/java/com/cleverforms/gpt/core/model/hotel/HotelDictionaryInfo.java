/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 28, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotel;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.shared.marker.HasCode;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.gpt.core.model.ActiveEntityModel;
import com.cleverforms.gpt.core.model.XmlizableLocalizableString;
/**
 * A base {@link ActiveEntityModel} for {@code Dictionary} hotel info data
 * @author Sergey Titarchuk
 */
public class HotelDictionaryInfo extends ActiveEntityModel implements HasCode<String> {

	/** The serial version UID */
	private static final long serialVersionUID = 4527412367639207739L;

	/** Key to name property */
	public final static String NAME = "name";

	/** Key to supplier ID property */
	public final static String SUPPLIER_ID = "supplierId";

	private XmlizableLocalizableString name;
	private String code;
	private long supplierId;

	public HotelDictionaryInfo() {}

	public XmlizableLocalizableString getName() {
		return name;
	}

	public void setName(XmlizableLocalizableString name) {
		this.name = name;
	}

	@Override
	public String getCode() {
		return code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	public long getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(long supplierId) {
		this.supplierId = supplierId;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ name, code, Long.valueOf(supplierId) });
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof HotelDictionaryInfo) {
			final HotelDictionaryInfo model = (HotelDictionaryInfo) obj;
			return super.equals(obj) && supplierId == model.supplierId
				&& Util.equalsWithNull(name, model.name)
				&& Util.equalsWithNull(code, model.code);
		}
		return false;
	}
	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper()
			.add(NAME, name)
			.add(CODE, code)
			.add(SUPPLIER_ID, supplierId);
	}

}

/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.save;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;

import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.server.util.Util;
import com.cleverforms.comms.shared.marker.HasEmail;
import com.cleverforms.comms.shared.marker.HasPhone;
import com.cleverforms.comms.shared.util.Format;
import com.cleverforms.comms.shared.util.Format.ObjectToStringHelper;
import com.cleverforms.gpt.core.GPTConnector;
import com.cleverforms.gpt.core.GPTUtil;
import com.cleverforms.gpt.core.GPTUtil.HotelAddData;
import com.cleverforms.gpt.core.model.ActiveModel;
import com.cleverforms.gpt.core.model.hotel.HotelCategory;
import com.cleverforms.gpt.core.model.hotel.HotelDestinationTypes;
import com.cleverforms.ics.db.model.dictionary.City;
import com.cleverforms.iss.server.model.hotel.Hotel;
import com.cleverforms.iss.shared.hotel.HotelProperty;

/**
 * A base request payload for create or update {@code Hotel} info data
 * @author Sergey Titarchuk
 */
public class HotelDescriptionInfoRequest extends ActiveModel implements HasEmail, HasPhone {

	/** The serial version UID */
	private static final long serialVersionUID = 9151838194900992796L;

	/** The URI template of method */
	public static final String URI_METHOD_CREATE = "/hotelInfo";

	/** The URI template of method */
	public static final String URI_METHOD_UPDATE = URI_METHOD_CREATE + "?hotelId={0}";

	/** Key to category ID property */
	public final static String CATEGORY_ID = "categoryId";

	/** Key to chain ID property */
	public final static String CHAIN_ID = "chainId";
	
	/** Key to check-in from property */
	public final static String CHECK_IN_FROM = "checkInFrom";
	
	/** Key to check-in to property */
	public final static String CHECK_IN_TO = "checkInTo";

	/** Key to check-out from property */
	public final static String CHECK_OUT_FROM = "checkOutFrom";

	/** Key to check-out to property */
	public final static String CHECK_OUT_TO = "checkOutTo";

	/** Key to city ID property */
	public final static String CITY_ID = "cityId";

	/** Key to content provider ID property */
	public final static String CONTENT_PROVIDER_ID = "contentProviderId";

	/** Key to country ID property */
	public final static String COUNTRY_ID = "countryId";

	/** Key to district ID property */
	public final static String DISTRICT_ID = "districtId";

	/** Key to description localizable property */
	public final static String DESCRIPTION_LOCALIZABLE = "descriptionLocalizable";

	/** Key to description type property */
	public final static String DESCRIPTION_TYPE = "descriptionType";

	/** Key to destination types property */
	public final static String DESTINATION_TYPES = "destinationTypes";

	/** Key to fax property */
	public final static String FAX = "fax";

	/** Key to full info available property */
	public final static String FULL_INFO_AVAILABLE = "fullInfoAvailable";

	/** Key to hotel classifications property */
	public final static String HOTEL_CLASSIFICATIONS = "hotelClassifications";

	/** Key to hotel code property */
	public final static String HOTEL_CODE = "hotelCode";

	/** Key to hotel address localizable property */
	public final static String HOTEL_ADDRESS_LOCALIZABLE = "hotelAddressLocalizable";

	/** Key to hotel meal types property */
	public final static String HOTEL_MEAL_TYPES = "hotelMealTypes";

	/** Key to hotel name localizable property */
	public final static String HOTEL_NAME_LOCALIZABLE = "hotelNameLocalizable";

	/** Key to hotel services property */
	public final static String HOTEL_SERVICES = "hotelServices";

	/** Key to hotel type ID property */
	public final static String HOTEL_TYPE_ID = "hotelTypeId";

	/** Key to images property */
	public final static String IMAGES = "images";

	/** Key to kids property */
	public final static String KIDS = "kids";

	/** Key to latitude property */
	public final static String LATITUDE = "latitude";

	/** Key to link to TripAdvisor property */
	public final static String LINK_TO_TRIP_ADVISOR = "linkToTripAdvisor";

	/** Key to logo image property */
	public final static String LOGO_IMAGE = "logoImage";

	/** Key to longitude property */
	public final static String LONGITUDE = "longitude";

	/** Key to main image property */
	public final static String MAIN_IMAGE = "mainImage";

	/** Key to next day checkout property */
	public final static String NEXT_DAY_CHECKOUT = "nextDayCheckout";

	/** Key to rating property */
	public final static String RATING = "rating";

	/** Key to restaurants property */
	public final static String RESTAURANTS = "restaurants";

	/** Key to rooms property */
	public final static String ROOMS = "rooms";

	/** Key to SPA property */
	public final static String SPA = "spa";

	/** Key to sports property */
	public final static String SPORTS = "sports";

	/** Key to standard category property */
	public final static String STD_CATEGORY = "stdCategory";

	/** Key to url property */
	public final static String URL = "url";

	/** Key to zip property */
	public final static String ZIP = "zip";

	private Long contentProviderId;
	private String hotelCode;
	private Map<String, String> hotelNameLocalizable;
	private HotelCategory stdCategory;
	// get it from /hotelDictionaryInfo method,
	private Long categoryId;
	private String email;
	private String phone;
	private String fax;
	private String url;
	private Long countryId;
	private Long cityId;
	private Long districtId;
	private String zip;
	private Double latitude;
	private Double longitude;
	private Map<String, String> hotelAddressLocalizable;
	private Long chainId;
	private Long hotelTypeId;
	private String descriptionType;
	private Map<String, String> descriptionLocalizable;
	private HotelServiceApi[] hotelServices;
	private HotelClassificationApi[] hotelClassifications;
	private HotelRoom[] rooms;
	private double rating;
	private String linkToTripAdvisor;
	private HotelDestinationTypes[] destinationTypes;
	private String checkInFrom;
	private String checkInTo;
	private String checkOutFrom;
	private String checkOutTo;
	private HotelMealType[] hotelMealTypes;
	private HotelSpa[] spa;
	private HotelSport[] sports;
	private HotelKids[] kids;
	private HotelRestaurant[] restaurants;
	private Image[] images;
	private boolean fullInfoAvailable;
	private boolean nextDayCheckout;

	public HotelDescriptionInfoRequest() {}

	public HotelDescriptionInfoRequest(long contentProviderId) {
		this.contentProviderId = contentProviderId;
	}

	public Long getContentProviderId() {
		return contentProviderId;
	}

	public void setContentProviderId(Long contentProviderId) {
		this.contentProviderId = contentProviderId;
	}

	public String getHotelCode() {
		return hotelCode;
	}

	public void setHotelCode(String hotelCode) {
		this.hotelCode = hotelCode;
	}

	public Map<String, String> getHotelNameLocalizable() {
		if (hotelNameLocalizable == null) hotelNameLocalizable = new HashMap<String, String>();
		return hotelNameLocalizable;
	}

	public void setHotelNameLocalizable(Map<String, String> hotelNameLocalizable) {
		this.hotelNameLocalizable = hotelNameLocalizable;
	}

	public HotelCategory getStdCategory() {
		return stdCategory;
	}

	public void setStdCategory(HotelCategory stdCategory) {
		this.stdCategory = stdCategory;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Long getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Map<String, String> getHotelAddressLocalizable() {
		if (hotelAddressLocalizable == null) hotelAddressLocalizable = new HashMap<String, String>();
		return hotelAddressLocalizable;
	}

	public void setHotelAddressLocalizable(Map<String, String> hotelAddressLocalizable) {
		this.hotelAddressLocalizable = hotelAddressLocalizable;
	}

	public Long getChainId() {
		return chainId;
	}

	public void setChainId(Long chainId) {
		this.chainId = chainId;
	}

	public Long getHotelTypeId() {
		return hotelTypeId;
	}

	public void setHotelTypeId(Long hotelTypeId) {
		this.hotelTypeId = hotelTypeId;
	}

	public String getDescriptionType() {
		return descriptionType;
	}

	public void setDescriptionType(String descriptionType) {
		this.descriptionType = descriptionType;
	}

	public Map<String, String> getDescriptionLocalizable() {
		if (descriptionLocalizable == null) descriptionLocalizable = new HashMap<String, String>();
		return descriptionLocalizable;
	}

	public void setDescriptionLocalizable(Map<String, String> descriptionLocalizable) {
		this.descriptionLocalizable = descriptionLocalizable;
	}

	public HotelServiceApi[] getHotelServices() {
		if (hotelServices == null) hotelServices = new HotelServiceApi[0];
		return hotelServices;
	}

	public void setHotelServices(HotelServiceApi[] hotelServices) {
		this.hotelServices = hotelServices;
	}

	public HotelClassificationApi[] getHotelClassifications() {
		if (hotelClassifications == null) hotelClassifications = new HotelClassificationApi[0];
		return hotelClassifications;
	}

	public void setHotelClassifications(HotelClassificationApi[] hotelClassifications) {
		this.hotelClassifications = hotelClassifications;
	}

	public HotelRoom[] getRooms() {
		if (rooms == null) rooms = new HotelRoom[0];
		return rooms;
	}

	public void setRooms(HotelRoom[] rooms) {
		this.rooms = rooms;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public String getLinkToTripAdvisor() {
		return linkToTripAdvisor;
	}

	public void setLinkToTripAdvisor(String linkToTripAdvisor) {
		this.linkToTripAdvisor = linkToTripAdvisor;
	}

	public HotelDestinationTypes[] getDestinationTypes() {
		if (destinationTypes == null) destinationTypes = new HotelDestinationTypes[0];
		return destinationTypes;
	}

	public void setDestinationTypes(HotelDestinationTypes[] destinationTypes) {
		this.destinationTypes = destinationTypes;
	}

	public String getCheckInFrom() {
		return checkInFrom;
	}

	public void setCheckInFrom(String checkInFrom) {
		this.checkInFrom = checkInFrom;
	}

	public String getCheckInTo() {
		return checkInTo;
	}

	public void setCheckInTo(String checkInTo) {
		this.checkInTo = checkInTo;
	}

	public String getCheckOutFrom() {
		return checkOutFrom;
	}

	public void setCheckOutFrom(String checkOutFrom) {
		this.checkOutFrom = checkOutFrom;
	}

	public String getCheckOutTo() {
		return checkOutTo;
	}

	public void setCheckOutTo(String checkOutTo) {
		this.checkOutTo = checkOutTo;
	}

	public HotelMealType[] getHotelMealTypes() {
		if (hotelMealTypes ==  null) hotelMealTypes = new HotelMealType[0];
		return hotelMealTypes;
	}

	public void setHotelMealTypes(HotelMealType[] hotelMealTypes) {
		this.hotelMealTypes = hotelMealTypes;
	}

	public HotelSpa[] getSpa() {
		if (spa == null) spa = new HotelSpa[0];
		return spa;
	}

	public void setSpa(HotelSpa[] spa) {
		this.spa = spa;
	}

	public HotelSport[] getSports() {
		if (sports ==  null) sports = new HotelSport[0];
		return sports;
	}

	public void setSports(HotelSport[] sports) {
		this.sports = sports;
	}

	public HotelKids[] getKids() {
		if (kids ==  null) kids = new HotelKids[0];
		return kids;
	}

	public void setKids(HotelKids[] kids) {
		this.kids = kids;
	}

	public HotelRestaurant[] getRestaurants() {
		if (restaurants ==  null) restaurants = new HotelRestaurant[0];
		return restaurants;
	}

	public void setRestaurants(HotelRestaurant[] restaurants) {
		this.restaurants = restaurants;
	}

	public Image[] getImages() {
		if (images ==  null) images = new Image[0];
		return images;
	}

	public void setImages(Image[] images) {
		this.images = images;
	}

	public boolean getFullInfoAvailable() {
		return fullInfoAvailable;
	}

	public void setFullInfoAvailable(Boolean fullInfoAvailable) {
		this.fullInfoAvailable = fullInfoAvailable != null ? fullInfoAvailable.booleanValue() : false;
	}

	public boolean getNextDayCheckout() {
		return nextDayCheckout;
	}

	public void setNextDayCheckout(Boolean nextDayCheckout) {
		this.nextDayCheckout = nextDayCheckout != null ? nextDayCheckout.booleanValue() : false;
	}

	@Override
	protected Object[] hashFields() {
		return ArrayUtils.addAll(super.hashFields(), new Object[]{ hotelCode, hotelNameLocalizable, stdCategory,
			categoryId, email, phone, fax, url, countryId, cityId, districtId, zip, latitude, longitude,
			hotelAddressLocalizable, chainId, hotelTypeId, descriptionType, descriptionLocalizable, hotelServices,
			hotelClassifications, Double.valueOf(rating), linkToTripAdvisor, destinationTypes, checkInFrom, 
			checkInTo, checkOutFrom, checkOutTo, Boolean.valueOf(fullInfoAvailable), Boolean.valueOf(nextDayCheckout) });
	}

	@Override
	protected ObjectToStringHelper stringHelper() {
		return super.stringHelper()
			.add(HOTEL_CODE, hotelCode)
			.add(HOTEL_NAME_LOCALIZABLE, hotelNameLocalizable)
			.add(STD_CATEGORY, stdCategory)
			.add(CATEGORY_ID, categoryId)
			.add(E_MAIL, email)
			.add(PHONE, phone)
			.add(FAX, fax)
			.add(URL, url)
			.add(COUNTRY_ID, countryId)
			.add(CITY_ID, cityId)
			.add(DISTRICT_ID, districtId)
			.add(ZIP, zip)
			.add(LATITUDE, latitude)
			.add(LONGITUDE, longitude)
			.add(HOTEL_ADDRESS_LOCALIZABLE, hotelAddressLocalizable)
			.add(CONTENT_PROVIDER_ID, contentProviderId)
			.add(CHAIN_ID, chainId)
			.add(HOTEL_TYPE_ID, hotelTypeId)
			.add(DESCRIPTION_TYPE, descriptionType)
			.add(DESCRIPTION_LOCALIZABLE, descriptionLocalizable)
			.add(HOTEL_SERVICES, hotelServices)
			.add(HOTEL_CLASSIFICATIONS, hotelClassifications)
			.add(ROOMS, rooms)
			.add(RATING, rating)
			.add(LINK_TO_TRIP_ADVISOR, linkToTripAdvisor)
			.add(DESTINATION_TYPES, destinationTypes)
			.add(CHECK_IN_FROM, checkInFrom)
			.add(CHECK_IN_TO, checkInTo)
			.add(CHECK_OUT_FROM, checkOutFrom)
			.add(CHECK_OUT_TO, checkOutTo)
			.add(HOTEL_MEAL_TYPES, hotelMealTypes)
			.add(SPA, spa)
			.add(SPORTS, sports)
			.add(KIDS, kids)
			.add(RESTAURANTS, restaurants)
			.add(IMAGES, images)
			.add(ACTIVE, getActive())
			.add(FULL_INFO_AVAILABLE, fullInfoAvailable)
			.add(NEXT_DAY_CHECKOUT, nextDayCheckout);
	}

	public HotelDescriptionInfoRequest serialize(GPTConnector connector, Hotel hotel) {
		if (hotel == null || connector == null) return null;
		final long systemId = connector.getSystemId();
		final GPTUtil util = GPTUtil.get();
		contentProviderId = GPTConnector.CONTENT_PROVIDER_ID;
		// use contentProviderId = 1 always
		hotelCode = "KIYAVIA" + hotel.getId();
		if (hotel.getHotelClass() != null) {
			stdCategory = HotelCategory.getByRating(hotel.getHotelClass().getRating());
			categoryId = Casting.asLong(hotel.getHotelClass().getAlias(systemId), null);
		}
		if (hotel.getHotelType() != null) hotelTypeId = Casting.asLong(hotel.getHotelType().getAlias(systemId), null);
		// names
		hotelNameLocalizable = util.getLocalizedProperties(hotel, HotelProperty.SEARCH_PROPERTIES);
		// addresses
		hotelAddressLocalizable = util.getLocalizedProperties(hotel, HotelProperty.ADDRESS_PROPERTIES, address -> {
			if (Util.isBlank(zip)) zip = Format.parseAddress(address).getZipCode();
		});
		// description
		descriptionLocalizable = util.getLocalizedProperties(hotel, HotelProperty.FULL_DESCRIPTION_PROPERTIES);
		if (descriptionLocalizable != null) descriptionType = "General";
		// other contacts
		email = hotel.getProperty(HotelProperty.EMAIL);
		phone = Format.phoneNumber(hotel.getProperty(HotelProperty.PHONE));
		fax = Format.phoneNumber(hotel.getProperty(HotelProperty.FAX));
		url = hotel.getProperty(HotelProperty.SITE);
		// geolocation
		final City city = hotel.getCity();
		if (city != null) {
			cityId = Casting.asLong(city.getAlias(systemId), null);
			if (city.getCountry() != null) countryId = Casting.asLong(city.getCountry().getAlias(systemId), null);
			if (city.getRegion() != null) districtId = Casting.asLong(city.getRegion().getAlias(systemId), null);
		}
		latitude = Casting.asDouble(hotel.getProperty(HotelProperty.LATITUDE), null);
		longitude = Casting.asDouble(hotel.getProperty(HotelProperty.LONGITUDE), null);
		// step 0.5
		rating = hotel.getRating() != null ? Util.roundToHalf(hotel.getRating().doubleValue()) : 0;
		// classifications and services
		final HotelAddData hotelAddData = util.parseHotelAddData(hotel);
		hotelClassifications = hotelAddData.hotelClassifications();
//		hotelServices = hotelAddData.hotelServices();
		if (Util.isNotBlank(hotel.getAlias(systemId))) {
			// images
			final Path mainImage = hotel.mainImage();
			images = util.serializeImages(connector.contextPath(), hotel.getId(), Hotel.FOLDER, Casting.asLong(hotel.getAlias(systemId), null), mainImage);
		}
		setActive(true);
		return this;
	}

}

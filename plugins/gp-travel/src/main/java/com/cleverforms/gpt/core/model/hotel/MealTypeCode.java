/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotel;

import java.io.Serializable;

/**
 * A small enumerator for meal type code of {@code Hotel}.
 * @author Sergey Titarchuk
 */
public enum MealTypeCode implements Serializable {

	ALL("ALL INCLUSIVE"),
	A_LA_CARTE("A-LA CARTE"),
	ABF("AMERICAN BREAKFAST"),
	BB("BED & BREAKFAST"),
	BBT("BED & BREAKFAST AND TREATMENT"),
	BBF("BUFFET BREAKFAST"),
	CBF("CONTINENTAL BREAKFAST"),
	DNR("DINNER"),
	EBF("ENGLISH BREAKFAST"),
	FB("FULL BOARD"),
	FBT("FULL BOARD AND TREATMENT"),
	HB("HALF BOARD"),
	HBT("HALF BOARD AND TREATMENT"),
	RO("ROOM ONLY");

	private final String title;
	
	private MealTypeCode(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}

}

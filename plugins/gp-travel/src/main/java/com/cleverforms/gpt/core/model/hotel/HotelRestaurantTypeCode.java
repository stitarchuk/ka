/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 24, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotel;

import java.io.Serializable;

import com.cleverforms.iss.shared.hotel.CoffeeRoomType;

/**
 * A small enumerator for restaurant type code of {@code Hotel}.
 * @author Sergey Titarchuk
 */
public enum HotelRestaurantTypeCode implements Serializable {

	B("BAR"),
	R("RESTAURANT"),
	C("COFFEE_SHOP");

	private final String title;
	
	private HotelRestaurantTypeCode(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}

	public static synchronized HotelRestaurantTypeCode parse(CoffeeRoomType type) {
		if (type != null) {
			switch (type) {
				case BAR: return B;
				case CAFE:
				case BUFFET:
				case BUFFET_BREAKFAST: return C;
				case RESTAURANT:
				default: break;
			}
		}
		return R;
	}

}

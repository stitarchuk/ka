/*
 * GP Travel Enterprise API connector library.
 * Copyright(c) 2019-2024. 
 * October 23, 2019.
 * gp-travel
 * 
 */
package com.cleverforms.gpt.core.model.hotelinfo.load;

import java.util.Arrays;

import com.cleverforms.comms.shared.marker.HasCurrency;
import com.cleverforms.comms.shared.marker.HasUnit;
import com.cleverforms.comms.shared.proxy.ValueProxy;
import com.cleverforms.comms.shared.util.Format;

/**
 * A base {@code ValueProxy} for hotel {@code Fee} info data.
 * @author Sergey Titarchuk
 */
public class Fee implements ValueProxy, HasCurrency<String>, HasUnit<String> {

	/** The serial version UID */
	private static final long serialVersionUID = -1791053494305730484L;

	/** Key to amount property */
	public final static String AMOUNT = "amount";

	/** Key to fee property */
	public final static String FEE = "fee";

	/** Key to frequency property */
	public final static String FREQUENCY = "frequency";

	private String currency;
	private Double amount;
	private Boolean fee;
	private String unit;
	private String frequency;

	public Fee() {}

	@Override
	public String getCurrency() {
		return currency;
	}

	@Override
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Boolean getFee() {
		return fee;
	}

	public void setFee(Boolean fee) {
		this.fee = fee;
	}

	@Override
	public String getUnit() {
		return unit;
	}

	@Override
	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	@Override
	public String toString() {
		return Format.toStringHelper(this)
			.add(AMOUNT, amount)
			.add(CURRENCY, currency)
			.add(FEE, fee)
			.add(UNIT, unit)
			.add(FREQUENCY, frequency)
			.toString();
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(new Object[] { currency, amount, fee, unit, frequency });
	}

}

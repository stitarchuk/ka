package com.cleverforms.parser;

import java.io.File;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import com.cleverforms.comms.server.WithLoggerImpl;
import com.cleverforms.iss.server.provider.ISSPersistentProvider;
import com.cleverforms.parser.exchangerate.ExchangeRateParser;

@Rollback(true)
@Transactional
@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = { "file:src/test/resources/applicationContext.xml" })
public class ParserTest extends WithLoggerImpl {
	
	@Autowired
	public ISSPersistentProvider provider;

	@Test
	@Rollback(false)
	public void testOfflineFileListeners() {
		try {
			Thread.sleep(Long.MAX_VALUE);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		debug("end");
	}

	@Test
	public void testExchangeParser() {
		try {
			new ExchangeRateParser().parse(new File("C:\\Users\\perekladov\\Desktop\\currency.xml"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

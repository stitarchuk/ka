package com.cleverforms.parser;

import java.util.Date;

public abstract class DateUtils {
	@SuppressWarnings("deprecation")
	public static Date dateTime(Date date, Date time) {
		return new Date(date.getYear(), date.getMonth(), date.getDate(), time.getHours(), time.getMinutes(), time.getSeconds());
	}
	
	@SuppressWarnings("deprecation")
	public static Date utcToLocal(Date date) {
		return new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
	}
}

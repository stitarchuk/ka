package com.cleverforms.parser.pegasus;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cleverforms.comms.server.tasks.FileParserImpl;
import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.ics.core.provider.ICSPersistentProvider;
import com.cleverforms.ics.db.model.dictionary.Consumer;
import com.cleverforms.ics.db.model.orgchart.Sector;
import com.cleverforms.ics.shared.enumerator.OrderState;
import com.cleverforms.iss.server.model.airline.AirlineRoute;
import com.cleverforms.iss.server.model.airline.AirlineSegment;
import com.cleverforms.iss.server.model.airline.AirlineTariff;
import com.cleverforms.iss.server.model.airline.AirlineTicket;
import com.cleverforms.iss.shared.airline.AirlineTicketType;
import com.cleverforms.parser.Parser;
import com.cleverforms.parser.XmlUtils;

public class PegasusParser extends FileParserImpl {

	@Autowired
	Parser parser;

	public final Sector sector = new Sector();

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

	@Override
	public ICSPersistentProvider provider() {
		return (ICSPersistentProvider) super.provider();
	}

	public void setSectorId(long sectorId) {
		sector.setId(sectorId);
	}

	@Override
	public String name() {
		return "Pegasus data parser";
	}

	@Override
	public boolean parseFile(final File file) {
		return provider().txExecute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus status) {
				try {
					DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document document = documentBuilder.parse(file);
					XPath xPath = XPathFactory.newInstance().newXPath();

					AirlineTicket ticket = new AirlineTicket();

					ticket.setSector(sector);

					ticket.setState(OrderState.SALE);

					ticket.setExtNumber("PEGASUS-" + xPath.compile("/TICKET/PNR").evaluate(document));

					ticket.setSupplier(parser.findCompany("PC"));

					ticket.setBaseCurrency(parser.findCurrency(xPath.compile("/TICKET/CURRENCY").evaluate(document)));
					ticket.setCurrency(parser.findCurrency("UAH"));

					Date docDate = sdf.parse(xPath.compile("/TICKET/TICKET_DATE").evaluate(document));
					ticket.setCreated(docDate);

					ticket.setNumber(xPath.compile("/TICKET/TICKET_NO").evaluate(document));
					Consumer passenger = parser.findConsumer(xPath.compile("/TICKET/PAX_NAME").evaluate(document), xPath.compile("/TICKET/PAX_SURNAME")
							.evaluate(document));
					ticket.setConsumer(passenger);
					ticket.setBaseAmount(Casting.asDouble(xPath.compile("/TICKET/TOTAL_AMOUNT").evaluate(document)));

					NodeList segmentElements = (NodeList) xPath.compile("/TICKET/COUPONS/COUPON").evaluate(document, XPathConstants.NODESET);

					AirlineRoute route = new AirlineRoute();
					route.addTicket(ticket);
					for (int i = 0; i < segmentElements.getLength(); i++) {
						Element item = (Element) segmentElements.item(i);

						AirlineSegment segment = new AirlineSegment();

						String flightNo = XmlUtils.readTextContent(item, "FLIGHT_NO");

						// Segment carrier
						segment.setCarrier(parser.findCompany(flightNo.substring(0, 2)));

						// Segment departure airport
						segment.setAirportFrom(parser.findAirportByCode(XmlUtils.readTextContent(item, "ORIGIN"), null));

						// Segment arrival airport
						segment.setAirportTo(parser.findAirportByCode(XmlUtils.readTextContent(item, "DESTINATION"), null));

						// Segment departure date
						segment.setDeparture(sdf.parse(XmlUtils.readTextContent(item, "DEP_DATE")));

						// Segment arrival date
						segment.setArrive(sdf.parse(XmlUtils.readTextContent(item, "ARR_DATE")));

						// Segment class
						segment.setTicketCategory(XmlUtils.readTextContent(item, "REZ_CLASS"));

						AirlineTariff tariff = new AirlineTariff();
						tariff.setCode(XmlUtils.readTextContent(item, "FAREBASIS"));
						tariff.setConsumer(passenger);
						segment.addTariff(tariff);

						// Segment flight number
						segment.setTripNumber(flightNo.substring(2));

						parser.addAirlineSegment(route, segment);
					}

					ticket.setValidUntil(route.getDeparture());

					ticket.setType(AirlineTicketType.TRAVEL);

					parser.getProvider().save(route);

					return true;
				} catch (XPathExpressionException | ParserConfigurationException | SAXException | IOException | ParseException e) {
					error("", e);
					return false;
				}
			}
		});
	}
}

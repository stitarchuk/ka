package com.cleverforms.parser.contentinn;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cleverforms.comms.server.tasks.FileParserImpl;
import com.cleverforms.comms.shared.util.Util;
import com.cleverforms.ics.core.provider.ICSPersistentProvider;
import com.cleverforms.ics.db.model.dictionary.Company;
import com.cleverforms.ics.db.model.dictionary.Currency;
import com.cleverforms.ics.db.model.orgchart.Person;
import com.cleverforms.ics.db.model.orgchart.Post;
import com.cleverforms.ics.db.model.orgchart.Sector;
import com.cleverforms.ics.shared.enumerator.OrderState;
import com.cleverforms.iss.server.model.hotel.HotelFood;
import com.cleverforms.iss.server.model.hotel.HotelService;
import com.cleverforms.iss.server.model.hotel.RoomCategory;
import com.cleverforms.iss.server.model.hotel.RoomType;
import com.cleverforms.parser.Parser;

public class ContentInnParser extends FileParserImpl implements InitializingBean {

	@Autowired
	Parser parser;

	public final Sector sector = new Sector();

	int supplierId;

	Set<String> resellerCodes = new HashSet<>();

	@Override
	public void afterPropertiesSet() {
		try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("resellerCodes"), Util.ENCODING_UTF_8))) {
			String line;
			while ((line = fileReader.readLine()) != null) {
				resellerCodes.add(line);
			}
		} catch (IOException e) {
			error("", e);
		}
	}

	@Override
	public ICSPersistentProvider provider() {
		return (ICSPersistentProvider) super.provider();
	}

	public void setSectorId(long sectorId) {
		sector.setId(sectorId);
	}

	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}

	@Override
	public String name() {
		return "ContentInn data parser";
	}

	@Override
	public boolean parseFile(final File file) {
		return provider().txExecute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus status) {
				try {
					DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document document = documentBuilder.parse(file);
					XPath xPath = XPathFactory.newInstance().newXPath();

					HotelService service = new HotelService();

					service.setSector(sector);

					String originalReservationId = xPath.compile("ReservationDetails/@OriginalReservationId").evaluate(document);
					if (!originalReservationId.isEmpty()) {
						warn("Original reservation id is not empty");
						return false;
					}

					String resellerCode = xPath.compile("ReservationDetails/Reseller/@Code").evaluate(document);
					String confirmationNumber = xPath.compile("ReservationDetails/@ConfirmationNumber").evaluate(document);
					service.setExtNumber("CONTENT_INN-" + (resellerCodes.contains(resellerCode) ? "CON2_" : "") + confirmationNumber);

					double baseAmount = Double.parseDouble(xPath.compile("ReservationDetails/SellingPrice[@Currency=\"UAH\"]/@Amount").evaluate(document));

					String fileStatus = xPath.compile("ReservationDetails/@Status").evaluate(document);
					switch (fileStatus) {
					case "OK": {
						service.setState(OrderState.SALE);
						long servicesWithSameNumberCount = (Long) parser.getProvider().getSingleResult(
								"select count(1) from HotelService where doc_number like '" + confirmationNumber + "%'");
						if (servicesWithSameNumberCount == 0) {
							service.setNumber(confirmationNumber);
						} else {
							service.setNumber(confirmationNumber + "/" + servicesWithSameNumberCount);
						}
						break;
					}
					case "XX": {
						service.setState(OrderState.REFUND);
						long servicesWithSameNumberCount = (Long) parser.getProvider().getSingleResult(
								"select count(1) from HotelService where doc_number like '" + confirmationNumber + "-R%'");
						if (servicesWithSameNumberCount == 0) {
							service.setNumber(confirmationNumber + "-R");
						} else {
							service.setNumber(confirmationNumber + "-R/" + servicesWithSameNumberCount);
						}

						baseAmount = -baseAmount;

						break;
					}
					default:
						warn("status: " + fileStatus);
						return false;
					}

					String voucherIssuedTimeString = xPath.compile("ReservationDetails/VoucherIssuedTime").evaluate(document);
					if (voucherIssuedTimeString.isEmpty()) {
						warn("Voucher issued time not found");
						return false;
					}
					SimpleDateFormat voucherIssuedTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
					Date voucherIssuedTime = voucherIssuedTimeFormat.parse(voucherIssuedTimeString);

					String voucherIssuedBy = xPath.compile("ReservationDetails/VoucherIssuedBy").evaluate(document);
					if (voucherIssuedBy.isEmpty()) {
						warn("VoucherIssuedBy not found");
						return false;
					}
					Person agent = (Person) parser.getProvider().getSingleResult("from Person where lower(aliases[20]) = lower(?0)", voucherIssuedBy);
					Post post = null;
					if (agent != null) {
						post = agent.getPost();
					}
					service.setPost(post);

					service.setBaseAmount(baseAmount);
					Currency uah = parser.findCurrency("UAH");
					service.setCurrency(uah);
					service.setBaseCurrency(uah);

					NodeList documentPassengerNodes = (NodeList) xPath.compile("ReservationDetails/Rooms/Room/Travellers/Traveller").evaluate(document,
							XPathConstants.NODESET);
					Element documentPassengerElement = (Element) documentPassengerNodes.item(0);
					NodeList documentPassengerFirstNameElement = documentPassengerElement.getElementsByTagName("FirstName");
					String firstName = documentPassengerFirstNameElement.item(0).getFirstChild().getNodeValue();
					NodeList documentPassengerLastNameElement = documentPassengerElement.getElementsByTagName("LastName");
					String lastName = documentPassengerLastNameElement.item(0).getFirstChild().getNodeValue();
					service.setConsumer(parser.findConsumer(firstName, lastName));

					service.setSupplier((Company) parser.getProvider().getSingleResult("from Company where id = " + supplierId));
					service.setHotel(parser.findHotelByName(xPath.compile("ReservationDetails/Hotel/Name").evaluate(document),
							xPath.compile("ReservationDetails/Hotel/City").evaluate(document),
							xPath.compile("ReservationDetails/Hotel/Country").evaluate(document), "EN"));
					SimpleDateFormat checkInOutFormat = new SimpleDateFormat("yyyy-MM-dd");
					service.setCheckIn(checkInOutFormat.parse(xPath.compile("ReservationDetails/CheckIn").evaluate(document)));
					Date checkOut = checkInOutFormat.parse(xPath.compile("ReservationDetails/CheckOut").evaluate(document));
					service.setCheckOut(checkOut);

					double commission = Double.parseDouble(xPath.compile("ReservationDetails/Commission[@Currency=\"UAH\"]/@Amount").evaluate(document));
					if (commission != 0) {
						parser.addHotelTax(service, "COMMISSION", commission, voucherIssuedTime);
					}

					service.setValidUntil(checkOut);
					service.setCreated(voucherIssuedTime);
					service.setRoomCategory((RoomCategory) parser.getProvider().getSingleResult("from RoomCategory"));
					service.setFood((HotelFood) parser.getProvider().getSingleResult("from HotelFood"));
					service.setRoomType((RoomType) parser.getProvider().getSingleResult("from RoomType"));
					parser.getProvider().save(service);
					return true;
				} catch (NumberFormatException | XPathExpressionException | DOMException | ParserConfigurationException | SAXException | IOException
						| ParseException e) {
					error("", e);
					return false;
				}
			}
		});
	}
}

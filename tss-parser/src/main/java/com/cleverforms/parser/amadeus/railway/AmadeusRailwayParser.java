package com.cleverforms.parser.amadeus.railway;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cleverforms.comms.server.tasks.FileParserImpl;
import com.cleverforms.ics.core.provider.ICSPersistentProvider;
import com.cleverforms.ics.db.model.dictionary.Company;
import com.cleverforms.ics.db.model.dictionary.Currency;
import com.cleverforms.ics.db.model.financial.AbstractService;
import com.cleverforms.ics.db.model.orgchart.Person;
import com.cleverforms.ics.db.model.orgchart.Post;
import com.cleverforms.ics.db.model.orgchart.Sector;
import com.cleverforms.ics.shared.dictionary.property.CompanyProperty;
import com.cleverforms.ics.shared.enumerator.OrderState;
import com.cleverforms.iss.server.model.airline.AirlineRoute;
import com.cleverforms.iss.server.model.airline.AirlineSegment;
import com.cleverforms.iss.server.model.airline.AirlineTicket;
import com.cleverforms.iss.server.model.railway.RailwayRoute;
import com.cleverforms.iss.server.model.railway.RailwayTicket;
import com.cleverforms.iss.shared.airline.AirlineTicketType;
import com.cleverforms.iss.shared.railway.RailwayTicketType;
import com.cleverforms.parser.Parser;

public class AmadeusRailwayParser extends FileParserImpl {

	@Autowired
	Parser parser;

	public final Sector sector = new Sector();
	public final Sector lowcostSector = new Sector();
	public final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

	@Override
	public ICSPersistentProvider provider() {
		return (ICSPersistentProvider) super.provider();
	}

	public void setSectorId(long sectorId) {
		sector.setId(sectorId);
	}

	public void setLowcostSectorId(long sectorId) {
		lowcostSector.setId(sectorId);
	}

	@Override
	public String name() {
		return "Amadeus railway data parser";
	}

	@Override
	public boolean parseFile(final File file) {
		return provider().txExecute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus status) {
				try {
					DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document document = documentBuilder.parse(file);
					XPath xPath = XPathFactory.newInstance().newXPath();

					String agentName = xPath.compile("/ReservationsList/ReservationsList/agentName").evaluate(document);
					if (agentName.isEmpty()) {
						warn("/ReservationsList/ReservationsList/agentName not found");
						return false;
					}
					Person agent = (Person) parser.getProvider().getSingleResult("from Person where lower(aliases[21]) = lower(?0)", agentName);
					Post post = null;
					if (agent != null) {
						post = agent.getPost();
					}

					Element confirmationNumberElement = (Element) xPath.compile("/ReservationsList/ReservationsList/confirmationNumber").evaluate(document,
							XPathConstants.NODE);
					if (confirmationNumberElement == null) {
						warn("/ReservationsList/ReservationsList/confirmationNumber not found");

						return false;
					}
					String confirmationNumber = confirmationNumberElement.getTextContent();

					String fileStatus = xPath.compile("/ReservationsList/ReservationsList/status").evaluate(document);
					OrderState state;
					switch (fileStatus) {
					case "OK":
						state = OrderState.SALE;
						break;
					case "XX":
						state = OrderState.VOID;
						break;
					case "RF":
						state = OrderState.REFUND;
						break;
					default:
						warn("Unknown status: " + fileStatus);
						return true;
					}

					Date timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").parse(xPath.compile("/ReservationsList/ReservationsList/@Timestamp")
							.evaluate(document));

					String trainNumber = xPath.compile("/ReservationsList/ReservationsList/trainNumber").evaluate(document);

					Node departureStationCodeElement = (Node) xPath.compile("/ReservationsList/ReservationsList/departureStationCode").evaluate(document,
							XPathConstants.NODE);
					if (departureStationCodeElement == null) {
						warn("/ReservationsList/ReservationsList/departureStationCode not found");

						return false;
					}
					String departureStationCode = departureStationCodeElement.getTextContent();

					Node arrivalStationCodeElement = (Node) xPath.compile("/ReservationsList/ReservationsList/arrivalStationCode").evaluate(document,
							XPathConstants.NODE);
					if (arrivalStationCodeElement == null) {
						warn("/ReservationsList/ReservationsList/arrivalStationCode not found");

						return false;
					}
					String arrivalStationCode = arrivalStationCodeElement.getTextContent();

					Date departureDate = dateFormat.parse(xPath.compile("/ReservationsList/ReservationsList/departureDate").evaluate(document));

					Date arrivalDate = dateFormat.parse(xPath.compile("/ReservationsList/ReservationsList/arrivalDate").evaluate(document));

					Currency baseCurrency = parser.findCurrency(xPath.compile("/ReservationsList/ReservationsList/currency").evaluate(document));

					NodeList passengerNodes = (NodeList) xPath.compile("/ReservationsList/ReservationsList/passenger").evaluate(document,
							XPathConstants.NODESET);

					String departureStationName = xPath.compile("/ReservationsList/ReservationsList/departureStation").evaluate(document);

					String arrivalStationName = xPath.compile("/ReservationsList/ReservationsList/arrivalStation").evaluate(document);

					String service = xPath.compile("/ReservationsList/ReservationsList/service").evaluate(document);
					if (service.equals("lowcost")) {
						AirlineRoute route = new AirlineRoute();

						AirlineSegment segment = new AirlineSegment();

						// Segment carrier
						Company carrier = parser.findCompany(trainNumber.substring(0, 2));
						segment.setCarrier(carrier);

						segment.setAirportFrom(parser.findAirportByCode(departureStationCode, departureStationName));

						segment.setAirportTo(parser.findAirportByCode(arrivalStationCode, arrivalStationName));

						segment.setDeparture(departureDate);

						segment.setArrive(arrivalDate);

						segment.setTicketCategory(xPath.compile("/ReservationsList/ReservationsList/wagonType").evaluate(document));

						segment.setTripNumber(trainNumber.substring(2));

						parser.addAirlineSegment(route, segment);

						for (int i = 0; i < passengerNodes.getLength(); i++) {
							Element passengerElement = (Element) passengerNodes.item(i);

							AirlineTicket ticket = new AirlineTicket();

							ticket.setPost(post);

							String ticketNumber = passengerElement.getElementsByTagName("ticketNumber").item(0).getTextContent();
							if (ticketNumber.isEmpty()) {
								ticketNumber = confirmationNumber + "-" + (i + 1);
							}
							if (parser.findAirlineTicket(ticketNumber) != null) {
								warn("Ticket " + ticketNumber + " already exists");
								return false;
							}
							ticket.setNumber(ticketNumber);

							ticket.setSector(lowcostSector);

							ticket.setType(AirlineTicketType.TRAVEL);

							ticket.setCurrency(parser.findCurrency("UAH"));

							ticket.setState(state);

							ticket.setExtNumber("RAIL_AMA-" + confirmationNumber);

							ticket.setCreated(timeStamp);

							ticket.setSupplier(carrier);

							ticket.setBaseCurrency(baseCurrency);

							ticket.setValidUntil(route.getDeparture());

							ticket.setConsumer(parser.findConsumer(passengerElement.getElementsByTagName("firstName").item(0).getTextContent(),
									passengerElement.getElementsByTagName("lastName").item(0).getTextContent()));

							NodeList markUpElements = passengerElement.getElementsByTagName("markup");
							if (markUpElements.getLength() == 0) {
								warn("/ReservationsList/ReservationsList/passenger/markup not found");

								return false;
							}
							double commission = Double.parseDouble(passengerElement.getElementsByTagName("agentCommission").item(0).getTextContent());
							if (commission != 0) {
								parser.addAirlineTax(ticket, "COMMISSION", ticket.getState().equals(OrderState.SALE) ? commission : -commission, timeStamp);
							}
							double baseAmount = Double.parseDouble(passengerElement.getElementsByTagName("purchasePrice").item(0).getTextContent());
							ticket.setBaseAmount(ticket.getState().equals(OrderState.SALE) ? baseAmount : -baseAmount);

							route.addTicket(ticket);
						}

						parser.getProvider().save(route);

						return true;
					} else if (service.equals("railua") || service.equals("contentrail") || service.equals("rail_europe")) {
						RailwayRoute route = new RailwayRoute();

						Company supplier;
						switch (service) {
						case "railua":
							supplier = parser.getProvider().persistentUtil().findCompany(CompanyProperty.NAME_UK, "УЗ");
							break;
						case "contentrail":
							supplier = parser.getProvider().persistentUtil().findCompany(CompanyProperty.NAME_UK, "РЖД");
							break;
						case "rail_europe":
							supplier = parser.getProvider().persistentUtil().findCompany(CompanyProperty.NAME_UK, "Міжнародні ЗД");
							break;
						default:
							warn("Unknown service: " + service);
							return true;
						}

						route.setStationFrom(parser.findRailwayStationByCode(departureStationCode, departureStationName));

						route.setStationTo(parser.findRailwayStationByCode(arrivalStationCode, arrivalStationName));

						route.setDeparture(departureDate);

						route.setArrive(arrivalDate);

						String[] seats = xPath.compile("/ReservationsList/ReservationsList/seats").evaluate(document).split(",");

						route.setNumber(confirmationNumber);

						String serviceType = xPath.compile("/ReservationsList/ReservationsList/serviceType").evaluate(document);
						switch (serviceType) {
						case "eticket":
							route.setElectronic(true);
							break;
						case "order":
							route.setElectronic(false);
							break;
						default:
							warn("Unknown service type: " + serviceType);
							return false;
						}

						route.setTrainNumber(trainNumber);

						String wagonNumber = xPath.compile("/ReservationsList/ReservationsList/wagonNumber").evaluate(document);

						for (int i = 0; i < passengerNodes.getLength(); i++) {
							Element passengerElement = (Element) passengerNodes.item(i);

							RailwayTicket ticket = new RailwayTicket();

							ticket.setPost(post);

							String ticketNumber;
							if (service.equals("railua")) {
								ticketNumber = confirmationNumber + "-" + (i + 1);
							} else {
								ticketNumber = passengerElement.getElementsByTagName("ticketNumber").item(0).getTextContent();
								if (ticketNumber.isEmpty()) {
									ticketNumber = confirmationNumber + "-" + (i + 1);
								}
							}
							if (state == OrderState.REFUND) {
								RailwayTicket refundTicket = parser.findRailwayTicket(ticketNumber);
								if (refundTicket == null) {
									warn("Ticket " + ticketNumber + " not found");
									return false;
								}
								AbstractService parent = new AbstractService();
								parent.setId(refundTicket.getId());
								ticket.setParent(parent);
								ticketNumber += "-R";
							}
							if (parser.findRailwayTicket(ticketNumber) != null) {
								warn("Ticket " + ticketNumber + " already exists");
								return false;
							}
							ticket.setNumber(ticketNumber);

							ticket.setPlaces(seats[i].strip());

							ticket.setSector(sector);

							ticket.setState(state);

							ticket.setSupplier(supplier);

							ticket.setCreated(timeStamp);

							ticket.setWagonNumber(wagonNumber);

							ticket.setType(RailwayTicketType.TRAVEL);

							String firstName = passengerElement.getElementsByTagName("firstName").item(0).getTextContent();
							String lastName = passengerElement.getElementsByTagName("lastName").item(0).getTextContent();
							ticket.setConsumer(parser.findConsumer(firstName, lastName));

							ticket.setExtNumber("RAIL_AMA-" + confirmationNumber);

							NodeList markUpElements = passengerElement.getElementsByTagName("markup");
							if (markUpElements.getLength() == 0) {
								warn("/ReservationsList/ReservationsList/passenger/markup not found");

								return false;
							}
							double commission = Double.parseDouble(passengerElement.getElementsByTagName("agentCommission").item(0).getTextContent());
							if (commission != 0) {
								parser.addRailwayTax(ticket, "COMMISSION", ticket.getState().equals(OrderState.VOID) ? -commission : commission, timeStamp);
							}
							if (service.equals("railua")) {
								double additionalServices = Double.parseDouble(markUpElements.item(0).getTextContent())
										+ Double.parseDouble(passengerElement.getElementsByTagName("vatFromMarkup").item(0).getTextContent());
								if (additionalServices != 0) {
									parser.addRailwayTax(ticket, "ADDITIONAL_SERVICES", ticket.getState().equals(OrderState.VOID) ? -additionalServices
											: additionalServices, timeStamp);
								}
								double baseAmount = Double.parseDouble(passengerElement.getElementsByTagName("totalPrice").item(0).getTextContent())
										- additionalServices - commission;
								if (baseAmount != 0) {
									ticket.setBaseAmount(ticket.getState().equals(OrderState.VOID) ? -baseAmount : baseAmount);
								}
							} else {
								double baseAmount = Double.parseDouble(passengerElement.getElementsByTagName("purchasePrice").item(0).getTextContent());
								if (baseAmount != 0) {
									ticket.setBaseAmount(ticket.getState().equals(OrderState.VOID) ? -baseAmount : baseAmount);
								}
							}
							Currency currency = parser.findCurrency(passengerElement.getElementsByTagName("currency").item(0).getTextContent());
							ticket.setCurrency(currency);
							ticket.setBaseCurrency(currency);

							ticket.setValidUntil(route.getDeparture());

							route.addTicket(ticket);
						}

						parser.getProvider().save(route);

						return true;
					} else {
						warn("Unknown service: " + service);
						return false;
					}
				} catch (NumberFormatException | XPathExpressionException | DOMException | ParserConfigurationException | SAXException | IOException
						| ParseException e) {
					error("", e);
					return false;
				}
			}
		});
	}
}

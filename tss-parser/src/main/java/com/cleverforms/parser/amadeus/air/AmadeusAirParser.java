package com.cleverforms.parser.amadeus.air;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import com.cleverforms.comms.server.tasks.FileParserImpl;
import com.cleverforms.comms.server.util.Casting;
import com.cleverforms.comms.shared.exception.ParsingException;
import com.cleverforms.comms.shared.util.UUID;
import com.cleverforms.comms.shared.util.date.DateTime;
import com.cleverforms.comms.shared.util.date.DateTime.Unit;
import com.cleverforms.ics.core.provider.ICSPersistentProvider;
import com.cleverforms.ics.db.model.dictionary.Consumer;
import com.cleverforms.ics.db.model.financial.AbstractService;
import com.cleverforms.ics.db.model.orgchart.Person;
import com.cleverforms.ics.db.model.orgchart.Post;
import com.cleverforms.ics.db.model.orgchart.Sector;
import com.cleverforms.ics.shared.enumerator.OrderState;
import com.cleverforms.iss.server.model.airline.AirlineRoute;
import com.cleverforms.iss.server.model.airline.AirlineSegment;
import com.cleverforms.iss.server.model.airline.AirlineTariff;
import com.cleverforms.iss.server.model.airline.AirlineTicket;
import com.cleverforms.iss.shared.airline.AirlineTicketType;
import com.cleverforms.parser.CodeAndAmount;
import com.cleverforms.parser.NumberAndPrice;
import com.cleverforms.parser.NumberAndPriceAndSupplier;
import com.cleverforms.parser.Parser;
import com.cleverforms.parser.Price;
import com.cleverforms.parser.StringAndString;
import com.cleverforms.parser.StringUtils;

public class AmadeusAirParser extends FileParserImpl {

	@Autowired
	Parser parser;

	public final Sector sector = new Sector();

	SimpleDateFormat departureFormat = new SimpleDateFormat("yyyyddMMMHHmm", Locale.ENGLISH);
	SimpleDateFormat departureFormatWithAmPm = new SimpleDateFormat("yyyyddMMMHHmma", Locale.ENGLISH);
	SimpleDateFormat arrivalFormat = new SimpleDateFormat("yyyyHHmm ddMMM", Locale.ENGLISH);
	SimpleDateFormat arrivalFormatWithAmPm = new SimpleDateFormat("yyyyHHmmaddMMM", Locale.ENGLISH);
	SimpleDateFormat documentFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);

	@Override
	public ICSPersistentProvider provider() {
		return (ICSPersistentProvider) super.provider();
	}

	public void setSectorId(long sectorId) {
		sector.setId(sectorId);
	}

	@Override
	public String name() {
		return "Amadeus airline data parser";
	}

	@Override
	public boolean parseFile(final File file) {
		return provider().txExecute(new TransactionCallback<Boolean>() {
			@SuppressWarnings("deprecation")
			@Override
			public Boolean doInTransaction(TransactionStatus status) {
				try {
					// Get document date from the filename
					Date documentDate = documentFormat.parse(file.getName().substring(4, 18));
					// Convert document date from UTC to local
					documentDate = com.cleverforms.parser.DateUtils.utcToLocal(documentDate);

					String data = FileUtils.readFileToString(file);

					int index = 0, tempIndex = data.indexOf("\nAMD");
					if (tempIndex != -1) {
						index = tempIndex + 4;
						tempIndex = StringUtils.indexOf(data, ";", 2, index);
						if (tempIndex != -1) {
							index = tempIndex + 1;
							if (data.substring(index, index + 4).equals("VOID")) {
								/*-String agentSine = data.substring(index + 10, index + 14);
								Person agent = (Person) parser.getProvider().getSingleResult("from Person where lower(aliases[1]) = lower(?0)", agentSine);
								Post post = null;
								if (agent != null) {
									post = agent.getPost();
								}*/

								tempIndex = data.indexOf("\nT-", index);
								if (tempIndex != -1) {
									index = tempIndex;
									String voidTicketNumber = data.substring(index += 4, index = data.indexOf('\r', index));
									AirlineTicket ticket = parser.findAirlineTicket(voidTicketNumber);
									if (ticket == null) {
										warn("Ticket " + voidTicketNumber + " not found");
										return false;
									}
									ticket.setState(OrderState.VOID);
									// ticket.setPost(post);
									parser.getProvider().save(ticket);
									return true;
								} else {
									tempIndex = data.indexOf("\nTMC", index);
									if (tempIndex != -1) {
										index = tempIndex;
										String voidEmdNumber = data.substring(index += 5, index = data.indexOf(';', index));
										AirlineTicket emdToVoid = parser.findAirlineTicket(voidEmdNumber);
										if (emdToVoid == null) {
											warn("Ticket " + voidEmdNumber + " not found");
											return false;
										}
										// emdToVoid.setPost(post);
										emdToVoid.setState(OrderState.VOID);
										parser.getProvider().save(emdToVoid);
										return true;
									} else {
										throw new ParsingException("Ticket or emd number to void not found");
									}
								}
							}
						}
					}
					tempIndex = data.indexOf("\nB-", index);
					if (tempIndex == -1) {
						throw new ParsingException("B- not found");
					}
					index = tempIndex + 3;
					String b = data.substring(index, data.indexOf('\r', index));

					tempIndex = data.indexOf("\nC-", index);
					if (tempIndex == -1) {
						throw new ParsingException("C- not found");
					}
					index = tempIndex + 3;
					String agentSine = data.substring(index + 15, index + 21);
					Person agent = (Person) parser.getProvider().getSingleResult("from Person where lower(aliases[1]) = lower(?0)", agentSine);
					Post post = null;
					if (agent != null) {
						post = agent.getPost();
					}

					if (b.startsWith("TTP") || b.startsWith("BT")) {
						index = data.indexOf("MUC1A");
						String pnr = data.substring(index + 6, index + 12);
						String robot = data.substring(index = StringUtils.indexOf(data, ";", 8, index) + 1, index = data.indexOf(";", index));

						if (b.startsWith("TTP")) {
							AirlineRoute route = new AirlineRoute();
							List<AirlineTicket> emds = new ArrayList<>();
							List<AirlineSegment> segments = new ArrayList<>();

							while ((tempIndex = data.indexOf("\nH-", index)) != -1) {
								if (!"000".equals(data.substring(index = tempIndex + 3, index += 3))) {
									index += 4;
									AirlineSegment segment = parseSegment(data, index, documentDate);
									parser.addAirlineSegment(route, segment);
									segments.add(segment);
								}
							}

							List<NumberAndPriceAndSupplier> tsmNumbersAndAmountsAndSuppliers = new ArrayList<>();
							List<StringAndString> emdTicketNumbersToTsmNumbers = new ArrayList<>();

							while ((tempIndex = data.indexOf("\nEMD", index)) != -1) {
								index = tempIndex;
								String supplier = data.substring(index = data.indexOf(";", index) + 4, index += 2);
								String tsmNumber = data.substring(index = StringUtils.indexOf(data, ";", 4, index) + 2, index = data.indexOf(";", index));
								String currencyCode = data.substring(index = StringUtils.indexOf(data, ";", 127, index) + 1, index += 3);
								String amount = data.substring(index, index += 11).strip();
								tsmNumbersAndAmountsAndSuppliers.add(new NumberAndPriceAndSupplier(tsmNumber, new Price(Double.parseDouble(amount),
										currencyCode), supplier));
							}

							index = 0;
							while ((tempIndex = data.indexOf("\nICW", index)) != -1) {
								index = tempIndex;
								String emdTicketNumber = data.substring(index += 4, index += 3) + '-' + data.substring(index, index += 10);
								String tsmNumber = data.substring(index = data.indexOf(";D", index) + 2, index = data.indexOf(";", index));
								emdTicketNumbersToTsmNumbers.add(new StringAndString(tsmNumber, emdTicketNumber));
							}

							index = data.indexOf("\nK-", index);
							if (data.charAt(index + 3) != '\r') {
								index += 4;
							} else {
								index = data.indexOf("\nKS-", index) + 5;
								if (index == 4) {
									throw new ParsingException("K- or KS- not found.");
								}
							}
							String currencyCode = data.substring(index = StringUtils.indexOf(data, ";", 12, index) + 1, index += 3);
							double amount = Double.parseDouble(data.substring(index, data.indexOf(';', index)).strip());
							List<CodeAndAmount> taxes = new ArrayList<>();
							index = data.indexOf('\n', index);

							index = data.indexOf('\n', index);
							if (data.substring(index + 1, index + 5).equals("TAX-") || data.substring(index + 1, index + 4).equals("KFT")
									|| data.substring(index + 1, index + 5).equals("KNTI")) {
								for (String tax : data.substring(index + 5, index = data.indexOf("\r", index + 5)).split(";")) {
									if (tax.isEmpty()) {
										continue;
									} else {
										if (tax.charAt(0) != 'O') {
											String taxCode = tax.substring(13, 15);
											double taxAmount = Casting.asDouble(tax.substring(4, 13).strip());
											taxes.add(new CodeAndAmount(taxCode, taxAmount));
											amount -= taxAmount;
										}
									}
								}
							}

							index = data.indexOf("\nM-");
							String[] tariffs = data.substring(index = index + 3, data.indexOf('\r', index)).split(";");

							HashMap<AirlineTicket, AirlineTicket> emdsToTickets = new HashMap<>();

							while ((index = data.indexOf("\nI-", index)) != -1) {
								int nextISegment = data.indexOf("\nI-", index + 4);
								String lastName = data.substring(index = index + 9, index = data.indexOf('/', index)).strip();
								tempIndex = index + 1;
								index++;
								while (data.charAt(index) != '(' && data.charAt(index) != ';') {
									index++;
								}
								String firstName = data.substring(tempIndex, index).strip();
								if (firstName.endsWith(" MR")) {
									firstName = firstName.substring(0, firstName.length() - 3);
								} else if (firstName.endsWith(" MRS")) {
									firstName = firstName.substring(0, firstName.length() - 4);
								} else if (firstName.endsWith(" MS")) {
									firstName = firstName.substring(0, firstName.length() - 3);
								} else if (firstName.endsWith(" MSTR")) {
									firstName = firstName.substring(0, firstName.length() - 5);
								}
								firstName = firstName.strip();

								String sss = data.substring(index, index = data.indexOf("\r", index));
								if (!sss.contains("TICKETS-UA") || robot.equals("IEVKV2102")) {
									String ticketNumber = data.substring(index = data.indexOf("\nT-", index) + 4, index = data.indexOf('\r', index));
									AirlineTicket ticket;
									if ((ticket = (AirlineTicket) parser
											.getProvider()
											.getSingleResult(
													"from AirlineTicket where extNumber = ?0 and state = 'BOOKING' and year(created) = ?1 and month(created) = ?2 and day(created) = ?3 and lower(consumer.name) = ?4 and lower(consumer.lastName) = ?5",
													"AMADEUS-" + pnr, documentDate.getYear() + 1900, documentDate.getMonth() + 1, documentDate.getDate(),
													firstName.toLowerCase(), lastName.toLowerCase())) != null) {
										if (ticket.getRoute().getAirportFrom().equals(route.getAirportFrom())
												&& ticket.getRoute().getAirportTo().equals(route.getAirportTo())
												&& ticket.getRoute().getArrive().compareTo(route.getArrive()) == 0
												&& ticket.getRoute().getDeparture().compareTo(route.getDeparture()) == 0) {
											ticket.setState(OrderState.SALE);

											ticket.setNumber(ticketNumber);

											ticket.setPost(post);

											tempIndex = data.indexOf("\nTFD", index);
											if (tempIndex != -1) {
												parser.addAirlineTax(
														ticket,
														"COMMISSION",
														Double.parseDouble(data
																.substring(tempIndex = StringUtils.indexOf(data, ";", 4, tempIndex),
																		tempIndex = data.indexOf(";", tempIndex + 1)).substring(4).strip()), documentDate);
												String discountField = data.substring(tempIndex + 1, tempIndex = data.indexOf(";", tempIndex + 1));
												if (!discountField.isEmpty()) {
													double discount = Double.parseDouble(discountField);
													parser.addAirlineTax(ticket, discount < 0 ? "DISCOUNT" : "VIP_COMMISSION", discount, documentDate);
												}
												index = tempIndex;
											}

											parser.getProvider().save(ticket);

											continue;
										}
									}
									ticket = new AirlineTicket();

									ticket.setExtNumber("AMADEUS-" + pnr);

									ticket.setConsumer(parser.findConsumer(firstName, lastName));

									if (parser.findAirlineTicket(ticketNumber) != null) {
										warn("Ticket " + ticketNumber + " already exists");

										return false;
									}
									ticket.setNumber(ticketNumber);

									ticket.setType(AirlineTicketType.TRAVEL);

									ticket.setPost(post);

									while ((tempIndex = data.indexOf("TMC", index)) != -1 && (nextISegment == -1 || tempIndex < nextISegment)) {
										index = tempIndex;
										String emdNumber = data.substring(index + 4, index = data.indexOf(";", index));
										String tsmNumber = data.substring(index += 3, index = data.indexOf("\r", index));
										AirlineTicket emd = new AirlineTicket();
										emd.setSector(sector);
										emd.setConsumer(parser.findConsumer(firstName, lastName));
										emd.setState(OrderState.SALE);
										emd.setExtNumber("AMADEUS-" + pnr);
										emd.setCurrency(parser.findCurrency("UAH"));
										emd.setType(AirlineTicketType.EMD);
										emd.setCreated(documentDate);
										emd.setNumber(emdNumber);
										emd.setRoute(route);
										emd.setPost(post);
										for (NumberAndPriceAndSupplier numberAndPriceAndSupplier : tsmNumbersAndAmountsAndSuppliers) {
											if (!numberAndPriceAndSupplier.number.equals(tsmNumber)) {
												continue;
											}
											emd.setBaseAmount(numberAndPriceAndSupplier.price.amount);
											emd.setBaseCurrency(parser.findCurrency(numberAndPriceAndSupplier.price.currency));
											emd.setSupplier(parser.findCompany(numberAndPriceAndSupplier.supplier));
										}
										emdsToTickets.put(emd, ticket);
										emds.add(emd);
									}

									tempIndex = data.indexOf("\nTFD", index);
									if (tempIndex != -1) {
										parser.addAirlineTax(
												ticket,
												"COMMISSION",
												Double.parseDouble(data
														.substring(tempIndex = StringUtils.indexOf(data, ";", 4, tempIndex),
																tempIndex = data.indexOf(";", tempIndex + 1)).substring(4).strip()), documentDate);
										String discountField = data.substring(tempIndex + 1, tempIndex = data.indexOf(";", tempIndex + 1));
										if (!discountField.isEmpty()) {
											double discount = Double.parseDouble(discountField);
											parser.addAirlineTax(ticket, discount < 0 ? "DISCOUNT" : "VIP_COMMISSION", discount, documentDate);
										}
										index = tempIndex;
									}
									tempIndex = data.indexOf("\nFO", index);
									if (tempIndex != -1 && data.charAt(tempIndex + 3) != 'I') {
										tempIndex += 3;
										index = tempIndex;
										while (!Character.isLetter(data.charAt(tempIndex++))) {
										}
										String reissueTicketNumber = data.substring(index, index = tempIndex - 1);
										AirlineTicket reissueTicket = parser.findAirlineTicket(reissueTicketNumber);
										if (reissueTicket == null) {
											warn("Ticket " + reissueTicketNumber + " not found");
											return false;
										}
										ticket.setState(OrderState.EXCHANGE);
										AbstractService as = new AbstractService();
										as.setId(reissueTicket.getId());
										ticket.setParent(as);
									} else {
										ticket.setState(OrderState.SALE);
									}
									tempIndex = data.indexOf("\nFV", index);
									if (tempIndex != -1) {
										if (data.substring(tempIndex + 3, tempIndex + 6).equals("*F*")) {
											ticket.setSupplier(parser.findCompany(data.substring(tempIndex + 6, tempIndex + 8)));
										} else {
											ticket.setSupplier(parser.findCompany(data.substring(tempIndex + 3, tempIndex + 5)));
										}
									} else {
										warn("FV not found");
										return false;
									}

									route.addTicket(ticket);
									int i = 0;
									for (AirlineSegment segment : segments) {
										AirlineTariff tariff = new AirlineTariff();
										tariff.setConsumer(ticket.getConsumer());
										tariff.setCode(tariffs[i++].strip());
										segment.addTariff(tariff);
									}
									ticket.setBaseAmount(amount);
									ticket.setBaseCurrency(parser.findCurrency(currencyCode));
									ticket.setCurrency(parser.findCurrency("UAH"));
									ticket.setSector(sector);
									ticket.setCreated(documentDate);
									ticket.setValidUntil(route.getDeparture());
									for (CodeAndAmount tax : taxes) {
										parser.addAirlineTax(ticket, tax.code, tax.amount, documentDate);
									}
								}
							}
							if (!route.getTickets().isEmpty()) {
								parser.provider.save(route);
								for (Entry<AirlineTicket, AirlineTicket> emdToTicket : emdsToTickets.entrySet()) {
									AbstractService as = new AbstractService();
									as.setId(emdToTicket.getValue().getId());
									emdToTicket.getKey().setParent(as);
									emdToTicket.getKey().setSupplier(emdToTicket.getValue().getSupplier());
									emdToTicket.getKey().setValidUntil(emdToTicket.getValue().getValidUntil());
								}
							}

							for (AirlineTicket emd : emds) {
								parser.provider.save(emd);
							}
						} else {
							Map<String, AirlineSegment> numbersToSegments = new HashMap<>();

							while ((index = data.indexOf("\nU-", index)) != -1) {
								if (!"000".equals(data.substring(index + 3, index = index + 6))) {
									String segmentNumber = data.substring(index = data.indexOf(';', index) + 1, index += 3);
									String s = data.substring(index, index + 3);
									if (!"MIS".equals(s) && !"EMD".equals(s) && !"HHL".equals(s)) {
										while (segmentNumber.charAt(0) == '0') {
											segmentNumber = segmentNumber.substring(1);
										}
										AirlineSegment segment = parseSegment(data, index, documentDate);
										numbersToSegments.put(segmentNumber, segment);
									}
								}
							}

							Map<String, AirlineRoute> segmentsToRoutes = new HashMap<>();
							Map<AirlineRoute, List<AirlineSegment>> routesToSegmentsList = new HashMap<>();

							while ((index = data.indexOf("\nI-", index)) != -1) {
								List<AirlineTicket> tickets = new ArrayList<>();
								int nextIString = data.indexOf("\nI-", index + 1);
								String lastName = data.substring(index = index + 9, index = data.indexOf('/', index)).strip();
								tempIndex = index + 1;
								index++;
								while (data.charAt(index) != '(' && data.charAt(index) != ';') {
									index++;
								}
								String firstName = data.substring(tempIndex, index).strip();
								if (firstName.endsWith(" MR")) {
									firstName = firstName.substring(0, firstName.length() - 3);
								} else if (firstName.endsWith(" MRS")) {
									firstName = firstName.substring(0, firstName.length() - 4);
								} else if (firstName.endsWith(" MS")) {
									firstName = firstName.substring(0, firstName.length() - 3);
								} else if (firstName.endsWith(" MSTR")) {
									firstName = firstName.substring(0, firstName.length() - 5);
								}
								firstName = firstName.strip();
								Consumer consumer = parser.findConsumer(firstName, lastName);

								if (!data.substring(index, index = data.indexOf("\r", index)).contains("TICKETS-UA") || robot.equals("IEVKV2102")) {
									if (data.indexOf("\nTSA-", index) == -1) {
										String s = "";
										for (String ss : numbersToSegments.keySet()) {
											s += ss + ",";
										}
										s = s.substring(0, s.length() - 1);

										index = processTicketBlock(tickets, consumer, pnr, documentDate, post, s, data, tempIndex, numbersToSegments,
												segmentsToRoutes, routesToSegmentsList, tempIndex);
									} else {
										while ((tempIndex = data.indexOf("\nTSA-", index)) != -1) {
											if (nextIString != -1 && tempIndex > nextIString) {
												break;
											}

											tempIndex = data.indexOf("\r", tempIndex);
											int tempIndex2 = data.lastIndexOf(";", tempIndex);
											String s = data.substring(tempIndex2 + 2, tempIndex);

											index = processTicketBlock(tickets, consumer, pnr, documentDate, post, s, data, tempIndex, numbersToSegments,
													segmentsToRoutes, routesToSegmentsList, tempIndex);
										}
									}
								}
								while ((tempIndex = data.indexOf("\nTFD", index)) != -1) {
									if (nextIString != -1 && tempIndex > nextIString) {
										break;
									}
									parser.addAirlineTax(
											tickets.get(0),
											"COMMISSION",
											Double.parseDouble(data
													.substring(tempIndex = StringUtils.indexOf(data, ";", 4, tempIndex),
															tempIndex = data.indexOf(";", tempIndex + 1)).substring(4).strip()), documentDate);
									String discountField = data.substring(tempIndex + 1, tempIndex = data.indexOf(";", tempIndex + 1));
									if (!discountField.isEmpty()) {
										double discount = Double.parseDouble(discountField);
										parser.addAirlineTax(tickets.get(0), discount < 0 ? "DISCOUNT" : "VIP_COMMISSION", discount, documentDate);
									}
									index = tempIndex;
								}
								int fvCount = 0;
								while ((tempIndex = data.indexOf("\nFV", index)) != -1) {
									if (nextIString != -1 && tempIndex > nextIString) {
										break;
									}
									AirlineTicket ticket;
									if (tickets.size() == 1) {
										ticket = tickets.get(0);
									} else {
										ticket = tickets.get(fvCount);
									}
									if (data.substring(tempIndex + 3, tempIndex + 6).equals("*F*")) {
										ticket.setSupplier(parser.findCompany(data.substring(tempIndex + 6, tempIndex + 8)));
									} else {
										ticket.setSupplier(parser.findCompany(data.substring(tempIndex + 3, tempIndex + 5)));
									}
									index = tempIndex + 5;
									fvCount++;
								}
							}
							for (AirlineRoute route : segmentsToRoutes.values()) {
								if (!route.getTickets().isEmpty()) {
									parser.getProvider().save(route);
								}
							}
						}
					} else if (b.startsWith("TRF")) {
						AirlineTicket ticket = new AirlineTicket();
						String currencyCode = data.substring(index = data.indexOf("\nRFD", index) + 16, index += 3);
						ticket.setBaseCurrency(parser.findCurrency(currencyCode));
						ticket.setCurrency(parser.findCurrency("UAH"));
						ticket.setCreated(documentDate);
						ticket.setSector(sector);

						List<CodeAndAmount> taxes = new ArrayList<>();

						String fareRefundString = data.substring(index = StringUtils.indexOf(data, ";", 2, index) + 1, data.indexOf(";", index));
						if (!fareRefundString.isEmpty()) {
							double fareRefund = Double.parseDouble(fareRefundString);
							ticket.setBaseAmount(-fareRefund);
						}
						String cancelationFeeString = data.substring(index = StringUtils.indexOf(data, ";", 3, index) + 1, data.indexOf(";", index));
						if (!cancelationFeeString.isEmpty()) {
							taxes.add(new CodeAndAmount("CANCELLATION FEE", Double.parseDouble(cancelationFeeString)));
						}
						tempIndex = data.indexOf("\nKRF", index) + 6;
						if (tempIndex != 5) {
							index = tempIndex;
							String krf = data.substring(index, index = data.indexOf(";;", index));
							if (!krf.isEmpty()) {
								for (String tax : krf.split(";")) {
									String taxAmount = tax.substring(4, 13).strip();
									if (taxAmount.isEmpty()) {
										continue;
									}
									String taxCode = tax.substring(13, 15);
									taxes.add(new CodeAndAmount(taxCode, -Double.parseDouble(taxAmount)));
								}
							}
						}

						index = data.indexOf("\nR-", index) + 3;
						String refundTicketNumber = data.substring(index, data.indexOf(";", index));

						AirlineTicket refundTicket = parser.findAirlineTicket(refundTicketNumber);
						if (refundTicket == null) {
							warn("Ticket " + refundTicketNumber + " not found");
							return false;
						}
						ticket.setConsumer(refundTicket.getConsumer());
						ticket.setRoute(refundTicket.getRoute());
						ticket.setExtNumber(refundTicket.getExtNumber());
						ticket.setNumber(refundTicket.getNumber() + "-R");
						AbstractService as = new AbstractService();
						as.setId(refundTicket.getId());
						ticket.setParent(as);
						ticket.setState(OrderState.REFUND);
						ticket.setSupplier(refundTicket.getSupplier());
						for (CodeAndAmount tax : taxes) {
							parser.addAirlineTax(ticket, tax.code, tax.amount, documentDate);
						}
						ticket.setType(AirlineTicketType.TRAVEL);
						ticket.setValidUntil(documentDate);
						ticket.setPost(post);
						parser.getProvider().save(refundTicket);
						parser.getProvider().save(ticket);
					} else if (b.startsWith("TTM")) {
						index = data.indexOf("MUC1A");
						String pnr = data.substring(index + 6, index + 12);
						String robot = data.substring(index = StringUtils.indexOf(data, ";", 8, index) + 1, index = data.indexOf(";", index));

						AirlineRoute route = new AirlineRoute();
						while ((index = data.indexOf("\nU-", index)) != -1) {
							if (!"000".equals(data.substring(index + 3, index = index + 6))) {
								String segmentNumber = data.substring(index = data.indexOf(';', index) + 1, index += 3);
								String s = data.substring(index, index + 3);
								if (!"MIS".equals(s) && !"EMD".equals(s)) {
									while (segmentNumber.charAt(0) == '0') {
										segmentNumber = segmentNumber.substring(1);
									}
									parser.addAirlineSegment(route, parseSegment(data, index, documentDate));
								}
							}
						}

						List<NumberAndPriceAndSupplier> tsmNumbersAndAmountsAndSuppliers = new ArrayList<>();
						List<StringAndString> emdTicketNumbersToTsmNumbers = new ArrayList<>();

						while ((tempIndex = data.indexOf("\nEMD", index)) != -1) {
							index = tempIndex;
							String supplier = data.substring(index = data.indexOf(";", index) + 4, index += 2);
							String tsmNumber = data.substring(index = StringUtils.indexOf(data, ";", 4, index) + 2, index = data.indexOf(";", index));
							String currencyCode = data.substring(index = StringUtils.indexOf(data, ";", 127, index) + 1, index += 3);
							String amount = data.substring(index, index += 11).strip();
							tsmNumbersAndAmountsAndSuppliers.add(new NumberAndPriceAndSupplier(tsmNumber, new Price(Double.parseDouble(amount), currencyCode),
									supplier));
						}

						index = 0;
						while ((tempIndex = data.indexOf("\nICW", index)) != -1) {
							index = tempIndex;
							String emdTicketNumber = data.substring(index += 4, index += 3) + '-' + data.substring(index, index += 10);
							String tsmNumber = data.substring(index = data.indexOf(";D", index) + 2, index = data.indexOf(";", index));
							emdTicketNumbersToTsmNumbers.add(new StringAndString(tsmNumber, emdTicketNumber));
						}

						while ((index = data.indexOf("\nI-", index)) != -1) {
							if (data.substring(index += 3, index + 3).equals("GRP")) {
								continue;
							}
							int nextISegment = data.indexOf("\nI-", index);
							String lastName = data.substring(index += 6, index = data.indexOf('/', index)).strip();
							tempIndex = index + 1;
							index++;
							while (data.charAt(index) != '(' && data.charAt(index) != ';') {
								index++;
							}
							String firstName = data.substring(tempIndex, index).strip();
							if (firstName.endsWith(" MR")) {
								firstName = firstName.substring(0, firstName.length() - 3);
							} else if (firstName.endsWith(" MRS")) {
								firstName = firstName.substring(0, firstName.length() - 4);
							} else if (firstName.endsWith(" MS")) {
								firstName = firstName.substring(0, firstName.length() - 3);
							} else if (firstName.endsWith(" MSTR")) {
								firstName = firstName.substring(0, firstName.length() - 5);
							}
							firstName = firstName.strip();

							String sss = data.substring(index, index = data.indexOf("\r", index));
							if (!sss.contains("TICKETS-UA") || robot.equals("IEVKV2102")) {
								while ((tempIndex = data.indexOf("TMC", index)) != -1 && (nextISegment == -1 || tempIndex < nextISegment)) {
									index = tempIndex;
									String emdNumber = data.substring(index + 4, index = data.indexOf(";", index));
									String tsmNumber = data.substring(index += 3, index = data.indexOf("\r", index));
									AirlineTicket emd = new AirlineTicket();
									emd.setSector(sector);
									emd.setConsumer(parser.findConsumer(firstName, lastName));
									emd.setState(OrderState.SALE);
									emd.setExtNumber("AMADEUS-" + pnr);
									emd.setCurrency(parser.findCurrency("UAH"));
									emd.setType(AirlineTicketType.EMD);
									emd.setCreated(documentDate);
									emd.setNumber(emdNumber);
									emd.setPost(post);
									for (StringAndString emdTicketNumberToTsmNumber : emdTicketNumbersToTsmNumbers) {
										if (!emdTicketNumberToTsmNumber.str1.equals(tsmNumber)) {
											continue;
										}
										AirlineTicket ticket = parser.findAirlineTicket(emdTicketNumberToTsmNumber.str2);
										if (ticket != null) {
											AbstractService as = new AbstractService();
											as.setId(ticket.getId());
											emd.setParent(as);

											emd.setSupplier(ticket.getSupplier());
										}

										break;
									}
									emd.setValidUntil(route.getDeparture());
									for (NumberAndPriceAndSupplier numberAndPriceAndSupplier : tsmNumbersAndAmountsAndSuppliers) {
										if (!numberAndPriceAndSupplier.number.equals(tsmNumber)) {
											continue;
										}
										emd.setBaseAmount(numberAndPriceAndSupplier.price.amount);
										emd.setBaseCurrency(parser.findCurrency(numberAndPriceAndSupplier.price.currency));
										emd.setSupplier(parser.findCompany(numberAndPriceAndSupplier.supplier));
									}
									route.addTicket(emd);
								}
							}
						}

						parser.getProvider().save(route);
						return true;
					} else {
						throw new ParsingException("Unknown document type");
					}

					return true;
				} catch (NumberFormatException | ParseException | IOException e) {
					error("", e);
					return false;
				}
			}
		});
	}

	@SuppressWarnings("deprecation")
	public AirlineSegment parseSegment(String data, int index, Date documentDate) throws ParseException {
		AirlineSegment segment = new AirlineSegment();
		// Departure airport
		segment.setAirportFrom(parser.findAirportByCode(data.substring(index + 1, index = index + 4), null));
		// Arrival airport
		segment.setAirportTo(parser.findAirportByCode(data.substring(index = data.indexOf(';', index + 1) + 1, index = index + 3), null));
		// Carrier
		segment.setCarrier(parser.findCompany(data.substring(index = data.indexOf(';', index + 1) + 1, index = index + 6).strip()));
		// Flight number
		segment.setTripNumber(data.substring(index, index = index + 5).strip());
		// Class
		segment.setTicketCategory(data.substring(index, index = index + 2).strip());
		// Departure date
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(documentDate);
		int documentYear = calendar.get(Calendar.YEAR);
		String departureDate = data.substring(index + 2, index = index + 11);
		if (data.charAt(index) == 'P') {
			segment.setDeparture(departureFormatWithAmPm.parse(documentYear + departureDate + "PM"));
		} else {
			segment.setDeparture(departureFormat.parse(documentYear + departureDate));
		}
		if (segment.getDeparture().before(documentDate)) {
			segment.setDeparture(DateTime.add(segment.getDeparture(), 1, Unit.YEAR));
		}
		// Get arrival date
		String arrivalDate = data.substring(index + 1, index + 11);
		if (arrivalDate.charAt(4) == 'P' || arrivalDate.charAt(4) == 'A') {
			segment.setArrive(arrivalFormatWithAmPm.parse(segment.getDeparture().getYear() + 1900 + arrivalDate.substring(0, 5) + "M"
					+ arrivalDate.substring(5)));
		} else {
			segment.setArrive(arrivalFormat.parse(segment.getDeparture().getYear() + 1900 + arrivalDate));
		}
		if (segment.getArrive().before(segment.getDeparture())) {
			segment.setArrive(DateTime.add(segment.getArrive(), 1, Unit.YEAR));
		}
		return segment;
	}

	public int parseEmds(String data, int index, Date documentDate, Map<String, List<NumberAndPrice>> ticketNumbersToTsmNumbersAndPrice) {
		Map<String, NumberAndPrice> elementNumbersToTsmNumbersAndAmounts = new HashMap<>();
		int tempIndex;
		while ((tempIndex = data.indexOf("\nEMD", index)) != -1) {
			index = tempIndex;
			String elementNumber = data.substring(index = data.indexOf(";", index) + 1, index += 3);
			while (elementNumber.charAt(0) == '0') {
				elementNumber = elementNumber.substring(1);
			}
			String tsmNumber = data.substring(index = StringUtils.indexOf(data, ";", 4, index) + 2, index = data.indexOf(";", index));
			String currencyCode = data.substring(index = StringUtils.indexOf(data, ";", 127, index) + 1, index += 3);
			String amount = data.substring(index, index += 11).strip();
			elementNumbersToTsmNumbersAndAmounts.put(elementNumber, new NumberAndPrice(tsmNumber, new Price(Double.parseDouble(amount), currencyCode)));
		}

		while ((tempIndex = data.indexOf("\nICW", index)) != -1) {
			index = tempIndex;
			String emdTicketNumber = data.substring(index += 4, index += 3) + '-' + data.substring(index, index += 10);
			String elementNumber = data.substring(index = StringUtils.indexOf(data, ";", 2, index) + 2, index = data.indexOf("\r", index));
			if (!ticketNumbersToTsmNumbersAndPrice.containsKey(emdTicketNumber)) {
				ticketNumbersToTsmNumbersAndPrice.put(emdTicketNumber, new ArrayList<NumberAndPrice>());
			}
			ticketNumbersToTsmNumbersAndPrice.get(emdTicketNumber).add(elementNumbersToTsmNumbersAndAmounts.get(elementNumber));
		}

		return index;
	}

	public int processTicketBlock(List<AirlineTicket> tickets, Consumer consumer, String pnr, Date documentDate, Post post, String s, String data,
			int tempIndex, Map<String, AirlineSegment> numbersToSegments, Map<String, AirlineRoute> segmentsToRoutes,
			Map<AirlineRoute, List<AirlineSegment>> routesToSegmentsList, int index) {
		AirlineTicket ticket = new AirlineTicket();
		ticket.setSector(sector);
		ticket.setState(OrderState.BOOKING);
		ticket.setConsumer(consumer);
		ticket.setExtNumber("AMADEUS-" + pnr);
		ticket.setNumber(UUID.uuid(8, 26));
		ticket.setCreated(documentDate);
		ticket.setType(AirlineTicketType.TRAVEL);
		ticket.setPost(post);

		AirlineRoute route = segmentsToRoutes.get(s);
		if (route == null) {
			List<AirlineSegment> segments = new ArrayList<>();
			route = new AirlineRoute();
			for (String s1 : s.split(",")) {
				int ind = s1.indexOf("-");
				if (ind == -1) {
					AirlineSegment newSegment = new AirlineSegment(), listSegment = numbersToSegments.get(s1);
					newSegment.setAirportFrom(listSegment.getAirportFrom());
					newSegment.setAirportTo(listSegment.getAirportTo());
					newSegment.setArrive(listSegment.getArrive());
					newSegment.setDeparture(listSegment.getDeparture());
					newSegment.setCarrier(listSegment.getCarrier());
					newSegment.setSeatNumber(listSegment.getSeatNumber());
					newSegment.setTicketCategory(listSegment.getTicketCategory());
					newSegment.setTripNumber(listSegment.getTripNumber());
					newSegment.setTariffs(new HashSet<AirlineTariff>(listSegment.getTariffs()));
					parser.addAirlineSegment(route, newSegment);
					segments.add(newSegment);
				} else {
					for (int i = Integer.parseInt(s1.substring(0, ind)); i <= Integer.parseInt(s1.substring(ind + 1)); i++) {
						AirlineSegment newSegment = new AirlineSegment(), listSegment = numbersToSegments.get("" + i);
						newSegment.setAirportFrom(listSegment.getAirportFrom());
						newSegment.setAirportTo(listSegment.getAirportTo());
						newSegment.setArrive(listSegment.getArrive());
						newSegment.setDeparture(listSegment.getDeparture());
						newSegment.setCarrier(listSegment.getCarrier());
						newSegment.setSeatNumber(listSegment.getSeatNumber());
						newSegment.setTicketCategory(listSegment.getTicketCategory());
						newSegment.setTripNumber(listSegment.getTripNumber());
						newSegment.setTariffs(new HashSet<AirlineTariff>(listSegment.getTariffs()));
						parser.addAirlineSegment(route, newSegment);
						segments.add(newSegment);
					}
				}
			}
			segmentsToRoutes.put(s, route);
			routesToSegmentsList.put(route, segments);
		}
		route.addTicket(ticket);

		ticket.setValidUntil(route.getDeparture());

		ticket.setCurrency(parser.findCurrency("UAH"));

		double amount = 0;

		tempIndex = data.indexOf("\nK-", tempIndex);
		if (tempIndex == -1) {
			throw new ParsingException("K- not found.");
		} else {
			if (data.charAt(tempIndex + 3) == '\r') {
				tempIndex = data.indexOf("\nKN-", tempIndex + 4);
				if (tempIndex == -1) {
					ticket.setBaseCurrency(parser.findCurrency("UAH"));
				} else {
					String currencyCode = data.substring(index = StringUtils.indexOf(data, ";", 12, tempIndex) + 1, index += 3);
					ticket.setBaseCurrency(parser.findCurrency(currencyCode));
					amount = Casting.asDouble(data.substring(index, data.indexOf(';', index)).strip());
				}
			} else {
				String currencyCode = data.substring(index = StringUtils.indexOf(data, ";", 12, tempIndex) + 1, index += 3);
				ticket.setBaseCurrency(parser.findCurrency(currencyCode));
				amount = Casting.asDouble(data.substring(index, data.indexOf(';', index)).strip());
			}
		}

		index = data.indexOf('\n', index);
		if (data.substring(index + 1, index + 5).equals("TAX-") || data.substring(index + 1, index + 4).equals("KFT")
				|| data.substring(index + 1, index + 5).equals("KNTI")) {
			for (String tax : data.substring(index + 5, index = data.indexOf('\r', index + 5)).split(";")) {
				if (tax.isEmpty()) {
					continue;
				} else {
					String taxCode = tax.substring(13, 15);
					double taxAmount = Casting.asDouble(tax.substring(4, 13).strip());
					parser.addAirlineTax(ticket, taxCode, taxAmount, documentDate);
					amount -= taxAmount;
				}
			}
		}

		ticket.setBaseAmount(amount);

		index = data.indexOf("\nM-", index);
		String mString = data.substring(index = index + 3, index = data.indexOf('\r', index));
		String[] tariffCodes = null;
		if (!mString.isEmpty()) {
			tariffCodes = mString.split(";");
		}

		if (tariffCodes != null) {
			int i = 0;
			for (AirlineSegment segment : routesToSegmentsList.get(route)) {
				AirlineTariff tariff = new AirlineTariff();
				tariff.setConsumer(ticket.getConsumer());
				tariff.setCode(tariffCodes[i++].strip());
				segment.addTariff(tariff);
			}
		}
		tickets.add(ticket);
		return index;
	}

}
package com.cleverforms.parser.kiyaviaua;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class KiyaviaUaInsuranceDocument {
	public class Good {
		public String lastName, firstName;
		public double amount;
	}

	protected Document document;

	protected XPath xPath = XPathFactory.newInstance().newXPath();

	public KiyaviaUaInsuranceDocument(File file) throws ParserConfigurationException, FactoryConfigurationError, SAXException, IOException {
		DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		this.document = documentBuilder.parse(file);
	}

	public Date getTicketingDate() throws XPathExpressionException, ParseException {
		String ticketingDate = xPath.compile("/order_1с_snapshot/booking/@ticketing_date").evaluate(document);
		try {
			return new SimpleDateFormat("yyyy-MM-dd").parse(ticketingDate);
		} catch (ParseException e) {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(ticketingDate);
		}
	}

	public String getOperation() throws XPathExpressionException {
		return xPath.compile("/order_1с_snapshot/booking/@operation").evaluate(document);
	}
	
	public String getLocator() throws XPathExpressionException {
		return xPath.compile("/order_1с_snapshot/booking/@locator").evaluate(document);
	}
	
	public String getBookingId() throws XPathExpressionException {
		return xPath.compile("/order_1с_snapshot/booking/@booking_id").evaluate(document);
	}

	public Double getServiceFee() throws XPathExpressionException {
		return Double.parseDouble(xPath.compile("/order_1с_snapshot/booking/fees/item/@service_fee").evaluate(document));
	}

	public List<Good> getGoods() throws XPathExpressionException {
		List<Good> tickets = new ArrayList<>();
		NodeList goodsNodes = (NodeList) xPath.compile("/order_1с_snapshot/booking/goods/item").evaluate(document, XPathConstants.NODESET);
		for (int i = 0; i < goodsNodes.getLength(); i++) {
			Good good = new Good();
			Element ticketElement = (Element) goodsNodes.item(i);
			good.lastName = ticketElement.getAttribute("last_name");
			good.firstName = ticketElement.getAttribute("first_name");
			good.amount = Double.parseDouble(ticketElement.getAttribute("amount"));
			tickets.add(good);
		}
		return tickets;
	}
}

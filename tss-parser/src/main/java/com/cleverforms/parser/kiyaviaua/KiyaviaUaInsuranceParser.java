package com.cleverforms.parser.kiyaviaua;

import java.io.File;
import org.springframework.beans.factory.annotation.Autowired;
import com.cleverforms.comms.server.tasks.FileParserImpl;
import com.cleverforms.parser.Parser;

public class KiyaviaUaInsuranceParser extends FileParserImpl {
	@Autowired
	Parser parser;

	@Override
	public String name() {
		return "Kiyavia insurance data parser";
	}

	@Override
	public boolean parseFile(File file) throws Exception {
		// return insuranceParser.parse(new KiyaviaUaInsuranceTssDocument(file));
		return true;
	}
}

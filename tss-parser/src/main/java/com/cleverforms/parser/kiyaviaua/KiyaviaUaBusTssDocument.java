package com.cleverforms.parser.kiyaviaua;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class KiyaviaUaBusTssDocument extends KiyaviaUaBusDocument {
	public KiyaviaUaBusTssDocument(File file) throws ParserConfigurationException, FactoryConfigurationError, SAXException, IOException {
		super(file);
	}

	/*-@Override
	public String getType() {
		return "BUS_KA.UA";
	}

	@Override
	public String getNumber() throws XPathExpressionException {
		return getBookingId();
	}

	@Override
	public List<DocumentRoute> getRoutes() throws XPathExpressionException, DOMException, ParsingException, ParseException {
		List<DocumentRoute> routes = new ArrayList<>();

		DocumentRoute route = new DocumentRoute();
		// TODO Set real station from, station to, arrival date, departure date, trip number
		route.stationFrom = "IEV";
		route.stationTo = "IEV";
		route.arrivalDate = new Date();
		route.departureDate = new Date();
		route.tripNumber = "";
		for (Good good : getGoods()) {
			DocumentTicket ticket = new DocumentTicket();
			ticket.number = getBookingId();
			ticket.passenger = new DocumentConsumer(good.firstName, good.lastName);
			ticket.tariff = "";
			ticket.seatNumber = "";
			ticket.baseAmount = good.amount;
			route.tickets.add(ticket);
			if (route.tickets.size() == 1) {
				ticket.taxes.put("COMMISSION", getServiceFee());
			}
		}

		routes.add(route);

		return routes;
	}

	@Override
	public Date getDate() throws XPathExpressionException, ParseException {
		return getTicketingDate();
	}

	@Override
	public OrderState getOperationType() throws XPathExpressionException, ParsingException {
		if (getOperation().equals("sale")) {
			return OrderState.SALE;
		} else if (getOperation().equals("refund")) {
			return OrderState.REFUND;
		} else {
			throw new ParsingException("Unknown operation type");
		}
	}

	@Override
	public Set<String> getVoidTicketNumbers() throws XPathExpressionException, ParsingException {
		Set<String> voidTicketNumbers = new HashSet<>();
		voidTicketNumbers.add(getLocator());
		return voidTicketNumbers;
	}

	@Override
	public String getBaseCurrency() throws XPathExpressionException, UnsupportedEncodingException, IOException {
		return "UAH";
	}

	@Override
	public String getCounterparty() {
		return "";
	}*/
}

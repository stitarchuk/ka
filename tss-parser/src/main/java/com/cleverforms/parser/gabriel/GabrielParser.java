package com.cleverforms.parser.gabriel;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Multipart;
import jakarta.mail.internet.InternetAddress;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import com.cleverforms.comms.server.tasks.EmailParserImpl;
import com.cleverforms.comms.shared.exception.ParsingException;
import com.cleverforms.ics.core.provider.ICSPersistentProvider;
import com.cleverforms.ics.db.model.financial.AbstractService;
import com.cleverforms.ics.db.model.orgchart.Sector;
import com.cleverforms.ics.shared.enumerator.OrderState;
import com.cleverforms.iss.server.model.airline.AirlineRoute;
import com.cleverforms.iss.server.model.airline.AirlineSegment;
import com.cleverforms.iss.server.model.airline.AirlineTicket;
import com.cleverforms.iss.shared.airline.AirlineTicketType;
import com.cleverforms.parser.Parser;
import com.cleverforms.parser.StringUtils;

public class GabrielParser extends EmailParserImpl {

	@Autowired
	public Parser parser;

	public final Sector sector = new Sector();

	@Override
	public ICSPersistentProvider provider() {
		return (ICSPersistentProvider) super.provider();
	}

	public void setSectorId(long sectorId) {
		sector.setId(sectorId);
	}

	@Override
	public String name() {
		return "Gabriel data parser";
	}

	@Override
	public boolean parseEmail(final Message email) {
		return provider().txExecute(new TransactionCallback<Boolean>() {
			@SuppressWarnings("deprecation")
			@Override
			public Boolean doInTransaction(TransactionStatus status) {
				try {
					String type;
					String sender = ((InternetAddress) email.getFrom()[0]).getAddress();
					switch (sender) {
					case "etix@s7.ru":
						type = "S7";
						break;
					case "callcenter@ps.kiev.ua":
					case "contact@flyuia.com":
						type = "PS";
						break;
					case "noreply@airturkmenistan.com":
						type = "T5";
						break;
					case "einterline@dniproavia.com":
						type = "Z6";
						break;
					default:
						warn("Unknown sender: " + sender);
						return false;
					}

					SimpleDateFormat documentDateFormat = new SimpleDateFormat("ddMMMyy", Locale.ENGLISH);
					SimpleDateFormat segmentDateFormat = new SimpleDateFormat("ddMMMyyyy", Locale.ENGLISH);
					SimpleDateFormat timeFormat = new SimpleDateFormat("HHmm");

					String subject = email.getSubject();
					if (subject.startsWith("Itinerary Receipt")) {
						String content = StringEscapeUtils.unescapeHtml(((Multipart) email.getContent()).getBodyPart(0).getContent().toString());

						AirlineTicket ticket = new AirlineTicket();

						ticket.setType(AirlineTicketType.TRAVEL);

						ticket.setSector(sector);

						ticket.setCurrency(parser.findCurrency("UAH"));

						int index = content.indexOf(type.equals("Z6") ? "DATE OF ISSUE" : "DATE OF ISSUE") + 14;
						Date dateOfIssue = documentDateFormat.parse(content.substring(index, index + 3) + content.substring(index + 3, index + 5).toLowerCase()
								+ content.substring(index + 5, index += 7));
						ticket.setCreated(dateOfIssue);

						index = content.indexOf("RLOC") + 5;
						ticket.setSupplier(parser.findCompany(content.substring(index, index + 2)));

						ticket.setExtNumber("GABRIEL-" + content.substring(index + 5, index + 10));

						index = content.indexOf(type.equals("Z6") ? "E-TICKET NUMBER" : "E-TICKET NUMBER") + 17;
						String ticketNumber;
						if (type.equals("Z6")) {
							ticketNumber = content.substring(index, content.indexOf(' ', index)).strip();
						} else {
							ticketNumber = content.substring(index, content.indexOf(' ', index));
						}
						if (parser.findAirlineTicket(ticketNumber) != null) {
							warn("Ticket " + ticketNumber + " already exists");
							return false;
						}
						ticket.setNumber(ticketNumber);

						index = content.indexOf("NAME:") + 6;
						String lastName = content.substring(index, index = content.indexOf('/', index));
						String firstName = content.substring(++index, index = content.indexOf(type.equals("Z6") ? "\r" : "<BR>", index));
						if (firstName.endsWith(" MRS")) {
							firstName = firstName.substring(0, firstName.length() - 4);
						} else if (firstName.endsWith(" MR")) {
							firstName = firstName.substring(0, firstName.length() - 3);
						} else if (firstName.endsWith(" MS")) {
							firstName = firstName.substring(0, firstName.length() - 3);
						} else if (firstName.endsWith(" MISS")) {
							firstName = firstName.substring(0, firstName.length() - 5);
						} else if (firstName.endsWith(" MSTR")) {
							firstName = firstName.substring(0, firstName.length() - 5);
						} else if (firstName.endsWith(" NM")) {
							firstName = firstName.substring(0, firstName.length() - 3);
						} else if (firstName.endsWith(" CHD")) {
							firstName = firstName.substring(0, firstName.length() - 4);
						} else if (firstName.endsWith("MRS")) {
							firstName = firstName.substring(0, firstName.length() - 3);
						} else if (firstName.endsWith("MR")) {
							firstName = firstName.substring(0, firstName.length() - 2);
						} else if (firstName.endsWith("MS")) {
							firstName = firstName.substring(0, firstName.length() - 2);
						} else if (firstName.endsWith("MSS")) {
							firstName = firstName.substring(0, firstName.length() - 3);
						} else if (firstName.endsWith("MSTR")) {
							firstName = firstName.substring(0, firstName.length() - 4);
						}
						ticket.setConsumer(parser.findConsumer(firstName, lastName));

						index = content.indexOf(type.equals("Z6") ? "ISSUED IN EXCH:" : "ISSUED IN EXCH:");
						if (index != -1) {
							String exchangeTicketNumber = content.substring(index + 17, index = content.indexOf(type.equals("Z6") ? "\r" : "<BR>", index + 20));
							AirlineTicket exchangeTicket = parser.findAirlineTicket(exchangeTicketNumber);
							if (exchangeTicket == null) {
								warn("Ticket " + exchangeTicketNumber + " not found");
								return false;
							}
							ticket.setState(OrderState.EXCHANGE);

							AbstractService as = new AbstractService();
							as.setId(exchangeTicket.getId());
							ticket.setParent(as);
						} else {
							ticket.setState(OrderState.SALE);
						}

						AirlineRoute route = new AirlineRoute();
						int dateOfIssueYear = dateOfIssue.getYear() + 1900;
						index = type.equals("Z6") ? content.indexOf("BAG\r") + 5 : content.indexOf("BAG<BR>") + 9;
						if (type.equals("PS") || type.equals("S7")) {
							index = content.indexOf("<BR>", index) + 6;
						}
						do {
							AirlineSegment segment = new AirlineSegment();

							String departureDayMonth = content.substring(index, index += 3) + content.substring(index, index += 2).toLowerCase();
							Date departureDate = segmentDateFormat.parse(departureDayMonth + dateOfIssueYear);
							if (departureDate.before(dateOfIssue)) {
								departureDate.setYear(departureDate.getYear() + 1);
							}

							segment.setCarrier(parser.findCompany(content.substring(++index, index += 2)));

							segment.setTripNumber(content.substring(++index, index = content.indexOf(type.equals("Z6") ? ' ' : ' ', index)));

							while (content.charAt(index) == (type.equals("Z6") ? ' ' : ' ')) {
								index++;
							}
							segment.setAirportFrom(parser.findAirportByCode(content.substring(index, index += 3), null));

							segment.setDeparture(com.cleverforms.parser.DateUtils.dateTime(departureDate,
									timeFormat.parse(content.substring(index += 21, index += 4))));

							while (content.charAt(index) == (type.equals("Z6") ? ' ' : ' ')) {
								index++;
							}
							segment.setAirportTo(parser.findAirportByCode(content.substring(index, index += 3), null));

							segment.setTicketCategory(content.substring(index += 21, ++index));

							index = content.indexOf("ARRIVAL:", index) + 8;
							if (index == 7) {
								warn("Arrival date not found");
								return false;
							}
							Date arrivalTime = timeFormat.parse(content.substring(index, index += 4));
							String arrivalDayMonth = content.substring(index + 1, index + 6);
							Date arrivalDate = segmentDateFormat.parse((arrivalDayMonth.equals(type.equals("Z6") ? "     " : "     ") ? departureDayMonth
									: arrivalDayMonth.substring(0, 3) + arrivalDayMonth.substring(3, 5).toLowerCase()) + (departureDate.getYear() + 1900));
							if (arrivalDate.before(departureDate)) {
								arrivalDate.setYear(arrivalDate.getYear() + 1);
							}
							segment.setArrive(com.cleverforms.parser.DateUtils.dateTime(arrivalDate, arrivalTime));

							parser.addAirlineSegment(route, segment);

							if (type.equals("Z6")) {
								index = content.indexOf("\r", index) + 2;
							} else {
								index = content.indexOf("<BR>", index) + 12;
							}
						} while ((type.equals("PS") || type.equals("S7")) && !"RESTRICTIONS:".equals(content.substring(index, index + 13)) || type.equals("T5")
								&& !"GOŞMAÇA MAGLUMAT:".equals(content.substring(index, index + 17)) || type.equals("Z6")
								&& !content.substring(index += 2, index + 13).equals("RESTRICTIONS:"));

						ticket.setValidUntil(route.getDeparture());

						double amount = 0;
						index = content.indexOf(type.equals("Z6") ? "TICKET TOTAL" : "TICKET TOTAL") + 12;
						while (content.charAt(index) == (type.equals("Z6") ? ' ' : ' ')) {
							index++;
						}
						String ticketTotalCurrencyCode = content.substring(index, index = content.indexOf(type.equals("Z6") ? ' ' : ' ', index));
						while (content.charAt(index) == (type.equals("Z6") ? ' ' : ' ')) {
							index++;
						}
						boolean it;
						if (ticketTotalCurrencyCode.equals("IT")) {
							it = true;
							ticket.setBaseCurrency(parser.findCurrency(content.substring(index, content.indexOf(type.equals("Z6") ? "\r" : "<BR>", index))));
						} else {
							it = false;
							String ticketTotalAmount = content.substring(index, index = content.indexOf(type.equals("Z6") ? "\r" : "<BR>", index));
							if (ticketTotalCurrencyCode.equals("NO")
									&& ticketTotalAmount.equals(type.equals("Z6") ? "ADDITIONAL COLLECTION " : "ADDITIONAL COLLECTION ")) {
								ticket.setBaseCurrency(parser.findCurrency("UAH"));
							} else {
								ticket.setBaseCurrency(parser.findCurrency(ticketTotalCurrencyCode));
								if (ticketTotalAmount.endsWith(type.equals("Z6") ? "  ADDITIONAL COLLECTION" : "  ADDITIONAL COLLECTION")) {
									amount = Double.parseDouble(ticketTotalAmount.substring(0, ticketTotalAmount.length() - 23));
								} else {
									amount = Double.parseDouble(ticketTotalAmount);
								}
							}
						}

						index = content.indexOf("TAX/FEE/CHARGE") + 14;
						if (index != 13) {
							while (content.charAt(index) == (type.equals("Z6") ? ' ' : ' ')) {
								index++;
							}
							String taxFeeCharge = content.substring(index, content.indexOf(type.equals("Z6") ? "\r" : "<BR>", index));
							if (Character.isDigit(taxFeeCharge.charAt(taxFeeCharge.length() - 1))) {
								switch (type) {
								case "PS":
								case "S7":
									index = content.indexOf("ДЕТАЛИЗАЦИЯ СБОРА") + 17;
									index = content.indexOf("<BR>", index);
									break;
								case "T5":
									throw new ParsingException("T5 TAX/FEE/CHARGE ITEMIZATION");
								case "Z6":
									index = content.indexOf("TAX/FEE/CHARGE ITEMIZATION") + 26;
									index = content.indexOf("\r", index) + 2;
									break;
								default:
									warn("Unknown type: " + type);
									return false;
								}
								Pattern taxPatern = Pattern.compile("\\d+[A-Z][A-Z0-9]");
								Matcher taxMatcher = taxPatern.matcher(content.substring(index += 6,
										content.indexOf(type.equals("Z6") ? "\r\n\r" : "<BR>\r\n<BR>", index)));
								while (taxMatcher.find()) {
									String taxRecord = taxMatcher.group();
									String taxCode = taxRecord.substring(taxRecord.length() - 2);
									Double taxAmount = Double.parseDouble(taxRecord.substring(0, taxRecord.length() - 2));
									parser.addAirlineTax(ticket, taxCode, taxAmount, dateOfIssue);
									if (!it) {
										amount -= taxAmount;
									}
								}
							} else {
								taxFeeCharge = taxFeeCharge.substring(3);
								for (int i = 0; i < taxFeeCharge.length(); i++) {
									if (taxFeeCharge.charAt(i) != ' ') {
										taxFeeCharge = taxFeeCharge.substring(i);
										break;
									}
								}
								for (String taxRecord : taxFeeCharge.split("/")) {
									String taxCode = taxRecord.substring(taxRecord.length() - 2);
									Double taxAmount = Double.parseDouble(taxRecord.substring(0, taxRecord.length() - 2));
									parser.addAirlineTax(ticket, taxCode, taxAmount, dateOfIssue);
									if (!it) {
										amount -= taxAmount;
									}
								}
							}
						}

						ticket.setBaseAmount(amount);

						route.addTicket(ticket);
						parser.getProvider().save(route);

						return true;
					} else if (subject.startsWith("EMD Receipt")) {
						String content = StringEscapeUtils.unescapeHtml(((Multipart) email.getContent()).getBodyPart(0).getContent().toString());

						int index = content.indexOf(type.equals("Z6") ? "DATE OF ISSUE" : "DATE OF ISSUE") + 14;
						Date dateOfIssue = documentDateFormat.parse(content.substring(index, index + 3) + content.substring(index + 3, index + 5).toLowerCase()
								+ content.substring(index + 5, index += 7));

						index = content.indexOf("RLOC") + 5;
						String carrier = content.substring(index, index + 2);
						String pnr = content.substring(index + 5, index + 10);

						index = content.indexOf(type.equals("Z6") ? "DOCUMENT NUMBER" : "DOCUMENT NUMBER") + 17;
						String documentNumber = content.substring(index, content.indexOf(type.equals("Z6") ? "\r" : "<BR>", index)).strip();

						AirlineTicket ticket;

						AirlineRoute route = new AirlineRoute();
						index = content.indexOf("СУММА  <BR>") + 13;
						do {
							AirlineSegment segment = new AirlineSegment();

							String departureDayMonth = content.substring(index, index += 3) + content.substring(index, index += 2).toLowerCase();
							Date departureDate = segmentDateFormat.parse(departureDayMonth + (dateOfIssue.getYear() + 1900));
							if (departureDate.before(dateOfIssue)) {
								departureDate.setYear(departureDate.getYear() + 1);
							}

							segment.setCarrier(parser.findCompany(content.substring(++index, index += 2)));

							segment.setTripNumber("");

							while (content.charAt(index) == ' ') {
								index++;
							}
							segment.setAirportFrom(parser.findAirportByCode(content.substring(index, index += 3), null));

							segment.setDeparture(departureDate);

							index += 23;
							segment.setAirportTo(parser.findAirportByCode(content.substring(index, index += 3), null));

							segment.setTicketCategory("");

							segment.setArrive(departureDate);

							parser.addAirlineSegment(route, segment);

							index = content.indexOf("ASSOCIATED E-TICKET/COUPON NUMBER:", index) + 41;
							String ticketNumber = content.substring(index, content.indexOf(" ", index + 1));
							ticket = parser.findAirlineTicket(ticketNumber);
							if (ticket == null) {
								warn("Ticket " + ticketNumber + " not found");
								return false;
							}

							index = StringUtils.indexOf(content, "<BR>", 4, index);
							index += 6;
						} while (!content.substring(index, index + 4).equals("<BR>"));

						index = content.indexOf("TOTAL") + 5;
						while (content.charAt(index) == (type.equals("Z6") ? ' ' : ' ')) {
							index++;
						}
						String ticketTotalCurrencyCode = content.substring(index, index = content.indexOf(type.equals("Z6") ? ' ' : ' ', index));
						while (content.charAt(index) == (type.equals("Z6") ? ' ' : ' ')) {
							index++;
						}
						double amount = Double.parseDouble(content.substring(index, index = content.indexOf(type.equals("Z6") ? "\r" : "<BR>", index)));

						AirlineTicket emd = new AirlineTicket();
						emd.setNumber(documentNumber);
						emd.setSector(sector);
						emd.setSupplier(parser.findCompany(carrier));
						emd.setConsumer(ticket.getConsumer());
						emd.setState(OrderState.SALE);
						emd.setExtNumber("GABRIEL-" + pnr);
						emd.setBaseCurrency(parser.findCurrency(ticketTotalCurrencyCode));
						emd.setCurrency(parser.findCurrency("UAH"));
						AbstractService as = new AbstractService();
						as.setId(ticket.getId());
						emd.setParent(as);
						route.addTicket(emd);
						emd.setType(AirlineTicketType.EMD);
						emd.setBaseAmount(amount);
						emd.setCreated(dateOfIssue);
						emd.setValidUntil(route.getDeparture());

						parser.getProvider().save(route);

						return true;
					} else {
						warn("Not an itinerary or an EMD");
						return false;
					}
				} catch (NumberFormatException | MessagingException | IOException | ParseException e) {
					error("", e);
					return false;
				}
			}
		});
	}

}

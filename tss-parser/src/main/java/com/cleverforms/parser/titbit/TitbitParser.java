package com.cleverforms.parser.titbit;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.cleverforms.comms.server.tasks.FileParserImpl;
import com.cleverforms.ics.core.provider.ICSPersistentProvider;
import com.cleverforms.ics.db.model.dictionary.City;
import com.cleverforms.ics.db.model.dictionary.Company;
import com.cleverforms.ics.db.model.dictionary.Consumer;
import com.cleverforms.ics.db.model.dictionary.Country;
import com.cleverforms.ics.db.model.dictionary.Currency;
import com.cleverforms.ics.db.model.orgchart.Person;
import com.cleverforms.ics.db.model.orgchart.Post;
import com.cleverforms.ics.db.model.orgchart.Sector;
import com.cleverforms.ics.shared.dictionary.property.CityProperty;
import com.cleverforms.ics.shared.dictionary.property.CompanyProperty;
import com.cleverforms.ics.shared.enumerator.OrderState;
import com.cleverforms.iss.server.model.travel.TravelService;
import com.cleverforms.parser.Parser;

public class TitbitParser extends FileParserImpl {

	@Autowired
	Parser parser;

	SimpleDateFormat titbitDateFormat = new SimpleDateFormat("MM/dd/yy");
	SimpleDateFormat titbitDateTimeFormat = new SimpleDateFormat("MM/dd/yy hh:mm:ss a");

	@Override
	public ICSPersistentProvider provider() {
		return (ICSPersistentProvider) super.provider();
	}

	@Override
	public String name() {
		return "Titbit data parser";
	}

	@Override
	public boolean parseFile(final File file) {
		return provider().txExecute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus status) {
				try (InputStream stream = new FileInputStream(file)) {
					DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document document = documentBuilder.parse(new InputSource(new InputStreamReader(stream, "windows-1251")));
					XPath xPath = XPathFactory.newInstance().newXPath();

					String stateName = xPath.compile("/Titbit_BookingRQ/booking/state").evaluate(document);
					OrderState state;
					switch (stateName) {
					case "confirmed":
						state = OrderState.SALE;
						break;
					case "request":
						state = OrderState.BOOKING;
						break;
					case "canceled":
						state = OrderState.VOID;
						break;
					default:
						warn("Unknown service state: " + stateName);
						return false;
					}

					Date documentDate = titbitDateTimeFormat.parse(xPath.compile("/Titbit_BookingRQ/@date").evaluate(document));

					String orderNumber = xPath.compile("/Titbit_BookingRQ/booking/@n_code").evaluate(document);

					String destination = xPath.compile("/Titbit_BookingRQ/booking/destination").evaluate(document);

					// String countryName = xPath.compile("/Titbit_BookingRQ/booking/country").evaluate(document);

					String bookingDStart = xPath.compile("/Titbit_BookingRQ/booking/@d_start").evaluate(document);
					Date bookingStart = null;
					if (bookingDStart.isEmpty()) {
						warn("/Titbit_BookingRQ/booking/@d_start is empty");

						return false;
					}
					bookingStart = titbitDateFormat.parse(bookingDStart);

					String bookingDFinish = xPath.compile("/Titbit_BookingRQ/booking/@d_finish").evaluate(document);
					Date bookingFinish = null;
					if (bookingDFinish.isEmpty()) {
						warn("/Titbit_BookingRQ/booking/@d_finish is empty");

						return false;
					}
					bookingFinish = titbitDateFormat.parse(bookingDFinish);

					List<Consumer> passengers = new ArrayList<>();
					NodeList passengerElements = (NodeList) xPath.compile("/Titbit_BookingRQ/clients/client").evaluate(document, XPathConstants.NODESET);
					for (int i = 0; i < passengerElements.getLength(); i++) {
						Element passengerElement = (Element) passengerElements.item(i);
						passengers.add(parser.findConsumer(passengerElement.getAttribute("firstname"), passengerElement.getAttribute("lastname")));
					}

					Element saleTotalElement = (Element) xPath.compile("/Titbit_BookingRQ/booking/saletotal").evaluate(document, XPathConstants.NODE);
					Currency baseCurrency = parser.findCurrency(saleTotalElement.getAttribute("c_currency"));
					double baseAmount = Double.parseDouble(saleTotalElement.getTextContent());

					String userId = xPath.compile("/Titbit_BookingRQ/booking/user/@id").evaluate(document);
					if (userId.isEmpty()) {
						warn("/Titbit_BookingRQ/booking/user/@id not found");

						return false;
					}
					Person agent = (Person) parser.getProvider().getSingleResult("from Person where lower(aliases[16]) = lower(?0)", userId);
					Post post = null;
					if (agent != null) {
						post = agent.getPost();
					}

					NodeList serviceElements = (NodeList) xPath.compile("/Titbit_BookingRQ/services/service").evaluate(document, XPathConstants.NODESET);
					if (serviceElements.getLength() > 0) {
						Element serviceElement = (Element) serviceElements.item(0);

						String category = ((Element) serviceElement.getElementsByTagName("category").item(0)).getTextContent().strip();
						Sector sector = new Sector();
						switch (category) {
						case "Ticket":
							sector.setId(7); // Інше
							break;
						case "Extra charge":
							sector.setId(7); // Інше
							break;
						case "Організація конференцій":
							sector.setId(76); // Організація конференцій
							break;
						case "Flight":
							sector.setId(12); // Чартерний рейс
							break;
						case "Харчування":
							sector.setId(7); // Інше
							break;
						case "Visa":
							sector.setId(78); // Візова підтримка
							break;
						case "Excursion":
							sector.setId(61); // Організація екскурсій
							break;
						case "Travel Package":
							sector.setId(62); // Послуги туризму
							break;
						case "Insurance":
							sector.setId(5); // Страхування
							break;
						case "Other service":
							sector.setId(7); // Інше
							break;
						case "Transfer":
							sector.setId(44); // Трансфери
							break;
						case "Hotel":
							sector.setId(4); // Готелі
							break;
						default:
							sector.setId(7); // Інше
							break;
						}

						String supplier = ((Element) serviceElement.getElementsByTagName("supplier").item(0)).getTextContent().strip();
						Company company = (Company) parser.getProvider().getSingleResult(
								"select c from Company c left join c.properties cp where index(cp) in (?1) and upper(cp) = ?0", supplier.toUpperCase(),
								CompanyProperty.NAME_PROPERTIES);
						if (company == null && !supplier.isEmpty()) {
							company = new Company();
							company.setCountry((Country) parser.getProvider().getSingleResult("from Country where id = 242"));
							company.setStartDate(new Date());
							Pattern pattern = Pattern.compile("[ҐґЄєІіЇї]");
							Matcher matcher = pattern.matcher(supplier);
							if (matcher.find()) {
								company.putProperty(CompanyProperty.NAME_UK, supplier);
							} else {
								pattern = Pattern.compile("[А-Яа-я]");
								matcher = pattern.matcher(supplier);
								if (matcher.find()) {
									company.putProperty(CompanyProperty.NAME_RU, supplier);
								} else {
									company.putProperty(CompanyProperty.NAME_EN, supplier);
								}
							}
							parser.getProvider().save(company);
							warn("Company '" + supplier + "' added");
						}

						TravelService service;

						if ((service = parser.findTitbitService(orderNumber)) == null) {
							service = new TravelService();

							service.setCreated(documentDate);
						} else {
							if (service.getOrder() != null) {
								warn("Service " + orderNumber + " already bound to an order!");

								return false;
							}
						}

						if (passengers.isEmpty()) {
							warn("No clients found");

							return false;
						}
						service.setConsumer(passengers.get(0));

						service.setPost(post);

						service.setSupplier(company);

						service.setNumber(orderNumber);

						service.setDateFrom(bookingStart);

						service.setDateTo(bookingFinish);

						service.setBaseCurrency(baseCurrency);

						service.setBaseAmount(baseAmount);

						service.setExtNumber("ТИТБИТ-" + orderNumber);

						service.setCurrency(baseCurrency);

						service.setSector(sector);

						service.setState(state);

						service.setValidUntil(new Date());

						final String sql = "from City city left join city.properties p where index(p) = ?1 and lower(p) = ?0";
						City city = (City) parser.getProvider().getSingleResult(sql, destination.toLowerCase(), CityProperty.NAME_UK);
						if (city == null) {
							city = (City) parser.getProvider().getSingleResult(sql, destination.toLowerCase(), CityProperty.NAME_RU);
							if (city == null) {
								city = (City) parser.getProvider().getSingleResult(sql, destination.toLowerCase(), CityProperty.NAME_EN);
							}
						}
						service.setCity(city);

						/*
						final String sql = "from Country country left join country.properties p where index(p) = ?1 and lower(p) = ?0";
						Country country = (Country) parser.getProvider().getSingleResult(sql, countryName.toLowerCase(), CountryProperty.NAME_UK);
						if (country == null) {
							country = (Country) parser.getProvider().getSingleResult(sql, countryName.toLowerCase(), CountryProperty.NAME_RU);
							if (country == null) {
								country = (Country) parser.getProvider().getSingleResult(sql, countryName.toLowerCase(), CountryProperty.NAME_EN);
							}
						}
						service.setCountry(country);*/

						String description = "";
						String resource = ((Element) serviceElement.getElementsByTagName("resource").item(0)).getTextContent().strip();
						if (!resource.isEmpty()) {
							description += resource;
						}
						String serviceField = ((Element) serviceElement.getElementsByTagName("service").item(0)).getTextContent().strip();
						if (!serviceField.isEmpty()) {
							if (!description.isEmpty()) {
								description += ", ";
							}
							description += serviceField;
						}
						service.setDescription(description);

						String additionalServices = "";
						for (int i = 1; i < serviceElements.getLength(); i++) {
							serviceElement = (Element) serviceElements.item(i);

							if (!additionalServices.isEmpty()) {
								additionalServices += "; ";
							}

							category = ((Element) serviceElement.getElementsByTagName("category").item(0)).getTextContent().strip();
							if (!category.isEmpty()) {
								additionalServices += category;
							}

							resource = ((Element) serviceElement.getElementsByTagName("resource").item(0)).getTextContent().strip();
							if (!resource.isEmpty()) {
								if (!additionalServices.isEmpty()) {
									additionalServices += ", ";
								}
								additionalServices += resource;
							}

							serviceField = ((Element) serviceElement.getElementsByTagName("service").item(0)).getTextContent().strip();
							if (!serviceField.isEmpty()) {
								if (!additionalServices.isEmpty()) {
									additionalServices += ", ";
								}
								additionalServices += serviceField;
							}

						}
						service.setAdditionalServices(additionalServices);

						parser.getProvider().save(service);
					}

					return true;
				} catch (NumberFormatException | XPathExpressionException | DOMException | ParserConfigurationException | SAXException | IOException
						| ParseException e) {
					error("", e);
					return false;
				}
			}
		});
	}
}

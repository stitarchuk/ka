package com.cleverforms.parser.lis;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cleverforms.comms.server.tasks.FileParserImpl;
import com.cleverforms.ics.core.provider.ICSPersistentProvider;
import com.cleverforms.ics.db.model.dictionary.Company;
import com.cleverforms.ics.db.model.dictionary.Consumer;
import com.cleverforms.ics.db.model.dictionary.Currency;
import com.cleverforms.ics.db.model.orgchart.Person;
import com.cleverforms.ics.db.model.orgchart.Post;
import com.cleverforms.ics.db.model.orgchart.Sector;
import com.cleverforms.ics.shared.enumerator.OrderState;
import com.cleverforms.iss.server.model.insurance.InsuranceProgram;
import com.cleverforms.iss.server.model.insurance.InsuranceService;
import com.cleverforms.parser.Parser;

public class LisParser extends FileParserImpl {

	@Autowired
	Parser parser;

	public final Sector sector = new Sector();

	public Company supplier;

	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public ICSPersistentProvider provider() {
		return (ICSPersistentProvider) super.provider();
	}

	public void setSectorId(long sectorId) {
		sector.setId(sectorId);
	}

	public void setSupplierId(int supplierId) {
		supplier = (Company) parser.getProvider().getSingleResult("from Company where id = " + supplierId);
	}

	@Override
	public String name() {
		return "Lis data parser";
	}

	@Override
	public boolean parseFile(final File file) {
		return provider().txExecute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus status) {
				try {
					DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document document = documentBuilder.parse(file);
					XPath xPath = XPathFactory.newInstance().newXPath();

					String state = xPath.compile("/Policy/PolicyHeader/State").evaluate(document);
					OrderState orderState;
					switch (state) {
					case "active":
						orderState = OrderState.SALE;
						break;
					case "void":
						orderState = OrderState.VOID;
						break;
					case "refund":
						orderState = OrderState.REFUND;
						break;
					default:
						warn("unknown status: " + state);
						return false;
					}

					String agencyID = xPath.compile("/Policy/PolicyHeader/AgencyID").evaluate(document);
					if (agencyID.isEmpty()) {
						warn("agencyID not found");
						return false;
					}
					Person agent = (Person) parser.getProvider().getSingleResult("from Person where lower(aliases[15]) = lower(?0)", agencyID);
					Post post = null;
					if (agent != null) {
						post = agent.getPost();
					}

					String policyNumber = xPath.compile("/Policy/PolicyHeader/PolicyNumber").evaluate(document);

					Date issueDate = dateFormat.parse(xPath.compile("/Policy/PolicyHeader/IssueDate").evaluate(document));

					Date dateFrom = dateFormat.parse(xPath.compile("/Policy/PolicyHeader/DateFrom").evaluate(document));

					Date dateTo = dateFormat.parse(xPath.compile("/Policy/PolicyHeader/DateTo").evaluate(document));

					NodeList allInsured = (NodeList) xPath.compile("/Policy/PolicyExtend/Persons/Insurant[IsInsured=1] | /Policy/PolicyExtend/Persons/Insured")
							.evaluate(document, XPathConstants.NODESET);
					int insuranceNumber = 1;
					for (int i = 0; i < allInsured.getLength(); i++) {
						Element insured = (Element) allInsured.item(i);

						String firstName = xPath.compile("FirstName").evaluate(insured);
						String lastName = xPath.compile("LastName").evaluate(insured);
						Consumer consumer = parser.findConsumer(firstName, lastName);

						InsuranceService service = new InsuranceService();

						service.setPost(post);

						service.setSector(sector);

						service.setState(orderState);

						service.setSupplier(supplier);

						String serviceNumber = policyNumber + "-" + insuranceNumber;
						if (parser.findInsuranceService(serviceNumber) != null) {
							warn("Insurance service " + serviceNumber + " already exists");
							return false;
						}
						service.setNumber(serviceNumber);

						service.setExtNumber("LIS-" + policyNumber);

						service.setCreated(issueDate);

						service.setDateFrom(dateFrom);

						service.setDateTo(dateTo);

						service.setValidUntil(dateTo);

						service.setBaseAmount(Double.parseDouble(xPath.compile("Premium/@Total").evaluate(insured)));

						Currency currency = parser.findCurrency(xPath.compile("Premium/@CurrencyID").evaluate(insured));
						service.setCurrency(currency);
						service.setBaseCurrency(currency);
						service.setInsuranceCurrency(currency);

						service.setProgram((InsuranceProgram) parser.getProvider().getSingleResult("from InsuranceProgram where id = 2"));

						service.setConsumer(consumer);

						NodeList programIds = (NodeList) xPath.compile("ProgramsPremium/ProgramPremium/@ProgramID").evaluate(insured, XPathConstants.NODESET);
						String description = "";
						for (int j = 0; j < programIds.getLength(); j++) {
							String programId = programIds.item(j).getTextContent();
							String programName;
							switch (programId) {
							case "17":
							case "12":
							case "105":
							case "201":
							case "234":
								programName = "Медичне страхування";
								break;
							case "30":
							case "15":
							case "106":
							case "202":
							case "132":
							case "235":
								programName = "Страхування від нещасного випадку";
								break;
							case "179":
							case "32":
							case "107":
							case "203":
							case "236":
							case "131":
								programName = "Страхування багажу";
								break;
							case "25":
								programName = "Страхування польоту";
								break;
							case "23":
								programName = "Страхування подорожі";
								break;
							case "129":
								programName = "Flight zone";
								break;
							default:
								programName = "Інше";
								break;
							}
							description += programName + ", ";
						}
						if (!description.isEmpty()) {
							description = description.substring(0, description.length() - 2);
						}
						service.setDescription(description);

						parser.getProvider().save(service);

						insuranceNumber++;
					}

					return true;
				} catch (NumberFormatException | XPathExpressionException | DOMException | ParserConfigurationException | SAXException | IOException
						| ParseException e) {
					error("", e);
					return false;
				}
			}
		});
	}
}

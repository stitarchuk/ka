package com.cleverforms.parser.wizzair;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import jakarta.mail.Message;
import jakarta.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import com.cleverforms.comms.server.tasks.EmailParserImpl;
import com.cleverforms.ics.core.provider.ICSPersistentProvider;
import com.cleverforms.ics.db.model.dictionary.Consumer;
import com.cleverforms.ics.db.model.orgchart.Sector;
import com.cleverforms.ics.shared.enumerator.OrderState;
import com.cleverforms.iss.server.model.airline.AirlineRoute;
import com.cleverforms.iss.server.model.airline.AirlineSegment;
import com.cleverforms.iss.server.model.airline.AirlineTicket;
import com.cleverforms.parser.Parser;
import com.cleverforms.parser.Tax;

public class WizzairParser extends EmailParserImpl {

	@Autowired
	Parser parser;

	public final Sector sector = new Sector();

	SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
	SimpleDateFormat timeFormat = new SimpleDateFormat("H:mm");

	@Override
	public ICSPersistentProvider provider() {
		return (ICSPersistentProvider) super.provider();
	}

	public void setSectorId(long sectorId) {
		sector.setId(sectorId);
	}

	@Override
	public String name() {
		return "Wizzair airline data parser";
	}

	@Override
	public boolean parseEmail(final Message email) {
		return provider().txExecute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus status) {
				try {
					String subject = email.getSubject();
					if (!subject.startsWith("Your itinerary:") && !subject.startsWith("Ваш маршрут подорожі:")) {
						warn("Not an itinerary");
						return false;
					}

					String content = email.getContent().toString();

					int index = content.indexOf("Код підтвердження замовлення рейсу:");
					if (index == -1) {
						warn("Код підтвердження замовлення рейсу not found");
						return false;
					}
					index = content.indexOf("<span", index + 35);
					index = content.indexOf('>', index + 5);
					String pnr = content.substring(++index, index += 6);

					Map<Tax, String> taxes = new LinkedHashMap<>();
					index = content.indexOf("Сума платежу");
					int tableEndIndex = content.indexOf("</table>", index);
					while (index < tableEndIndex) {
						index = content.indexOf("<td>", index);
						String date = content.substring(index += 4, index = content.indexOf("</td>", index));
						index = content.indexOf("<td>", index);
						String code = content.substring(index += 4, index = content.indexOf("</td>", index));
						if (code.equals("AG")) {
							index = content.indexOf("<td>", index + 5);
							String paymentNumber = content.substring(index += 4, index = content.indexOf("</td>", index));
							index = content.indexOf("<td>", index + 4);
							index = content.indexOf("<td>", index + 4);
							taxes.put(
									new Tax("AG", dateFormat.parse(date), Double.parseDouble(content.substring(index += 4,
											index = content.indexOf(' ', index)))), paymentNumber);
						} else {
							index = content.indexOf("</tr>", index + 5);
						}
					}

					AirlineRoute route = new AirlineRoute();
					index = 0;
					while ((index = content.indexOf("Номер рейсу:", index)) != -1) {
						AirlineSegment segment = new AirlineSegment();

						segment.setCarrier(parser.findCompany(content.substring(index = index + 13, index += 2)));

						segment.setTripNumber(content.substring(++index, index += 4));

						index = content.indexOf("Аеропорт призначення:", index) + 21;
						index = content.indexOf("<span>", index) + 6;
						index = content.indexOf("\r", index);
						segment.setAirportFrom(parser.findAirportByCode(
								content.substring(content.lastIndexOf("(", index) + 1, content.lastIndexOf(")", index)), null));

						index = content.indexOf("<td>", index) + 4;
						index = content.indexOf("\r", index);
						segment.setAirportTo(parser.findAirportByCode(content.substring(content.lastIndexOf("(", index) + 1, content.lastIndexOf(")", index)),
								null));

						index = content.indexOf("<td>", index);
						Date departureDate = dateFormat.parse(content.substring(index += 4, index = content.indexOf(' ', index)));
						Date departureTime = timeFormat.parse(content.substring(++index, index = content.indexOf('<', index)));
						segment.setDeparture(com.cleverforms.parser.DateUtils.dateTime(departureDate, departureTime));

						index = content.indexOf("<td>", index);
						Date arrivalDate = dateFormat.parse(content.substring(index += 4, index = content.indexOf(' ', index)));
						Date arrivalTime = timeFormat.parse(content.substring(++index, index = content.indexOf('<', index)));
						segment.setArrive(com.cleverforms.parser.DateUtils.dateTime(arrivalDate, arrivalTime));

						segment.setTicketCategory("");

						parser.addAirlineSegment(route, segment);
					}

					int tempIndex;
					index = content.indexOf("<td class=\"passenger-title\">") + 28;

					tempIndex = content.indexOf("<td class=\"passenger-title\">", index);
					index = tempIndex + 28;
					index = content.indexOf('<', index);
					String firstName = content.substring(index = content.indexOf('>', content.indexOf('>', index) + 1) + 1, index = content.indexOf('<', index));
					String lastName = content.substring(index = content.indexOf('>', content.indexOf('>', index) + 1) + 1, index = content.indexOf('<', index));
					Consumer passenger = parser.findConsumer(firstName, lastName);

					int i = 0;
					for (Entry<Tax, String> taxToNumber : taxes.entrySet()) {
						i++;
						AirlineTicket ticket;
						String paymentNumber = taxToNumber.getValue();
						if ((ticket = (AirlineTicket) parser.getProvider().getSingleResult("from AirlineTicket where extNumber = ?0",
								"WIZZAIR-" + paymentNumber)) != null) {
							continue;
						}

						ticket = new AirlineTicket();

						ticket.setConsumer(passenger);

						ticket.setCurrency(parser.findCurrency("UAH"));
						ticket.setBaseCurrency(parser.findCurrency("EUR"));

						ticket.setBaseAmount(taxToNumber.getKey().amount);

						ticket.setCreated(taxToNumber.getKey().date);

						ticket.setSector(sector);

						ticket.setState(OrderState.SALE);

						ticket.setNumber(pnr + "/" + i);

						ticket.setExtNumber("WIZZAIR-" + paymentNumber);

						ticket.setValidUntil(route.getDeparture());

						ticket.setSupplier(parser.findCompany("W6"));

						route.addTicket(ticket);
					}

					parser.getProvider().save(route);

					return true;
				} catch (NumberFormatException | MessagingException | IOException | ParseException e) {
					error("", e);
					return false;
				}
			}
		});
	}
}

package com.cleverforms.parser.sirena;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cleverforms.comms.server.tasks.FileParserImpl;
import com.cleverforms.ics.core.provider.ICSPersistentProvider;
import com.cleverforms.ics.db.model.dictionary.Company;
import com.cleverforms.ics.db.model.dictionary.Consumer;
import com.cleverforms.ics.db.model.financial.AbstractService;
import com.cleverforms.ics.db.model.orgchart.Person;
import com.cleverforms.ics.db.model.orgchart.Post;
import com.cleverforms.ics.db.model.orgchart.Sector;
import com.cleverforms.ics.shared.dictionary.property.CompanyProperty;
import com.cleverforms.ics.shared.enumerator.OrderState;
import com.cleverforms.iss.server.model.airline.AirlineRoute;
import com.cleverforms.iss.server.model.airline.AirlineSegment;
import com.cleverforms.iss.server.model.airline.AirlineTariff;
import com.cleverforms.iss.server.model.airline.AirlineTicket;
import com.cleverforms.iss.shared.airline.AirlineTicketType;
import com.cleverforms.parser.Parser;
import com.cleverforms.parser.XmlUtils;

public class SirenaParser extends FileParserImpl {

	@Autowired
	Parser parser;

	public final Sector sector = new Sector();

	public double conversion = 0.05;

	@Override
	public ICSPersistentProvider provider() {
		return (ICSPersistentProvider) super.provider();
	}

	public void setSectorId(long sectorId) {
		sector.setId(sectorId);
	}

	@Override
	public String name() {
		return "Sirena data parser";
	}

	@Override
	public boolean parseFile(final File file) {
		return provider().txExecute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus status) {
				try {
					DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document document = documentBuilder.parse(file);
					XPath xPath = XPathFactory.newInstance().newXPath();

					String oprNum = xPath.compile("/TICKETS/TICKET/DEAL/@opr_num").evaluate(document);
					if (oprNum.isEmpty()) {
						warn("/TICKETS/TICKET/DEAL/@opr_num not found");
						return false;
					}
					Person agent = (Person) parser.getProvider().getSingleResult("from Person where lower(aliases[3]) = lower(?0)", oprNum);
					Post post = null;
					if (agent != null) {
						post = agent.getPost();
					}

					String mcoType = xPath.compile("/TICKETS/TICKET/MCO_TYPE").evaluate(document);
					if (mcoType.equals("REF_NOTICE")) {
						return true;
					} else if (mcoType.equals("PENALTY")) {
						String toBso = xPath.compile("/TICKETS/TICKET/TO_BSONUM").evaluate(document);
						AirlineTicket ticket = parser.findAirlineTicket(toBso);
						if (ticket == null) {
							warn("Ticket " + toBso + " not found");
							return false;
						}

						Date documentDate = com.cleverforms.parser.DateUtils.utcToLocal(new SimpleDateFormat("yyyyMMdd'T'HHmmss").parse(xPath.compile(
								"/TICKETS/TICKET/DEAL/@utc").evaluate(document)));

						AirlineTicket emd = new AirlineTicket();

						NodeList taxElements = (NodeList) xPath.compile("/TICKETS/TICKET/TAXES/TAX").evaluate(document, XPathConstants.NODESET);
						double totalAmount = 0;
						for (int i = 0; i < taxElements.getLength(); i++) {
							Element item = (Element) taxElements.item(i);

							String code = XmlUtils.readTextContent(item, "CODE");
							double amount = Double.parseDouble(XmlUtils.readTextContent(item, "AMOUNT"));
							totalAmount += amount;

							parser.addAirlineTax(emd, code, amount, documentDate);
						}
						double conversionAmount = totalAmount * conversion;
						parser.addAirlineTax(emd, "CONVERSION", conversionAmount, documentDate);

						Company tkp = (Company) parser.getProvider().getSingleResult(
								"select c from Company c left join c.properties cp " + "where index(cp) in (?1) and upper(cp) = ?0", "ТКП",
								CompanyProperty.NAME_PROPERTIES);
						emd.setSupplier(tkp);
						emd.setSector(sector);
						emd.setConsumer(ticket.getConsumer());
						emd.setState(OrderState.SALE);
						emd.setExtNumber(ticket.getExtNumber());
						emd.setNumber(xPath.compile("/TICKETS/TICKET/BSONUM").evaluate(document));
						emd.setBaseCurrency(ticket.getBaseCurrency());
						emd.setCurrency(parser.findCurrency("UAH"));
						emd.setValidUntil(documentDate);
						AbstractService as = new AbstractService();
						as.setId(ticket.getId());
						emd.setParent(as);
						emd.setRoute(ticket.getRoute());
						emd.setType(AirlineTicketType.EMD);
						emd.setBaseAmount(0);
						emd.setCreated(documentDate);

						emd.setPost(post);

						parser.getProvider().save(emd);
						return true;
					}

					String transType = xPath.compile("/TICKETS/TICKET/TRANS_TYPE").evaluate(document);
					switch (transType) {
					case "SALE":
					case "REFUND":
					case "EXCHANGE": {
						AirlineTicket ticket = new AirlineTicket();
						ticket.setSector(sector);
						switch (transType) {
						case "SALE":
							ticket.setState(OrderState.SALE);
							break;
						case "REFUND":
							ticket.setState(OrderState.REFUND);
							break;
						case "EXCHANGE":
							ticket.setState(OrderState.EXCHANGE);
							break;
						default:
							warn("Unknown trans type");
							return false;
						}
						Company tkp = (Company) parser.getProvider().getSingleResult(
								"select c from Company c left join c.properties cp " + "where index(cp) in (?1) and upper(cp) = ?0", "ТКП",
								CompanyProperty.NAME_PROPERTIES);
						ticket.setSupplier(tkp);
						String pnr = xPath.compile("/TICKETS/TICKET/PNR_LAT").evaluate(document);
						ticket.setExtNumber("SIRENA-" + pnr);
						ticket.setCurrency(parser.findCurrency("UAH"));
						ticket.setBaseCurrency(parser.findCurrency(xPath.compile("/TICKETS/TICKET/CURRENCY").evaluate(document)));
						Date documentDate = com.cleverforms.parser.DateUtils.utcToLocal(new SimpleDateFormat("yyyyMMdd'T'HHmmss").parse(xPath.compile(
								"/TICKETS/TICKET/DEAL/@utc").evaluate(document)));
						ticket.setCreated(documentDate);
						if (transType.equals("REFUND")) {
							String bsoNum = xPath.compile("/TICKETS/TICKET/BSONUM").evaluate(document);
							ticket.setNumber(bsoNum + "-R");

							AbstractService as = new AbstractService();
							AirlineTicket refundTicket = parser.findAirlineTicket(bsoNum);
							if (refundTicket == null) {
								warn("Ticket " + bsoNum + " not found");
								return false;
							}
							as.setId(refundTicket.getId());
							ticket.setParent(as);
						} else if (transType.equals("EXCHANGE")) {
							ticket.setNumber(xPath.compile("/TICKETS/TICKET/BSONUM").evaluate(document));

							String exBsoNum = xPath.compile("/TICKETS/TICKET/EX_BSONUM").evaluate(document);
							AbstractService as = new AbstractService();
							AirlineTicket exchangeTicket = parser.findAirlineTicket(exBsoNum);
							if (exchangeTicket == null) {
								warn("Ticket " + exBsoNum + " not found");
								return false;
							}
							as.setId(exchangeTicket.getId());
							ticket.setParent(as);
						} else {
							ticket.setNumber(xPath.compile("/TICKETS/TICKET/BSONUM").evaluate(document));
						}
						String firstName = xPath.compile("/TICKETS/TICKET/NAME").evaluate(document);
						String lastName = xPath.compile("/TICKETS/TICKET/SURNAME").evaluate(document);
						if (firstName.endsWith(" MR")) {
							firstName = firstName.substring(0, firstName.length() - 3);
						} else if (lastName.endsWith(" MRS")) {
							firstName = firstName.substring(0, firstName.length() - 4);
						}
						Consumer passenger = parser.findConsumer(firstName, lastName);
						ticket.setConsumer(passenger);
						double fare = Double.parseDouble(xPath.compile("/TICKETS/TICKET/FARE").evaluate(document));
						ticket.setBaseAmount(transType.equals("REFUND") ? -fare : fare);
						NodeList taxElements = (NodeList) xPath.compile("/TICKETS/TICKET/TAXES/TAX").evaluate(document, XPathConstants.NODESET);
						for (int i = 0; i < taxElements.getLength(); i++) {
							Element item = (Element) taxElements.item(i);

							String code = XmlUtils.readTextContent(item, "CODE");
							double amount = Double.parseDouble(XmlUtils.readTextContent(item, "AMOUNT"));
							fare += amount;

							parser.addAirlineTax(ticket, code, transType.equals("REFUND") ? -amount : amount, documentDate);
						}

						SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmm", Locale.ENGLISH);
						NodeList segmentElements = (NodeList) xPath.compile("/TICKETS/TICKET/SEGMENTS/SEGMENT").evaluate(document, XPathConstants.NODESET);

						AirlineRoute route = new AirlineRoute();
						route.addTicket(ticket);
						for (int i = 0; i < segmentElements.getLength(); i++) {
							Element item = (Element) segmentElements.item(i);

							AirlineSegment segment = new AirlineSegment();

							// Segment carrier
							segment.setCarrier(parser.findCompany(XmlUtils.readTextContent(item, "CARRIER")));

							// Segment departure airport
							String port1code = XmlUtils.readTextContent(item, "PORT1CODE");
							segment.setAirportFrom(parser.findAirportByCode(port1code.isEmpty() ? XmlUtils.readTextContent(item, "CITY1CODE") : port1code, null));

							// Segment arrival airport
							String port2code = XmlUtils.readTextContent(item, "PORT2CODE");
							segment.setAirportTo(parser.findAirportByCode(port2code.isEmpty() ? XmlUtils.readTextContent(item, "CITY2CODE") : port2code, null));

							// Segment departure date
							segment.setDeparture(sdf.parse(XmlUtils.readTextContent(item, "FLYDATE") + XmlUtils.readTextContent(item, "FLYTIME")));

							// Segment class
							segment.setTicketCategory(XmlUtils.readTextContent(item, "CLASS"));

							// Segment flight number
							segment.setTripNumber(XmlUtils.readTextContent(item, "REIS"));

							// Segment arrival date
							segment.setArrive(sdf.parse(XmlUtils.readTextContent(item, "ARRDATE") + XmlUtils.readTextContent(item, "ARRTIME")));

							AirlineTariff tariff = new AirlineTariff();
							tariff.setCode(XmlUtils.readTextContent(item, "BASICFARE"));
							tariff.setConsumer(passenger);
							segment.addTariff(tariff);

							parser.addAirlineSegment(route, segment);
						}

						ticket.setValidUntil(route.getDeparture());

						double conversionAmount = fare * conversion;
						parser.addAirlineTax(ticket, "CONVERSION", transType.equals("REFUND") ? -conversionAmount : conversionAmount, documentDate);

						ticket.setPost(post);

						parser.getProvider().save(route);
						return true;
					}
					case "CANCEL": {
						String bsoNum = xPath.compile("/TICKETS/TICKET/BSONUM").evaluate(document);
						AirlineTicket ticket = parser.findAirlineTicket(bsoNum);
						if (ticket == null) {
							warn("Ticket " + bsoNum + " not found");
							return false;
						}

						Date documentDate = com.cleverforms.parser.DateUtils.utcToLocal(new SimpleDateFormat("yyyyMMdd'T'HHmmss").parse(xPath.compile(
								"/TICKETS/TICKET/DEAL/@utc").evaluate(document)));

						ticket.setState(OrderState.VOID);
						ticket.setValidUntil(documentDate);
						parser.getProvider().save(ticket);

						return true;
					}
					default:
						return false;
					}
				} catch (NumberFormatException | XPathExpressionException | ParserConfigurationException | SAXException | IOException | ParseException e) {
					error("", e);
					return false;
				}
			}
		});
	}
}

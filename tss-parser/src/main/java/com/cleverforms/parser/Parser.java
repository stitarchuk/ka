/**
 * 
 */
package com.cleverforms.parser;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import com.cleverforms.comms.server.WithLoggerImpl;
import com.cleverforms.ics.db.model.dictionary.Bank;
import com.cleverforms.ics.db.model.dictionary.City;
import com.cleverforms.ics.db.model.dictionary.Company;
import com.cleverforms.ics.db.model.dictionary.CompanyAccount;
import com.cleverforms.ics.db.model.dictionary.Consumer;
import com.cleverforms.ics.db.model.dictionary.Country;
import com.cleverforms.ics.db.model.dictionary.Currency;
import com.cleverforms.ics.db.model.dictionary.Tax;
import com.cleverforms.ics.db.model.financial.Service;
import com.cleverforms.ics.shared.dictionary.property.CityProperty;
import com.cleverforms.ics.shared.dictionary.property.CompanyProperty;
import com.cleverforms.iss.server.model.airline.AirlineRoute;
import com.cleverforms.iss.server.model.airline.AirlineSegment;
import com.cleverforms.iss.server.model.airline.AirlineTaxAmount;
import com.cleverforms.iss.server.model.airline.AirlineTicket;
import com.cleverforms.iss.server.model.airline.Airport;
import com.cleverforms.iss.server.model.bus.BusStation;
import com.cleverforms.iss.server.model.bus.BusTaxAmount;
import com.cleverforms.iss.server.model.bus.BusTicket;
import com.cleverforms.iss.server.model.hotel.Hotel;
import com.cleverforms.iss.server.model.hotel.HotelService;
import com.cleverforms.iss.server.model.hotel.HotelTaxAmount;
import com.cleverforms.iss.server.model.insurance.InsuranceService;
import com.cleverforms.iss.server.model.insurance.InsuranceTaxAmount;
import com.cleverforms.iss.server.model.railway.Railway;
import com.cleverforms.iss.server.model.railway.RailwayStation;
import com.cleverforms.iss.server.model.railway.RailwayTaxAmount;
import com.cleverforms.iss.server.model.railway.RailwayTicket;
import com.cleverforms.iss.server.model.travel.TravelService;
import com.cleverforms.iss.server.provider.ISSPersistentProvider;
import com.cleverforms.iss.shared.airline.AirportProperty;
import com.cleverforms.iss.shared.bus.BusStationProperty;
import com.cleverforms.iss.shared.railway.RailwayStationProperty;

/**
 * @author Maxim Perekladov
 *
 */
public class Parser extends WithLoggerImpl {

	@Autowired
	public ISSPersistentProvider provider;

	public ISSPersistentProvider getProvider() {
		return provider;
	}

	public Currency findCurrency(String currencyCode) {
		Currency currency = Currency.getByCode(provider, currencyCode); 
		if (currency != null) {
			return currency;
		} else {
			currency = new Currency();
			currency.setCode(currencyCode);
			currency.setName(currencyCode);
			provider.save(currency);
			warn("Currency '" + currencyCode + "' added");
			return currency;
		}
	}

	public Consumer findConsumer(String firstName, String lastName) {
		Consumer consumer = (Consumer) provider.getSingleResult("from Consumer c where lower(c.name) = lower(?0) and lower(c.lastName) = lower(?1)", firstName,
				lastName);
		if (consumer == null) {
			consumer = new Consumer();
			consumer.setName(firstName);
			consumer.setLastName(lastName);
			consumer = provider.save(consumer);
		}
		return consumer;
	}
	
	public Consumer findConsumer(String firstName, String lastName, String namePrefix) {
		Consumer consumer = (Consumer) provider.getSingleResult("from Consumer c where lower(c.name) = lower(?0) and lower(c.lastName) = lower(?1) and lower(c.namePrefix) = lower(?2)", firstName,
				lastName, namePrefix);
		if (consumer == null) {
			consumer = new Consumer();
			consumer.setName(firstName);
			consumer.setLastName(lastName);
			consumer.setNamePrefix(namePrefix);
			consumer = provider.save(consumer);
		}
		return consumer;
	}

	public Company findCompany(String code) {
		Company company = provider.persistentUtil().findCompanyByIATACode(code);
		if (company == null) {
			company = new Company();
			company.setCountry((Country) provider.getSingleResult("from Country"));
			company.setStartDate(new Date());
			Map<CompanyProperty, String> companyProperties = new HashMap<>();
			companyProperties.put(CompanyProperty.IATA_CODE, code);
			company.setProperties(companyProperties);
			provider.save(company);
			warn("Company with IATA code '" + code + "' added");
		}
		return company;
	}

	public City findCity(String name) {
		return (City) provider.getSingleResult("select c from City c left join c.properties cp where index(cp) in (?1) and lower(cp) = ?0", name.toLowerCase(),
				CityProperty.SEARCH_PROPERTIES);
	}

	public AirlineTicket findAirlineTicket(String number) {
		return (AirlineTicket) provider.getSingleResult("from AirlineTicket where number = ?0", number);
	}

	public Airport findAirportByCode(String code, String name) {
		Airport airport = Airport.getByCode(provider, code);
		if (airport == null) {
			airport = new Airport();

			airport.setCity((City) provider.getSingleResult("from City where id = 0"));

			airport.setIataCode(code);

			provider.save(airport);

			if (name != null && !name.isEmpty()) {
				airport.setName(name, getLocale(name));
			}

			warn("Airport '" + code + "' added");
		}
		return airport;
	}

	public Airport findAirportByName(String name) {
		return (Airport) provider.getSingleResult("select a from Airport a left join a.properties ap where index(ap) in (?1) and upper(ap) = ?0",
				name.toUpperCase(), AirportProperty.SEARCH_PROPERTIES);
	}

	public boolean addAirlineSegment(AirlineRoute route, AirlineSegment segment) {
		if (!route.getSegments().contains(segment)) {
			if (route.getSegments().isEmpty()) {
				route.setAirportFrom(segment.getAirportFrom());
				route.setDeparture(segment.getDeparture());
				route.setAirportTo(segment.getAirportTo());
				route.setArrive(segment.getArrive());
			} else {
				if (segment.getDeparture().before(route.getDeparture())) {
					route.setAirportFrom(segment.getAirportFrom());
					route.setDeparture(segment.getDeparture());
				}
				if (segment.getArrive().after(route.getArrive())) {
					route.setAirportTo(segment.getAirportTo());
					route.setArrive(segment.getArrive());
				}
			}
			route.addSegment(segment);
			return true;
		} else {
			return false;
		}
	}

	public void addAirlineTax(AirlineTicket ticket, String code, double amount, Date created) {
		Tax taxCode = Tax.getByCode(provider, code);
		if (taxCode == null) {
			taxCode = new Tax(code, code);
			provider.save(taxCode);
		}
		AirlineTaxAmount taxAmount = ticket.addTax(new AirlineTaxAmount(taxCode));
		taxAmount.setAmount(amount);
		taxAmount.setCreated(created);
		ticket.addTax(taxAmount);
	}

	/*-public String toJson(AirlineDocument doc, String code, Date date, String agent) throws XPathExpressionException, UnsupportedEncodingException, IOException,
			ParseException, ParsingException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSS");
		StringBuilder json = new StringBuilder("{\n\t\"source\":{\n\t\t\"code\":\"\",\n\t\t\"type\":{\n\t\t\t\"code\":\"");
		if (doc instanceof GabrielTssDocument) {
			json.append("gabriel");
		} else if (doc instanceof WizzairTssDocument) {
			json.append("wizzair");
		}
		json.append("\"\n\t\t}\n\t},\n\t\"type\":{\n\t\t\"code\":\"air_ticket\"\n\t},\n\t\"money_type\":{\n\t\t\"code\":\"document\"\n\t},\n\t\"status\":{\n\t\t\"code\":\"work\"\n\t},\n\t\"date\":\""
				+ sdf.format(date) + "\",\n\t\"pnr\":\"" + doc.readPnr() + "\",\n\t\"num\":\"");
		if (doc instanceof GabrielTssDocument) {
			json.append(doc.readTickets().iterator().next().number);
		} else if (doc instanceof WizzairTssDocument) {
			json.append("WU " + doc.readPnr());
		}
		json.append("\",\n\t\"blank\":null,\n\t\"person\":\"");
		if (doc instanceof GabrielTssDocument) {
			json.append(doc.readTickets().get(0).passenger.lastName + "\\\\" + doc.readTickets().get(0).passenger.firstName);
		} else if (doc instanceof WizzairTssDocument) {
			for (DocumentConsumer passenger : ((WizzairTssDocument) doc).getPassengers()) {
				json.append(passenger.lastName + "\\\\" + passenger.firstName + ",");
			}
			json.deleteCharAt(json.length() - 1);
		}
		json.append("\",\n\t\"currency\":");
		if (doc instanceof GabrielTssDocument) {
			json.append("null");
		} else if (doc instanceof WizzairTssDocument) {
			json.append("\"EUR\"");
		}
		json.append(",\n\t\"description\":null,\n\t\"is_blank_outcome\":false,\n\t\"target\":{\n\t\t\"id\":null,\n\t\t\"code\":\"" + code
				+ "\"\n\t},\n\t\"sold\":{\n\t\t\"id\":null,\n\t\t\"code\":\"" + code + "\"\n\t},\n\t\"created\":{\n\t\t\"user\":{\n\t\t\t\"login\":");
		if (doc instanceof GabrielTssDocument) {
			json.append("null");
		} else if (doc instanceof WizzairTssDocument) {
			json.append("\"" + agent + "\"");
		}
		json.append("\n\t\t}\n\t},\n\t\"ways\":[");
		int i = 1;
		for (Entry<DocumentSegment, String> segmentToTariffs : doc.readTickets().iterator().next().segmentsToTariffs.entrySet()) {
			json.append("\n\t\t{\n\t\t\t\"from\":{\n\t\t\t\t\"country\":{\n\t\t\t\t\t\"code\":null\n\t\t\t\t},\n\t\t\t\t\"city\":{\n\t\t\t\t\t\"code\":null\n\t\t\t\t},\n\t\t\t\t\"point\":{\n\t\t\t\t\t\"code\":\""
					+ segmentToTariffs.getKey().departureAirportCode
					+ "\"\n\t\t\t\t},\n\t\t\t\t\"date\":\""
					+ sdf.format(segmentToTariffs.getKey().departureDate)
					+ "\"\n\t\t\t},\n\t\t\t\"to\":{\n\t\t\t\t\"country\":{\n\t\t\t\t\t\"code\":null\n\t\t\t\t},\n\t\t\t\t\"city\":{\n\t\t\t\t\t\"code\":null\n\t\t\t\t},\n\t\t\t\t\"point\":{\n\t\t\t\t\t\"code\":\""
					+ segmentToTariffs.getKey().arrivalAirportCode
					+ "\"\n\t\t\t\t},\n\t\t\t\t\"date\":\""
					+ sdf.format(segmentToTariffs.getKey().arrivalDate)
					+ "\"\n\t\t\t},");
			if (doc instanceof WizzairTssDocument) {
				json.append("\n\t\t\t\"company\":{\n\t\t\t\t\"code\":\"" + segmentToTariffs.getKey().carrier
						+ "\"\n\t\t\t},\n\t\t\t\"ext_info\":{\n\t\t\t\t\"num_way\":\"" + segmentToTariffs.getKey().tripNumber + "\"\n\t\t\t},");
			}
			json.append("\n\t\t\t\"step\":1,\n\t\t\t\"order\":" + i++ + "\n\t\t},");
		}
		if (!doc.readTickets().iterator().next().segmentsToTariffs.entrySet().isEmpty()) {
			json.deleteCharAt(json.length() - 1);
		}
		json.append("\n\t],\n\t\"taxes\":[");
		if (doc instanceof GabrielTssDocument) {
			for (com.cleverforms.parser.airline.Tax tax : doc.readTickets().iterator().next().taxes) {
				if (tax.code.equals("BASEA_MOUNT")) {
					json.append("\n\t\t{\n\t\t\t\"type\":{\n\t\t\t\t\"code\":\"01\"\n\t\t\t},\n\t\t\t\"company\":{\n\t\t\t\t\"id\":null,\n\t\t\t\t\"code\":\""
							+ code + "\"\n\t\t\t},\n\t\t\t\"currency\":\"" + doc.readCurrency()
							+ "\",\n\t\t\t\"currency_rate\":null,\n\t\t\t\"coef_conv\":1,\n\t\t\t\"current\":{\n\t\t\t\t\"sum\":"
							+ ((GabrielTssDocument) doc).getBaseFare().amount + ",\n\t\t\t\t\"woc\":" + ((GabrielTssDocument) doc).getBaseFare().amount
							+ "\n\t\t\t},\n\t\t\t\"document\":{\n\t\t\t\t\"sum\":" + tax.amount + ",\n\t\t\t\t\"woc\":" + tax.amount
							+ "\n\t\t\t},\n\t\t\t\"is_use\":true,\n\t\t\t\"is_readonly\":true\n\t\t},");
				} else {
					json.append("\n\t\t{\n\t\t\t\"type\":{\n\t\t\t\t\"code\":\"" + tax.code
							+ "\"\n\t\t\t},\n\t\t\t\"company\":{\n\t\t\t\t\"id\":null,\n\t\t\t\t\"code\":\"" + code + "\"\n\t\t\t},\n\t\t\t\"currency\":\""
							+ doc.readCurrency() + "\",\n\t\t\t\"currency_rate\":null,\n\t\t\t\"coef_conv\":1,\n\t\t\t\"current\":{\n\t\t\t\t\"sum\":"
							+ tax.amount + ",\n\t\t\t\t\"woc\":" + tax.amount + "\n\t\t\t},\n\t\t\t\"is_use\":true,\n\t\t\t\"is_readonly\":true\n\t\t},");
				}
			}
			if (!doc.readTickets().iterator().next().taxes.isEmpty()) {
				json.deleteCharAt(json.length() - 1);
			}
		} else if (doc instanceof WizzairTssDocument) {
			json.append("\n\t\t{\n\t\t\t\"type\":{\n\t\t\t\t\"code\":\"WIZZ\"\n\t\t\t},\n\t\t\t\"company\":{\n\t\t\t\t\"id\":null,\n\t\t\t\t\"code\":\"WU\"\n\t\t\t},\n\t\t\t\"currency\":\""
					+ doc.readCurrency()
					+ "\",\n\t\t\t\"currency_rate\":null,\n\t\t\t\"coef_conv\":1,\n\t\t\t\"current\":{\n\t\t\t\t\"sum\":"
					+ doc.readTickets().iterator().next().taxes.iterator().next().amount
					+ ",\n\t\t\t\t\"woc\":"
					+ doc.readTickets().iterator().next().taxes.iterator().next().amount
					+ "\n\t\t\t},\n\t\t\t\"is_use\":true,\n\t\t\t\"is_readonly\":true\n\t\t}");
		}
		json.append("\n\t]\n}");
		return json.toString();
	}*/

	public RailwayTicket findRailwayTicket(String number) {
		return (RailwayTicket) provider.getSingleResult("from RailwayTicket where number = ?0", number);
	}

	public RailwayStation findRailwayStationByCode(String code, String name) {
		RailwayStation station = (RailwayStation) provider.getSingleResult("from RailwayStation where lower(railwayCode) = ?0", code.toLowerCase());
		if (station == null) {
			station = new RailwayStation();

			station.setCity((City) provider.getSingleResult("from City where id = 0"));

			station.setRailwayCode(code);

			station.setRailway((Railway) provider.getSingleResult("from Railway where id = 0"));

			if (name != null && !name.isEmpty()) {
				station.setName(name, getLocale(name));
			}

			provider.save(station);

			warn("Railway station '" + code + "' added");
		}
		return station;
	}

	public RailwayStation findRailwayStationByName(String name) {
		return (RailwayStation) provider.getSingleResult("select s from RailwayStation s left join s.properties sp "
				+ "where index(sp) in (?1) and upper(sp) = ?0", name.toUpperCase(), RailwayStationProperty.SEARCH_PROPERTIES);
	}

	public void addRailwayTax(RailwayTicket ticket, String code, double amount, Date created) {
		Tax taxCode = Tax.getByCode(provider, code);
		if (taxCode == null) {
			taxCode = new Tax(code, code);
			provider.save(taxCode);
		}
		RailwayTaxAmount taxAmount = ticket.addTax(new RailwayTaxAmount(taxCode));
		taxAmount.setAmount(amount);
		taxAmount.setCreated(created);
		ticket.addTax(taxAmount);
	}

	public BusTicket findBusTicket(String number) {
		return (BusTicket) provider.getSingleResult("from BusTicket where number = ?0", number);
	}

	public BusStation findBusStation(String stationName, String cityName) {
		City city = findCity(cityName);
		if (city == null) {
			return null;
		} else {
			final String sql = "from BusStation bs left join bs.properties p where index(p) = ?2 and lower(p) = ?0 and bs.city = ?1";
			BusStation bs = (BusStation) provider.getSingleResult(sql, stationName.toLowerCase(), city, BusStationProperty.NAME_UK);
			if (bs == null) {
				bs = (BusStation) provider.getSingleResult(sql, stationName.toLowerCase(), city, BusStationProperty.NAME_RU);
				if (bs == null) {
					bs = (BusStation) provider.getSingleResult(sql, stationName.toLowerCase(), city, BusStationProperty.NAME_EN);
				}
			}
			return bs;
		}
	}

	public BusStation addBusStation(String stationName, String cityName) {
		BusStation station = new BusStation();

		station.setName(stationName, getLocale(stationName));

		if (cityName != null && !cityName.isEmpty()) {
			City city = findCity(cityName);
			if (city == null) {
				city = new City();

				city.setName(cityName, getLocale(cityName));

				city.setCountry((Country) provider.getSingleResult("from Country where id = 242"));

				provider.save(city);

				warn("City '" + cityName + "' added");
			}
			station.setCity(city);
		}

		provider.save(station);

		warn("Bus station '" + stationName + "' added");

		return station;
	}

	public void addBusTax(BusTicket ticket, String code, double amount, Date created) {
		Tax taxCode = Tax.getByCode(provider, code);
		if (taxCode == null) {
			taxCode = new Tax(code, code);
			provider.save(taxCode);
		}
		BusTaxAmount taxAmount = ticket.addTax(new BusTaxAmount(taxCode));
		taxAmount.setAmount(amount);
		taxAmount.setCreated(created);
		ticket.addTax(taxAmount);
	}

	public void addInsuranceTax(InsuranceService service, String code, double amount, Date created) {
		Tax taxCode = Tax.getByCode(provider, code);
		if (taxCode == null) {
			taxCode = new Tax(code, code);
			provider.save(taxCode);
		}
		InsuranceTaxAmount taxAmount = service.addTax(new InsuranceTaxAmount(taxCode));
		taxAmount.setAmount(amount);
		taxAmount.setCreated(created);
		service.addTax(taxAmount);
	}

	public Hotel findHotelByName(String hotelName, String cityName, String countryName, String locale) {
		
		Hotel hotel = (Hotel) provider.getSingleResult("from Hotel h left join h.properties p where index(p) = 'NAME_" + locale + "' and lower(p) = lower(?0)", hotelName);
		if (hotel == null) {
			hotel = new Hotel();
			City city = (City) provider.getSingleResult("from City c left join c.properties p where index(p) = 'NAME_" + locale + "' and lower(p) = lower(?0)", cityName);
			if (city == null) {
				city = new City();
				city.setName(cityName, locale);
				Country country = (Country) provider
						.getSingleResult("from Country c left join c.properties p where index(p) = 'NAME_" + locale + "' and lower(p) = lower(?0)", countryName);
				if (country == null) {
					country = new Country();
					country.setName(countryName, locale);
					provider.save(country);
				}
				city.setCountry(country);
				provider.save(city);
			}
			hotel.setCity(city);
			hotel.setName(hotelName, locale);
			provider.save(hotel);
			warn("Hotel '" + hotelName + "' added");
		}
		return hotel;
	}

	public void addHotelTax(HotelService service, String code, double amount, Date created) {
		Tax taxCode = Tax.getByCode(provider, code);
		if (taxCode == null) {
			taxCode = new Tax(code, code);
			provider.save(taxCode);
		}
		HotelTaxAmount taxAmount = service.addTax(new HotelTaxAmount(taxCode));
		taxAmount.setAmount(amount);
		taxAmount.setCreated(created);
		service.addTax(taxAmount);
	}

	public Company findCompanyByOkpo(String okpo) {
		if (okpo == null) {
			throw new IllegalArgumentException("okpo is null");
		}
		return (Company) provider.getSingleResult("from Company c left join c.properties p where index(p) = 'EDRPOU' and lower(p) = ?0", okpo.toLowerCase());
	}

	public Company findCompanyByInn(String inn) {
		if (inn == null) {
			throw new IllegalArgumentException("inn is null");
		}
		return (Company) provider.getSingleResult("from Company c left join c.properties p where index(p) = 'TAX_NUMBER' and lower(p) = ?0", inn.toLowerCase());
	}

	public Bank findBankByMfo(String mfo) {
		if (mfo == null) {
			throw new IllegalArgumentException("mfo is null");
		}
		return (Bank) provider.getSingleResult("from Bank where lower(code) = ?0", mfo.toLowerCase());
	}

	public CompanyAccount findCompanyAccount(Company company, Bank bank, String code) {
		if (company == null) {
			throw new IllegalArgumentException("company is null");
		}
		if (bank == null) {
			throw new IllegalArgumentException("bank is null");
		}
		if (code == null) {
			throw new IllegalArgumentException("code is null");
		}
		return (CompanyAccount) provider.getSingleResult("from CompanyAccount where company_id = ?0 and bank_id = ?1 and lower(code) = ?2", company.getId(),
				bank.getId(), code.toLowerCase());
	}

	public String getLocale(String string) {
		Pattern pattern = Pattern.compile("[ҐґЄєІіЇї]");
		Matcher matcher = pattern.matcher(string);
		if (matcher.find()) {
			return "UK";
		} else {
			pattern = Pattern.compile("[А-Яа-я]");
			matcher = pattern.matcher(string);
			if (matcher.find()) {
				return "RU";
			} else {
				return "EN";
			}
		}
	}

	public InsuranceService findInsuranceService(String number) {
		return (InsuranceService) provider.getSingleResult("from InsuranceService where number = ?0", number);
	}

	public Service findService(String number) {
		return (Service) provider.getSingleResult("from Service where number = ?0", number);
	}

	public TravelService findTitbitService(String number) {
		return (TravelService) provider.getSingleResult("from TravelService where number = ?0", number);
	}

}

package com.cleverforms.parser.sabre;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import com.cleverforms.comms.server.tasks.FileParserImpl;
import com.cleverforms.comms.server.util.io.FileUtil;
import com.cleverforms.comms.shared.util.UUID;
import com.cleverforms.ics.core.provider.ICSPersistentProvider;
import com.cleverforms.ics.db.model.dictionary.Consumer;
import com.cleverforms.ics.db.model.dictionary.Currency;
import com.cleverforms.ics.db.model.financial.AbstractService;
import com.cleverforms.ics.db.model.orgchart.Person;
import com.cleverforms.ics.db.model.orgchart.Post;
import com.cleverforms.ics.db.model.orgchart.Sector;
import com.cleverforms.ics.shared.dictionary.property.CompanyProperty;
import com.cleverforms.ics.shared.enumerator.OrderState;
import com.cleverforms.iss.server.model.airline.AirlineRoute;
import com.cleverforms.iss.server.model.airline.AirlineSegment;
import com.cleverforms.iss.server.model.airline.AirlineTariff;
import com.cleverforms.iss.server.model.airline.AirlineTicket;
import com.cleverforms.iss.shared.airline.AirlineTicketType;
import com.cleverforms.parser.Parser;
import com.cleverforms.parser.StringUtils;

public class SabreParser extends FileParserImpl implements InitializingBean {

	@Autowired
	Parser parser;

	public final Sector sector = new Sector();

	SimpleDateFormat amPmFormat = new SimpleDateFormat("yyyyddMMMhhmma", Locale.ENGLISH);
	SimpleDateFormat noonFormat = new SimpleDateFormat("yyyyddMMMHHmm", Locale.ENGLISH);
	SimpleDateFormat emptyFormat = new SimpleDateFormat("yyyyddMMMKKmm", Locale.ENGLISH);
	SimpleDateFormat departureFormat = new SimpleDateFormat("ddMMM", Locale.ENGLISH);

	Properties companyCodes;

	@Override
	public void afterPropertiesSet() throws IOException {
		String propFileName = "companyCodes.properties";
		companyCodes = FileUtil.loadProperties(getClass().getResourceAsStream(propFileName));
	}

	@Override
	public ICSPersistentProvider provider() {
		return (ICSPersistentProvider) super.provider();
	}

	public void setSectorId(long sectorId) {
		sector.setId(sectorId);
	}

	@Override
	public String name() {
		return "Sabre data parser";
	}

	class ValidatingCarrierCodeAndSetFlag {
		String code;
		boolean set = false;

		public ValidatingCarrierCodeAndSetFlag(String code) {
			this.code = code;
		}
	}

	@Override
	public boolean parseFile(final File file) {
		return provider().txExecute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus status) {
				try {
					Calendar calendar = Calendar.getInstance();
					String data = Files.readString(file.toPath().toAbsolutePath(), StandardCharsets.UTF_8);
					calendar.setTimeInMillis(file.lastModified());
					int index = 0;
					Date docDate = noonFormat
							.parse(calendar.get(Calendar.YEAR) + data.substring(index = 2, index += 9));
					char operationType = data.charAt(13);

					Post post = null;
					if (operationType != '5') {
						String agentCode = data.substring(126, 130) + data.substring(133, 135);
						Person agent = (Person) parser.getProvider()
								.getSingleResult("from Person where lower(aliases[13]) = lower(?0)", agentCode);
						if (agent != null) {
							post = agent.getPost();
						}
					}

					switch (operationType) {
					case '1': {
						// Sale or exchange

						String pnr = data.substring(index = 55, index += 6);
						int tempIndex;

						Map<String, Consumer> m1NumberToPassenger = new HashMap<>();
						while ((tempIndex = data.indexOf("\nM1", index)) != -1) {
							String m1Number = data.substring(index = tempIndex + 3, index += 2);
							String name = data.substring(index, index += 64),
									lastName = name.substring(0, tempIndex = name.indexOf("/")),
									firstName = name.substring(++tempIndex).strip(), namePrefix = null;
							if (firstName.endsWith(" MR")) {
								firstName = firstName.substring(0, firstName.length() - 3);
								namePrefix = "MR";
							} else if (firstName.endsWith(" MS")) {
								firstName = firstName.substring(0, firstName.length() - 3);
								namePrefix = "MS";
							} else if (firstName.endsWith(" MRS")) {
								firstName = firstName.substring(0, firstName.length() - 4);
								namePrefix = "MRS";
							} else if (firstName.endsWith(" MSS")) {
								firstName = firstName.substring(0, firstName.length() - 4);
								namePrefix = "MSS";
							} else if (firstName.endsWith(" MSTR")) {
								firstName = firstName.substring(0, firstName.length() - 5);
								namePrefix = "MSTR";
							}
							m1NumberToPassenger.put(m1Number,
									(namePrefix == null) ? parser.findConsumer(firstName, lastName)
											: parser.findConsumer(firstName, lastName, namePrefix));
						}

						Map<AirlineTicket, ValidatingCarrierCodeAndSetFlag> ticketsToValidatingCarrierCodes = new HashMap<>();
						Map<String, AirlineTicket> ticketNumbersToTickets = new HashMap<>();
						while ((tempIndex = data.indexOf("\nM2", index)) != -1) {
							AirlineTicket ticket = new AirlineTicket();
							ticket.setConsumer(
									m1NumberToPassenger.get(data.substring(index = tempIndex + 3, index += 2)));

							// Category
							String passengerType = data.substring(index, index += 3);
							ticket.setConsumerType(passengerType);
							index += 38;

							// Taxes
							double totalTaxAmount = 0;
							for (int i = 0; i < 3; i++) {
								String taxAmountStr = data.substring(++index, index += 7).strip();
								if (taxAmountStr.isEmpty() || taxAmountStr.startsWith("PD")) {
									index += 2;
									continue;
								}
								double taxAmount = Double.parseDouble(taxAmountStr);
								totalTaxAmount += taxAmount;
								String taxCode = data.substring(index, index += 2);
								parser.addAirlineTax(ticket, taxCode, taxAmount, docDate);
							}
							ticket.setBaseCurrency(parser.findCurrency(data.substring(++index, index += 3)));
							String equivalentPaidAmount = data.substring(index, index += 8).strip();
							if (equivalentPaidAmount.endsWith("A")) {
								equivalentPaidAmount = equivalentPaidAmount.substring(0,
										equivalentPaidAmount.length() - 1);
							} else if (equivalentPaidAmount.startsWith("NO ADC")) {
								equivalentPaidAmount = "0";
							}
							ticket.setBaseAmount(Double.parseDouble(equivalentPaidAmount) - totalTaxAmount);
							String validatingCarrier = data.substring(index += 144, index += 2);
							ticketsToValidatingCarrierCodes.put(ticket,
									new ValidatingCarrierCodeAndSetFlag(validatingCarrier));
							String number = data.substring(index, index += 10);
							int conjunctionTicketCount = Integer.parseInt(String.valueOf(data.charAt(index)));
							if (parser.findAirlineTicket(number) != null) {
								warn("Ticket " + number + " already exists");
								return false;
							}
							long ticketNumberInt = Long.parseLong(number);
							String digitalCode = companyCodes.getProperty(validatingCarrier);
							if (digitalCode == null) {
								digitalCode = "";
							}
							ticket.setNumber(digitalCode + number);
							ticketNumbersToTickets.put(digitalCode + number, ticket);
							for (int i = 1; i <= conjunctionTicketCount; i++) {
								ticketNumbersToTickets.put(digitalCode + String.valueOf(ticketNumberInt + i), ticket);
							}
							ticket.setExtNumber("SABRE-" + pnr);
							ticket.setCurrency(parser.findCurrency("UAH"));
							ticket.setSector(sector);
							ticket.setCreated(docDate);
							ticket.setType(AirlineTicketType.TRAVEL);
							ticket.setSupplier(parser.findCompany(validatingCarrier));
							ticket.setPost(post);
							index = StringUtils.indexOf(data, "\n", 5, index) + 1;
							if (data.charAt(index) != '\r') {
								String exchangeTicketNumber = data.substring(index, index += 13);
								AirlineTicket exchangeTicket = parser.findAirlineTicket(exchangeTicketNumber);
								if (exchangeTicket == null) {
									warn("Ticket " + exchangeTicketNumber + " not found");
									return false;
								}
								AbstractService as = new AbstractService();
								as.setId(exchangeTicket.getId());
								ticket.setParent(as);
								ticket.setState(OrderState.EXCHANGE);
							} else {
								ticket.setState(OrderState.SALE);
							}

							index = data.indexOf("\n", index) + 1;
							String description = data.substring(index, data.indexOf("\r", index));
							if ((tempIndex = description.indexOf("/")) != -1) {
								ticket.setDescription(description.substring(tempIndex + 1));
							}

							ticket.setRoute(new AirlineRoute());
						}

						// AirlineRoute route = new AirlineRoute();
						Map<Integer, AirlineSegment> numberToSegment = new HashMap<>();
						while ((tempIndex = data.indexOf("\nM3", index)) != -1) {
							Integer number = Integer.parseInt(data.substring(index = tempIndex + 3, index += 2));
							if (data.charAt(index = tempIndex + 5) == '1') {
								// Add segment
								AirlineSegment segment = new AirlineSegment();
								numberToSegment.put(number, segment);

								// Departure airport
								segment.setAirportFrom(
										parser.findAirportByCode(data.substring(index += 14, index += 3), null));

								// Arrival airport
								segment.setAirportTo(
										parser.findAirportByCode(data.substring(index += 17, index += 3), null));

								// Carrier
								String carrier = data.substring(index += 17, index += 2);
								segment.setCarrier(parser.findCompany(carrier));

								// Flight number
								segment.setTripNumber(data.substring(index, index += 5).strip());

								// Ticket category
								segment.setTicketCategory(data.substring(index, index += 2).strip());

								// Departure date
								tempIndex = index;
								switch (data.charAt(index + 4)) {
								case 'A':
								case 'P':
									segment.setDeparture(amPmFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53) + data.substring(index, index + 5)
											+ 'M'));
									break;
								case ' ':
									segment.setDeparture(emptyFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index, index + 4)));
									break;
								case 'N':
									segment.setDeparture(noonFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index, index + 4)));
								default:
									break;
								}

								// Arrival date
								switch (data.charAt(index + 9)) {
								case 'A':
								case 'P':
									segment.setArrive(amPmFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index + 5, index + 10) + 'M'));
									break;
								case ' ':
									segment.setArrive(emptyFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index + 5, index + 9)));
									break;
								case 'N':
									segment.setArrive(noonFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index + 5, index + 9)));
								default:
									break;
								}

								String duration = (data.substring(tempIndex + 10, tempIndex + 12) + ":"
										+ data.substring(tempIndex + 13, tempIndex + 15)).replace(' ', '0');
								segment.setDuration(duration);

								String departureTerminal = data.substring(tempIndex + 62, tempIndex + 88).strip();
								segment.setTerminalFrom(departureTerminal);

								String arrivalTerminal = data.substring(tempIndex + 92, tempIndex + 118).strip();
								segment.setTerminalTo(arrivalTerminal);

								String segmentPnr = data.substring(tempIndex + 172, tempIndex + 178);

								for (Entry<AirlineTicket, ValidatingCarrierCodeAndSetFlag> ticketToValidatingCarrierCode : ticketsToValidatingCarrierCodes
										.entrySet()) {
									if (!ticketToValidatingCarrierCode.getValue().set
											&& ticketToValidatingCarrierCode.getValue().code.equals(carrier)) {
										ticketToValidatingCarrierCode.getKey()
												.setExtNumber(ticketToValidatingCarrierCode.getKey().getExtNumber()
														+ "/" + segmentPnr);
										ticketToValidatingCarrierCode.getValue().set = true;
									}
								}
							}
						}

						index = 0;
						while ((tempIndex = data.indexOf("\nM4", index)) != -1) {
							try {
								Integer number = Integer.parseInt(data.substring(index = tempIndex + 3, index += 2));
								String passengerTypeCode = data.substring(index, index += 3);

								String state = data.substring(index += 12, index += 2);

								String baggageAllowance = data.substring(index, index += 3);

								String tariffCode = data.substring(index, index += 13).strip();

								for (AirlineTicket ticket : ticketsToValidatingCarrierCodes.keySet()) {
									if (passengerTypeCode.equals(ticket.getConsumerType())) {
										AirlineTariff tariff = new AirlineTariff();
										tariff.setCode(tariffCode);
										tariff.setConsumer(ticket.getConsumer());
										tariff.setBaggage(baggageAllowance);
										tariff.setState(state);

										numberToSegment.get(number).addTariff(tariff);
									}
								}
							} catch (Exception e) {
							}
						}

						double commission = 0, discount = 0;
						try {
							tempIndex = data.indexOf("X*-FEE/FARE");
							tempIndex = data.indexOf("AFEE", tempIndex);
							commission = Double.parseDouble(
									data.substring(tempIndex + 4, tempIndex = data.indexOf("/", tempIndex + 4)));
							discount = Double
									.parseDouble(data.substring(tempIndex + 5, data.indexOf("/", tempIndex + 5)));
						} catch (Exception e) {
						}

						index = 0;
						while ((tempIndex = data.indexOf("\nM9", index)) != -1) {
							try {
								index = tempIndex + 8;

								String m9 = data.substring(index, data.indexOf("\r", index));
								if (m9.matches("SSR DOCS .. HK1/P.*")) {
									tempIndex = StringUtils.indexOf(m9, "/", 3, 0) + 1;
									String passport = m9.substring(tempIndex, tempIndex = m9.indexOf("/", tempIndex));
									String lastName = m9.substring(
											tempIndex = StringUtils.indexOf(m9, "/", 4, tempIndex + 1) + 1,
											tempIndex = m9.indexOf("/", tempIndex));
									int tempIndex2 = m9.indexOf("/", tempIndex += 1);
									String firstName;
									if (tempIndex2 == -1) {
										firstName = m9.substring(tempIndex);
									} else {
										firstName = m9.substring(tempIndex, tempIndex2);
									}
									for (Consumer consumer : m1NumberToPassenger.values()) {
										if ((consumer.getDocument() == null || consumer.getDocument().isEmpty())
												&& consumer.getLastName().equals(lastName)
												&& consumer.getName().equals(firstName)) {
											consumer.setDocument(passport);
										}
									}
								} else if (m9.startsWith("SSR TKNE ")) {
									String flightAir = m9.substring(9, 11), departureAirport = m9.substring(16, 19),
											arrivalAirport = m9.substring(19, 22), flightNum = m9.substring(22, 26),
											flightClass = m9.substring(26, 27), departureDate = m9.substring(27, 32);
									int lastCIndex = m9.lastIndexOf("C");
									/*- TODO Check if conjunction ticket number can occur */
									String ticketNumber = m9.substring(lastCIndex - 13, lastCIndex);
									while (flightNum.charAt(0) == '0') {
										flightNum = flightNum.substring(1);
									}

									for (AirlineSegment segment : numberToSegment.values()) {
										if (segment.getCarrier().getProperty(CompanyProperty.IATA_CODE)
												.equals(flightAir)
												&& segment.getAirportFrom().getIataCode().equals(departureAirport)
												&& segment.getAirportTo().getIataCode().equals(arrivalAirport)
												&& segment.getTripNumber().equals(flightNum)
												&& segment.getTicketCategory().equals(flightClass)
												&& departureFormat.format(segment.getDeparture()).toUpperCase()
														.equals(departureDate)) {
											for (Entry<String, AirlineTicket> ticketNumberToTicket : ticketNumbersToTickets
													.entrySet()) {
												if (ticketNumberToTicket.getKey().equals(ticketNumber)) {
													AirlineSegment segmentToAdd = new AirlineSegment();
													segmentToAdd.setAirportFrom(segment.getAirportFrom());
													segmentToAdd.setAirportTo(segment.getAirportTo());
													segmentToAdd.setArrive(segment.getArrive());
													segmentToAdd.setCarrier(segment.getCarrier());
													segmentToAdd.setDeparture(segment.getDeparture());
													segmentToAdd.setDuration(segment.getDuration());
													segmentToAdd.setSeatNumber(segment.getSeatNumber());

													for (AirlineTariff tariff : segment.getTariffs()) {
														AirlineTariff tariffToAdd = new AirlineTariff();
														tariffToAdd.setCode(tariff.getCode());
														tariffToAdd.setConsumer(tariff.getConsumer());
														tariffToAdd.setBaggage(tariff.getBaggage());
														tariffToAdd.setState(tariff.getState());

														segmentToAdd.addTariff(tariffToAdd);
													}

													segmentToAdd.setTerminalFrom(segment.getTerminalFrom());
													segmentToAdd.setTerminalTo(segment.getTerminalTo());
													segmentToAdd.setTicketCategory(segment.getTicketCategory());
													segmentToAdd.setTripNumber(segment.getTripNumber());

													parser.addAirlineSegment(ticketNumberToTicket.getValue().getRoute(),
															segmentToAdd);

													break;
												}
											}
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						for (AirlineTicket ticket : ticketsToValidatingCarrierCodes.keySet()) {
							AirlineTicket bookedTicket;
							if ((bookedTicket = (AirlineTicket) parser.getProvider().getSingleResult(
									"from AirlineTicket where extNumber = ?0 and state = 'BOOKING' and lower(consumer.name) = ?1 and lower(consumer.lastName) = ?2",
									"SABRE-" + pnr, ticket.getConsumer().getName().toLowerCase(),
									ticket.getConsumer().getLastName().toLowerCase())) != null) {
								if (bookedTicket.getRoute().getAirportFrom().equals(ticket.getRoute().getAirportFrom())
										&& bookedTicket.getRoute().getAirportTo()
												.equals(ticket.getRoute().getAirportTo())
										&& bookedTicket.getRoute().getArrive()
												.compareTo(ticket.getRoute().getArrive()) == 0
										&& bookedTicket.getRoute().getDeparture()
												.compareTo(ticket.getRoute().getDeparture()) == 0) {
									bookedTicket.setState(OrderState.SALE);

									bookedTicket.setNumber(ticket.getNumber());

									bookedTicket.setPost(post);

									parser.getProvider().save(bookedTicket);

									continue;
								}
							} else {
								ticket.setValidUntil(ticket.getRoute().getDeparture());
								if (commission != 0) {
									parser.addAirlineTax(ticket, "COMMISSION", commission, docDate);
								}
								if (discount > 0) {
									parser.addAirlineTax(ticket, "VIP_COMMISSION", discount, docDate);
								} else if (discount < 0) {
									parser.addAirlineTax(ticket, "DISCOUNT", discount, docDate);
								}

								parser.getProvider().save(ticket);
							}
						}

						return true;
					}
					case '2': {
						// Refund
						String pnr = data.substring(index = 55, index += 6);
						int tempIndex = 0, m1Count = 0;

						List<Consumer> passengers = new ArrayList<>();
						while ((tempIndex = data.indexOf("\nM1", index)) != -1) {
							index = tempIndex + 5;

							String name = data.substring(index, index += 64),
									lastName = name.substring(0, tempIndex = name.indexOf("/")),
									firstName = name.substring(++tempIndex).strip();
							if (lastName.endsWith(" MR") || lastName.endsWith(" MS")) {
								firstName = firstName.substring(0, firstName.length() - 3);
							} else if (firstName.endsWith(" MRS") || firstName.endsWith(" MSS")) {
								firstName = firstName.substring(0, firstName.length() - 4);
							} else if (firstName.endsWith(" MSTR")) {
								firstName = firstName.substring(0, firstName.length() - 5);
							}
							passengers.add(parser.findConsumer(firstName, lastName));
							m1Count++;
						}

						AirlineRoute route = new AirlineRoute();
						while ((tempIndex = data.indexOf("\nM3", index)) != -1) {
							if (data.charAt(index = tempIndex + 5) == '1') {
								// Add segment
								AirlineSegment segment = new AirlineSegment();

								// Departure airport
								String c1 = data.substring(index += 14, index += 3);
								segment.setAirportFrom(parser.findAirportByCode(c1, null));

								// Arrival airport
								segment.setAirportTo(
										parser.findAirportByCode(data.substring(index += 17, index += 3), null));

								// Carrier
								segment.setCarrier(parser.findCompany(data.substring(index += 17, index += 2)));

								// Flight number
								segment.setTripNumber(data.substring(index, index += 5).strip());

								// Ticket category
								segment.setTicketCategory(data.substring(index, index += 2).strip());

								// Departure date
								tempIndex = index;
								switch (data.charAt(index + 4)) {
								case 'A':
								case 'P':
									segment.setDeparture(amPmFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53) + data.substring(index, index + 5)
											+ 'M'));
									break;
								case ' ':
									segment.setDeparture(emptyFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index, index + 4)));
									break;
								case 'N':
									segment.setDeparture(noonFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index, index + 4)));
								default:
									break;
								}

								// Arrival date
								switch (data.charAt(index + 9)) {
								case 'A':
								case 'P':
									segment.setArrive(amPmFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index + 5, index + 10) + 'M'));
									break;
								case ' ':
									segment.setArrive(emptyFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index + 5, index + 9)));
									break;
								case 'N':
									segment.setArrive(noonFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index + 5, index + 9)));
								default:
									break;
								}

								String duration = (data.substring(tempIndex + 10, tempIndex + 12) + ":"
										+ data.substring(tempIndex + 13, tempIndex + 15)).replace(' ', '0');
								segment.setDuration(duration);

								String departureTerminal = data.substring(tempIndex + 62, tempIndex + 88).strip();
								segment.setTerminalFrom(departureTerminal);

								String arrivalTerminal = data.substring(tempIndex + 92, tempIndex + 118).strip();
								segment.setTerminalTo(arrivalTerminal);

								parser.addAirlineSegment(route, segment);
							}
						}

						List<String> m5rStrings = new ArrayList<>();
						while ((tempIndex = data.indexOf("\nM5", index)) != -1) {
							index = tempIndex + 8;
							if (data.charAt(index) == 'R') {
								m5rStrings.add(data.substring(index += 4, data.indexOf("\r", index)));
							}
						}

						for (int i = 0; i < m1Count; i++) {
							String m5r = m5rStrings.get(m5rStrings.size() - i - 1);
							String refundTicketNumber = m5r.substring(0, 10);

							AirlineTicket refundTicket = parser.findAirlineTicket(refundTicketNumber);
							if (refundTicket == null) {
								warn("Ticket " + refundTicketNumber + " not found");
								return false;
							}

							AirlineTicket ticket = new AirlineTicket();
							// TODO Fix consumer
							ticket.setConsumer(passengers.get(passengers.size() - i - 1));
							ticket.setSupplier(parser.findCompany("SU"));
							ticket.setNumber(refundTicketNumber + "-R");
							ticket.setValidUntil(docDate);
							AbstractService as = new AbstractService();
							as.setId(refundTicket.getId());
							ticket.setParent(as);
							ticket.setBaseCurrency(parser.findCurrency("UAH"));
							ticket.setBaseAmount(
									Double.parseDouble(m5r.substring(19, tempIndex = m5r.indexOf("/", 19))));
							parser.addAirlineTax(ticket, "TAX",
									Double.parseDouble(
											m5r.substring(tempIndex += 1, tempIndex = m5r.indexOf("/", tempIndex))),
									docDate);
							ticket.setExtNumber("SABRE-" + pnr);
							ticket.setCurrency(parser.findCurrency("UAH"));
							ticket.setSector(sector);
							ticket.setCreated(docDate);
							ticket.setState(OrderState.REFUND);
							ticket.setType(AirlineTicketType.TRAVEL);
							ticket.setPost(post);
							route.addTicket(ticket);
						}

						parser.getProvider().save(route);

						return false;
					}
					case '3': {
						// Booking

						String pnr = data.substring(index = 55, index += 6);
						int tempIndex;

						List<AirlineTicket> tickets = new ArrayList<>();

						while ((tempIndex = data.indexOf("\nM1", index)) != -1) {
							index = tempIndex + 5;
							String name = data.substring(index, index += 64),
									firstName = name.substring(0, tempIndex = name.indexOf("/")),
									lastName = name.substring(++tempIndex).strip();
							if (lastName.endsWith(" MR") || lastName.endsWith(" MS")) {
								lastName = lastName.substring(0, lastName.length() - 3);
							} else if (lastName.endsWith(" MRS") || lastName.endsWith(" MSS")) {
								lastName = lastName.substring(0, lastName.length() - 4);
							} else if (lastName.endsWith(" MSTR")) {
								lastName = lastName.substring(0, lastName.length() - 5);
							}
							Consumer consumer = parser.findConsumer(firstName, lastName);

							AirlineTicket ticket = new AirlineTicket();

							ticket.setConsumer(consumer);

							tempIndex = data.indexOf("X*PQ-EQUIV FARE ");
							if (tempIndex == -1) {
								warn("'X*PQ-EQUIV FARE ' not found");
								return false;
							}
							Currency currency = parser.findCurrency(data.substring(tempIndex + 16, tempIndex + 19));
							ticket.setBaseCurrency(currency);
							ticket.setBaseAmount(Double
									.parseDouble(data.substring(tempIndex + 19, data.indexOf("\r", tempIndex + 19))));
							ticket.setCurrency(currency);
							tempIndex = data.indexOf("X*PQ-TAXES ");
							String taxes = data.substring(tempIndex + 11, data.indexOf("\r", tempIndex + 12));
							for (String tax : taxes.split("/")) {
								double taxAmount = Double.parseDouble(tax.substring(0, tax.length() - 2));
								String taxCode = tax.substring(tax.length() - 2, tax.length());
								parser.addAirlineTax(ticket, taxCode, taxAmount, docDate);
							}
							tempIndex = data.indexOf("X*PQ-VALIDATING CARRIER ");
							if (tempIndex == -1) {
								warn("'X*PQ-VALIDATING CARRIER ' not found");
								return false;
							}
							ticket.setSupplier(parser.findCompany(data.substring(tempIndex + 24, tempIndex + 26)));

							ticket.setNumber(UUID.uuid(8, 26));
							ticket.setExtNumber("SABRE-" + pnr);
							ticket.setSector(sector);
							ticket.setCreated(docDate);
							ticket.setType(AirlineTicketType.TRAVEL);
							ticket.setPost(post);
							tickets.add(ticket);
							ticket.setState(OrderState.BOOKING);
						}

						AirlineRoute route = new AirlineRoute();
						while ((tempIndex = data.indexOf("\nM3", index)) != -1) {
							if (data.charAt(index = tempIndex + 5) == '1') {
								// Add segment
								AirlineSegment segment = new AirlineSegment();

								// Departure airport
								String c1 = data.substring(index += 14, index += 3);
								segment.setAirportFrom(parser.findAirportByCode(c1, null));

								// Arrival airport
								segment.setAirportTo(
										parser.findAirportByCode(data.substring(index += 17, index += 3), null));

								// Carrier
								segment.setCarrier(parser.findCompany(data.substring(index += 17, index += 2)));

								// Flight number
								segment.setTripNumber(data.substring(index, index += 5).strip());

								// Ticket category
								segment.setTicketCategory(data.substring(index, index += 2).strip());

								// Departure date
								tempIndex = index;
								switch (data.charAt(index + 4)) {
								case 'A':
								case 'P':
									segment.setDeparture(amPmFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53) + data.substring(index, index + 5)
											+ 'M'));
									break;
								case ' ':
									segment.setDeparture(emptyFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index, index + 4)));
									break;
								case 'N':
									segment.setDeparture(noonFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index, index + 4)));
								default:
									break;
								}

								// Arrival date
								switch (data.charAt(index + 9)) {
								case 'A':
								case 'P':
									segment.setArrive(amPmFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index + 5, index + 10) + 'M'));
									break;
								case ' ':
									segment.setArrive(emptyFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index + 5, index + 9)));
									break;
								case 'N':
									segment.setArrive(noonFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index + 5, index + 9)));
								default:
									break;
								}

								String duration = (data.substring(tempIndex + 10, tempIndex + 12) + ":"
										+ data.substring(tempIndex + 13, tempIndex + 15)).replace(' ', '0');
								segment.setDuration(duration);

								String departureTerminal = data.substring(tempIndex + 62, tempIndex + 88).strip();
								segment.setTerminalFrom(departureTerminal);

								String arrivalTerminal = data.substring(tempIndex + 92, tempIndex + 118).strip();
								segment.setTerminalTo(arrivalTerminal);

								parser.addAirlineSegment(route, segment);
							}
						}

						double commission = 0, discount = 0;
						try {
							tempIndex = data.indexOf("X*-FEE/FARE");
							tempIndex = data.indexOf("AFEE", tempIndex);
							commission = Double.parseDouble(
									data.substring(tempIndex + 4, tempIndex = data.indexOf("/", tempIndex + 4)));
							discount = Double
									.parseDouble(data.substring(tempIndex + 5, data.indexOf("/", tempIndex + 5)));
						} catch (Exception e) {
						}

						for (AirlineTicket ticket : tickets) {
							route.addTicket(ticket);
							ticket.setValidUntil(route.getDeparture());
							if (commission != 0) {
								parser.addAirlineTax(ticket, "COMMISSION", commission, docDate);
							}
							if (discount > 0) {
								parser.addAirlineTax(ticket, "VIP_COMMISSION", discount, docDate);
							} else if (discount < 0) {
								parser.addAirlineTax(ticket, "DISCOUNT", discount, docDate);
							}
						}

						index = 0;
						while ((tempIndex = data.indexOf("\nM9", index)) != -1) {
							try {
								index = tempIndex + 8;

								String m9 = data.substring(index, data.indexOf("\r", index));
								if (m9.matches("SSR DOCS .. HK1/P.*")) {
									tempIndex = StringUtils.indexOf(m9, "/", 3, 0) + 1;
									String passport = m9.substring(tempIndex, tempIndex = m9.indexOf("/", tempIndex));
									String lastName = m9.substring(
											tempIndex = StringUtils.indexOf(m9, "/", 4, tempIndex + 1) + 1,
											tempIndex = m9.indexOf("/", tempIndex));
									int tempIndex2 = m9.indexOf("/", tempIndex += 1);
									String firstName;
									if (tempIndex2 == -1) {
										firstName = m9.substring(tempIndex);
									} else {
										firstName = m9.substring(tempIndex, tempIndex2);
									}
									for (AirlineTicket ticket : tickets) {
										if ((ticket.getConsumer().getDocument() == null
												|| ticket.getConsumer().getDocument().isEmpty())
												&& ticket.getConsumer().getLastName().equals(lastName)
												&& ticket.getConsumer().getName().equals(firstName)) {
											ticket.getConsumer().setDocument(passport);
										}
									}
								}
							} catch (Exception e) {
							}
						}

						parser.getProvider().save(route);

						return true;
					}
					case '5':
					case 'C': {
						// Void a ticket or an EMD
						String voidTicketNumber = data.substring(index = 26, index += 13);

						AirlineTicket ticket = parser.findAirlineTicket(voidTicketNumber);
						if (ticket == null) {
							warn("Ticket " + voidTicketNumber + " not found");
							return false;
						}

						ticket.setState(OrderState.VOID);
						ticket.setValidUntil(docDate);
						parser.getProvider().save(ticket);

						return true;
					}
					case 'F': {
						// Enhanced void
						int tempIndex;
						while ((tempIndex = data.indexOf("\nM2", index)) != -1) {
							String validatingCarrier = data.substring(index = tempIndex + 232, index += 2);
							String number = data.substring(index, index += 10);
							String digitalCode = companyCodes.getProperty(validatingCarrier);
							String voidTicketNumber;
							if (digitalCode == null) {
								voidTicketNumber = number;
							} else {
								voidTicketNumber = digitalCode + number;
							}

							AirlineTicket ticket = parser.findAirlineTicket(voidTicketNumber);
							if (ticket == null) {
								warn("Ticket " + voidTicketNumber + " not found");
								return false;
							}

							ticket.setState(OrderState.VOID);
							ticket.setValidUntil(docDate);
							parser.getProvider().save(ticket);
						}

						return true;
					}
					case 'A': {
						// EMD

						String pnr = data.substring(index = 55, index += 6);
						int tempIndex;

						Map<String, Consumer> m1NumberToPassenger = new HashMap<>();
						while ((tempIndex = data.indexOf("\nM1", index)) != -1) {
							String m1Number = data.substring(index = tempIndex + 3, index += 2);
							String name = data.substring(index, index += 64),
									lastName = name.substring(0, tempIndex = name.indexOf("/")),
									firstName = name.substring(++tempIndex).strip();
							if (lastName.endsWith(" MR") || lastName.endsWith(" MS")) {
								firstName = firstName.substring(0, firstName.length() - 3);
							} else if (firstName.endsWith(" MRS") || firstName.endsWith(" MSS")) {
								firstName = firstName.substring(0, firstName.length() - 4);
							} else if (firstName.endsWith(" MSTR")) {
								firstName = firstName.substring(0, firstName.length() - 5);
							}
							m1NumberToPassenger.put(m1Number, parser.findConsumer(firstName, lastName));
						}

						AirlineRoute route = new AirlineRoute();
						while ((tempIndex = data.indexOf("\nM3", index)) != -1) {
							if (data.charAt(index = tempIndex + 5) == '1') {
								// Add segment
								AirlineSegment segment = new AirlineSegment();

								// Departure airport
								String c1 = data.substring(index += 14, index += 3);
								segment.setAirportFrom(parser.findAirportByCode(c1, null));

								// Arrival airport
								segment.setAirportTo(
										parser.findAirportByCode(data.substring(index += 17, index += 3), null));

								// Carrier
								segment.setCarrier(parser.findCompany(data.substring(index += 17, index += 2)));

								// Flight number
								segment.setTripNumber(data.substring(index, index += 5).strip());

								// Ticket category
								segment.setTicketCategory(data.substring(index, index += 2).strip());

								// Departure date
								tempIndex = index;
								switch (data.charAt(index + 4)) {
								case 'A':
								case 'P':
									segment.setDeparture(amPmFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53) + data.substring(index, index + 5)
											+ 'M'));
									break;
								case ' ':
									segment.setDeparture(emptyFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index, index + 4)));
									break;
								case 'N':
									segment.setDeparture(noonFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index, index + 4)));
								default:
									break;
								}

								// Arrival date
								switch (data.charAt(index + 9)) {
								case 'A':
								case 'P':
									segment.setArrive(amPmFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index + 5, index + 10) + 'M'));
									break;
								case ' ':
									segment.setArrive(emptyFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index + 5, index + 9)));
									break;
								case 'N':
									segment.setArrive(noonFormat.parse(data.substring(index + 168, index + 172)
											+ data.substring(index - 58, index - 53)
											+ data.substring(index + 5, index + 9)));
								default:
									break;
								}

								String duration = (data.substring(tempIndex + 10, tempIndex + 12) + ":"
										+ data.substring(tempIndex + 13, tempIndex + 15)).replace(' ', '0');
								segment.setDuration(duration);

								String departureTerminal = data.substring(tempIndex + 62, tempIndex + 88).strip();
								segment.setTerminalFrom(departureTerminal);

								String arrivalTerminal = data.substring(tempIndex + 92, tempIndex + 118).strip();
								segment.setTerminalTo(arrivalTerminal);

								parser.addAirlineSegment(route, segment);
							}
						}

						while ((tempIndex = data.indexOf("\nMG", index)) != -1) {
							index = tempIndex;
							AirlineTicket ticket = new AirlineTicket();
							// TODO Fix consumer
							ticket.setConsumer(m1NumberToPassenger.entrySet().iterator().next().getValue());
							ticket.setSupplier(parser.findCompany(data.substring(index += 23, index += 2)));
							ticket.setNumber(data.substring(index += 1, index += 13));
							String associateTicketNumber = data.substring(index += 5, index += 10);
							AirlineTicket associateTicket = parser.findAirlineTicket(associateTicketNumber);
							if (associateTicket == null) {
								warn("Ticket " + associateTicketNumber + " not found");
								return false;
							}
							ticket.setValidUntil(associateTicket.getRoute().getDeparture());
							AbstractService as = new AbstractService();
							as.setId(associateTicket.getId());
							ticket.setParent(as);
							ticket.setBaseCurrency(parser.findCurrency(data.substring(index += 92, index += 3)));
							ticket.setBaseAmount(Double.parseDouble(data.substring(index, index += 18).strip()));
							ticket.setExtNumber("SABRE-" + pnr);
							ticket.setCurrency(parser.findCurrency("UAH"));
							ticket.setSector(sector);
							ticket.setCreated(docDate);
							ticket.setState(OrderState.SALE);
							ticket.setType(AirlineTicketType.EMD);
							ticket.setPost(post);
							route.addTicket(ticket);
						}

						parser.getProvider().save(route);

						return true;
					}
					default:
						warn("unknown operation type: " + operationType);
						return false;
					}
				} catch (NumberFormatException | IOException | ParseException e) {
					error("", e);
					return false;
				}
			}
		});
	}

}

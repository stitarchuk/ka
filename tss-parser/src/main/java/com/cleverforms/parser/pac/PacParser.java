package com.cleverforms.parser.pac;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cleverforms.comms.server.tasks.FileParserImpl;
import com.cleverforms.comms.shared.util.Util;
import com.cleverforms.ics.core.provider.ICSPersistentProvider;
import com.cleverforms.ics.db.model.orgchart.Person;
import com.cleverforms.ics.db.model.orgchart.Post;
import com.cleverforms.ics.db.model.orgchart.Sector;
import com.cleverforms.ics.shared.dictionary.property.CompanyProperty;
import com.cleverforms.ics.shared.enumerator.OrderState;
import com.cleverforms.iss.server.model.railway.RailwayRoute;
import com.cleverforms.iss.server.model.railway.RailwayStation;
import com.cleverforms.iss.server.model.railway.RailwayTicket;
import com.cleverforms.iss.shared.enumerator.TicketCategory;
import com.cleverforms.iss.shared.railway.RailwayTicketType;
import com.cleverforms.parser.Parser;

public class PacParser extends FileParserImpl {

	@Autowired
	Parser parser;

	public final Sector sector = new Sector();

	@Override
	public ICSPersistentProvider provider() {
		return (ICSPersistentProvider) super.provider();
	}

	public void setSectorId(long sectorId) {
		sector.setId(sectorId);
	}

	@Override
	public String name() {
		return "Pac data parser";
	}

	@Override
	public boolean parseFile(final File file) {
		return provider().txExecute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus status) {
				try {
					DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document document = documentBuilder.parse(file);
					XPath xPath = XPathFactory.newInstance().newXPath();

					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

					String agentCode = xPath.compile("/responce/agent").evaluate(document);
					if (agentCode.isEmpty()) {
						warn("/responce/agent not found");
						return false;
					}
					Person agent = (Person) parser.getProvider().getSingleResult("from Person where lower(aliases[19]) = lower(?0)", agentCode);
					Post post = null;
					if (agent != null) {
						post = agent.getPost();
					}

					String operation = xPath.compile("/responce/operation").evaluate(document);
					switch (operation) {
					case "payment": {
						String number = xPath.compile("/responce/number").evaluate(document);
						String[] numbers = number.split(",");

						String uio = xPath.compile("/responce/uio").evaluate(document);
						String[] uios = uio.split(",");

						Date date = sdf.parse(xPath.compile("/responce/date").evaluate(document));
						String subagentEdrpou = xPath.compile("/responce/subagent/edrpou").evaluate(document);
						Set<String> subagents = new HashSet<>();
						BufferedReader fileReader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("subagents"), Util.ENCODING_UTF_8));
						String line;
						while ((line = fileReader.readLine()) != null) {
							subagents.add(line);
						}

						String ticketType = xPath.compile("/responce/ticket/type").evaluate(document);

						boolean isElectronic;
						switch (xPath.compile("/responce/ticket/electronic").evaluate(document)) {
						case "False":
							isElectronic = false;
							break;
						case "True":
							isElectronic = true;
							break;
						default:
							warn("Electronic is not True or False");
							return false;
						}

						NodeList passengerNodes = (NodeList) xPath.compile("/responce/passengers/passenger").evaluate(document, XPathConstants.NODESET);

						for (int i = 0; i < passengerNodes.getLength(); i++) {
							RailwayTicket ticket = new RailwayTicket();

							ticket.setPost(post);

							ticket.setSupplier(parser.getProvider().persistentUtil().findCompany(CompanyProperty.NAME_UK, "УЗ"));

							ticket.setSector(sector);

							ticket.setState(OrderState.SALE);

							ticket.setExtNumber(subagents.contains(subagentEdrpou) ? "RAIL_ЦИТ" : "RAIL_ПАК" + "-"
									+ (uios.length == 1 ? (uio + String.format("-%04d", i + 1)) : uios[i] + "-0001"));

							switch (ticketType) {
							case "travel":
								ticket.setType(RailwayTicketType.TRAVEL);
								break;
							case "transportation":
								ticket.setType(RailwayTicketType.TRANSPORTATION);
								break;
							case "reserve":
								ticket.setType(RailwayTicketType.RESERVE);
								break;
							default:
								warn("Unknown ticket type: " + ticketType);
								return false;
							}

							if (numbers.length == 1) {
								if (passengerNodes.getLength() == 1) {
									ticket.setNumber(number);
								} else {
									ticket.setNumber(number + "/" + (i + 1));
								}
							} else {
								ticket.setNumber(numbers[i]);
							}

							ticket.setCurrency(parser.findCurrency("UAH"));

							ticket.setBaseCurrency(parser.findCurrency("UAH"));

							Element passengerElement = (Element) passengerNodes.item(i);

							if (ticketType.equals("transportation")) {
								String refersTo = xPath.compile("/responce/ticket/refersTo").evaluate(document);
								RailwayTicket refersToTicket = (RailwayTicket) parser.getProvider().getSingleResult(
										"from RailwayTicket where extNumber like '%" + refersTo + "'");
								if (refersToTicket == null) {
									warn("Ticket " + refersTo + " not found");
									return false;
								}
								ticket.setConsumer(refersToTicket.getConsumer());
							} else {
								NodeList kind = passengerElement.getElementsByTagName("kind");
								if (kind.getLength() != 0) {
									ticket.setTicketCategory(TicketCategory.parse(kind.item(0).getFirstChild().getNodeValue()));
								}
								NodeList passportNodes = passengerElement.getElementsByTagName("passport");
								if (passportNodes.getLength() != 0) {
									ticket.setConsumer(parser.findConsumer(((Element) passportNodes.item(0)).getElementsByTagName("firstname").item(0)
											.getTextContent(), ((Element) passportNodes.item(0)).getElementsByTagName("lastname").item(0).getTextContent()));
								} else {
									warn("Passport not found");
									return false;
								}
							}
							RailwayRoute route = new RailwayRoute();

							route.setNumber(number);

							route.setElectronic(isElectronic);

							route.addTicket(ticket);

							route.setArrive(sdf.parse(((Element) passengerElement.getElementsByTagName("details").item(0)).getElementsByTagName("arrival")
									.item(0).getTextContent()));

							route.setDeparture(sdf.parse(((Element) passengerElement.getElementsByTagName("details").item(0)).getElementsByTagName("departure")
									.item(0).getTextContent()));

							String stationFromName = ((Element) passengerElement.getElementsByTagName("details").item(0)).getElementsByTagName("station_from")
									.item(0).getTextContent();
							RailwayStation stationFrom = parser.findRailwayStationByName(stationFromName);
							if (stationFrom == null) {
								warn("Station " + stationFromName + " not found");
								return false;
							}
							route.setStationFrom(stationFrom);

							String stationToName = ((Element) passengerElement.getElementsByTagName("details").item(0)).getElementsByTagName("station_to")
									.item(0).getTextContent();
							RailwayStation stationTo = parser.findRailwayStationByName(stationToName);
							if (stationTo == null) {
								warn("Station " + stationToName + " not found");
								return false;
							}
							route.setStationTo(stationTo);

							Node placeNode = ((Element) passengerElement.getElementsByTagName("details").item(0)).getElementsByTagName("place").item(0);
							if (placeNode.hasChildNodes()) {
								ticket.setPlaces(placeNode.getTextContent());
							} else {
								ticket.setPlaces("");
							}

							route.setTrainNumber(((Element) passengerElement.getElementsByTagName("details").item(0)).getElementsByTagName("train").item(0)
									.getTextContent());

							ticket.setWagonNumber(((Element) passengerElement.getElementsByTagName("details").item(0)).getElementsByTagName("wagon").item(0)
									.getTextContent());

							ticket.setValidUntil(route.getDeparture());

							Element costElement = (Element) passengerElement.getElementsByTagName("cost").item(0);
							ticket.setBaseAmount(Double.parseDouble(costElement.getElementsByTagName("ticket").item(0).getTextContent()));
							parser.addRailwayTax(ticket, "COMMISSION",
									Double.parseDouble(costElement.getElementsByTagName("servicefee").item(0).getTextContent()), date);
							parser.addRailwayTax(ticket, "ADDITIONAL_SERVICES",
									Double.parseDouble(costElement.getElementsByTagName("techfee").item(0).getTextContent()), date);

							parser.getProvider().save(route);
						}

						return true;
					}
					case "void": {
						String number = xPath.compile("/responce/number").evaluate(document);
						String[] numbers = number.split(",");

						for (String nextNumber : numbers) {
							RailwayTicket ticket = parser.findRailwayTicket(nextNumber);
							if (ticket == null) {
								warn("Ticket " + nextNumber + " not found");
								return false;
							}
							ticket.setState(OrderState.VOID);

							ticket.setValidUntil(sdf.parse(xPath.compile("/responce/cansel").evaluate(document)));

							parser.getProvider().save(ticket);
						}

						return true;
					}
					case "cansel":
					case "return":
						warn("operation: " + operation);
						return false;
					default:
						warn("Unknown PAC operation type: " + operation);
						return false;
					}
				} catch (NumberFormatException | XPathExpressionException | DOMException | ParserConfigurationException | SAXException | IOException
						| ParseException e) {
					error("", e);
					return false;
				}
			}
		});
	}
}

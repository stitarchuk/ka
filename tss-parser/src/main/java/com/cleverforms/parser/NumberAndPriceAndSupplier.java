package com.cleverforms.parser;

public class NumberAndPriceAndSupplier extends NumberAndPrice {
	public String supplier;

	public NumberAndPriceAndSupplier(String number, Price price, String supplier) {
		super(number, price);
		this.supplier = supplier;
	}
}
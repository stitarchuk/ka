package com.cleverforms.parser.company;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.cleverforms.comms.server.tasks.FileParserImpl;
import com.cleverforms.ics.core.provider.ICSPersistentProvider;
import com.cleverforms.ics.db.model.dictionary.Bank;
import com.cleverforms.ics.db.model.dictionary.Company;
import com.cleverforms.ics.db.model.dictionary.CompanyAccount;
import com.cleverforms.ics.db.model.dictionary.Country;
import com.cleverforms.ics.db.model.orgchart.Sector;
import com.cleverforms.ics.db.model.orgchart.Unit;
import com.cleverforms.ics.shared.dictionary.property.CompanyProperty;
import com.cleverforms.parser.Parser;

public class CompanyParser extends FileParserImpl {

	@Autowired
	Parser parser;

	public final Sector sector = new Sector();

	@Override
	public ICSPersistentProvider provider() {
		return (ICSPersistentProvider) super.provider();
	}

	public void setSectorId(long sectorId) {
		sector.setId(sectorId);
	}

	@Override
	public String name() {
		return "Company data parser";
	}

	@Override
	public boolean parseFile(final File file) {
		return provider().txExecute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus status) {
				try {
					DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document document = documentBuilder.parse(file);
					XPath xPath = XPathFactory.newInstance().newXPath();

					String bnkmfo = xPath.compile("/ptn/ptn_bnkmfo").evaluate(document);
					String bnknm = xPath.compile("/ptn/ptn_bnknm").evaluate(document);
					Bank bank = parser.findBankByMfo(bnkmfo);
					if (bank != null) {
						bank.setName(bnknm);
					} else {
						bank = new Bank();
						bank.setName(bnknm);
						bank.setCode(bnkmfo);
					}
					parser.getProvider().save(bank);

					String sch = xPath.compile("/ptn/ptn_sch").evaluate(document);

					String okpo = xPath.compile("/ptn/ptn_okpo").evaluate(document);
					String inn = xPath.compile("/ptn/ptn_inn").evaluate(document);
					Company company = parser.findCompanyByOkpo(okpo);
					CompanyAccount companyAccount = null;
					if (company != null) {
						company.putProperty(CompanyProperty.TAX_NUMBER, inn);
						companyAccount = parser.findCompanyAccount(company, bank, sch);
					} else {
						company = parser.findCompanyByInn(inn);
						if (company != null) {
							company.putProperty(CompanyProperty.REGISTRATION_NUMBER, okpo);
							companyAccount = parser.findCompanyAccount(company, bank, sch);
						} else {
							company = new Company();
							Unit unit = new Unit();
							unit.setId(1);
							company.setUnit(unit);
							Country ukraine = new Country();
							ukraine.setId(206);
							company.setCountry(ukraine);
							company.putProperty(CompanyProperty.TAX_NUMBER, inn);
							company.putProperty(CompanyProperty.REGISTRATION_NUMBER, okpo);
						}
					}
					if (companyAccount == null) {
						companyAccount = new CompanyAccount();
						companyAccount.setBank(bank);
						companyAccount.setCode(sch);
						companyAccount.setName(bnknm + " " + sch);
						companyAccount.setCompany(company);
						company.addAccount(companyAccount);
					}
					company.putProperty(CompanyProperty.NAME_UK, xPath.compile("/ptn/ptn_nmsh").evaluate(document));
					company.putProperty(CompanyProperty.FULL_NAME, xPath.compile("/ptn/ptn_nm").evaluate(document));

					parser.getProvider().save(company);

					return true;
				} catch (XPathExpressionException | ParserConfigurationException | SAXException | IOException e) {
					error("", e);
					return false;
				}
			}
		});
	}
}

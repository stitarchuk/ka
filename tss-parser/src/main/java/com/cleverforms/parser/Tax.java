package com.cleverforms.parser;

import java.util.Date;

public class Tax {
	public String code;
	public Date date;
	public double amount;

	public Tax(String code, Date date, double amount) {
		this.code = code;
		this.date = date;
		this.amount = amount;
	}
}
package com.cleverforms.parser.gillbus;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cleverforms.comms.server.tasks.FileParserImpl;
import com.cleverforms.ics.core.provider.ICSPersistentProvider;
import com.cleverforms.ics.db.model.dictionary.Consumer;
import com.cleverforms.ics.db.model.dictionary.Currency;
import com.cleverforms.ics.db.model.financial.AbstractService;
import com.cleverforms.ics.db.model.orgchart.Person;
import com.cleverforms.ics.db.model.orgchart.Post;
import com.cleverforms.ics.db.model.orgchart.Sector;
import com.cleverforms.ics.shared.dictionary.property.CompanyProperty;
import com.cleverforms.ics.shared.enumerator.OrderState;
import com.cleverforms.iss.server.model.bus.BusRoute;
import com.cleverforms.iss.server.model.bus.BusStation;
import com.cleverforms.iss.server.model.bus.BusTicket;
import com.cleverforms.parser.Parser;

public class GillbusParser extends FileParserImpl {

	@Autowired
	Parser parser;

	public final Sector sector = new Sector();

	@Override
	public ICSPersistentProvider provider() {
		return (ICSPersistentProvider) super.provider();
	}

	public void setSectorId(long sectorId) {
		sector.setId(sectorId);
	}

	@Override
	public String name() {
		return "Gillbus data parser";
	}

	@Override
	public boolean parseFile(final File file) {
		return provider().txExecute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus status) {
				try {
					DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
					Document document = documentBuilder.parse(file);
					XPath xPath = XPathFactory.newInstance().newXPath();

					SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

					String orderNumber = xPath.compile("/order/@number").evaluate(document);

					NodeList ticketElements = (NodeList) xPath.compile("/order/tickets/ticket").evaluate(document, XPathConstants.NODESET);
					for (int i = 0; i < ticketElements.getLength(); i++) {
						Element ticketElement = (Element) ticketElements.item(i);

						String agentName = xPath.compile("agency/agent").evaluate(ticketElement);
						if (agentName.isEmpty()) {
							warn("agency/agent not found");
							return false;
						}
						Person agent = (Person) parser.getProvider().getSingleResult("from Person where lower(aliases[24]) = lower(?0)", agentName);
						Post post = null;
						if (agent != null) {
							post = agent.getPost();
						}

						String ticketNumber = ticketElement.getAttribute("number");

						Element operation = (Element) ticketElement.getElementsByTagName("operation").item(0);

						Date documentDate = dateFormat.parse(operation.getAttribute("date"));

						String operationType = operation.getAttribute("type");
						switch (operationType) {
						case "2":
						case "4": {
							BusTicket ticket = new BusTicket();

							ticket.setPost(post);

							if (operationType.equals("2")) {
								ticket.setState(OrderState.SALE);
							} else {
								BusTicket refundTicket = parser.findBusTicket(ticketNumber);
								if (refundTicket == null) {
									warn("Ticket " + ticketNumber + " not found");
									return false;
								}

								ticketNumber += "-R";

								ticket.setState(OrderState.REFUND);

								AbstractService as = new AbstractService();
								as.setId(refundTicket.getId());
								ticket.setParent(as);
							}

							if (parser.findBusTicket(ticketNumber) != null) {
								warn("Ticket " + ticketNumber + " already exists");
								return false;
							}

							ticket.setNumber(ticketNumber);

							ticket.setSector(sector);

							ticket.setCreated(documentDate);

							ticket.setSupplier(parser.getProvider().persistentUtil().findCompany(CompanyProperty.NAME_EN, "E-Travels"));

							ticket.setExtNumber("GILLBUS-" + orderNumber);

							Element passengerElement = (Element) ticketElement.getElementsByTagName("passenger").item(0);
							Consumer passenger = parser.findConsumer(passengerElement.getAttribute("firstName"), passengerElement.getAttribute("lastName"));
							ticket.setConsumer(passenger);

							ticket.setSeatNumber(((Element) passengerElement.getElementsByTagName("seatNumber").item(0)).getFirstChild().getNodeValue());

							Element tripElement = (Element) ticketElement.getElementsByTagName("trip").item(0);
							BusRoute route = new BusRoute();

							route.addTicket(ticket);

							route.setCarrierName(((Element) tripElement.getElementsByTagName("carrier").item(0)).getAttribute("name"));

							String cityFromName = ((Element) tripElement.getElementsByTagName("dispatch").item(0)).getAttribute("name");
							String stationFromName = ((Element) ((Element) tripElement.getElementsByTagName("dispatch").item(0))
									.getElementsByTagName("station").item(0)).getAttribute("name");
							BusStation stationFrom = parser.findBusStation(stationFromName, cityFromName);
							if (stationFrom == null) {
								route.setStationFrom(parser.addBusStation(stationFromName, cityFromName));
							} else {
								route.setStationFrom(stationFrom);
							}

							String cityToName = ((Element) tripElement.getElementsByTagName("arrival").item(0)).getAttribute("name");
							String stationToName = ((Element) ((Element) tripElement.getElementsByTagName("arrival").item(0)).getElementsByTagName("station")
									.item(0)).getAttribute("name");
							BusStation stationTo = parser.findBusStation(stationToName, cityToName);
							if (stationTo == null) {
								route.setStationTo(parser.addBusStation(stationToName, cityToName));
							} else {
								route.setStationTo(stationTo);
							}

							route.setDeparture(dateFormat.parse(((Element) tripElement.getElementsByTagName("dispatch").item(0)).getAttribute("date")));

							ticket.setValidUntil(route.getDeparture());

							route.setArrive(dateFormat.parse(((Element) tripElement.getElementsByTagName("arrival").item(0)).getAttribute("date")));

							route.setTripNumber(tripElement.getAttribute("number"));

							Element priceElement = (Element) ticketElement.getElementsByTagName("price").item(0);
							if (operationType.equals("2")) {
								ticket.setBaseAmount(Double.parseDouble(priceElement.getAttribute("amount")));
							} else {
								ticket.setBaseAmount(-Double.parseDouble(xPath.evaluate("tariff/return/@amount", priceElement)));
							}

							Currency currency = parser.findCurrency(priceElement.getAttribute("currency"));
							ticket.setCurrency(currency);
							ticket.setBaseCurrency(currency);

							ticket.setTariff(((Element) priceElement.getElementsByTagName("tariff").item(0)).getAttribute("code"));

							parser.getProvider().save(route);
							break;
						}
						default:
							return true;
						}
					}

					return true;
				} catch (NumberFormatException | XPathExpressionException | DOMException | ParserConfigurationException | SAXException | IOException
						| ParseException e) {
					error("", e);
					return false;
				}
			}
		});
	}
}

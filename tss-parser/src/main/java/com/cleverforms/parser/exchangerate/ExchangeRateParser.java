package com.cleverforms.parser.exchangerate;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.cleverforms.iss.server.provider.ISSPersistentProvider;

public class ExchangeRateParser {

	@Autowired
	protected ISSPersistentProvider provider;

	public void parse(File file) throws Exception {
		DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document document = documentBuilder.parse(file);
		XPath xPath = XPathFactory.newInstance().newXPath();

		NodeList w6CursNodes = (NodeList) xPath.compile("CURRENCY_CURS/W6/CURS").evaluate(document, XPathConstants.NODESET);
		System.out.println("W6");
		for (int i = 0; i < w6CursNodes.getLength(); i++) {
			Element w6CursElement = (Element) w6CursNodes.item(i);
			String code = w6CursElement.getAttribute("code");
			double rate = Double.parseDouble(w6CursElement.getFirstChild().getNodeValue());
			System.out.println(code + ": " + rate);
		}
		NodeList mgbCursNodes = (NodeList) xPath.compile("CURRENCY_CURS/MGB/CURS").evaluate(document, XPathConstants.NODESET);
		System.out.println("Privat");
		for (int i = 0; i < mgbCursNodes.getLength(); i++) {
			Element mgbCursElement = (Element) mgbCursNodes.item(i);
			String code = mgbCursElement.getAttribute("code");
			double rate = Double.parseDouble(mgbCursElement.getFirstChild().getNodeValue());
			System.out.println(code + ": " + rate);
		}
	}
}

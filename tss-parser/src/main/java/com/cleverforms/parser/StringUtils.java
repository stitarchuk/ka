package com.cleverforms.parser;

public abstract class StringUtils {
	public static int indexOf(String where, String what, int count, int fromIndex) {
		if (count < 1) {
			throw new IllegalArgumentException("" + count);
		}
		int index = where.indexOf(what, fromIndex);
		int str2length = what.length();
		while (--count > 0 && index != -1)
			index = where.indexOf(what, index + str2length);
		return index;
	}

	public static int indexOf(String str1, int fromIndex, String... strings) {
		int index = -1;
		for (int i = 0; i < strings.length; i++) {
			int nextIndex = str1.indexOf(strings[i], fromIndex);
			if (nextIndex != -1 && (nextIndex < index || index == -1)) {
				index = nextIndex;
			}
		}
		return index;
	}
}

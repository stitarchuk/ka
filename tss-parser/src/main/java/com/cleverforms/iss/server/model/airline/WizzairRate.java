/*
 * ISS GDS file parser.
 * Copyright(c) 2013-2024. 
 * November 29, 2015.
 * tss-parser
 * 
 */
package com.cleverforms.iss.server.model.airline;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import com.cleverforms.comms.server.model.PersistentEntity;
import com.cleverforms.comms.server.provider.BasePersistentProvider;
import com.cleverforms.comms.shared.proxy.BaseProxy;
/**
 * 
 * @author Sergey Titarchuk
 *
 */
@Entity
@Table(schema = "airline", name = "wizzair_rate")
public class WizzairRate extends PersistentEntity {
	
	private static final long serialVersionUID = 3097600564007891448L;
	
	private double rate;
	private Date relevance;
	
	@Column(name = "rate")
	public double getRate() {
		return rate;
	}
	
	public void setRate(double rate) {
		this.rate = rate;
	}
	
	@Column(name = "relevance")
	public Date getRelevance() {
		return relevance;
	}
	
	public void setRelevance(Date relevance) {
		this.relevance = relevance;
	}
	
	@Override
	public WizzairRate populate(BasePersistentProvider provider, BaseProxy proxy) {
		return null;
	}
	
}
